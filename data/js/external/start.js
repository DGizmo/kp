$(function() {
	'use strict';

	var newUser = {},
		validate = {
			empty:       'Поле обязательное для заполнения',
			incorrect:   'Неверный формат',
			badformat:   'Неверный формат',
			badvalue:    'Недопустимое значение',
			badpassword: 'Неверный формат пароля',
			eqpassword:  'Пароли не совпадают',
			emailexists: 'E-mail занят',
			email404:    'E-mail не найден',
			badpass:     'Неверный пароль',
			banned:      'Ваш аккаунт временно заблокирован'
		},
		countryMask = {
			ru: '+7? (999) 999-99-99',
			ua: '+380? (99) 999-99-99',
			by: '+375? (99) 999-99-99',
			kz: '+7? (999) 999-99-99'
		},
		infoMessage = {
			forgotPassword: {
				title: 'Восстановление пароля',
				message: 'Инструкция по восстановлению пароля отправлена Вам на почту'
			}
		},
		distributors = [],
		timeout;

	jQuery.expr[':'].containsLower = jQuery.expr.createPseudo(function(arg) {
		return function(elem) {
			return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
		};
	});

	$.fn.serializeObject = function() {
		var o = {},
			a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	function togglePopup(popup, title) {
		if (title) document.title = title;
		$('.popup').fadeOut(100);
		var $target = $('#form-' + popup).closest('.popup');
		if ($target.find('form').length) $target.find('form')[0].reset();
		$target.fadeIn(100).find('input:first').focus();
		if ($target.find('#email')) $target.find('#email').val(getParameterByName('email'));
	}

	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	function validatePassword(pass) {
		var re = /^.{6,32}$/;
		return re.test(pass);
	}

	function validatePhone(phone) {
		var re = /^(\+\d{1,3}\s)+([\(]\d{1,3}[\)])\s(\d{3})-(\d{2})-(\d{2})$/;
		return re.test(phone);
	}

	function isValid(attr) {
		var errors = [],
			i;

		for (i in attr) {
			switch (i) {
				case 'first_name':
					if (!(attr.first_name.length)) errors.push({attr: 'first_name', message: validate.empty});
					if (!/[A-Za-zА-Яа-я0-9_'-]/.test(attr.first_name) || (attr.first_name.length < 2)) errors.push({ attr: 'first_name', message: validate.incorrect});
				break;

				case 'last_name':
					if (!(attr.last_name.length)) errors.push({attr: 'last_name', message: validate.empty});
					if (!/[A-Za-zА-Яа-я0-9_'-]/.test(attr.last_name) || (attr.last_name.length < 2)) errors.push({ attr: 'last_name', message: validate.incorrect});
				break;

				case 'email':
					if (!validateEmail(attr.email)) errors.push({attr: 'email', message: validate.incorrect});
				break;

				case 'password':
					if (!validatePassword(attr.password)) errors.push({attr: 'password', message: validate.badpassword});
					if (attr.password !== attr.password1) {
						errors.push({ attr: 'password', message: validate.eqpassword });
						errors.push({ attr: 'password1', message: validate.eqpassword });
					}
				break;

				case 'phone':
					if (!validatePhone(attr.phone)) errors.push({attr: 'phone', message: validate.incorrect});
				break;
			}
		}

		if (errors.length) return errors;
	}

	function getParameterByName(name) {
		name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
		var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
			results = regex.exec(location.hash);
		return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	}

	$(document).on('click', function(event) {
		if ($(event.target).closest('.dropdown').length || $(event.target).hasClass('dropdown')) return false;
		$('.dropdown').removeClass('dropdown--open');
	});

	$('.popup__btn-back').on('click', function() {
		var prev = $(this).attr('prev');
		togglePopup(prev, prev === 'login' ? 'Вход - Киноплан' : '');
	});

	$('#btn-singup').on('click', function(event) {
		event.preventDefault();
		togglePopup('phone', 'Регистрация - Киноплан');
	});

	$('#btn-forgot').on('click', function(event) {
		event.preventDefault();
		togglePopup('forgot', 'Восстановление пароля - Киноплан');
	});

	$('body').on('click', '.js-submit', function(event) {
		event.preventDefault();
		$(this).closest('form').submit();
	});

	$('input').keypress(function(event) {
		if (event.which === 13) {
			event.preventDefault();
			var self = this;
			setTimeout(function() {
				$(self).closest('form').submit();
			}, 0);
		}
	});

	if ((document.location.href).indexOf('singup') !== -1) {
		togglePopup('phone', 'Регистрация - Киноплан');
		$('#form-phone').find('input').focus();
	} else {
		togglePopup('login', 'Вход - Киноплан');
		if (!($('#input-login-email').val())) {
			$('#input-login-email').focus();
		} else if ($('#input-login-email').val() && $('#input-login-password').val()) {
			$('#btn-login').focus();
		}
	}

	if (getParameterByName('email')) newUser.force = 1;

	/*=============================
	=            Login            =
	=============================*/

	$('#form-login').submit(function(event) {
		event.preventDefault();

		var $self = $(this),
			formData = $(this).serializeObject(),
			errors;

		$(this).find('.form-group')
			.removeClass('error')
			.find('.text-error').text('');

		errors = isValid({email: formData.email});

		$(errors).each(function(index, el) {
			$self.find('input[name=' + el.attr + ']')
				.closest('.form-group').addClass('error')
				.find('.text-error').text(el.message);
		});

		if (errors) {
			$(this).find('input[name=' + errors[0].attr + ']:first').focus();
			return;
		}

		$.post('/login', formData)
			.done(function(res) {
				if (res.email) {
					$self.find('input[name=email]').focus()
						.closest('.form-group').addClass('error')
						.find('.text-error').text(validate[res.email]);
					return;
				}
				if (res.password) {
					$self.find('input[name=password]').focus()
						.closest('.form-group').addClass('error')
						.find('.text-error').text(validate[res.password]);
					return;
				}
				if (res.redirect_url) document.location.href = res.redirect_url;
			});

		return false;
	});

	/*=============================
	=            Phone            =
	=============================*/

	$('#input-phone').mask(countryMask.ru).focus();

	$('#form-phone').on('click', '.dropdown', function() {
		$(this).toggleClass('dropdown--open');
	});

	$('#form-phone').on('click', '.dropdown-list__item:not(.selected)', function() {
		var country = $(this).data('country');

		$('#input-phone').val('').mask(countryMask[country]);

		$(this).closest('.dropdown')
			.find('.dropdown-list__item')
				.removeClass('selected').end()
			.children('.icon-flag')
				.addClass('hidden').end()
			.children('.icon-flag-' + country)
				.removeClass('hidden').end()
			.next('input').focus();

		$(this).addClass('selected');

	});

	$('#form-phone').on('click', '.js-login', function(event) {
		event.preventDefault();
		togglePopup('login');
	});

	$('#form-phone').submit(function(event) {
		event.preventDefault();

		var $self = $(this),
			formData = $(this).serializeObject(),
			errors;

		$(this).find('.form-group')
			.removeClass('error')
			.find('.text-error').text('');

		errors = isValid(formData);

		$(errors).each(function(index, el) {
			$self.find('input[name=' + el.attr + ']')
				.closest('.form-group').addClass('error')
				.find('.text-error').text(el.message);
		});

		if (errors) {
			$(this).find('input[name=' + errors[0].attr + ']:first').focus();
			return;
		}

		$(this).find('.js-submit').addClass('disabled');

		$.post('/api/user/v2/code', {phone: formData.phone})
			.done(function(res) {
				if (res.ok) {
					newUser.user_id = res.user_id;
					$('#form-check-phone').find('.js-phone').text(formData.phone);
					$self.find('.js-submit').removeClass('disabled');
					togglePopup('check-phone');
				}

			});
	});

	/*===================================
	=            Check Phone            =
	===================================*/

	$('#form-check-phone').submit(function(event) {
		event.preventDefault();

		var $self = $(this),
			formData = $(this).serializeObject();

		if (!(formData.code) || !(formData.code.length)) {
			$(this).find('.form-group').addClass('error').find('.text-error').text(validate.empty);
			return;
		}

		$(this).find('.js-submit').addClass('disabled');

		$.post('/api/user/v2/validate', {code: formData.code, user_id: newUser.user_id, web: 1})
			.always(function() {
				$self.find('.form-group').removeClass('error').find('.text-error').text('');
			})
			.done(function(res) {
				$self.find('.js-submit').removeClass('disabled');
				if (res.ok) {
					togglePopup('register');
				} else {
					$self.find('.form-group').addClass('error').find('.text-error').text('Не верный код!');
					$self.find('input').focus();
				}
			});
	});

	/*====================================
	=            Registration            =
	====================================*/

	$('#form-register').submit(function(event) {
		event.preventDefault();

		var $self = $(this),
			formData = $(this).serializeObject(),
			errors;

		$(this).find('.form-group')
			.removeClass('error')
			.find('.text-error').text('');

		errors = isValid(formData);

		$(errors).each(function(index, el) {
			$self.find('input[name=' + el.attr + ']')
				.closest('.form-group').addClass('error')
				.find('.text-error').text(el.message);
		});

		if (errors) {
			$(this).find('input[name=' + errors[0].attr + ']:first').focus();
			return;
		}

		$(this).find('.js-submit').addClass('disabled');

		$.post('/api/user/v2/check_email ', {email: formData.email})
			.done(function(res) {
				$self.find('.js-submit').removeClass('disabled');
				if (res.email) {
					$self.find('input[name=email]').focus()
						.closest('.form-group').addClass('error')
						.find('.text-error').text(validate[res.email]);
					return;
				}
				newUser.first_name = formData.first_name;
				newUser.last_name = formData.last_name;
				newUser.email = formData.email;

				if (newUser.force) {
					newUser.type = 'cinema';
					togglePopup('password');
				} else {
					togglePopup('role');
				}
			});

	});

	/*============================
	=            Role            =
	============================*/

	$('#form-role').on('change', 'input', function() {
		var $form = $('#form-role'),
			$btn  = $form.find('.js-submit');

		if ($form.find('input:checked').length) {
			$btn.removeClass('disabled');
		} else {
			$btn.addClass('disabled');
		}

	});

	$('#form-role').submit(function(event) {
		event.preventDefault();
		newUser.type = $(this).serializeObject().type;

		delete newUser.cinema_id;
		delete newUser.distributor_id;

		$('#form-cinema, #form-distributor').find('.js-submit').addClass('disabled');

		if (!newUser.type) return;

		switch (newUser.type) {
			case 'distributor':
				togglePopup('distributor');
			break;
			case 'guest':
				togglePopup('password');
			break;
			default:
				togglePopup('cinema');
			break;
		}

	});

	/*=====================================
	=            Select cinema            =
	=====================================*/

	function cinemaTemplate(data) {
		return [
			'<div class="dropdown-list__item" data-id="' + data.id + '">',
				'<div class="dropdown-list__item-name">' + (data.title.ru || data.title) + '</div>',
				'<span class="dropdown-list__item-address">' + (data.city.title.ru + ', ' || '') + (data.address || '') + '</span>',
			'</div>'
		].join('');
	}

	$('#form-cinema').on('keyup', 'input', function(event) {
		event.preventDefault();

		var $dropdown = $('#form-cinema').find('.dropdown'),
			$list = $dropdown.find('.dropdown-list'),
			value = $(this).val();

		if ($(this).val().length >= 2) {
			clearTimeout(timeout);
			timeout = setTimeout(function() {
				$.get('/api/start/search.json/?q=' + value)
					.done(function(res) {
						var results = res.list;
						$list.empty();
						$(results).each(function(index, cinema) {
							$list.append(cinemaTemplate(cinema));
						});
						results.length && $dropdown.find('input').is(':focus') ? $dropdown.addClass('dropdown--open') : $dropdown.removeClass('dropdown--open');
					});
			}, 500);
		} else {
			$dropdown.removeClass('dropdown--open');
		}
	});

	$('#form-cinema').on('click', '.dropdown-list__item', function(event) {
		event.preventDefault();
		var $form = $('#form-cinema');

		$form.find('input').val(
			$(this).find('.dropdown-list__item-name').text() + ', ' + $(this).find('.dropdown-list__item-address').text()
		);

		$form
			.find('input').val($(this).find('.dropdown-list__item-name').text() + ', ' + $(this).find('.dropdown-list__item-address').text()).end()
			.find('.dropdown').removeClass('dropdown--open').end()
			.find('.js-submit').removeClass('disabled').focus();

		newUser.cinema_id = $(this).data('id');
	});

	$('#form-cinema').submit(function(event) {
		event.preventDefault();
		if (newUser.cinema_id) togglePopup('password');
	});

	/*=====================================
	=            Select distributor       =
	=====================================*/

	function distributorTemplate(data) {
		return [
			'<div class="dropdown-list__item" data-id="' + data.id + '">',
				'<div class="dropdown-list__item-name">' + (data.name.full || data.name.short || '') + '</div>',
			'</div>'
		].join('');
	}

	function renderDistributors(distributors, container) {
		container.empty();
		$(distributors).each(function(index, distributor) {
			container.append(distributorTemplate(distributor));
		});
	}

	$('#form-distributor input').on('focus', function() {
		var $dropdown = $('#form-distributor').find('.dropdown');

		if (distributors.length) {
			$dropdown.addClass('dropdown--open');
		} else {
			$.get('/api/start/distributor.json', function(res) { distributors = res.distributor.list; })
				.done(function() {
					renderDistributors(distributors, $dropdown.find('.dropdown-list'));
					$dropdown.addClass('dropdown--open');
				});
		}
	});

	$('#form-distributor').on('keyup', 'input', function(event) {
		event.preventDefault();
		var $list = $('#form-distributor').find('.dropdown-list'),
			reg = new RegExp($(this).val(), 'i'),
			arr = [],
			i;

		for (i in distributors) {
			if (reg.test(distributors[i].name.full) || reg.test(distributors[i].name.short)) {
				arr.push(distributors[i]);
			}
		}

		if (arr.length) {
			renderDistributors(arr, $list);
			$(this).parent().addClass('dropdown--open');
		} else {
			$(this).parent().removeClass('dropdown--open');
		}

	});

	$('#form-distributor').on('click', '.dropdown-list__item', function(event) {
		event.preventDefault();

		$('#form-distributor')
			.find('input').val($(this).find('.dropdown-list__item-name').text()).end()
			.find('.dropdown').removeClass('dropdown--open').end()
			.find('.js-submit').removeClass('disabled').focus();

		newUser.distributor_id = $(this).data('id');
	});

	$('#form-distributor').submit(function(event) {
		event.preventDefault();
		if (newUser.distributor_id) togglePopup('password');
	});

	/*================================
	=            Password            =
	================================*/

	$('#form-password').submit(function(event) {
		event.preventDefault();

		var $self = $(this),
			formData = $(this).serializeObject(),
			errors;

		$(this).find('.form-group')
			.removeClass('error')
			.find('.text-error').text('');

		errors = isValid(formData);

		$(errors).each(function(index, el) {
			$self.find('input[name=' + el.attr + ']')
				.closest('.form-group').addClass('error')
				.find('.text-error').text(el.message);
		});

		if (errors) {
			$(this).find('input[name=' + errors[0].attr + ']:first').focus();
			return;
		}

		newUser.password  = formData.password;
		newUser.password1 = formData.password1;
		newUser.web = 1;

		$(this).find('.js-submit').addClass('disabled');

		$.post('/api/user/v2/signup', newUser)
			.done(function(res) {
				if (res.signup) document.location.href = '/';
			})
			.fail(function(res) {
				if (jQuery.parseJSON(res.responseText).msg === 'Invite not found') togglePopup('cinema');
			})
			.always(function() {
				$self.find('.js-submit').removeClass('disabled');
			});
	});

	/*==============================
	=            Forgot            =
	==============================*/

	$('#form-forgot').submit(function(event) {
		event.preventDefault();

		var $self = $(this),
			formData = $(this).serializeObject(),
			errors;

		$(this).find('.form-group')
			.removeClass('error')
			.find('.text-error').text('');

		errors = isValid(formData);

		$(errors).each(function(index, el) {
			$self.find('input[name=' + el.attr + ']')
				.closest('.form-group').addClass('error')
				.find('.text-error').text(el.message);
		});

		if (errors) {
			$(this).find('input[name=' + errors[0].attr + ']:first').focus();
			return;
		}

		$(this).find('.js-submit').addClass('disabled');

		$.post('/api/user/v2/forgot', formData)
			.done(function(res) {
				$self.find('.js-submit').removeClass('disabled');
				if (res.email) {
					$self.find('input[name=email]').focus()
						.closest('.form-group').addClass('error')
						.find('.text-error').text(validate[res.email]);
					return;
				}

				togglePopup('login');
				$.growl.notice({ title: infoMessage.forgotPassword.title, message: infoMessage.forgotPassword.message, fixed: true });
			});

	});

});

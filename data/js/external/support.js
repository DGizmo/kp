//-------------------- раздел обучения --------------------//
$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	_.templateSettings = {
		evaluate: /\{\{(.+?)\}\}/g,
		interpolate: /\{\{=(.+?)\}\}/g,
		escape: /\{\{-(.+?)\}\}/g
	};

	window.app.ArticleModel = Backbone.Model.extend({
		initialize: function() {
			this.fetch();
		},
		url: function() {
			return '/api/support/' + this.get('_id');
		},
		parse: function(r) {
			return r.article;
		}
	});

	window.app.ArticleView = Backbone.View.extend({
		template: _.template('<h1>{{= title }}</h1>{{= text }}<br>'),
		initialize: function() {
			this.listenTo(this.model, 'sync', this.render);
		},
		render: function() {
			this.$el.html(this.template(this.model.attributes));
		}
	});

	window.app.SupportArticlesList = Backbone.Model.extend({
		initialize: function() {
			this.fetch();
		},
		url: '/api/support',
		parse: function(r) {
			r.articles = r.articles.sort(function(a, b) { return a.position - b.position; });

			return r;
		}
	});

	window.app.SupportWrView = Backbone.View.extend({
		el: $('#support .main_content'),
		template: _.template([
			'<div class="paragraphs">',
				'<div class="paragraphs_wrap">',
					'<h1>Центр знаний</h1>',
					'<ol class="paragraphs_list">',
						'{{ for (var i in articles) { }}',
							'<li class="paragraph" data-article_id="{{= articles[i]._id }}">{{= articles[i].title }}</li><br>',
						'{{ } }}',
					'</ol>',
					'<br>',
				'</div>',
			'</div>',
			'<div class="content_text">',
				'<div class="shadow"></div>',
				'<div class="content_text_wrap"></div>',
			'</div>'
		].join('')),
		events: {
			'click .paragraph': function(e) {
				if (!$(e.currentTarget).hasClass('chosen')) this.toggleArticle($(e.currentTarget).data('article_id'));
			}
		},
		initialize: function(options) {
			this.model = new window.app.SupportArticlesList();
			this.model.set({currentArticleId: options.currentArticleId});
			this.listenTo(this.model, 'sync', this.render);
		},
		render: function() {
			this.$el.html(this.template(this.model.attributes));
			this.afterRender();
		},
		afterRender: function() {
			if (this.model.get('currentArticleId')) {
				this.toggleArticle(this.model.get('currentArticleId'));
				this.$el.find('.paragraphs').scrollTo(this.$el.find('.paragraph.chosen'), {offset: -15});
			} else {
				this.toggleArticle(this.$el.find('.paragraph:first').data('article_id'));
			}
		},
		toggleArticle: function(articleId) {
			this.$el.find('.paragraph.chosen').removeClass('chosen');
			this.$el.find('.paragraph[data-article_id="' + articleId + '"]').addClass('chosen');

			this.currentArticle     = new window.app.ArticleModel({_id: articleId});
			this.currentArticleView = new window.app.ArticleView({
				model: this.currentArticle,
				el: this.$el.find('.content_text_wrap')
			});

			history.pushState(null, null, '/support/' + articleId);
		}
	});

});

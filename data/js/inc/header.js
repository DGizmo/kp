$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.HeaderUserView = Backbone.View.extend({
		className: 'header-user',
		template: '#user_header',
		events: {
			'click .header-user-popup-inner__profile':             'changeProfilePopup',
			'click .header-user-popup-inner__cinemas':             'changeSettingsPopup',
			'click .header-user-popup-inner__distributor-setting': 'distributorSetting',
			'click .header-user-popup-inner__logout':              'logout'
		},
		changeProfilePopup: /* istanbul ignore next */ function changeProfilePopup(section) {
			this.userView = new window.app.UserView({
				model: this.model,
				section: section ? section : false
			});

			_gaq.push(['_trackEvent', 'Пользователь', 'Открытие настроек']);
		},
		changeSettingsPopup: /* istanbul ignore next */ function changeSettingsPopup() {
			this.userViewCinemas = new window.app.UserCinemasWrapper({
				model: this.model,
				collection: window.app.App.Collections.UserCinemaGroupsCollection
			});
		},
		distributorSetting: /* istanbul ignore next */ function distributorSetting(e) {
			e.preventDefault();
			this.distributorDashboardSettings = new window.app.DistributorDashboardSettings({
				user: this.model,
				collection: reqres.request('get:app:entity', 'distributorsCollection')
			});
		},
		logout: /* istanbul ignore next */ function logout() {
			window.location.href = '/logout';
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.options.parent.$el.find('.nt-sidebar-toggle').before(this.render().el);
			this.listenTo(this.model, 'change:photo_medium', this.render);
			this.listenTo(Backbone, 'user:profile:open:photo', function() {
				this.changeProfilePopup('photo');
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});

	window.app.HeaderView = Backbone.View.extend({
		className: 'head_wrapper',
		template: '#header_tmpl',
		events: {
			'click .header__cinema .select': /* istanbul ignore next */ function(e) {
				var $select = $(e.currentTarget);
				if (!$select.hasClass('single') && !$select.find('.active').hasClass('blocked')) {
					$select.find('.list').toggleClass('visible');
				}
			},
			'click .header__cinema .list .item': /* istanbul ignore next */ function(e) {
				var $oneItem = $(e.currentTarget),
					$active  = $oneItem.closest('.select').find('.active'),
					$list    = $oneItem.closest('.select').find('.list'),
					newCinemaId;

				if (!$oneItem.hasClass('selected')) {
					newCinemaId = parseInt($oneItem.data('cinema-id'));

					$oneItem.addClass('selected').siblings().removeClass('selected');

					$active.attr('cinema-active-id', newCinemaId);
					$active.find('.cinema_and_city').text($oneItem.find('.cinema_and_city').text());

					$list.removeClass('visible');

					this.model.set({
						current: {
							cinema_id: newCinemaId,
							node_id: parseInt(this.model.getCinema(newCinemaId).dd24_pro, 10) || false
						}
					}, { silent: true });

					Backbone.trigger('cinema-changed', this.model.get('current'));
					this.checkSelectorLength();
				}
			}
		},
		checkSelectorLength: function checkSelectorLength() {
			if (this.$el.find('.select .active').length && this.$el.find('.nt-sidebar-toggle').length) {
				var $selectorText = this.$el.find('.select .active'),
					freeSpace = this.$el.find('.nt-sidebar-toggle').offset().left - $selectorText.get(0).getBoundingClientRect().right,
					newWidth  = $selectorText.width() - 100 + freeSpace;

				if (newWidth > this.$el.find('.select .active .cinema_and_city').width()) {
					$selectorText.width('auto');
				} else {
					$selectorText.width($selectorText.width() - 100 + freeSpace);
				}
			}
		},
		initialize: function initialize() {
			this.titles = {
				default:             'Главная',
				dashboard:           'Главная',
				releases:            'Релизы',
				release:             'Релиз',
				distributors:        'Дистрибьюторы',
				keys:                'KDM',
				my:                  'Мой репертуар',
				schedule:            'Расписание сеансов',
				employee:            'Коллеги',
				planning:            'Репертуарное планирование',
				cinemas:             'Кинотеатр',
				distributor_cinemas: 'Кинотеатры',
				statistic:           'Статистика',
				advertising:         'Медиапланирование трейлеров',
				booking:             'Букинг',
				keygen:              'Генерация ключей',
				analytics:           'Аналитика',
				distributor_reports: 'Отчеты кинотеатров',
				service:             'Оборудование',
				commercials:         'Планирование коммерческой рекламы'
			};

			this.model = reqres.request('get:user');

			$('body').prepend(this.render().el);

			this.headerUserView = new window.app.HeaderUserView({
				model: this.model,
				parent: this
			});

			this.listenTo(Backbone, 'header:cinemaSelect:activeToggle', this.cinemaSelectToggle);

			this.listenTo(Backbone, 'window-resized', this.checkSelectorLength);

			commands.setHandlers({
				'set:booking-project:header': {
					callback: function(options) {
						var text = 'Букинг';
						if (!options.clean) {
							text = [
								'Букинг <span class="pr_title">',
								options.projectName,
								' </span><span class="pr_date">',
								options.projectDate,
								'</span>'
							].join('');
						}
						this.$('.header__page span[data-page="booking"]').html(text);
					},
					context: this
				},
				'set:releases-list:last-change': {
					callback: function(options) {
						var now  = moment(),
							lastChangeDate = moment(options.lastChange, 'X').add(1, 'minute'),
							text = [
								'Релизы <span class="last_change_date">',
								'Последнее изменение: ',
								(now.diff(lastChangeDate, 'days') >= 1) ? lastChangeDate.format('DD.MM.YYYY, HH:mm') : lastChangeDate.from(now),
								'</span>'
							].join('');

						this.$('.header__page span[data-page="releases"]').html(text);
					},
					context: this
				}
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				self = this;

			this.$el.html(template({
				cinemas: this.model.getCinemas(),
				titles:  this.titles
			}));

			setTimeout(function() {
				if (self.model.get('type') === 'distributor') return;
				self.checkSelectorLength();
			}, 0);

			return this;
		},
		cinemaSelectToggle: function cinemaSelectToggle(action) {
			if (action === 'block') {
				this.$el.find('.header__cinema .active').addClass('blocked');
			} else {
				this.$el.find('.header__cinema .active').removeClass('blocked');
			}
		}
	});
});

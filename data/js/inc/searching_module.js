$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.SearchingModuleView = Backbone.View.extend({
		className: 'searching-module',
		template: _.template('<input type="text" class="input"><i class="remove"></i><div class="results"></div>'),
		events: {
			'focus .input': function() {
				if (this.options.animation) this.$el.addClass('focus');
			},
			'blur .input': function() {
				if (this.options.animation) this.$el.removeClass('focus');
			},
			keydown:         'onKeyDown',
			'input .input':  'onChangeInput',
			'click .remove': 'resetInput'
		},
		initialize: function initalize(options) {
			this.options = _.defaults(options || {}, {
				animation: true
			});
			this.onSearchingFinish = this.options.onSearchingFinish;
			this.subViews = [];

			if (typeof this.onSearchingFinish !== 'function')
				throw 'searching module onSearchingFinish undefined.';
		},
		render: function render() {
			this.$el.html(this.template);

			return this;
		},
		onChangeInput: function onChangeInput(event) {
			var self = this,
				inputValue = $(event.currentTarget).val();

			if (inputValue.length) {
				if (this.options.animation) this.$('.input').addClass('full');
				this.$('.remove').show();
			} else {
				_.invoke(this.subViews, 'remove');
				if (this.options.animation) this.$('.input').removeClass('full');
				this.$('.remove').hide();
			}

			clearTimeout(this.timer);
			this.timer = setTimeout(function() {
				_.invoke(self.subViews, 'remove');
				self.onSearchingFinish(inputValue.replace(/(^\s+|\s+$)/g, '')); // удаление пробелов в начале и конце строки
			}, 200);
		},
		resetInput: function resetInput() {
			this.$('.input')
				.val('')
				.trigger('input');
		},
		onKeyDown: function onKeyDown(event) {
			var key           = event.keyCode,
				selectedItem  = this.$('.selected'),
				top           = this.$('.results').offset().top,
				bottom        = top + this.$('.results').height();

			if (key === 13) selectedItem.trigger('click');
			if (key === 40 && !selectedItem.length) {
				event.preventDefault();
				this.$('.results .item:first').addClass('selected');
			}

			if (key === 40 && selectedItem.next().length) {
				selectedItem.next().addClass('selected');
				selectedItem.removeClass('selected');
				if (selectedItem.next().offset().top + selectedItem.next().height() > bottom)
					this.$('.results').scrollTo('.item:eq(' + (selectedItem.next().index() - 4) + ')');
			} else if (key === 38 && selectedItem.prev().length) {
				selectedItem.prev().addClass('selected');
				selectedItem.removeClass('selected');
				if (selectedItem.prev().offset().top < top)
					this.$('.results').scrollTo('.selected');
			}
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

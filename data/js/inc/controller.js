$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.Controller = Backbone.Router.extend({
		routes: {
			'':                                   'index',

			'dashboard(/)':                       'dashboard',

			//Список релизов
			'releases(/)':                        'releasesList',
			'releases/:query(/)(:page)(/)':       'releasesList',

			//Карточка релизов
			'release/:id(/)(:page)(/)':           'releaseCard',

			//Остальное
			'distributors(/:id)(/)':              'distributors',
			'keys(/)': 'keys',
			'schedule(/)(:mode)':                 'schedule',
			'employee(/)':                        'employee',
			'planning(/)':                        'planning',
			'cinemas(/)':                         'cinemaInfo',
			'cinema/(:id)(/)':                    'distributorCinemaCard',
			'analytics(/)':                       'analytics',
			'advertising(/)':                     'advertising',
			'commercials(/:id)(/)':               'commercials',

			'booking(/)':                                        'bookingList',
			'booking/:projectID/:requestID/:from_date(/)(:new)': 'bookingProject',

			'keygen(/)':                          'keygenReleasesList',
			'keygen/(:projectID)/(:requestID)':   'KeygenProjects',

			//Отчеты дистрибьютора
			'reports(/)':                         'reports',
			// Киноплан-сервис
			'service(/)':                         'service',

			'statistic(/)':                       'statistic',
			'statistic/cinemas/search/:value':    'search_cinema',
			'statistic/cinemas(/)':               's_cinemas',
			'statistic/cinemas/:param/:value(/)': 's_cinemas_param',
			'statistic/charts(/)':                's_charts',

			'*path':                              'redirectHome'

		},
		initialize: function initialize() {
			this.titles = reqres.request('get:header:titles');
			this.user   = reqres.request('get:user');

			Backbone.history.start({pushState: true, root: '/'});

			this.listenTo(Backbone, 'router:navigate', function(route) {
				this.navigate(route, {trigger: true});
			});
		},
		el: $('.wrapper'),
		currentView: null,
		switchView: function switchView(view) {
			Backbone.trigger('popover:hide');

			if (this.currentView !== null && this.currentView.cid !== view.cid) {
				this.currentView.remove().$el.empty();
			}
			this.el.html(view.render().el);
			this.currentView = view;
		},
		togglePage: function togglePage(page, title) {

			if (page !== 'release' && this.titles[page]) {
				document.title = this.titles[page] + ' - Киноплан';
				_gaq.push(['_trackEvent', title || this.titles[page], 'Открытие']);
			}

			if (page === 'planning') {
				$('#halls_schedule').find('#my_repertoire_wr').focus();
			} else if (page === 'dashboard') {
				if ($('#' + page).length > 0) {
					$('#' + page).show();
				} else {
					$('#' + page + '_distributor').show();
				}
			}

			$('.header__page')
				.show()
				.find('.header__page__item').hide().filter('[data-page="' + page + '"]').show();

			$('.list').removeClass('active');
			if (page === 'release') {
				$('nav.sidebar .list[data-tour="releases"]').addClass('active');
			} else {
				$('nav.sidebar .list[data-tour="' + page + '"]').addClass('active');
			}

			$('.header__cinema, .copy-cinema-schedule').removeClass('visible');
		},
		index: function index() {
			if (!_.contains(['guest', 'partner'], this.user.get('type'))) {
				this.dashboard();
			} else {
				this.releasesList();
			}
		},
		redirectHome: function redirectHome() {
			this.navigate('/', {trigger: true});
		},
		dashboard: function dashboard() {
			if (_.contains(['guest', 'partner'], this.user.get('type'))) {
				this.index();
				return;
			}

			this.togglePage('dashboard');

			if (this.user.get('type') === 'distributor') {
				this.switchView(new window.app.DashboardDistributorWrapView({
					collections: window.app.App.Collections
				}));
			} else {
				this.switchView(new window.app.DashView({
					collections:   window.app.App.Collections,
					notifications: window.app.App.notifications
				}));
			}
		},
		releasesList: function releasesList(query, page) {
			this.togglePage('releases');
			this.switchView(new window.app.ReleasesWrapView({
				mode:  query || '',
				param: page  || ''
			}));
		},
		releaseCard: function releaseCard(id, page) {
			var pages = {
				materials: 'release_trailers',
				trailers:  'release_trailers',
				analysis:  'release_analysis',
				files:     'release_files'
			};

			if (typeof id === 'undefined' || !_.isFinite(id)) {
				this.navigate('/releases/', {trigger: true});
				return;
			}

			this.togglePage('release');
			this.switchView(new window.app.ReleaseCardWrapView({
				releaseId: parseInt(id),
				tab: pages[page] || 'release_info'
			}));
		},
		distributors: function distributors(id) {
			if (this.user.get('type') === 'guest') {
				this.navigate('/releases', {trigger: true});
				return;
			}

			this.togglePage('distributors');
			this.switchView(new window.app.DistributorsWrapView({
				collection: reqres.request('get:app:entity', 'distributorsCollection'),
				distributor: parseInt(id) || ''
			}));
		},
		keys: function keys() {
			this.togglePage('keys');
			this.switchView(new window.app.KeysWrapView({
				collection: reqres.request('get:app:entity', 'keysCollection')
			}));
		},
		schedule: function schedule(mode) {
			this.togglePage('schedule');
			$('.header__cinema, .copy-cinema-schedule').addClass('visible');

			this.switchView(new window.app.WeekScheduleWrapView({
				collections: window.app.App.Collections,
				user_watch: mode || 'week'
			}));
		},
		employee: function employee() {
			if (!_.findWhere(this.user.get('cinema_rights'), {master: 1})) {
				this.index();
				return;
			}

			this.togglePage('employee');
			this.switchView(new window.app.EmployeesView({
				collection: reqres.request('get:app:entity', 'employeesCollection')
			}));
		},
		planning: function planning() {
			if (!window.app.App.Collections.repertoire && !window.app.App.Collections.network) {
				window.app.App.Collections.repertoire = new window.app.RepertoireCollection();
				window.app.App.Collections.network = new window.app.NetworkRepertoireCollection();
				window.app.App.State = 'planning:cinema';
			}

			this.togglePage('planning');
			$('.header__cinema').addClass('visible');

			window.app.App.repview = new window.app.RepertoireView({
				model:      window.app.App.Collections.repertoire.parentviewModel || new Backbone.Model(),
				repertoire: window.app.App.Collections.repertoire,
				network:    window.app.App.Collections.network
			});

			this.switchView(window.app.App.repview);
		},
		cinemaInfo: function cinemaInfo() {
			if (this.user.get('type') !== 'distributor') {
				this.togglePage('cinemas');
				$('.header__cinema').addClass('visible');
				this.switchView(new window.app.CinemaInfoView());
			} else {
				this.togglePage('distributor_cinemas');
				this.switchView(new window.app.DistributorCinemasWrapperView({
					collections: window.app.App.Collections
				}));
			}
		},
		distributorCinemaCard: function distributorCinemaCard(id) {
			this.togglePage('distributor_cinemas');
			this.switchView(new window.app.DistributorCinemaCardWrapView({
				modelId: id,
				collections: window.app.App.Collections
			}));
		},
		analytics: function analytics() {
			this.togglePage('analytics');
			this.switchView(new window.app.AnalyticsView({
				collection: reqres.request('get:app:entity', 'analyticsCollection')
			}));
		},
		reports: function reports() {
			if (!window.app.App.Collections.distributorsReportsReleasesCollection) {
				window.app.App.Collections.distributorsReportsReleasesCollection = new window.app.DistributorReportsReleasesCollection();
			}

			if (!window.app.App.Collections.distributorsReportsCinemasCollection) {
				window.app.App.Collections.distributorsReportsCinemasCollection = new window.app.DistributorReportsCinemasCollection();
			}

			if (!window.app.App.Collections.distributorsReportsCollection) {
				window.app.App.Collections.distributorsReportsCollection = new window.app.DistributorReportsCollection();
			}

			this.togglePage('distributor_reports');
			this.switchView(new window.app.DistributorReportsWrapperView({
				releasesCollection: window.app.App.Collections.distributorsReportsReleasesCollection,
				cinemasCollection: window.app.App.Collections.distributorsReportsCinemasCollection,
				reportsCollection: window.app.App.Collections.distributorsReportsCollection
			}));
		},
		service: function service() {
			if (!_.contains(['cinema', 'mechanic'], this.user.get('type'))) {
				this.redirectHome();
				return;
			}

			this.togglePage('service');
			this.switchView(new window.app.ServiceWrapView({user: this.user}));
		},
		advertising: function advertising() {
			this.togglePage('advertising');
			$('.header__cinema').addClass('visible');

			this.switchView(new window.app.AdvertisingView({
				collections: {
					releases: reqres.request('get:app:entity', 'advertisingReleases'),
					trailers: reqres.request('get:app:entity', 'advertisingTrailers')
				}
			}));
		},
		commercials: function commercials(page) {
			this.togglePage('commercials');
			$('.header__cinema').addClass('visible');

			this.switchView(new window.app.CommercialWrapperView({
				collection: reqres.request('get:app:entity', 'commercialsCollection'),
				page: page === 'new' ? 'new' : parseInt(page) || ''
			}));
		},
		bookingList: function bookingList() {
			this.togglePage('booking');

			this.switchView(new window.app.BookingReleasesListView({
				collection: reqres.request('get:app:entity', 'bookingCollection')
			}));
		},
		bookingProject: function bookingProject(projectID, requestID, fromDate, newBooking) {
			this.togglePage('booking');

			var startDate = moment(fromDate, 'YYYY-MM-DD');
			this.switchView(
				new window.app.BookingProjectWrapView({
					model: new window.app.BookingProjectModel({
						project_id: projectID,
						request_id: requestID,
						from_date:  startDate,
						to_date:    moment(startDate).add(4, 'weeks')
					}),
					newBooking: newBooking || false
				})
			);
		},
		keygenReleasesList: function keygenReleasesList() {
			this.togglePage('keygen');
			this.switchView(new window.app.KeygenReleasesListView());
		},
		KeygenProjects: function KeygenProjects(projectID, requestID) {
			this.togglePage('keygen');
			this.switchView(new window.app.KeygenProjectView(projectID, requestID));
		},
		statistic: function statistic() {
			if (!this.user.accessTo('statistic')) {
				return;
			}

			this.togglePage('statistic');
			$('.tabs li, .tab').removeClass('active');
			$('#stat, .stat_tab').addClass('active');
			$('.fs').hide();
			$('.filt_select_stat').show();

			this.switchView(new window.app.StatisticsWrapperView());
		},
		search_cinema: function search_cinema(value) {
			if (!this.user.accessTo('statistic')) {
				return;
			}

			window.app.App.statisticsWrapperView = new window.app.StatisticsWrapperView();
			this.switchView(window.app.App.statisticsWrapperView);

			window.app.App.StatisticsCinema.urlfor = window.app.App.StatisticsCinema.baseUrl + '?q=' + value;
			window.app.App.StatisticsCinema.fetch();

			this.togglePage('statistic', 'Статистика: кинотеарты');
			$('.tabs li, .tab').removeClass('active');
			$('#cinema, .cinema_tab').addClass('active');
			$('.cinema_search').show();
			$('#cinema_search input').val(value);
		},
		s_cinemas: function s_cinemas() {
			if (!this.user.accessTo('statistic')) {
				return;
			}

			if (!window.app.App.statisticsWrapperView) {
				window.app.App.statisticsWrapperView = new window.app.StatisticsWrapperView();
				this.switchView(window.app.App.statisticsWrapperView);
			}

			this.togglePage('statistic', 'Статистика: кинотеарты');
			$('.tabs li, .tab').removeClass('active');
			$('#cinema, .cinema_tab').addClass('active');
			$('.cinema_search').show();
		},
		s_cinemas_param: function s_cinemas_param(param, value) {
			if (!this.user.accessTo('statistic')) {
				return;
			}

			window.app.App.statisticsWrapperView = new window.app.StatisticsWrapperView();
			window.app.App.StatisticsCinema.setParam(param, value);

			this.switchView(window.app.App.statisticsWrapperView);

			this.togglePage('statistic', 'Статистика: кинотеарты');
			$('.tabs li, .tab').removeClass('active');
			$('#cinema, .cinema_tab').addClass('active');
			$('.cinema_search').show();
		},
		s_charts: function s_charts() {
			if (!this.user.accessTo('statistic')) {
				return;
			}

			if (!window.app.App.statisticsWrapperView) {
				window.app.App.statisticsWrapperView = new window.app.StatisticsWrapperView();
				this.switchView(window.app.App.statisticsWrapperView);
			}

			this.togglePage('statistic', 'Статистика: диаграммы');
			$('.tabs li, .tab').removeClass('active');
			$('#chart, .chart_tab').addClass('active');

			$('.fs').hide();
			$('.filt_select_chart').show();
		}
	});

});

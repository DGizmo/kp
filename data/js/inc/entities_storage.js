$(function() {
	'use strict';
	window.app = window.app || {};

	var EntitiesStorage = function(entities) {
		this.entities = {};
		this.add(entities || {});
		this.initialize.apply(this, arguments);
	};

	_.extend(EntitiesStorage.prototype, {
		initialize: function() {},

		add: function add(entities) {
			if (typeof entities !== 'object') throw 'Entity name is not a Object';
			_.each(entities, function(constr, name) {
				if (typeof this.entities[name] === 'object') return;
				this.entities[name] = constr;
			}, this);
			return this;
		},

		get: function get(name) {
			if (typeof name !== 'string') throw 'Entity name is not a String';

			var Entity = this.entities[name];

			if (typeof Entity === 'function') {
				this.entities[name] = new Entity();
				return this.entities[name];
			}

			return Entity;
		},

		remove: function remove(name) {
			if (typeof name !== 'string') throw 'Entity name is not a String';
			delete this.entities[name];
			return this;
		}
	});

	window.app.EntitiesStorage = EntitiesStorage;

});

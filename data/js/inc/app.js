$(function() {
	'use strict';
	window.app  = window.app || /* istanbul ignore next */ {};
	window._gaq = window._gaq || [];

	window.app.AppView = Backbone.View.extend({
		el: 'body',
		events: {
			click: function(e) {
				if ($(e.target).closest('.active').length || $(e.target).closest('.arrow_down_select').length) return;
				$('.header__cinema .list').removeClass('visible');

				if ($(e.target).closest('.release_search').length) return;
				$('.release_search').removeClass('focus').find('.results').empty().removeClass('has_results');

				Backbone.trigger('body:click', e);
			},
			mouseup: function(e) {
				var el = $('.nt-sidebar, .nt-sidebar-toggle, .header__cinema, .releases_news .all, .kdm_news .all, .shipment_news .all, .messages .all, .messages .item, .context-menu__item');
				if (!el.is(e.target) && el.has(e.target).length === 0) {
					$('.nt-sidebar-toggle-icon, .nt-sidebar-item').removeClass('open');
					Backbone.trigger('chat-close');
				}
			},
			popstate: function() {
				if (window.app.tour._inited) {
					window.app.tour.end();
				} else if (window.app.readOnlyTour._inited) {
					window.app.readOnlyTour.end();
				}
			}
		},
		initialize: function initialize() {
			this.Collections = {};
			this.Models = {};

			this.loader = this.$el.children('.loader');

			var user = this.user = new window.app.User();

			this.listenTo(Backbone, 'loader', function(action) { this.loader[action](); });
			this.listenTo(Backbone, 'remove:tipsy', this.removeTipsy);

			$(window).resize(_.bind(_.throttle(this.windowResized, 100), this));

			user.fetch({success: _.bind(function() {
				_gaq.push(['_setAccount', 'UA-44257021-1']);
				_gaq.push(['_trackPageview']);
				_gaq.push(['_setCustomVar', 1, 'gaUserId', user.id, 1]);

				if (user.getCinemas().length) {
					user.set({
						current: {
							cinema_id: user.get('cinema')[0].id
						},
						defaultCinemaSettings: {
							defaultCinemaAd:   0,
							schedule_rounding: 'ceil',
							cinemaOpen:        '10:00',
							cinemaClose:       '02:00'
						},
						defaultHallSettings: {
							defaultHallOpen:  '10:00',
							defaultHallClose: '01:00',
							defaultHallPause: 10
						}
					});
				}

				this.sidebar       = new window.app.SidebarView();
				this.header        = new window.app.HeaderView();
				this.notifications = new window.app.NotificationsSidebarView();

				if ((user.get('type') === 'cinema') && user.getCinemas().length && ($.cookie('tour_ver') !== window.app.tour._options.version)) {
					$.cookie('tour_ver', window.app.tour._options.version, { expires: 365 });
					_gaq.push(['_trackEvent', 'Обучение', 'Репертуар: Запуск']);

					if (user.getCinema().rights === ('read' || 'rw') ? true : false) {
						window.app.readOnlyTour.start();
					} else {
						window.app.tour.start();
					}
				}

				// if (!($.cookie('mechanic-webinar'))) {
				// 	$.cookie('mechanic-webinar', 1, { expires: 365 });
				// 	this.mechanicWebinarPopup = new window.app.MechanicWebinarPopup();
				// }

				this.$el.children('.wrapper').css({height: this.$('.sidebar').height() - 50});
				this.listenTo(Backbone, 'window-resized', function() {
					this.$el.children('.wrapper').css({height: this.$('.sidebar').height() - 50});
				});

				if (!this.Collections.UserCinemaGroupsCollection) {
					this.Collections.UserCinemaGroupsCollection = new window.app.UserCinemaGroupsCollection();

					this.Collections.UserCinemaGroupsCollection.fetch();
				}

				$.reject({
					reject: {
						all: false,
						msie: 10,
						chrome: 30,
						opera: 20,
						firfox: 28,
						safari: 6,
						webkit: 534.30
					},
					afterReject: _.bind(function() {
						_gaq.push(['_trackEvent', 'Пользователь', 'Неподдерживаемый браузер', user.id]);
					}, this),
					display: ['chrome', 'firefox', 'safari', 'opera'],
					browserShow: true,
					browserInfo: {
						chrome: {
							text: 'Google Chrome',
							url: 'http://www.google.com/chrome/'
						},
						firefox: {
							text: 'Mozilla Firefox',
							url: 'http://www.mozilla.com/firefox/'
						},
						safari: {
							text: 'Safari',
							url: 'http://www.apple.com/safari/download/'
						},
						opera: {
							text: 'Opera',
							url: 'http://www.opera.com/download/'
						}
					},
					header: 'Киноплан предупреждает!',
					paragraph1: 'Данная версия браузера не поддерживает многие современные технологии, ' +
								'из-за чего многие страницы отображаются некорректно, ' +
								'а главное — на сайтах могут работать не все функции.',
					paragraph2: 'Обновите ваш браузер на один из предложенных ниже. Все они бесплатны, ' +
								'легко устанавливаются и просты в использовании.',
					close: true,
					closeMessage: 'Закрыв это окно Вы не сможете воспользоваться всеми функциями Киноплана.',
					closeLink: 'Закрыть',
					closeURL: '#',
					closeESC: true,
					closeCookie: true,
					cookieSettings: {
						path: '/',
						expires: 86400
					},
					imagePath: '/img/jreject/',
					overlayBgColor: '#000',
					overlayOpacity: 0.8,
					fadeInTime: 'fast',
					fadeOutTime: 'fast',
					analytics: true
				});

				Raven.setUser({
					name:  user.get('first_name') + ' ' + user.get('last_name'),
					type:  user.get('type'),
					email: user.get('email'),
					id:    user.id
				});

				window.app.controller = new window.app.Controller();

			}, this)});

			this.entitiesStorage = new window.app.EntitiesStorage({
				advertisingReleases:    window.app.AdvertisingReleases,
				advertisingTrailers:    window.app.AdvertisingTrailers,
				analyticsCollection:    window.app.AnalyticsCollection,
				commercialsCollection:  window.app.Commercials,
				distributorsCollection: window.app.Distributors,
				employeesCollection:    window.app.EmployeesCollection,
				keysCollection:         window.app.Keys,
				bookingCollection:      window.app.BookingCollection,
				bookingProposalCinemas: window.app.BookingProposalCinemas
			});

			Raven.config('http://d1d0a2f0dbf9431f9934e08785bfab4f@sentry.dcp24.ru/4', {
				collectWindowErrors: true,
				fetchContext: true,
				logger: 'javascript',
				whitelistUrls: [/https?:\/\/((www)\.)?((release)\.)?kinoplan24\.ru/]
			}).install();

			reqres.setHandlers({
				'get:user': {
					callback: function() {
						return user;
					},
					context: this
				},
				'get:header:titles': {
					callback: function() {
						return this.header.titles;
					},
					context: this
				},
				'get:app:state': {
					callback: function() {
						return this.State;
					},
					context: this
				},
				'get:app:entity': {
					callback: function(entity) {
						return this.entitiesStorage.get(entity);
					},
					context: this
				}
			});

			commands.setHandlers({
				'show:feedback-popup': {
					callback: function() {
						this.feedbackPopup = new window.app.FeedbackPopup();
					},
					context: this
				}
			});

			Backbone.TemplateCache.add('#unapprovedTemplate', [
				'<div class="unapproved">',
					'<a href="/"><b>Кино</b>план</a>',
					'<p>Благодарим Вас за регистрацию и предоставленную информацию! Как только в кинотеатре, ',
						'который Вы указали при регистрации подтвердят Ваше трудоустройство — менеджер Киноплана ',
						'откроет Вам доступ к {{- accessTo }}.</p>',
				'</div>'
			].join(''));
		},
		removeTipsy: function removeTipsy() {
			this.$el.children('.tipsy').remove();
		},
		windowResized:function windowResized() {
			Backbone.trigger('window-resized');
		}
	});

});

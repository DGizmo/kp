$(function() {
	'use strict';
	window.app = window.app || {};

	window.app.DistributorReportsRelease = Backbone.Model.extend({
		defaults: {
			id:         null,
			date:       null,
			title:      '',
			copies:     '',
			reports:    0,
			seances:    0,
			viewers:    0,
			box_office: 0
		}
	});

	window.app.DistributorReportsReleasesCollection = Backbone.Collection.extend({
		model: window.app.DistributorReportsRelease,
		initialize: function initialize() {
			reqres.setHandlers({
				'get:distributor:reports:release:collection:model': {
					callback: function(id) {
						return this.get(id);
					},
					context: this
				}
			});
		},
		parse: function parse(r) {
			r = r.sort(function(a, b) {
				return +moment(b.date) - +moment(a.date);
			});

			r.each(function(model) {
				if (!model.reports) model.reports = 'Нет отчетов';
			});

			return r;
		},
		url: '/api/distributors/reports'
	});

	window.app.DistributorReportsCinemas = Backbone.Model.extend({
		defaults: {
			release_title: '',
			cinema:        {},
			seances:       0,
			viewers:       0,
			box_office:    0,
			date:          null
		}
	});

	window.app.DistributorReportsCinemasCollection = Backbone.Collection.extend({
		model: window.app.DistributorReportsCinemas,
		initialize: function initialize() {
			reqres.setHandlers({
				'get:distributor:reports:cinemas:collection:model': {
					callback: function(id) {
						return this.get(id);
					},
					context: this
				}
			});
		},
		parse: function parse(r) {
			var releaseTitle = reqres.request('get:distributor:reports:release:collection:model', this.release_id).get('title');

			_.each(r, function(data) {
				data.releaseTitle = releaseTitle;
				data.id = data.cinema.id;
			}, this);

			r = r.sort(function(a, b) {
				return +moment(a.date) - +moment(b.date);
			});

			return r;
		},
		url: function url() {
			return '/api/distributors/reports/releases/' + this.release_id;
		}
	});

	window.app.DistributorReport = Backbone.Model.extend({
		defaults: {
			id:            null,
			release_title: '',
			cinema:        {},
			date:          null,
			seances:       0,
			viewers:       0,
			box_office:    0,
			last_report:   null
		}
	});

	window.app.DistributorReportsCollection = Backbone.Collection.extend({
		model: window.app.DistributorReport,
		parse: function parse(r) {
			var releaseTitle = reqres.request('get:distributor:reports:release:collection:model', this.release_id).get('title'),
				cinemaData = reqres.request('get:distributor:reports:cinemas:collection:model', this.cinema_id).get('cinema');

			_.each(r, function(data) {
				data.releaseTitle = releaseTitle;
				data.cinemaTitle = [cinemaData.title, cinemaData.city].join(', ');
			}, this);

			r = r.sort(function(a, b) {
				return +moment(a.date) - +moment(b.date);
			});

			return r;
		},
		url: function url() {
			return '/api/distributors/reports/releases/' + this.release_id + '/cinemas/' + this.cinema_id;
		},
		groupByWeeks: function groupByWeeks() {
			return _.groupBy(this.models, function(model) { return moment(model.get('date')).week(); });
		}
	});

	window.app.DistributorReportsWrapperView = Backbone.View.extend({
		className: 'distributor-reports',
		template: '#distributorReportsWrapper',
		events: {
			'click .back': function() {
				if (this.contactsView === this.currentView) {
					Backbone.trigger('distributor:reports:switch:report:view', 'reports', this.currentCinema, false);
					this.$('.sections-tabs-inner__item:not(.active)').addClass('active').siblings().removeClass('active');
				} else if (this.reportsState === 'cinemas') {
					Backbone.trigger('distributor:reports:switch:report:view', 'releases', null, false);
				} else if (this.reportsState === 'reports') {
					Backbone.trigger('distributor:reports:switch:report:view', 'cinemas', this.cinemasCollection.release_id, false);
				}
			},
			'keyup .input': function(e) {
				var $input = $(e.currentTarget),
					$clear = $input.siblings('.remove'),
					value  = $input.val().toLowerCase();

				value.length ? $clear.show() : $clear.hide();

				this.$('.reports-table-item').hide().children('.reports-table__cell:first-child:containsLower(' + value + ')').closest('.reports-table-item').show();
			},
			'click .remove': function(e) {
				$(e.currentTarget).hide().siblings('.input').val('');
				this.$el.find('.reports-table-item').show();
			},
			'click .sections-tabs-inner__item': function(e) {
				var $target = $(e.currentTarget),
					tab = $target.data('tab');

				$target.addClass('active').siblings().removeClass('active');

				if (tab === 'contacts') {
					this.showContacts(this.currentCinema);
				} else {
					Backbone.trigger('distributor:reports:switch:report:view', this.reportsState, this.currentCinema, false);
				}
			}
		},
		initialize: function initialize(options) {
			this.options       = options || {};
			this.currentView   = null;
			this.reportsState  = 'releases';
			this.currentCinema = null;
			this.scrollWidth   = _.getScrollBarWidth();
			this.contactsView  = null;

			this.releasesCollection = this.options.releasesCollection;
			this.cinemasCollection  = this.options.cinemasCollection;
			this.reportsCollection  = this.options.reportsCollection;

			this.listenTo(Backbone, 'distributor:reports:switch:report:view', function(reportsState, id, fetchParam) {
				this.reportsState = reportsState;

				if (reportsState === 'releases') {
					this.$('.back').addClass('hide');
					this.getReportsCollectionData(this.releasesCollection, {withFetch: fetchParam});
					this.$('.searching-module').hide();
				} if (reportsState === 'cinemas') {
					this.$('.back').removeClass('hide');

					if (this.cinemasCollection.release_id !== id) {
						this.cinemasCollection.release_id = id;
					} else {
						fetchParam = false;
					}

					this.getReportsCollectionData(this.cinemasCollection, {withFetch: fetchParam});
					this.cinemasCollection.release_id = id;

					this.$('.searching-module').show();
				} else if (reportsState === 'reports') {
					if (this.currentCinema !== id) {
						this.currentCinema = id;
						this.reportsCollection.cinema_id = id;
					} else {
						fetchParam = false;
					}

					this.reportsCollection.release_id = this.cinemasCollection.release_id;

					this.getReportsCollectionData(this.reportsCollection, {withFetch: fetchParam});

					this.$('.searching-module').show();
				}

				if (reportsState === 'reports') {
					this.$('.sections-tabs-inner').removeClass('hide');
				} else {
					this.$('.sections-tabs-inner').addClass('hide');
				}
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template());

			this.getReportsCollectionData(this.releasesCollection, {withFetch: this.releasesCollection.length ? false : true});

			return this;
		},
		getReportsCollectionData: function getReportsCollectionData(collection, fetchParam) {
			if (this.currentView) {
				this.currentView.remove();
				this.currentView = null;
			}

			Backbone.trigger('loader', 'show');

			if (fetchParam.withFetch) {
				collection.fetch({
					success: _.bind(function(data) {
						this.renderReports(this.reportsState === 'reports' ? collection.groupByWeeks() : data.models, this.reportsState);

						Backbone.trigger('loader', 'hide');
					}, this)
				});
			} else {
				this.renderReports(this.reportsState === 'reports' ? collection.groupByWeeks() : collection.models, this.reportsState);

				Backbone.trigger('loader', 'hide');
			}
		},
		renderReports: function renderReports(data, reportsState) {
			var view;

			view = new window.app.DistributorReportsView({
				data: data,
				reportsState: reportsState,
				scrollWidth: this.scrollWidth
			});

			this.currentView = view;

			this.$el.children('.reports').append(view.render().el);
		},
		showContacts: function showContacts(cinemaId) {
			var self = this,
				view;

			this.currentView.remove();
			this.currentView = null;
			this.$('.searching-module').hide();

			if (this.contactsView && this.contactsView.options.cinemaId === cinemaId) {
				this.currentView = this.contactsView;

				this.$el.children('.reports').append(this.contactsView.render().el);
			} else {
				$.get('/api/distributors/cinemas/' + cinemaId, function(data) {
					view = new window.app.DistributorContactsView({
						cinemaId: cinemaId,
						data: data.contacts
					});

					self.contactsView = view;
					self.currentView = view;

					self.$el.children('.reports').append(self.contactsView.render().el);
				});
			}
		},
		remove: function remove() {
			if (this.currentView) {
				this.currentView.remove();
				this.currentView = null;
			}

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.DistributorReportsView = Backbone.View.extend({
		className: 'reports-table',
		template: '#distributorReports',
		events: {
			'click .reports-table-body .reports-table-item:not(.disabled)': function(e) {
				var id = $(e.currentTarget).data('id');

				if (this.reportsState === 'releases') {
					Backbone.trigger('distributor:reports:switch:report:view', 'cinemas', id, true);
				} else if (this.reportsState === 'cinemas') {
					Backbone.trigger('distributor:reports:switch:report:view', 'reports', id, true);
				}
			}
		},
		initialize: function initialize(options) {
			this.options      = options || {};
			this.reportsState = options.reportsState || 'releases';
			this.data         = options.data;
			this.scrollWidth  = options.scrollWidth;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				headers,
				data,
				releaseTitle,
				cinema;

			if (this.reportsState === 'releases') {
				headers = ['Дата', 'Название', 'Копии', 'Отчиталось кинотеатров', 'Сеансов', 'Зрителей', 'Сборы (руб.)'];
				data = _.map(this.data, function(model) {
					return {
						id: model.get('id'),
						disabled: model.get('reports') === 'Нет отчетов' ? true : false,
						content: [
							moment(model.get('date')).format('DD MMMM YYYY'),
							model.get('title'),
							model.get('copies') ? model.get('copies') : 0,
							model.get('reports'),
							model.get('seances'),
							model.get('viewers'),
							model.get('box_office')
						]
					};
				});
			} else if (this.reportsState === 'cinemas') {
				headers = ['Кинотеатр', 'Залы', 'Сеансов', 'Зрителей', 'Сборы (руб.)', 'Последний отчет'];
				releaseTitle = _.first(this.data).get('releaseTitle');
				data = _.map(this.data, function(model) {
					return {
						id: model.get('cinema').id,
						content: [
							[model.get('cinema').title, model.get('cinema').city].join(', '),
							model.get('cinema').halls,
							model.get('seances'),
							model.get('viewers'),
							model.get('box_office'),
							moment.unix(model.get('date')).format('HH:mm, DD MMMM YYYY')
						]
					};
				});
			} else if (this.reportsState === 'reports') {
				headers = ['Дата', 'Сеансов', 'Зрителей', 'Сборы (руб.)', 'Дата отчета'];
				cinema = _.property(_.first(_.keys(this.data)))(this.data)[0].get('cinemaTitle');
				releaseTitle = _.property(_.first(_.keys(this.data)))(this.data)[0].get('releaseTitle');
				data = _.map(_.keys(this.data), function(key) {
					var summSeances   = 0,
						summViewers   = 0,
						summBoxOffice = 0;

					return {
						reports: _.map(_.property(key)(this.data), function(model) {
							summSeances   = summSeances + model.get('seances');
							summViewers   = summViewers + model.get('viewers');
							summBoxOffice = summBoxOffice + model.get('box_office');
							return [
								moment(model.get('date')).format('DD MMMM YYYY'),
								model.get('seances'),
								model.get('viewers'),
								model.get('box_office'),
								moment.unix(model.get('last_report')).format('HH:mm, DD MMMM YYYY')
							];
						}),
						seances: summSeances,
						viewers: summViewers,
						boxOffice: summBoxOffice
					};
				}, this);
			}

			this.$el.html(template({
				reportsState: this.reportsState,
				headers: headers,
				data: data,
				releaseTitle: releaseTitle ? releaseTitle : '',
				cinema: cinema ? cinema : ''
			}));

			if (this.scrollWidth) this.$('.reports-table-scroll').css('width', 'calc(100% + ' + this.scrollWidth + 'px)');

			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.DistributorContactsView = Backbone.View.extend({
		className: 'cinema_contacts',
		template: '#distributor_cinema_contacts',
		initialize: function initialize(options) {
			this.options = options || {};
			this.data = options.data;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({
				contacts: this.data
			}));

			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

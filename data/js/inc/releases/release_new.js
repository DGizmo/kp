$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.NewReleaseModel = Backbone.Model.extend({
		defaults: {
			title_ru: '',
			age: 0,
			ages: ['0', '6', '12', '16', '18'],
			startDate: null,
			durationClean: null,
			durationFull: null,
			formats: [],
			sound_format: []
		},
		validate: function validate(attr) {
			var errors = [];

			if (attr.title_ru.length < 1) {
				errors.push({
					attr: 'title_ru',
					message: 'Укажите название релиза'
				});
			}
			if (!(/^[\d]{1,2}$/).test(attr.age)) {
				errors.push({
					attr: 'age',
					message: 'Укажите ограничение по возрасту'
				});
			}
			if (!_.isFinite(attr.startDate)) {
				errors.push({
					attr: 'startDate',
					message: 'Укажите дату релиза'
				});
			}
			if (!(attr.formats.length)) {
				errors.push({
					attr: 'formats',
					message: 'Укажите формат релиза'
				});
			}
			if (!_.some([attr.durationClean, attr.durationFull], function(duration) { return (/^[\d]{1,3}$/).test(duration); })) {
				errors.push({
					attr: 'durationClean',
					message: 'Укажите длительность релиза'
				});
			} else if (attr.durationClean && !(/^[\d]{1,3}$/).test(attr.durationClean)) {
				errors.push({
					attr: 'durationClean',
					message: 'Длительность: неверный формат'
				});
			} else if (attr.durationFull && !(/^[\d]{1,3}$/).test(attr.durationFull)) {
				errors.push({
					attr: 'durationFull',
					message: 'Длительность с роликами: неверный формат'
				});
			}

			if (errors.length) return errors;
		}
	});

	window.app.AddNewReleasePopupView = Backbone.View.extend({
		className: 'release-edit',
		template: '#release_new',
		events: {
			'click button': 'saveRelease',
			'click .release-edit-form_group-dropdown-toggle': function(event) {
				$(event.currentTarget).closest('.release-edit-form_group-dropdown').addClass('open');
			},
			'click .release-edit-form_group-dropdown-list__item': function(event) {
				this.$('.release-edit-form_group-dropdown-toggle__chosen-value').html($(event.currentTarget).html());

				$(event.currentTarget).addClass('selected').siblings('.selected').removeClass('selected');
				this.$('.release-edit-form_group-dropdown-toggle').closest('.release-edit-form_group-dropdown').removeClass('open');

				this.model.set({
					age: $(event.currentTarget).data('age')
				});
			},
			'mouseenter .release-edit-form_group-dropdown-list': function(e) {
				$(e.currentTarget).children().removeClass('selected');
			}
		},
		initialize: function initialize() {
			var self = this;

			this.model = new window.app.NewReleaseModel();
			$('body').prepend(this.render().el);

			/* istanbul ignore next */
			this.$el.arcticmodal({
				afterClose: function() {
					self.remove();
				}
			});

			this.$el.find('.release-edit-form_group.date input').each(function() {
				$(this).datepicker({
					weekStart: 4,
					autoclose: true,
					calendarWeeks: true,
					todayBtn: 'linked',
					todayHighlight: true,
					language: 'ru',
					format: 'dd M yyyy'
				});
			});

			this.listenTo(this.model, 'invalid', function(model, errors) {
				this.$('.release-edit-form_group').removeClass('error');
				$.growl.error({message: _.pluck(errors, 'message').join('<br>')});

				_.each(errors, function(error) {
					this.$('[name*="' + error.attr + '"]').closest('.release-edit-form_group').addClass('error');
				}, this);
			});
		},
		saveRelease: function saveRelease() {
			var data;
			this.model.set('formats', _.map(this.$('input:checkbox:checked'), function(item) { return $(item).val(); }));
			this.model.unset('ages', { silent: true });

			_.each(this.$('input, textarea').not('[type="checkbox"]'), function(elem) {
				var $target = $(elem),
					prop    = $target.prop('name'),
					value;

				if ($target.parent().hasClass('date')) {
					value = $target.val() === '' ? '' : +($target.datepicker('getUTCDate') / 1000);
					this.model.set(prop, value);
				} else {
					this.model.set(prop, $target.val());
				}
			}, this);

			data = $.extend({}, this.model.attributes);

			data.formats = $.toJSON(this.model.get('formats'));
			data.sound_format = $.toJSON(this.model.get('sound_format'));

			if (this.model.isValid()) {
				this.$('input, textarea').removeClass('error');
				$.post('/api/release/new', data)
					.done(function() {
						$.arcticmodal('close');
					});
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});
});

//-------------------- Окно избранное --------------------//
$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	// Описываем представление для окна изабранного
	window.app.FavoriteModalView = Backbone.View.extend({
		className: 'favorite_modal',
		template: '#favorite_modal_item',
		events: {
			'click .close': /* istanbul ignore next */ function(e) {
				e.preventDefault();

				this.remove();
			},
			'submit form.fave': /* istanbul ignore next */ function(e) {
				e.preventDefault();

				var myList = this.model.get('my'), //было лайкнуто
					cinema_id = (_.map($('form.fave').serializeArray(), function(r) {
						return r.name;
					})).join(','), //стало лайкнуто
					newList = [],
					different,
					differentNames,
					nameNum,
					cinemaName,
					result;

				cinema_id.split(',').each(function(n) {
					newList.push(parseInt(n));
				});

				different = _.difference(myList, newList),
					differentNames = [];

				if (different.length) {
					nameNum = 1;

					different.each(function(num) {
						cinemaName = $('.popupWrap__content__fave__label').filter('[data-release-id=' + num + ']').attr('data-cinema-info');

						differentNames.push(nameNum + '. ' + cinemaName);
						nameNum++;
					});

					result = confirm('Информация о репертуаре и расписании по данному релизу будет удалена у кинотеатров:\n\n' + differentNames.join('\n') + '');
					if (!result) return;
				}
				this.model.addToFavourites(cinema_id);
				$('.modal_popup_fav.js-popup').arcticmodal('close');

				this.remove();
			},
			'reset form.fave': /* istanbul ignore next */ function() {
				this.remove();
			},
			'click .popupWrap__content__fave__group__check': /* istanbul ignore next */ function(e) {
				var $groupCheck = $(e.currentTarget),
					$cinemas = $groupCheck.closest('.popupWrap__content__group').find('.popupWrap__content__fave__check');

				$cinemas.each(function() {
					if ($(this).attr('readonly') !== 'readonly') {
						$(this).prop('checked', $groupCheck.is(':checked') ? true : false);
					}
				});

				if (!$groupCheck.is(':checked')) {
					this.$el.find('.popupWrap__content__fave__all__check').prop('checked', false);
				}
			},
			'click .popupWrap__content__fave__all__check': /* istanbul ignore next */ function(e) {
				var $all = $(e.currentTarget),
					$cinemas = this.$el.find('.popupWrap__content__fave__check, .popupWrap__content__fave__group__check');

				if ($all.is(':checked')) {
					$cinemas.each(function() {
						var cinema = $(this);

						if (cinema.attr('readonly') !== 'readonly') {
							cinema.prop('checked', true);
						}
					});
				} else {
					$cinemas.each(function() {
						var cinema = $(this);

						if (cinema.attr('readonly') !== 'readonly') {
							cinema.prop('checked', false);
						}
					});
				}
			},
			'click .popupWrap__content__fave__check': /* istanbul ignore next */ function(e) {
				if ($(e.currentTarget).attr('readonly') === 'readonly') return false;

				var $all          = this.$el.find('.popupWrap__content__fave__all__check'),
					$cinemas      = this.$el.find('.popupWrap__content__fave__check'),
					checkedLength = this.$el.find('.popupWrap__content__fave__check:checked').length,
					cinemasLength = $cinemas.length;

				if ($all.is(':checked') && checkedLength < cinemasLength) {
					$all.prop('checked', false);
				} else if (checkedLength === cinemasLength) {
					$all.prop('checked', true);
				}
			}
		},
		render: function render() {
			var template       = Backbone.TemplateCache.get(this.template),
				user           = reqres.request('get:user'),
				extModel       = $.extend({}, this.model.attributes),
				cinemaGroups   = reqres.request('get:user:cinemasGroups').toJSON(),
				userCinemasIds = _.pluck(user.getCinemas(), 'id'),
				cinemaGroup,
				userCinema,
				groupedCinemas;

			if (cinemaGroups.length) {
				extModel.cinemas = [];
				groupedCinemas   = _.union(_.flatten(_.map(cinemaGroups, function(oneGroup) { return oneGroup.cinemas; })));

				extModel.cinemas = _.map(cinemaGroups, function(group) {
					return {
						title: group.title,
						cinemas: _.map(group.cinemas, function(cinema) {
							userCinema = user.getCinema(cinema);
							return {
								id: cinema,
								title: userCinema.title.ru,
								city: userCinema.city.title.ru,
								rights_id: userCinema.rights_id
							};
						}, this)
					};
				});

				cinemaGroup         = {};
				cinemaGroup.title   = 'Остальные кинотеатры';
				cinemaGroup.cinemas = _.map(_.difference(userCinemasIds, groupedCinemas), function(ungroupedCinema) {
					userCinema = user.getCinema(ungroupedCinema);
					return {
						id: ungroupedCinema,
						title: userCinema.title.ru,
						city: userCinema.city.title.ru,
						rights_id: userCinema.rights_id
					};
				}, this);

				if (cinemaGroup.cinemas.length) {
					extModel.cinemas.push(cinemaGroup);
				}

				extModel.widthGroups = true;
			} else {
				extModel.cinemas = user.getCinemas();
				extModel.widthGroups = false;
			}

			this.$el.html(template(extModel));
			return this;
		}
	});

});

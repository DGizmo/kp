$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.Release = Backbone.Model.extend({
		urlRoot: '/api/v2/release',
		defaults: {
			id: null,
			age: '',
			alternate: null,
			audience: null,
			budget: {currency: '', value: ''},
			cast: [],
			color: '',
			copies: null,
			countries: [],
			cover: '',
			date: {russia: {}, world: {}},
			dcp_trailers: false,
			description: '',
			directors: [],
			distributors: [],
			duration: {clean: null, full: null, custom: null},
			files: [],
			formats: [],
			genres: [],
			honors: [],
			image_format: [],
			imdb_id: '',
			integrated_trailers: [],
			kdm: [],
			kinopoisk_id: '',
			license: null,
			materials: [],
			memorandum: null,
			motion_format: [],
			my: [],
			note: null,
			packages: [],
			passport: null,
			passport_valid: {till: '', from: ''},
			rating: {kinopoisk: null, imdb: null},
			release_trailers: [],
			releases: [],
			schedule: '',
			screenshots: [],
			show: 1,
			sound_format: [],
			title: {en: '', ru: ''},
			trailers: [],
			unf_passport: '',
			year: null,
			chosen_duration: 'full'
		},
		validate: function validate(attrs) {
			if (typeof attrs.duration.custom !== 'number' && attrs.duration.custom !== null && !_.isUndefined(attrs.duration.custom)) {
				return 'Неверный формат данных используемой длительности';
			}

			if (attrs.duration.custom < 0 || attrs.duration.custom > 500) {
				return 'Используемая длительность должна лежать в диапазоне от 1 до 500 минут';
			}

			if (typeof attrs.chosen_duration !== 'string' || !_.contains(['full', 'clean', 'custom'], attrs.chosen_duration)) {
				return 'Неверный формат данных выбранной длительности';
			}
		},
		saveDuration: function saveDuration() {
			if (this.isValid()) {
				$.ajax({
					url: this.url() + '/duration',
					type: 'PUT',
					dataType: 'json',
					data: JSON.stringify({
						tx:              reqres.request('get:user').get('tx'),
						duration:        this.get('duration').custom,
						chosen_duration: this.get('chosen_duration')
					})
				})
				.done(_.bind(function() {
					Backbone.trigger('release:duration:changed', {
						id: this.id,
						duration: this.get('duration'),
						chosen_duration: this.get('chosen_duration')
					});
				}, this))
				.fail(_.bind(function() {
					this.attributes = this.previousAttributes();
					$.growl.error({ title: 'Ошибка', message: 'Не удалось сохранить длительность' });
				}, this));

				_gaq.push(['_trackEvent', 'Релизы', 'Карточка релиза', 'Выбор используемой длительности']);
			} else {
				this.attributes = this.previousAttributes();
				$.growl.error({ title: 'Ошибка', message: this.validationError });
			}
		},
		removeDuration: function removeDuration() {
			var newDuration = this.get('duration'),
				chosenDuration = this.get('chosen_duration');

			newDuration.custom = 0;

			this.set({
				duration: newDuration,
				chosen_duration: chosenDuration === 'custom' ? 'full' : chosenDuration
			});

			this.saveDuration();
		},
		saveColor: function saveColor(color) {
			$.ajax({
				url: this.url() + '/color',
				type: 'PUT',
				dataType: 'json',
				data: JSON.stringify({
					tx:    reqres.request('get:user').get('tx'),
					color: color
				})
			})
			.done(_.bind(function() {
				this.set('color', color);

				Backbone.trigger('release:color:changed', {
					id: this.id,
					color: color
				});

			}, this));

			_gaq.push(['_trackEvent', 'Релизы', 'Карточка релиза', 'Изменение цвета релиза']);
		},
		addToFavourites: function addToFavourites(cinema_id) {
			var cinemas  = [],
				releaseStart = this.get('date').russia.start;

			if (cinema_id.length) {
				cinema_id.split(',').each(function(t) {
					cinemas.push(parseInt(t));
				});
			}

			this.set('my', cinemas);

			$.post(this.url() + '/favourite', {
				tx: reqres.request('get:user').get('tx'),
				web: 1,
				start_week: moment(releaseStart).weeks(),
				cinema_id: cinema_id
			})
			.done(_.bind(function() {
				Backbone.trigger('release:favourites', {
					week: moment(releaseStart).weeks(),
					year: moment(releaseStart).year(),
					id: this.id
				});
			}, this))
			.error(_.bind(function() {
				this.set('my', this.previous('my'));
			}, this));
		},
		url: function url() {
			return '/api/v2/release/' + this.get('id');
		},
		parse: function parse(release) {
			if (release.trailers && release.trailers.length) {
				_.each(release.trailers, function(trailer) {
					if (!trailer.download_status) trailer.download_status = {};
				});
				release.trailers.sort(function(a, b) { return b.release_date - a.release_date; });
			}

			if (release.integrated_trailers) {
				release.integrated_trailers = _.filter(release.integrated_trailers, function(trailer) { return trailer.title; });
			}

			if (release.files && release.files.length) {
				release.files.sort(function(a, b) {
					if (a.title < b.title) return -1;
					if (b.title < a.title) return 1;
					return 0;
				});
			}

			return release;
		},
		getFilesNumber: function getFilesNumber() { // считаем количество файлов релиза
			var filesNumber = 0;

			if (this.get('unf_passport')) filesNumber++;

			if (this.get('memorandum')) filesNumber++;

			if (this.get('license')) filesNumber++;

			if (this.get('files').length) filesNumber = filesNumber + this.get('files').length;

			return filesNumber;
		},
		getActiveKey: function getActiveKey() { // Узнаем есть ли у релиза активные ключи
			var releaseKdm = this.get('kdm'),
				activeKey = 0,
				curDate = Date.create(),
				i,
				validTill;

			if (releaseKdm.length) {
				for (i in releaseKdm) {
					validTill = Date.create(releaseKdm[i].valid_till_time * 1000);
					if (validTill >= curDate) activeKey++;
				}
			}

			return activeKey;
		},
		parseTrailers: function parseTrailers() {
			var version = this.get('trailers').groupBy(function(n) {
				if (n.version === null) n.version = ' ';
				return n.version;
			});

			return version;
		}
	});

	window.app.DatePoint = Backbone.Model.extend({
		defaults: {
			start:    19700101,
			month:    'янв',
			dayMonth: '1',
			dayWeek:  'Ср'
		}
	});

	window.app.Releases = Backbone.Collection.extend({
		model: window.app.Release, // каждый элемент коллекции будет являтся объектом модели Release
		comparator: function comparator(release) {
			if (this.releases_filter === 'distributor' || this.releases_filter === 'formats') {
				return parseInt(release.get('date').russia.start.replace(/-/g, ''));
			} else {
				return release.get('title').ru.toUpperCase();
			}
		},
		getNavigateUrl: function getNavigateUrl() { // функция возвращает урлу, по которой будем забирать релизы
			return this.distributor ? '/releases/distributor/' + this.distributor : this.format ? '/releases/format/' + this.format : this.my ? '/releases/my' : '/releases/' + this.year + '/' + this.month;
		},
		url: function url() {
			return '/api' + this.getNavigateUrl() + '?web=1&first=' + this.first;
		},
		parse: function parse(r) {
			this.lastChange = r.timestamp;
			return r.list;
		},
		initialize: function initialize() {
			var self        = this,
				actualYear  = _.getWeekStartDate({type: 'date'}).getFullYear(),
				actualMonth = _.getWeekStartDate({type: 'date'}).getMonth() + 1,
				release,
				newRelease;

			this.year            = actualYear;
			this.prevYear        = actualYear;
			this.nextYear        = actualYear;
			this.month           = actualMonth;
			this.prevMonth       = actualMonth;
			this.nextMonth       = actualMonth;
			this.distributor     = false;
			this.format          = false;
			this.first           = 1;
			this.my              = false;
			this.releases_filter = null;
			this.needUpdate      = true;
			this.currentScroll   = 40;

			this.listenTo(Backbone, 'socket:favourite planning:add', function(data) {
				release = this.get(data.favourite_id);

				if (release) {
					release.fetch();
				} else if (this.releases_filter === 'myrep') {
					newRelease = new window.app.Release({
						id: data.favourite_id
					});

					newRelease.fetch({
						success: function(res) {
							Backbone.trigger('releases:add-release', res);
						}
					});
				}
			});

			this.listenTo(Backbone, 'socket:releases:update', function(data) {
				_.each(data.release_id, function(relId) {
					release = self.get(relId);

					if (release) {
						release.fetch();
					} else {
						newRelease = new window.app.Release({id: relId});

						newRelease.fetch({
							success: function(res) {
								if (res.get('error') || res.error) return;

								if (self.releases_filter === 'date' || self.releases_filter === 'myrep') {
									self.moveReleaseDate(res);
								}
							}
						});
					}
				});
			});

			this.listenTo(Backbone, 'socket:releases:update:all', function() {
				if (this.releases_filter === 'date') {
					this.setYearAndMonth(actualYear, actualMonth);
				}
				this.update(this.releases_filter);
			});

			this.listenTo(Backbone, 'release:delete', function(id) {
				this.remove(this.get(id));
			});
		},
		setDistributor: function setDistributor(id) { // set-метод для дистрибьютора
			var self    = this,
				curDist = _.find(self.distList, function(dist) { return dist.id === parseInt(id); });

			$('nav.sidebar .list[data-tour="releases"]').attr('page', 'releases/distributor/' + id);

			if (_.isUndefined(curDist)) { // Если дистрибьютора с переданным id не существует
				this.distributor = false;
				this.my          = false;
				this.format      = false;
				this.update('date');
				return;
			}

			this.needUpdate      = this.distributor !== id ? true : false ;
			this.distributor     = id;
			this.distributorName = curDist.name.full || curDist.name.short || '';
			this.format          = false;
			this.my              = false;
			this.releases_filter = 'distributor';

			$('#filt_dist').val(id).trigger('refresh');
			$('#filt_dist-styler .jq-selectbox__select').addClass('chosen_item');
			$('#releases_wrap .filter').find('.release_alt').hide();

			this.update('distributor', id);
		},
		setFormat: function setFormat(name) {
			$('nav.sidebar .list[data-tour="releases"]').attr('page', 'releases/format/' + name);

			this.distributor = false;
			this.my = false;
			this.format = name;
			this.releases_filter = 'formats';

			$('#filt_format').val(name).trigger('refresh');
			$('#filt_format-styler .jq-selectbox__select').addClass('chosen_item');
			$('#releases_wrap .filter').find('.release_alt').show();

			this.update('formats', name);
		},
		setYearAndMonth: function setYearAndMonth(year, month) {
			$('nav.sidebar .list[data-tour="releases"]').attr('page', 'releases');

			this.distributor = false;
			this.my = false;
			this.format = false;
			this.year = year;
			this.month = month;
			this.prevMonth = month;
			this.nextMonth = month;
			this.prevYear = year;
			this.nextYear = year;
			this.releases_filter = 'date';

			$('#filt_dist-styler .jq-selectbox__select, #filt_format-styler .jq-selectbox__select').removeClass('chosen_item');
			$('#releases_wrap .filter').find('.release_alt').show();
		},
		showMy: function showMy() {
			$('nav.sidebar .list[data-tour="releases"]').attr('page', 'releases/my');

			this.my = true;
			this.distributor = false;
			this.format = false;

			$('#releases_wrap .myfav_btn').addClass('active blocker');
			$('#releases_wrap .filter').find('.release_alt').show();
			this.update('myrep', 'my');
		},
		update: function update(mode, param) { // обновление коллекции, выполняется после каждого set-метода при изменении свойства коллекции
			var self = this;
			this.releases_filter = mode;
			this.param = param;

			if (this.needUpdate) {
				this.remove(this.models);
				this.reset();
				Backbone.trigger('loader', 'show');

				this.fetch({
					success: function() {
						if (mode === 'distributor' || mode === 'formats') self.sort();
						$('#releases_wrap').after(window.app.App.releasesView.render().el);
					}
				});
			} else {
				$('#releases_wrap').after(window.app.App.releasesView.render().el);
			}
		},
		moveReleaseDate: function moveReleaseDate(release) {
			var date      = Date.create(release.get('date').russia.start),
				month     = date.format('{M}'),
				year      = date.format('{year}'),
				prevMonth = this.prevMonth,
				nextMonth = this.nextMonth,
				relDate,
				leftDate,
				rightDate;

			if (month < 10) month = '0' + month;

			if (prevMonth < 10) prevMonth = '0' + prevMonth;

			if (nextMonth < 10) nextMonth = '0' + nextMonth;

			relDate = parseInt(year.toString() + month.toString());
			leftDate = parseInt(this.prevYear.toString() + prevMonth.toString());
			rightDate = parseInt(this.nextYear.toString() + nextMonth.toString());

			if (relDate >= leftDate && relDate <= rightDate) {
				Backbone.trigger('releases:add-release', release);
			}
		}
	});

	window.app.ReleasesWrapView = Backbone.View.extend({
		id: 'releases_wrap',
		template: '#releases_wrap_tmpl',
		param: null,
		distList: [],
		events: {
			'click .btn-new-release': 'addNewRelease',
			'change #filt_dist': function(e) {
				var $selector     = $(e.currentTarget),
					selectorValue = $selector.val();

				this.$el.find('.myfav_btn').removeClass('active');

				if (selectorValue === 'date') {
					this.resetFilters();
					return;
				}

				this.releases.setDistributor(selectorValue);

				this.$el.find('#filt_format').val('date').trigger('refresh'); // Сбрасываем фильтр по форматам

				_gaq.push(['_trackEvent', 'Фильтр', 'Дистрибьюторы']);
				window.app.controller.navigate(this.releases.getNavigateUrl(), {
					trigger: false
				});
			},
			'change #filt_format': function(e) {
				var $selector     = $(e.currentTarget),
					selectorValue = $selector.val();

				this.$el.find('.myfav_btn').removeClass('active');

				if (selectorValue === 'date') {
					this.resetFilters();
					return;
				}

				this.releases.setFormat(selectorValue);

				this.$el.find('#filt_dist').val('date').trigger('refresh'); // Сбрасываем фильтр по дистрибьюторам

				_gaq.push(['_trackEvent', 'Фильтр', 'Форматы']);
				window.app.controller.navigate(this.releases.getNavigateUrl(), {
					trigger: false
				});
			},
			'click .myfav_btn': function(e) {
				this.$el.find('#filt_dist').val('date').trigger('refresh');
				this.$el.find('#filt_format').val('date').trigger('refresh');

				if (!$(e.currentTarget).hasClass('blocker')) {

					if (!$(e.currentTarget).hasClass('active')) {
						$(e.currentTarget).toggleClass('active');
						this.releases.showMy();

						_gaq.push(['_trackEvent', 'Фильтр', 'Репертуар']);
						window.app.controller.navigate(this.releases.getNavigateUrl(), {
							trigger: false
						});
					} else {
						$(e.currentTarget).toggleClass('active');
						this.resetFilters();
					}
				}
			},
			'change .alter_input': function(e) {
				var self = this,
					$input = $(e.currentTarget),
					$releasesDateList = $input.closest('.wrapper').find('.date-point').length ? $input.closest('.wrapper').find('.date-point') : $input.closest('.wrapper').find('.releases_body'),
					inputStatus = $input.prop('checked');

				$releasesDateList.each(function() {
					var $allReleases = $(this).find('.info_item'),
						allReleasesCount = $allReleases.length,
						$alterReleases = $allReleases.filter('[data-alter="1"]'),
						allAlterCount = $alterReleases.length;

					if (!inputStatus) {
						if (allReleasesCount === allAlterCount) {
							$(this).addClass('hide');
							$allReleases.addClass('hide');
						} else {
							$alterReleases.addClass('hide');
						}
					} else {
						if (allReleasesCount === allAlterCount) {
							$(this).removeClass('hide');
							$allReleases.removeClass('hide');
						} else {
							$alterReleases.removeClass('hide');
						}
					}
				});

				$.post('/api/user/alternate', {
					value: inputStatus ? 1 : 0
				}, function(r) {
					if (r.ok) {
						self.user.set({
							alternate: inputStatus ? 1 : 0
						});
					}
				}, 'json');
			}
		},
		initialize: function initialize(options) {
			var App = window.app.App;

			if (!App.releases) { // Если обертка инициализируется впервые, то коллекция релизов будет загружаться
				App.releases = new window.app.Releases();
				App.allRel = new window.app.Releases();
				App.allRel.stopListening();
			} else { // Если коллекция релизов уже была создана, то при повторном заходе в раздел грузить коллекцию не будем
				App.releases.needUpdate = false;
			}

			this.mode     = App.releases.mode || options.mode || 'date';
			this.param    = options.param;
			this.user     = reqres.request('get:user');
			this.releases = App.releases;

			App.releasesView = new window.app.ReleasesView({
				collection: this.releases
			});
		},
		render: function render() {
			var self = this,
				template = Backbone.TemplateCache.get(this.template),
				wrapperObj = {
					userType:     this.user.get('type'),
					userApproved: this.user.isApproved(),
					userCinema:   this.user.getCinemas()
				};

			if (!this.releases.distList) {   // Если список дистрибьюторов для селектора не был зугружен - загружаем его
				$.get('/api/distributors/count')
				.done(function(data) {
					self.releases.distList = data.list;
					wrapperObj.distributors = self.releases.distList;
					wrapperObj.isMaster = _.findWhere(self.user.get('cinema_rights'), {master: 1}) || false;
					self.$el.html(template(wrapperObj));
					setTimeout(function() {
						self.afterRender();
					}, 0);
				});
			} else {
				wrapperObj.distributors = this.releases.distList;
				wrapperObj.isMaster = _.findWhere(self.user.get('cinema_rights'), {master: 1}) || false;
				self.$el.html(template(wrapperObj));
				setTimeout(function() {
					self.afterRender();
				}, 0);
			}

			return this;
		},
		afterRender: function afterRender() {
			var self = this;

			self.$el.find('#filt_dist, #filt_format').styler();

			if (this.mode === 'date') { // Если загружаемся первый раз на релизах по дате
				this.releases.distributor = false;
				this.releases.my = false;
				this.releases.format = false;
				this.releases.update(this.mode, this.param);
			} else if (this.mode === 'distributor') { // Если загружаемся первый раз на релизах с фильтром по дистрибьютору по дате
				this.releases.setDistributor(this.param);
			} else if (this.mode === 'format') {
				this.releases.setFormat(this.param);
			} else if (this.mode === 'my') {
				this.releases.showMy();
			}

			if (this.user.get('alternate') === 1) {
				this.$el.find('.alter_input').prop('checked', true);
			} else {
				this.$el.find('.alter_input').prop('checked', false);
			}

			this.addReleaseSearching();
		},
		addReleaseSearching: function addReleaseSearching() {
			this.releaseSearching = new window.app.SearchingModuleView({
				animation: true,
				onSearchingFinish: function onSearchingFinish(value) {
					if (value.length < 3) return;
					$.get('/api/releases/filter/?q=' + value, _.bind(function(releases) {
						_.each(releases.list, function(release) {
							var searchingReleaseView = new window.app.SearchingReleaseView({release: release});
							this.subViews.push(searchingReleaseView);
							this.$('.results').append(searchingReleaseView.render().el);
						}, this);
					}, this));
				}
			});
			this.$('.filter').prepend(this.releaseSearching.render().el);
			this.releaseSearching.$el.addClass('searching-module--right');
		},
		resetFilters: function resetFilters() {
			var actualYear  = _.getWeekStartDate({type: 'date'}).getFullYear(),
				actualMonth = _.getWeekStartDate({type: 'date'}).getMonth() + 1;

			this.releases.setYearAndMonth(actualYear, actualMonth);
			window.app.App.allRel.setYearAndMonth(actualYear, actualMonth);

			this.releases.update('date', null);

			_gaq.push(['_trackEvent', 'Фильтр', 'Все релизы']);
			window.app.controller.navigate('/releases', {
				trigger: false
			});
		},
		addNewRelease: function addNewRelease(event) {
			event.preventDefault();
			this.addNewReleasePopupView = new window.app.AddNewReleasePopupView();
		},
		remove: function remove() {
			window.app.App.releasesView.remove();
			if (this.releaseSearching) this.releaseSearching.remove();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ReleaseView = Backbone.View.extend({
		className: 'info_item',
		template: '#release_tmpl',
		events: {
			'click .releases_cell:not(".noclick")': 'showRelease',
			'click .js-modal': function() {
				var cinemas      = this.user.getCinemas(),
					FavModalView = {},
					result;

				if (!this.user.isMultiplex() && this.user.isRepertoireReadOnly()) return;

				if (this.model.get('my').length === 0 && cinemas.length === 1) {
					_gaq.push(['_trackEvent', 'Релизы', 'В репертуар', this.model.get('id')]);

					this.model.addToFavourites((this.user.getCinemas())[0].id.toString());
				} else if (this.model.get('my').length !== 0 && cinemas.length === 1) {
					_gaq.push(['_trackEvent', 'Релизы', 'Удаление из репертуара', this.model.get('id')]);

					result = confirm('Вы уверены, что хотите удалить релиз "' + this.model.get('title').ru + '" из избранного?');
					if (!result) return;
					this.model.addToFavourites('');
				} else {
					this.createAddFavouritePopup();

					FavModalView = new window.app.FavoriteModalView({
						model: this.model
					});
					$('.modal_popup_fav').html(FavModalView.render().$el).show();
				}
			}
		},
		initialize: function initialize() {
			this.user = reqres.request('get:user');
			this.listenTo(this.model, 'change', this.releaseUpdate);
		},
		render: function render() {
			var self     = this,
				template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.WEEKDAY = {
				'понедельник': 'Пн',
				'вторник':     'Вт',
				'среда':       'Ср',
				'четверг':     'Чт',
				'пятница':     'Пт',
				'суббота':     'Сб',
				'воскресенье': 'Вс'
			};

			extModel.collection = this.model.collection;
			extModel.userType   = this.user.get('type');

			this.$el.html(template(extModel));

			setTimeout(function() {
				if (self.model.get('alternate') === 1 && window.app.App.releases.releases_filter !== 'distributor') {
					self.$el.attr('data-alter', 1);

					if (self.user.get('alternate') === 0) {
						self.$el.addClass('hide');
					}
				}
			}, 0);

			return this;
		},
		createAddFavouritePopup: function createAddFavouritePopup() {
			$('.modal_popup_fav.js-popup').arcticmodal({
				beforeOpen: function() {
					$('.js-popup').css({
						opacity: 0
					});
				},
				afterOpen: function() {
					var $popup = $('.js-popup');

					$popup.find('.arcticmodal-close').click(function() {
						$('.modal_popup_fav.js-popup').arcticmodal('close');
						$popup.find('.close').click();
					});

					$('.popupWrap__content__fave').each(function() {
						var labelWidth = 0,
							$form = $(this),
							formWidth;

						$popup.find('.popupWrap__content__fave__label').each(function() {
							var width = $(this).width();

							if (labelWidth < width) {
								labelWidth = width;
							}
						});

						formWidth = (labelWidth * 2) + 100;

						$form.css({
							width: formWidth
						}).addClass('popupWrap__content__fave_ready');
						$popup.css({
							width: formWidth,
							opacity: 1
						});
					});
				},
				beforeClose: function() {
					var $popup = $('.js-popup');

					$popup.find('.close').click();
					$popup.css({
						width: 'auto'
					});
				}
			});
		},
		releaseUpdate: function releaseUpdate() {
			var releases = window.app.App.releases,
				filter   = releases.releases_filter;

			if (filter === 'myrep' && this.model.get('my').length === 0) {
				releases.remove(this.model);
				this.removeView();
			} else if (this.model.get('date').russia.start.replace(/-/g, '') !== this.$el.closest('.date-point').attr('start') && (filter === 'date' || filter === 'myrep')) {
				releases.remove(this.model);
				this.removeView();

				releases.moveReleaseDate(this.model);
			} else {
				this.render();
			}
		},
		removeView: function removeView() {
			var $datePoint = this.$el.closest('.date-point');

			this.$el.empty();
			this.remove();

			if (!$datePoint.find('.info_item').length) {
				$datePoint.remove();
			}
		},
		showRelease: function showRelease(e) {
			e.preventDefault();
			Backbone.trigger('router:navigate', '/release/' + this.model.get('id'));
			_gaq.push(['_trackEvent', 'Релизы', 'Переход по релизу']);
		}
	});

	window.app.DatePointView = Backbone.View.extend({
		className: 'date-point',
		template: '#releases_date_point_tmpl',
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));

			this.$el.attr('start', this.model.get('start'));

			return this;
		}
	});

	window.app.ReleasesView = Backbone.View.extend({
		id: 'main_wr',
		className: 'release-wrapper scroll scrollable',
		template: '#releases_tmpl',
		initialize: function initialize() {
			this.user = reqres.request('get:user');
			this.subViews = [];

			this.listenTo(Backbone, 'releases:add-release', function(model) {
				this.addOneReleaseWithDatePoints(model);
			});
		},
		events: {
			scroll: function() {
				this.collection.currentScroll = this.$el.scrollTop();

				if (this.collection.releases_filter === 'date' && !(window.app.App.loader.is(':visible'))) {
					var scrollHeight = this.$el.get(0).scrollHeight - this.$el.get(0).clientHeight,
						scrollTop = this.$el.scrollTop() + 200;

					if (scrollTop >= scrollHeight) {
						this.loadNextMonth();
					}

					if (this.$el.scrollTop() === 0) {
						$('.load_prev_releases').css('opacity', '1');
					}
				}
			},
			'click .load_prev_releases': 'loadPrevMonth'
		},
		render: function render() {
			var template   = Backbone.TemplateCache.get(this.template),
				self       = this,
				datePoints = [],
				WEEKDAY    = {
					'понедельник': 'Пн',
					'вторник': 'Вт',
					'среда': 'Ср',
					'четверг': 'Чт',
					'пятница': 'Пт',
					'суббота': 'Сб',
					'воскресенье': 'Вс'
				},
				i,
				date,
				datePoint,
				datePointView;

			this.$el.html(template(this.collection));

			if (this.collection.releases_filter !== 'distributor' && this.collection.releases_filter !== 'formats') {
				this.collection.each(function(release) {
					datePoints.push(release.get('date').russia.start);
				});

				datePoints = _.uniq(datePoints).sort();

				for (i in datePoints) {
					date          = Date.create(datePoints[i]);
					datePoint     = new window.app.DatePoint();
					datePointView = new window.app.DatePointView({
						model: datePoint
					});

					this.subViews.push(datePointView);

					datePoint.set({
						start:    datePoints[i].replace(/-/g, ''),
						month:    date.format('{mon}'),
						dayMonth: date.format('{d}'),
						dayWeek:  WEEKDAY[date.format('{weekday}')]
					});

					this.$el.append(datePointView.render().el);

				}

				this.collection.sort();
				this.collection.each(function(release) {
					var releaseView = new window.app.ReleaseView({
						model: release
					});

					self.subViews.push(releaseView);

					self.$el.find('.date-point[start=' + release.get('date').russia.start.replace(/-/g, '') + ']').find('.releases_body').append(releaseView.render().el);
				});
			} else {
				this.collection.each(function(release) {
					var releaseView = new window.app.ReleaseView({
						model: release
					});

					self.subViews.push(releaseView);

					self.$el.find('.releases_body').append(releaseView.render().el);
				});
			}

			setTimeout(function() {
				var inputStatus    = self.$el.siblings('#releases_wrap').find('.alter_input').prop('checked'),
					scrollHeight,
					clientHeight,
					$allReleases,
					allReleasesCount,
					$alterReleases,
					allAlterCount,
					loadedMonthes,
					timerId;

				Backbone.trigger('loader', 'hide');
				commands.execute('set:releases-list:last-change', {lastChange: self.collection.lastChange});

				if (self.collection.releases_filter === 'date') {
					self.$el.find('.load_prev_releases').show();

					self.$el.scrollTop(40);

					scrollHeight = self.$el.get(0).scrollHeight;
					clientHeight = self.$el.get(0).clientHeight;

					if ($('#releases').is(':visible') && scrollHeight <= clientHeight) { // Функция подгрузки следующих месяцов до появления скрола
						loadedMonthes = 0;
						timerId = setInterval(function() {
							var scrollHeight = self.$el.get(0).scrollHeight,
								clientHeight = self.$el.get(0).clientHeight;

							loadedMonthes = loadedMonthes + 1;

							if (scrollHeight > clientHeight || loadedMonthes > 3) {
								clearInterval(timerId);
							}

							self.loadNextMonth();
						}, 1000);
					}
				}

				self.$el.find('.date-point').each(function() {
					$allReleases     = $(this).find('.info_item');
					allReleasesCount = $allReleases.length;
					$alterReleases   = $allReleases.filter('[data-alter="1"]');
					allAlterCount    = $alterReleases.length;

					if (!inputStatus) {
						if (allReleasesCount === allAlterCount) {
							$(this).addClass('hide');
							$allReleases.addClass('hide');
						} else {
							$alterReleases.addClass('hide');
						}
					} else {
						if (allReleasesCount === allAlterCount) {
							$(this).removeClass('hide');
							$allReleases.removeClass('hide');
						} else {
							$alterReleases.removeClass('hide');
						}
					}
				});
			}, 0);

			setTimeout(function() {
				self.$el.siblings('#releases_wrap').find('.myfav_btn').removeClass('blocker');

				if (self.collection.needUpdate) {
					var curWeekStart = _.getWeekStartDate({
							week: _.getWeekNumber()
						}).format('YYYYMMDD'),
						curDatePoint = self.$el.find('.date-point[start="' + curWeekStart + '"]');
					if (curDatePoint.length) self.$el.scrollTo(curDatePoint, {
						offset: -15
					});
				} else {
					window.app.App.releases.needUpdate = true;
					self.$el.scrollTop(self.collection.currentScroll);
				}

			}, 0);

			return this;
		},
		loadPrevMonth: function loadPrevMonth() {
			if (!(window.app.App.loader.is(':visible'))) {
				if (this.collection.prevMonth > 1) {
					this.collection.prevMonth--;

					window.app.App.allRel.month = this.collection.prevMonth;
					window.app.App.allRel.year = this.collection.prevYear;
				} else {
					this.collection.prevYear--;

					window.app.App.allRel.year = this.collection.prevYear;
					this.collection.prevMonth = 12;
					window.app.App.allRel.month = this.collection.prevMonth;
				}

				Backbone.trigger('loader', 'show');
				$('.load_prev_releases').css('opacity', '0');
				this.addReleases('prevMonth');
			}
		},
		loadNextMonth: function loadNextMonth() {
			if (!(window.app.App.loader.is(':visible'))) {
				if (this.collection.nextMonth < 12) {
					this.collection.nextMonth++;

					window.app.App.allRel.month = this.collection.nextMonth;
					window.app.App.allRel.year = this.collection.nextYear;
				} else {
					this.collection.nextYear++;

					window.app.App.allRel.year = this.collection.nextYear;
					this.collection.nextMonth = 1;
					window.app.App.allRel.month = this.collection.nextMonth;
				}

				Backbone.trigger('loader', 'show');
				this.addReleases('nextMonth');
			}
		},
		addReleases: function addReleases(mode) {
			var self = this,
				WEEKDAY = {
					'понедельник': 'Пн',
					'вторник': 'Вт',
					'среда': 'Ср',
					'четверг': 'Чт',
					'пятница': 'Пт',
					'суббота': 'Сб',
					'воскресенье': 'Вс'
				};

			window.app.App.allRel.fetch({
				success: function() {
					var datePoints = [],
						date,
						datePoint,
						datePointView,
						i;

					window.app.App.allRel.each(function(release) {
						datePoints.push(release.get('date').russia.start);
						self.collection.add(release);
						release.collection = self.collection;
					});

					datePoints = _.uniq(datePoints).sort();

					if (mode === 'prevMonth') {
						datePoints.reverse();
					}

					for (i in datePoints) {
						date = Date.create(datePoints[i]),
						datePoint = new window.app.DatePoint(),
						datePointView = new window.app.DatePointView({
							model: datePoint
						});

						self.subViews.push(datePointView);

						datePoint.set({
							start: datePoints[i].replace(/-/g, ''),
							month: date.format('{mon}'),
							dayMonth: date.format('{d}'),
							dayWeek: WEEKDAY[date.format('{weekday}')]
						});

						if (mode === 'nextMonth') {
							self.$el.append(datePointView.render().el);
						} else if (mode === 'prevMonth') {
							self.$el.find('.load_prev_releases').after(datePointView.render().el);
						}
					}

					window.app.App.allRel.sort();
					window.app.App.allRel.each(function(release) {
						var releaseView = new window.app.ReleaseView({
							model: release
						});

						self.subViews.push(releaseView);

						self.$el.find('.date-point[start=' + release.get('date').russia.start.replace(/-/g, '') + ']').find('.releases_body').append(releaseView.render().el);
					});

					if (mode === 'prevMonth') {
						self.$el.scrollTop(40);
					}

					setTimeout(function() {
						window.app.App.loader.hide();

						if (self.user.get('alternate') === 0) {
							_.each(datePoints, function(oneDate) {
								var trueDate = oneDate.replace(/-/g, ''),
									matchPoint = self.$el.find('.date-point[start="' + trueDate + '"]'),
									allPointReleaseCount = matchPoint.find('.info_item').length,
									alterPointReleaseCount = matchPoint.find('.info_item').filter('[data-alter="1"]').length;

								if (allPointReleaseCount === alterPointReleaseCount) {
									matchPoint.addClass('hide');
								}
							});
						}
					}, 0);
				}
			});
		},
		addOneReleaseWithDatePoints: function addOneReleaseWithDatePoints(release) {
			var self = this,
				releaseDate = release.get('date').russia.start,
				releaseView = new window.app.ReleaseView({
					model: release
				}),
				searchDatePoint = self.$el.find('.date-point[start=' + releaseDate.replace(/-/g, '') + ']'),
				WEEKDAY = {
					'понедельник': 'Пн',
					'вторник': 'Вт',
					'среда': 'Ср',
					'четверг': 'Чт',
					'пятница': 'Пт',
					'суббота': 'Сб',
					'воскресенье': 'Вс'
				},
				date,
				datePoint,
				datePointView;

			self.collection.add(release);

			if (searchDatePoint.length > 0) {
				searchDatePoint.find('.releases_body').append(releaseView.render().el);
			} else {
				date = Date.create(releaseDate);
				datePoint = new window.app.DatePoint();
				datePointView = new window.app.DatePointView({
					model: datePoint
				});

				datePoint.set({
					start: releaseDate.replace(/-/g, ''),
					month: date.format('{mon}'),
					dayMonth: date.format('{d}'),
					dayWeek: WEEKDAY[date.format('{weekday}')]
				});

				self.$el.find('.date-point').each(function(id, nextDatePoint) {
					if (parseInt(datePoint.get('start')) < parseInt($(nextDatePoint).attr('start'))) {
						$(nextDatePoint).before(datePointView.render().el);

						setTimeout(function() {
							datePointView.$el.find('.releases_body').append(releaseView.render().el);
						}, 0);

						return false;
					}
				});
			}
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.SearchingReleaseView = Backbone.View.extend({
		className: 'item',
		template: '#searching_release_tmpl',
		events: {
			click: function() {
				Backbone.trigger('router:navigate', 'release/' + this.options.release.id);
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.options.release));
			return this;
		}
	});
});

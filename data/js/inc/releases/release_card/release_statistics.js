$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReleaseStatistics = Backbone.Model.extend({
		defaults: {
			looking_files:     [],
			looking_trailers:  [],
			count_click:       1,
			download_trailers: [],
			license:           [],
			unf_passport:      [],
			memorandum:        []
		},
		url: function url() {
			return '/api/release/' + this.get('id') + '/statistics';
		},
		parse: function parse(r) {
			return r.logs;
		}
	});

	window.app.ReleaseStatisticsView = Backbone.View.extend({
		className: 'section',
		template: '#release_statistics_tmpl',
		initialize: function initialize(options) {
			this.options = options;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				data = $.extend({}, this.model.attributes);

			data.release = this.options.release;
			this.$el.html(template(data));

			return this;
		}
	});
});

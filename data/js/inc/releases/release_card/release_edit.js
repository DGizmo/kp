$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.EditReleaseView = Backbone.View.extend({
		className: 'release-edit',
		template: '#release_edit',
		events: {
			'click #confirm-edit-release': 'chackState',
			'click button': 'cancelEdit',
			'focus .release-edit__input-confirm': 'resetConfirmInput'
		},
		initialize: function initialize() {
			var self = this;

			this.state = 'confirm';
			this.count = 0;

			$('body').prepend(this.render().el);

			this.$el.arcticmodal({
				afterClose: function() {
					self.remove();
				}
			});

			this.listenTo(this.model, 'change', this.render);

			this.$el.find('.release-edit-form_group.date input').each(function() {
				$(this).datepicker({
					weekStart: 4,
					autoclose: true,
					calendarWeeks: true,
					todayBtn: 'linked',
					todayHighlight: true,
					language: 'ru',
					format: 'dd MM yyyy'
				});
			});
		},
		isValid: function isValid() {
			var valid = true;
			this.$el.find('[required]').each(function(index, el) {
				$(el).removeClass('error');
				if (!$(el).find('input').val()) {
					valid = false;
					$.growl.error({
						message: 'Укажите дату релиза'
					});
					$(el).addClass('error');
				}
			});
			return valid;
		},
		chackState: function chackState(event) {
			event.preventDefault();
			this.state === 'confirm' ? this.confirm() : this.save();
		},
		confirm: function confirm() {
			var self = this,
				$form = this.$el.find('.release-edit-form'),
				getTimestamp = function(name) {
					return +$form.find('[name="' + name + '"]').datepicker('getUTCDate') / 1000;
				},
				startDate = getTimestamp('startDate'),
				startWorldDate = getTimestamp('startWorldDate'),
				passportBeginDate = getTimestamp('passportBeginDate');

			this.data = this.$el.find('.release-edit-form').serialize() + '&date=' + (startDate || '') + '&world_date=' + (startWorldDate || '') + '&passport_begin=' + (passportBeginDate || '');

			if (this.isValid()) {
				this.$el.find('.required').removeClass('error');
				this.$el.find('.release-edit-form_group__input, .release-edit-form_group__textarea').prop('disabled', true);
				this.$el.find('.release-edit__input-confirm, .release-edit__loader').show();
				this.$el.find('#confirm-edit-release').val('Внести изменения');

				$.post('/api/release/save/')
					.done(function(data) {
						if (data.error) {
							$.growl.error({
								message: data.error
							});
						} else {
							self.state = 'save';
							self.$el.find('.release-edit__loader').hide('fast');
							self.$el.find('button').removeClass('hidden');
						}
					})
					.fail(function(data) {
						$.growl.error({
							message: data.statusText
						});
					});
			}
		},
		save: function save() {
			var self = this,
				$codeInput = this.$el.find('.release-edit__input-confirm'),
				$loader = this.$el.find('.release-edit__loader'),
				$error = this.$el.find('.release-edit__error'),
				code = $codeInput.val() || 'empty',
				data = 'code=' + code + '&release_id=' + this.model.get('id') + '&' + this.data;

			$error.hide();
			$loader.show('fast');

			$.post('/api/release/save/', data)
				.done(function(data) {
					$loader.hide();
					if (data.error) {
						if (++self.count < 3) {
							$codeInput.val('').addClass('error');
							$error.show('fast');
							return;
						}
						$.growl.error({
							message: 'Код не верный!'
						});
						$.arcticmodal('close');

					} else {
						self.model.fetch();
						$codeInput.removeClass('error');
						$error.hide('fast');

						$.growl.notice({
							message: ''
						});
						$.arcticmodal('close');
					}
				})

			.fail(function(data) {
				$.growl.error({
					message: data.statusText
				});
			});
		},
		cancelEdit: function cancelEdit(event) {
			event.preventDefault();
			$(event.currentTarget).addClass('hidden');

			this.state = 'confirm';
			this.count = 0;
			this.data = '';

			this.$el.find('.release-edit-form_group__input, .release-edit-form_group__textarea').prop('disabled', false);
			this.$el.find('.release-edit__error, .release-edit__input-confirm, .release-edit__loader').hide();
			this.$el.find('.release-edit__input-confirm').removeClass('error');
			this.$el.find('#confirm-edit-release').val('Получить СМС код');
		},
		resetConfirmInput: function resetConfirmInput(event) {
			$(event.currentTarget).removeClass('error');
			this.$el.find('.release-edit__error').hide();
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});
});

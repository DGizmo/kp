$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReleaseCardWrapView = Backbone.View.extend({
		id:'release',
		template: '#release_card_wr_tmpl',
		events: {
			'click .back': function(e) {
				e.preventDefault();
				window.history.back();
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template);
			this.afterRender();
			return this;
		},
		afterRender: function afterRender() {
			var release;

			if (window.app.App.releases) release = window.app.App.releases.get(this.options.releaseId);

			if (!release) release = new window.app.Release({id: this.options.releaseId});

			Backbone.trigger('loader', 'show');
			release.fetch({
				success: _.bind(function(model) {
					Backbone.trigger('loader', 'hide');

					if (model.get('error')) {
						$.growl.error({message: model.get('error') });
						Backbone.trigger('router:navigate', '/releases/');
						return;
					}

					this.subView = new window.app.ReleaseCardView({
						model: model,
						tab:   this.options.tab
					});

				}, this)
			});
		},
		remove: function remove() {
			if (this.subView) this.subView.remove();
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ReleaseCardView = Backbone.View.extend({
		el: '#release .release_item',
		template: '#release_card_tmpl',
		currentView: null,
		events: {
			'click .dropdown-toggle': function(event) {
				event.preventDefault();
				event.stopPropagation();
				$(event.currentTarget).parent().toggleClass('open');
			},
			'click .dropdown-menu__item': function(event) {
				event.preventDefault();
				this.model.saveColor($(event.currentTarget).data('color'));
			},
			'click .url_license': function() {
				$.post(this.model.url() + '/statistics/file', {type: 'license'});
			},
			'click .edit-release__edit': 'editRelease',
			'click .js-poster': function(e) {
				var $image = $(e.currentTarget).clone(),
					container = $('<div class="poster_wrap"><div class="closeWrap"><i class="arcticmodal-close"></i><i class="arcticmodal-close arcticmodal-close_hover"></i><i class="arcticmodal-close arcticmodal-close_active"></i></div></div>').append($image);

				$.arcticmodal({
					content: container,
					beforeOpen: function(data) {
						$(data.body)
							.addClass('center');
					}
				});
			},
			'click .js-modal': function(e) {
				e.preventDefault();

				if (this.user.isMultiplex()) {
					$('.modal_popup_fav.js-popup').arcticmodal({
						beforeOpen: function() {
							$('.js-popup').css({opacity:0});
						},
						afterOpen: function() {
							$('.js-popup').find('.arcticmodal-close').click(function() {
								$('.modal_popup_fav.js-popup').arcticmodal('close');
								$('.js-popup').find('.close').click();
							});

							$('.popupWrap__content__fave').each(function() {
								var labelWidth = 0,
									$form = $(this),
									formWidth;

								$('.js-popup').find('.popupWrap__content__fave__label').each(function() {
									var width = $(this).width();

									if (labelWidth < width) {
										labelWidth = width;
									}
								});

								formWidth = (labelWidth * 2) + 100;

								$form.css({width:formWidth}).addClass('popupWrap__content__fave_ready');
								$('.js-popup').css({width:formWidth, opacity: 1});
							});
						},
						beforeClose: function() {
							$('.js-popup').find('.close').click();
							$('.js-popup').css({width:'auto'});
						}
					});
				} else if (!this.user.isMultiplex() && this.user.isRepertoireReadOnly()) {
					return;
				}

				var FavModalView = new window.app.FavoriteModalView({model: this.model}),
					result;

				if (this.model.get('my').length === 0) {
					if (this.user.isMultiplex()) {
						$('.modal_popup_fav').html(FavModalView.render().$el).show();
					} else {
						this.model.addToFavourites((this.user.getCinemas())[0].id.toString());
						_gaq.push(['_trackEvent', 'Релизы', 'В репертуар', 'release_info']);
					}
				} else {
					if (this.user.isMultiplex()) {
						$('.modal_popup_fav').html(FavModalView.render().$el).show();
					} else {
						result = confirm('Вы уверены, что хотите удалить релиз "' + this.model.get('title').ru + '" из избранного?');
						if (!result) return;
						this.model.addToFavourites('');
					}
				}
			},
			'click .mistake': function() {
				commands.execute('show:feedback-popup');
			},
			'focus .textarea': function() {
				var $textarea = $('.textarea'),
					$parent   = $textarea.parent();

				$textarea.trigger('autosize.destroy').removeClass('no_transition').autosize();

				if ($parent.hasClass('active')) {
					$parent.removeClass('active');
				}

				if ($textarea.val()) {
					$('.add').html('Сохранить').show();
				} else {
					$('.add').html('Добавить').show();
				}
			},
			'focusout .textarea': 'focusOut',
			'click .remove': function() {
				var self = this;

				$.post(this.model.url() + '/note/delete', {tx: this.user.get('tx')}, function() {
					var changedRelease = _.find(self.user.get('notes'), function(r) {
						return r.id === self.model.get('id');
					});

					if (changedRelease) {
						changedRelease.text = '';
					}
				});

				this.model.set('note', false);

				$('.textarea').val('').text('').addClass('no_transition').css({height: 14}).closest('.note').removeClass('active');
			},
			'click .delete_custom': function() {
				var self = this,
					i = 1,
					cinemas;

				cinemas = _.chain(this.user.getCinemas())
					.filter(function(cinema) { return _.contains(self.model.get('my'), cinema.id); })
					.map(function(cinema) {
						return i++ + '. ' +
							(cinema.title.ru || cinema.title.en) + ', ' +
							(cinema.city.title.ru || cinema.city.title.en);
					}).value();

				if (confirm('Информация о репертуаре и расписании по данному релизу будет удалена у кинотеатров:\n\n' + cinemas.join('\n') + '')) {
					$.post('/api/release/remove/' + this.model.id)
						.done(function(res) {
							if (res.ok) {
								Backbone.trigger('release:delete', self.model.id);
								Backbone.trigger('router:navigate', '/releases');
							}
						});
				}
			},
			'click .tabs .item': function(e) {
				$(e.currentTarget).addClass('active').siblings().removeClass('active');
				this.currentTab = $(e.currentTarget).data('tab');
			},
			'click .tabs .item[data-tab="release_info"]': function() {
				this.releaseInfoView = new window.app.ReleaseInfoView({model: this.model});
				this.switchView(this.releaseInfoView);
			},
			'click .tabs .item[data-tab="release_files"]': function() {
				this.releaseFilesView = new window.app.ReleaseFilesView({model: this.model});
				this.switchView(this.releaseFilesView);
			},
			'click .tabs .item[data-tab="release_kdm"]': function() {
				this.releaseKdmView = new window.app.ReleaseKdmView(this.options);
				this.switchView(this.releaseKdmView);
			},
			'click .tabs .item[data-tab="release_trailers"]': function() {
				this.releaseTrailersView = new window.app.ReleaseTrailersView(this.options);
				this.switchView(this.releaseTrailersView);
			},
			'click .tabs .item[data-tab="release_report"]': function() {
				this.releaseReportView = new window.app.ReleaseReportsWrapperView({model: this.model});
				this.switchView(this.releaseReportView);
			},
			'click .tabs .item[data-tab="release_analysis"]': function() {
				this.releaseAnalysisView = new window.app.DashboardDistributorAnalysisCollectionView({
					releaseId:   this.model.get('id'),
					parentWrap:  this.$('.release_card_wrapper'),
					collections: window.app.App.Collections
				});

				this.currentView.remove();
				this.currentView = this.releaseAnalysisView;
			},
			'click .tabs .item[data-tab="release_statistics"]': function() {
				if (!this.releaseStatistics) {
					Backbone.trigger('loader', 'show');

					var releaseId = this.model.get('id'),
						self = this;
					this.releaseStatistics = new window.app.ReleaseStatistics({id: releaseId});

					this.releaseStatistics.fetch({
						success: function() {
							self.releaseStatisticsView = new window.app.ReleaseStatisticsView({
								model: self.releaseStatistics,
								release: self.model.attributes
							});
							self.switchView(self.releaseStatisticsView);
							Backbone.trigger('loader', 'hide');
						}
					});
				} else {
					this.switchView(this.releaseStatisticsView);
				}
			},
			'click .tabs .item[data-tab="release_painting"]': function() {
				this.releasePaintingView = new window.app.ReleasePaintingView({model: this.model});
				this.switchView(this.releasePaintingView);
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.user = reqres.request('get:user');

			this.currentTab = this.options.tab;

			var title = this.model.get('title');
			document.title = (title.ru || title.en || 'Релиз') + ' - Киноплан';

			this.render();

			this.listenTo(Backbone, 'body:click', function() {
				this.$('.dropdown').removeClass('open');
			});

			this.listenTo(this.model, 'sync change:note change:color', this.render);

			this.listenTo(Backbone, 'release:favourites', this.render);

			this.listenTo(Backbone, 'socket:favourite planning:add', function(data) {
				if (this.model.get('id') === data.favourite_id) this.model.fetch();
			});

			this.listenTo(Backbone, 'socket:note', function(data) {
				if (this.model.get('id') === data.id) this.model.set('note', data.value);
			});

			if (!(this.model.collection)) {
				this.listenTo(Backbone, 'socket:releases:update', function(data) {
					if (_.contains(data.release_id, this.model.get('id'))) this.model.fetch();
				});
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				user     = this.user.toJSON(),
				extModel = $.extend({}, this.model.attributes),
				note     = _.findWhere(user.notes, {id: extModel.id}),
				isReportsDistributor = _.some(extModel.distributors, function(dist) {
					return _.contains(['LUX', 'VLG'], dist.name.short);
				}),
				hasRightsForLikedCinemas = _.chain(user.cinema_rights)
					.filter(function(cinema) {
						return _.contains(extModel.my, parseInt(cinema.cinema_id));
					}, this)
					.some(function(cinema) {
						return cinema.master ||
							(cinema.multi_account && _.contains(['write', 'rw'], cinema.level));
					}).value();

			extModel.defautPalette = reqres.request('get:user').defaultPalette;
			extModel.filesNumber   = this.model.getFilesNumber();
			extModel.activeKey     = this.model.getActiveKey();
			extModel.editable      = this.checkEditable();
			extModel.currentUser   = user;
			extModel.note          = note ? note.text : '';
			extModel.distributorId = _.find(extModel.distributors, function(dist) {
				return dist.id === user.distributor_id;
			});
			extModel.reportsAccess =
				(user.type === 'cinema' &&
				isReportsDistributor &&
				hasRightsForLikedCinemas) ||
				user.admin;

			this.$el.html(template(extModel));

			this.afterRender();

			return this;
		},
		afterRender: function afterRender() {
			var self = this,
				$curTab = this.$('.tabs .item[data-tab="' + this.currentTab + '"]'),
				normalAutoSize;

			if ($curTab.length) {
				$curTab.trigger('click');
			} else {
				this.$('.tabs .item[data-tab="release_info"]').trigger('click');
			}

			this.$('.textarea').addClass('no_transition').autosize({
				callback: function() {
					if (normalAutoSize !== 0) {
						var normalTextHeight = self.$el.find('.textarea').height() - 14;
						$(this).css({height: normalTextHeight});
						normalAutoSize = 0;
					}
				}
			});
		},
		switchView: function switchView(view) {
			if (this.currentView !== null && this.currentView.cid !== view.cid) {
				this.currentView.remove();
			}

			this.currentView = view;

			this.$el.find('.release_card_wrapper').html(view.render().el);
		},
		checkEditable: function checkEditable() {
			return this.user.isApproved() && this.user.get('type') === 'distributor' &&
				_.where(this.model.get('distributors'), {id: this.user.get('distributor_id')}).length;
		},
		editRelease: function editRelease(event) {
			event.preventDefault();
			this.userSettingsView = new window.app.EditReleaseView({model: this.model});
		},
		focusOut: function focusOut() {
			var self           = this,
				val            = this.$('.textarea').val(),
				changedRelease = _.find(self.user.get('notes'), function(r) {
					return r.id === self.model.get('id');
				}),
				url = this.model.url() + (val.length ? '/note/save' : '/note/delete');

			$.post(url, {text: val, tx: this.user.get('tx')}, function() {
				if (changedRelease) {
					changedRelease.text = val;
				} else {
					self.user.get('notes').push({
						id:   self.model.get('id'),
						text: val
					});
				}

				self.model.set('note', val);
			});
		},
		removeView: function removeView() {
			Backbone.trigger('router:navigate', '/releases');
			$('.release-wrapper').scrollTop(this.model.get('currentScroll'));
			// тут удалять само представление не обязательно, оно переррисуется при следующем обращении, и в этом конкретном случае оно нам не мешает
		}
	});

});

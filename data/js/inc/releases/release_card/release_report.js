$(function() {
	'use strict';
	window.app = window.app || {};

	window.app.ReleaseReportModel = Backbone.Model.extend({
		defaults: {
			id:         null,
			cinema_id:  null,
			date:       null,
			seances:    0,
			viewers:    0,
			box_office: 0
		},
		urlRoot: function urlRoot() {
			return '/api/v2/release/' + this.collection.releaseId + '/reports/';
		},
		validate: function validate(attr) {
			var errors = [],
				numberCheck = function(value) {
					return value >= 0 && (typeof value === 'number' ? true : value.replace(/[0-9-]/g, '') === '');
				};

			if (!numberCheck(attr.seances)) {
				errors.push({
					title: 'Сеансы',
					message: 'Неверный формат',
					field: 'seances'
				});
			}

			if (!numberCheck(attr.viewers)) {
				errors.push({
					title: 'Зрители',
					message: 'Неверный формат',
					field: 'viewers'
				});
			}

			if (!numberCheck(attr.box_office)) {
				errors.push({
					title: 'Сборы',
					message: 'Неверный формат',
					field: 'box_office'
				});
			}

			if (errors.length) return errors;
		}
	});

	window.app.ReleaseReportCollection = Backbone.Collection.extend({
		model: window.app.ReleaseReportModel,
		initialize: function initialize(models, options) {
			this.options = options || {};
			this.releaseId = this.options.releaseId;
			this.modelsGroupedByCinema = {};
		},
		parse: function parse(r) {
			r = r.sort(function(a, b) {
				return +moment(a.date) - +moment(b.date);
			});

			return r;
		},
		url: function url() {
			return '/api/v2/release/' + this.releaseId + '/reports';
		},
		groupByCinema: function groupByCinema() {
			this.modelsGroupedByCinema = this.groupBy(function(model) { return model.get('cinema_id'); });
		},
		getCinemaReports: function getCinemaReports(cinema) {
			return this.modelsGroupedByCinema[cinema] || [];
		}
	});

	window.app.ReleaseReportsWrapperView = Backbone.View.extend({
		className: 'section release-reports',
		initialize: function initialize(options) {
			this.options    = options || {};
			this.subViews   = [];
			this.cinemas    = _.filter(reqres.request('get:user').getCinemas(), function(cinema) {
				return _.contains(this.model.get('my'), parseInt(cinema.id));
			}, this);

			this.collection = new window.app.ReleaseReportCollection([], { releaseId: this.model.get('id') });

			Backbone.trigger('loader', 'show');
			this.collection.fetch({
				success: _.bind(function() {
					Backbone.trigger('loader', 'hide');
					this.collection.groupByCinema();
					this.renderView();
				}, this)
			});
		},
		renderView: function renderView() {
			this.subViews = _.map(this.cinemas, function(cinema) {
				return new window.app.ReleaseReportCinemaView({
					title:       cinema.title.ru,
					city:        cinema.city.title.ru,
					cinemaId:    cinema.id,
					releaseId:   this.model.get('id'),
					releaseDate: this.model.get('date').russia.start,
					models:      this.collection.getCinemaReports(cinema.id),
					parent:      this
				});
			}, this);

			_.each(this.subViews, function(view) {
				this.$el.append(view.render().el);
			}, this);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ReleaseReportCinemaView = Backbone.View.extend({
		className: 'release-reports-cinema',
		template: _.template('<span class="release-reports-cinema__title">{{- title }}, {{- city }}</span><span class="release-reports-cinema__add">+ Добавить день</span>'),
		events: {
			'click .release-reports-cinema__add': function() {
				this.addDay();
			}
		},
		initialize: function initialize(options) {
			this.options  = options || {};
			this.subViews = [];
			this.models   = this.options.models;
		},
		render: function render() {
			this.$el.html(this.template({
				title: this.options.title,
				city:  this.options.city
			}));

			_.each(this.models, function(model) {
				this.addDay(model);
			}, this);

			return this;
		},
		addDay: function addDay(model) {
			var newDayModel = model ? model : this.createDayModel(),
				modelWeek   = moment(newDayModel.get('date')).week(),
				neededWeek  = _.find(this.subViews, function(week) { return moment(week.options.date).week() === modelWeek; });

			if (this.models.length) _.last(this.models).trigger('save');

			if (!model) this.models.push(newDayModel);

			if (neededWeek) {
				neededWeek.addDay(newDayModel);
			} else {
				this.createWeekView(newDayModel);
			}
		},
		createWeekView: function createWeekView(model) {
			var week = new window.app.ReleaseReportWeekView({
					models:   [model],
					date:     model.get('date'),
					cinemaId: this.options.cinemaId,
					containSingleDay: this.models.length === 1 ? true : false
				});

			this.$el.children('.release-reports-cinema__add').before(week.render().el);

			this.subViews.push(week);
		},
		createDayModel: function createDayModel() {
			var newModel = new window.app.ReleaseReportModel({
					cinema_id: this.options.cinemaId,
					date:      this.models.length ? moment(_.last(this.models).get('date')).add(1, 'day').format('YYYY-MM-DD') : this.options.releaseDate
				});

			this.options.parent.collection.add(newModel);

			return newModel;
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ReleaseReportWeekView = Backbone.View.extend({
		className: 'release-reports-week',
		template: '#release-report-week',
		initialize: function initialize(options) {
			this.options  = options || {};
			this.subViews = [];
			this.date     = this.options.date;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({
				dateRange: moment(this.date).startOf('week').format('DD MMMM YYYY') + ' - ' + moment(this.date).startOf('week').add(6, 'days').format('DD MMMM YYYY')
			}));

			_.each(this.options.models, function(model) {
				this.addDay(model);
			}, this);

			return this;
		},
		addDay: function renderDays(model) {
			var day = new window.app.ReleaseReportView({
				model: model,
				parent: this
			});

			this.$el.append(day.render().el);

			this.subViews.push(day);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ReleaseReportView = Backbone.View.extend({
		className: 'release-reports-day',
		template: '#release-report',
		events: {
			'click .release-reports-day__save': 'save',
			'click .release-reports-day__change': function() {
				this.saved = false;
				this.render();
			},
			'focusin .release-reports-day__seances, .release-reports-day__viewers, .release-reports-day__box-office': function(e) {
				$(e.currentTarget).select();
			},
			'change .release-reports-day__seances, .release-reports-day__viewers, .release-reports-day__box-office': function(e) {
				var $input = $(e.currentTarget);

				$input.removeClass('error');
				this.model.set($input.data('field'), $input.val(), { validate: true });
			},
			'keypress .release-reports-day__seances, .release-reports-day__viewers, .release-reports-day__box-office': function(e) {
				if (e.keyCode === 13) this.save();
			}
		},
		initialize: function initialize(options) {
			this.options   = options || {};
			this.startDay  = this.model.get('date');
			this.parent    = this.options.parent;
			this.saved     = this.model.get('id') ? true : false;
			this.firstDate = moment(this.model.get('date')).isSame(moment(this.parent.date)) && this.parent.options.containSingleDay ? true : false;

			this.listenTo(this.model, 'save', function() {
				if (!this.saved && this.model.isValid()) {
					this.model.save();
					this.firstDate = false;
					this.saved = true;
					this.render();
				}
			});

			this.listenTo(this.model, 'invalid', function(model) {
				_.each(model.validationError, function(error) {
					$.growl.error({
						title: error.title,
						message: error.message
					});
					this.$el.find('[data-field="' + error.field + '"]').addClass('error');
				}, this);
			});
		},
		render: function render() {
			var template       = Backbone.TemplateCache.get(this.template),
				extModel       = $.extend({}, this.model.attributes),
				self           = this,
				startDate      = moment(this.parent.options.date),
				dateFirstValue = moment(this.model.get('date')).format('DD MMMM YYYY'),
				$date;

			extModel.date = moment(this.model.get('date')).format('DD MMMM YYYY');
			extModel.dateParam = this.firstDate && !this.saved ? 'readonly' : 'disabled';
			extModel.saved = this.saved;

			this.$el.html(template(extModel));

			if (this.firstDate) {
				$date = this.$('.release-reports-day__date');

				$date.val(moment(dateFirstValue, 'DD MMMM YYYY').format('YYYY-MM-DD'));
				$date.datepicker({
					autoclose: true,
					language: 'ru',
					startDate: startDate.clone().add(-1, 'week').format('YYYY-MM-DD'),
					endDate: startDate.clone().add(1, 'week').format('YYYY-MM-DD'),
					minViewMode: 'days',
					format: 'yyyy-mm-dd',
					weekStart: 4,
					calendarWeeks: true
				})
				.on('hide', function() {
					if ($date.val().length && moment($date.val()).week() !== startDate.week()) {
						self.parent.date = $date.val();
						self.model.set('date', $date.val());
						self.parent.render();
					} else if ($date.val().length) {
						self.model.set('date', $date.val());
						self.render();
					} else {
						self.render();
					}
				});
				$date.val(dateFirstValue);
			}

			return this;
		},
		save: function save() {
			this.model.save({
				seances:    this.$el.children('.release-reports-day__seances').val(),
				viewers:    this.$el.children('.release-reports-day__viewers').val(),
				box_office: this.$el.children('.release-reports-day__box-office').val()
			}, {
				success: _.bind(function() {
					this.saved = true;
					this.render();
				}, this)
			});
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReleaseKdmView = Backbone.View.extend({
		className: 'section',
		template: '#release_kdm_tmpl',
		initialize: function initialize(options) {
			this.options = options || {};
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.currentUser = reqres.request('get:user').toJSON();
			this.$el.html(template(extModel));
			return this;
		}
	});
});

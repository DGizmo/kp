$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReleaseInfoView = Backbone.View.extend({
		className: 'release-card-sections-wrapper',
		template: '#release_info_tmpl',
		events: {
			'click .release-card__distributor':     'navigateToDistributor',
			'click .release-card-trailer-preview':  'showTrailer',
			'click .release-card__choose-duration': 'chooseDuration',
			'click .release-card__edit-duration':   'editDuration',
			'click .release-card__set-duration':    'setDuration',
			'click .release-card__remove-duration': 'removeDuration'
		},
		navigateToDistributor: function navigateToDistributor(event) {
			event.preventDefault();
			Backbone.trigger('router:navigate', $(event.currentTarget).attr('href'));
		},
		showTrailer: function showTrailer(event) {
			var $link      = $(event.currentTarget),
				trailerId = $link.data('id') || 0,
				type = $link.data('type'),
				trailer;

			if (trailerId) {
				$.post(this.model.url() + '/statistics/file', {id: trailerId, type: 'trailer' });
			}

			if (!$('html').hasClass('firefox') && !$('html').hasClass('opera') || type === 'youtube') {
				event.preventDefault();

				trailer = type === 'youtube' ? _.findWhere(this.model.get('release_trailers'), {id: trailerId})
					: this.options.trailersVersions[$link.index()];

				this.subView = new window.app.ReleaseTrailerModalView({
					trailer: trailer,
					type:    type,
					attributes: {
						style: 'width: ' + (type === 'youtube' ? 500 : 700) + 'px;margin: 0 auto;'
					}
				});
			}
		},
		chooseDuration: function chooseDuration(event) {
			if (!this.options.cinemasRights) return;
			var durationType  = $(event.currentTarget).closest('td').data('duration-type');

			this.model.set({chosen_duration: durationType});
			this.model.saveDuration();
		},
		editDuration: function editDuration(event) {
			if (!this.options.cinemasRights) return;
			event.stopPropagation();
			var durationType  = $(event.currentTarget).closest('td').data('duration-type'),
				durationValue = _.property(durationType)(this.model.get('duration'));

			this.createEditeValuePopover(event, durationValue);
		},
		setDuration: function setDuration(event) {
			if (!this.options.cinemasRights) return;
			event.preventDefault();
			event.stopPropagation();
			this.createEditeValuePopover(event, 0);
		},
		removeDuration: function() {
			if (!this.options.cinemasRights) return;
			this.model.removeDuration();
		},
		createEditeValuePopover: function createEditeValuePopup(event, durationValue) {
			this.editDurationValue = new window.app.EditValueView({
				value: durationValue,
				onSave: _.bind(function onSave(value) {
					var newDuration = this.model.toJSON().duration;

					newDuration.custom = value;
					this.model.set({
						duration:        newDuration,
						chosen_duration: newDuration ? 'custom' : 'full'
					}).trigger('change');
					this.model.saveDuration();
				}, this)
			});

			$(event.target).parent().prepend(this.editDurationValue.render().el).find('input').focus();
		},

		initialize: function initialize(options) {
			var user = reqres.request('get:user');

			this.options = _.defaults(options || {}, {
				isDistributorsAllowed: user.get('type') !== 'guest'
			});

			this.options.cinemasRights = _.chain(user.getCinemas())
				.map(function(cinema) { return cinema.rights_id; })
				.without(1)
				.value()
				.length ? true : false;

			this.listenTo(this.model, 'change', this.render);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = this.model.toJSON();

			extModel.isDistributorsAllowed = this.options.isDistributorsAllowed;

			extModel.trailersVersions = this.options.trailersVersions = _.chain(this.model.get('trailers'))
				.filter(function(trailer) { return trailer.show_preview && trailer.preview; })
				.groupBy(function(n) { return n.version || ' '; })
				.pluck(0)
				.value();

			extModel.integratedTrailersDuration = _.reduce(this.model.get('integrated_trailers'), function(duration, trailer) {
				return duration + trailer.duration;
			}, 0);

			extModel.countries = _.map(this.model.get('countries'), function(country) { return country.name; }).join(', ');

			extModel.cinemasRights = this.options.cinemasRights;

			extModel.userType = reqres.request('get:user').get('type');

			this.$el.html(template(extModel));
			return this;
		}
	});

	window.app.ReleaseTrailerModalView = Backbone.View.extend({
		template: _.template([
			'<video poster="{{- thumbHD || thumb || "/img/trailer.jpg" }}" width="700" height="100%" preload="none" controls>',
					'<source src="{{- preview }}" type="video/mp4">',
			'</video>'
		].join('')),
		youtubeTemplate: _.template('{{= code }}'),
		initialize: function initialize(options) {
			this.options = options || {};

			$('#js-popup').html(this.render().el);

			/* istanbul ignore next */
			$('#js-popup').children().arcticmodal({
				afterClose: _.bind(function() { this.remove(); }, this)
			});
		},
		render: function render() {
			var template = this.options.type === 'youtube' ? this.youtubeTemplate : this.template;
			this.$el.html(template(this.options.trailer));
			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

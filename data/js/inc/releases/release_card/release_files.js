$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReleaseFilesView = Backbone.View.extend({
		className: 'section',
		template:  '#release_files_tmpl',
		events: {
			'click .files-screenshots__link': 'downloadScreenshot',
			'click .files-links__link':       'downloadFile'
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		},
		downloadScreenshot: function downloadScreenshot(e) {
			e.preventDefault();
			var link = document.createElement('a');

			link.href = $(e.currentTarget).attr('href') || $(e.currentTarget).parent().attr('href');
			link.download = '';
			link.target = '_blank';

			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		},
		downloadFile: function downloadFile(e) {
			var fileId = $(e.currentTarget).data('file-id'),
				url    = this.model.url() + '/statistics/file',
				data   = fileId === 'unf_passport' || fileId === 'memorandum' || fileId === 'license' ?
					{type: fileId} : {id: fileId, type: 'file'};

			$.post(url, data);
		}
	});
});

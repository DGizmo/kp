$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReleaseTrailersView = Backbone.View.extend({
		className: 'section',
		template: '#release_trailers_tmpl',
		events: {
			'click .js_trailer': function(e) {
				var $link      = $(e.currentTarget),
					$trailerId = $link.data('id') || 0,
					$parent    = $link.parent(),
					$video     = $parent.find('.video_popup__wrap');

				if ($trailerId) {
					$.post(this.model.url() + '/statistics/file', {
						id: $trailerId,
						type: 'trailer'
					});
				}

				if (!$('html').hasClass('firefox') && !$('html').hasClass('opera')) {
					e.preventDefault();
					$video.arcticmodal();
				} else if ($link.hasClass('youtube')) {
					e.preventDefault();
					$video.arcticmodal();
				}
			},
			'submit .download-trailer': function(e) {
				e.preventDefault();

				var $form     = $(e.currentTarget),
					node_id   = (_.map($form.serializeArray(), function(r) {
						return r.name;
					})).join(','),
					nodesList = [],
					trailerId = $(e.currentTarget).data('trailer-id');

				if (!node_id) {
					$form.hide();
					return false;
				}

				node_id.split(',').each(function(n) {
					nodesList.push(parseInt(n));
				});

				$form.find(':checkbox:first').prop('checked', false);
				$form.find(':checkbox:not(:first):checked').prop('disabled', true);

				_gaq.push(['_trackEvent', 'Загрузка трейлеров', 'Релиз', trailerId]);

				$.post('/api/trailer/do/start', {
						trailer_id: trailerId,
						node_id: nodesList
					})
					.done(function() {
						Backbone.trigger('trailer:download:start');
						$.growl.ok({
							message: 'Трейлер отправлен в очередь на загрузку'
						});
					});
				$form.hide();
			},
			'click .download-trailer__check-all': function(e) {
				var $checkBooxes = $(e.currentTarget).closest('.download-trailer').find(':checkbox:enabled');
				$checkBooxes.not(':eq(0)').prop('checked', $checkBooxes.first().prop('checked'));
			},
			'click .download2dcp': function(e) {
				e.preventDefault();
				var $form = $(e.currentTarget).closest('.info-wrapper').find('.download-trailer'),
					cinemas = this.user.getCinemas(),
					trailerId;

				if ($form.is(':visible')) {
					$form.submit();
					return;
				}

				if (cinemas.length === 1) {
					trailerId = $(e.currentTarget).data('trailer-id');

					_gaq.push(['_trackEvent', 'Загрузка трейлеров', 'Релиз', trailerId]);

					$.post('/api/trailer/do/start', {
							trailer_id: trailerId,
							node_id:    cinemas[0].dd24_pro
						})
						.done(function() {
							Backbone.trigger('trailer:download:start');
							$.growl.ok({message: 'Трейлер отправлен в очередь на загрузку'});
							$(e.currentTarget).parent().html('<span class="download-trailer__status">Загружется на сервер ' +
								cinemas[0].title.ru + ', ' +
								cinemas[0].city.title.ru + '</span>');
						});
				} else {
					$form.show();
				}
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.user = reqres.request('get:user');
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes),
				self     = this;

			extModel.version       = this.model.parseTrailers();
			extModel.currentUser   = this.user.toJSON();
			extModel.currentCinema = extModel.currentUser.current.cinema_id;

			this.$el.html(template(extModel));

			setTimeout(function() {
				self.$el.find('.trailer_item .item-wrapper').each(function(id, trailer) {
					if ($(trailer).find('.download2dcp').length === 0) {

						$(trailer).find('.dcp').css({
							top: '85px'
						});

						$(trailer).find('.md').css({
							top: '103px'
						});
					}
				});
			});
			return this;
		}
	});
});

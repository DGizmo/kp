$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReleasePaintingCollection = Backbone.Collection.extend({
		model: window.app.Repertoire,
		initialize: function initialize(models, options) {
			this.options = options || {};
		},
		url: function url() {
			return this.url;
		},
		parse: function parse(res) {
			res = _.map(res, function(cinema) {
				cinema.total = _.reduce(cinema.weeks, function(sum, week) { return sum + week.count; }, 0);
				return cinema;
			});

			return res;
		},
		comparator: function comparator(release) {
			var cinema = reqres.request('get:user').getCinema(release.get('cinema_id'));
			return cinema.title.ru;
		},
		setRelease: function setRelease(releaseID) {
			this.url = '/api/schedule/get/?cinema_id=0&release_id=' + releaseID + '&all=1';
		},
		getTotalSeances: function getTotalSeances() {
			return _.reduce(_.flatten(this.pluck('weeks')), function(sum, week) {
				return sum + week.count;
			}, 0);
		}
	});

	window.app.ReleasePaintingView = Backbone.View.extend({
		className: 'section',
		template: '#release_painting_tmpl',
		events: {
			'click .to_planning': function() {
				$('.favorite.js-modal').trigger('click');
			},
			'click .btn[data-action="show-context-menu"]': 'showContextMenu'
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.options.user = reqres.request('get:user');

			this.collection = new window.app.ReleasePaintingCollection();

			this.listenTo(this.collection, 'sync',   this.addOneCinemaRepertoire);
			this.listenTo(this.collection, 'change', this.updateButtons);
			this.listenTo(Backbone, 'socket:schedule:update', this.update);

			this.loadReleases(this.model.get('id'));
		},
		loadReleases: function loadReleases(releaseID) {
			var self = this;

			this.collection.setRelease(releaseID);

			Backbone.trigger('loader', 'show');
			this.collection.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
					self.renderWrapper();
				}
			});
		},
		addOneCinemaRepertoire: function addOneCinemaRepertoire() {
			var user = this.options.user;

			_.each(this.collection.models, function(model) {
				model.set({
					cinema:  user.getCinema(model.get('cinema_id')),
					release: this.model.attributes
				});

				var View = new window.app.ReleasePaintingCinemaView({
					model:        model,
					saveOnChange: true,
					page:         'releaseCard',
					tx:           user.get('tx')
				});

				this.$el.find('.release_painting').append(View.render().el);
			}, this);
		},
		update: function update(release) {
			if (this.model.get('id') === release.schedule_id) {
				this.loadReleases(release.schedule_id);
			}
		},
		updateButtons: function updateButtons() {
			if (this.collection.getTotalSeances()) {
				this.$('.release_painting-header .btn').removeClass('disabled');
			} else {
				this.$('.release_painting-header .btn').addClass('disabled');
			}
		},
		renderWrapper: function render() {
			var template  = Backbone.TemplateCache.get(this.template),
				nextWeek  = moment().startOf('week').add(1, 'week');

			this.$el.html(template({
				isEmpty: !this.collection.models.length,
				exportURL: '/export/view/all?release_id=' + this.model.id,
				prolongationURL: '/export/order/prolongation' +
					'?release_id=' + this.model.id +
					'&week=' + nextWeek.week() +
					'&year=' + nextWeek.year()
			}));

			return this;
		},
		showContextMenu: function showContextMenu(event) {
			event.preventDefault();
			event.stopPropagation();

			var href = $(event.currentTarget).attr('href');

			this.contextMenu = new window.app.ContextMenuView({
				top:  event.pageY,
				left: event.pageX + 1,
				items: [
					{
						title: 'Базовый',
						action: function() {
							location.href = href;
						},
						context: this
					},
					{
						title: 'Расширенный',
						action: function() {
							location.href = href + '&all=1';
						},
						context: this
					}
				]
			});
		}
	});
});

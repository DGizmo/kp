$(function() {
	'use strict';
	window.app  = {};

	// Переопределение стандартных скобок шаблонизатора на фигурные,
	// т.к. стандартные совпадают со скобками шаблонизатора Mojolicious
	_.templateSettings = {
		evaluate: /\{\{(.+?)\}\}/g,
		interpolate: /\{\{=(.+?)\}\}/g,
		escape: /\{\{-(.+?)\}\}/g
	};

	$.arcticmodal('setDefault', {
		overlay: {
			css: {
				backgroundColor: '#000',
				opacity: '.86'
			}
		}
	});

	Date.setLocale('ru');
	moment.locale('ru');

	Number.prototype.toMonthName = function() {
		var month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
			'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
		];
		return month[this];
	};

	Date.prototype.getWeek = function() {
		return moment(this).weeks();
	};

	$.growl.error = function(options) {
		var settings;
		if (options == null) {
			options = {};
		}
		settings = {
			title: 'Ошибка!',
			style: 'error'
		};
		return $.growl($.extend(settings, options));
	};

	$.growl.notice = function(options) {
		var settings;
		if (options == null) {
			options = {};
		}
		settings = {
			title: 'Изменения сохранены!',
			style: 'notice'
		};
		return $.growl($.extend(settings, options));
	};

	$.growl.ok = function(options) {
		var settings;
		if (options == null) {
			options = {};
		}
		settings = {
			title: '',
			style: 'notice'
		};
		return $.growl($.extend(settings, options));
	};

	$.growl.warning = function(options) {
		var settings;
		if (options == null) {
			options = {};
		}
		settings = {
			title: 'Warning!',
			style: 'warning'
		};
		return $.growl($.extend(settings, options));
	};

	window.reqres = new Backbone.Wreqr.RequestResponse();
	window.commands = new Backbone.Wreqr.Commands();

});

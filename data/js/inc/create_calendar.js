//-------------------- Создание календаря --------------------//
$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.CreateCalendar = function(options) {
		options  = options || {};
		var date = options.date_param ? new Date(options.date_param) : new Date();

		this.current          = {};
		this.days             = [];
		this.weeks            = [];
		this.monthWeeksStarts = options.monthWeeksStarts ? options.monthWeeksStarts : false;

		this.changeDate = function(year, month) {
			this.days    = [];
			this.weeks   = [];
			this.check   = {};

			this.current.month = typeof (month) !== 'undefined' ? month : date.getMonth();
			this.current.year  = year || date.getFullYear();
			this.current.week  = date.getWeek();

			var weeks = [],
				tweek = 0,
				daysInCurrentMonth = moment({year: this.current.year, month: this.current.month, day: 1}).daysInMonth(),
				halls = reqres.request('get:user').getCinema().halls,
				i,
				z,
				day,
				oneDay,
				dayOfWeek,
				nw,
				start_time,
				j;

			for (i = 1; i <= daysInCurrentMonth; i++) {
				day        = moment({year: this.current.year, month: this.current.month, day: i});
				start_time = [];

				if (halls) {
					for (j in halls) {
						start_time.push({
							hall: halls[j].number,
							date: Date.create(day.clone().toDate()).addHours(10).addMinutes(30)
						});
					}
				}

				if (this.monthWeeksStarts) {
					if (day.day() === 4) {
						nw = day.weeks();

						for (z = 0; z < 7; z++) {
							oneDay = {};
							dayOfWeek = day.clone().add(z, 'days');

							oneDay.date           = dayOfWeek.toDate();
							oneDay.month          = dayOfWeek.format('MMM');
							oneDay.day_of_month   = dayOfWeek.date();
							oneDay.day_of_week    = dayOfWeek.day();
							oneDay.number_of_week = dayOfWeek.weeks();
							oneDay.start          = (z === 0 ? true : false);
							oneDay.start_time     = start_time;

							this.days.push(oneDay);
						}
					}
				} else {
					nw  = day.weeks();
					day = day.toDate();
					this.days.push({
						date:           day,
						month:          moment(day).format('MMM'),
						day_of_month:   i,
						day_of_week:    day.getWeekday(),
						number_of_week: nw,
						start:          moment(day).day() === 4 ? true : false,
						start_time:     start_time
					});
				}

				if (nw) {
					weeks.push(nw);

					tweek = nw;
				}
			}

			if (this.days[0].number_of_week !== this.days[2].number_of_week) this.check.start = this.days[0].number_of_week;
			if (this.days[this.days.length - 1].number_of_week !== this.days[this.days.length - 3].number_of_week) this.check.end = this.days[this.days.length - 1].number_of_week;

			this.weeks = _.uniq(weeks).map(function(num) { return {name: 'week', value: num}; });
		};
		this.changeDate();
	};
});

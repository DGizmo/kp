$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.App = new window.app.AppView();

	var ws = new ReconnectingWebSocket((window.location.protocol === 'http:' ? 'ws' : 'wss') + '://' + window.location.host + '/sync');

	ws.onmessage = function(event) {
		var m = $.parseJSON(event.data),
			action  = m ? m.action : '';
		// console.log(m, action);
		switch (action) {
			case 'ads':
				Backbone.trigger('socket:advertising:release', m);
			break;
			case 'notify':
				if (m.data.item !== 'custom') Backbone.trigger('socket:notifications:add', m.data);
			break;
			case 'messages':
				Backbone.trigger('socket:messages:add', m.message);
			break;
			case 'messages_update':
				Backbone.trigger('socket:messages:delete', m.message);
			break;
			case 'update':
				if (m.data.release_id && m.data.release_id.length < 5) {
					Backbone.trigger('socket:releases:update', m.data);
				} else {
					Backbone.trigger('socket:releases:update:all');
				}
			break;
			case 'favourite':
				Backbone.trigger('socket:favourite', m.data);
			break;
			case 'note':
				Backbone.trigger('socket:note', m);
			break;
			case 'schedule':
				Backbone.trigger('socket:schedule:update', m.data);
				Backbone.trigger('socket:repertoire:update', m.data);
			break;
			case 'schedule_week':
				Backbone.trigger('socket:schedule_week', m.data);
			break;
			case 'schedule_halls':
				Backbone.trigger('socket:schedule_halls');
			break;
			case 'set_tx':
				Backbone.trigger('socket:user:set:tx', m.value);
			break;
			case 'trailer':
				Backbone.trigger('socket:trailer', m.trailer);
			break;
			case 'logout':
				window.location.href = '/logout';
			break;
		}
	};
});

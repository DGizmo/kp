//-------------------- Дистрибьюторы --------------------//
$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.Distributor = Backbone.Model.extend({
		defaults: {
			id: null,
			logo: null,
			name: null,
			address: null,
			phone: null,
			group_contacts: {}
		},
		parse: function parse(res) {
			res.group_contacts = res.contacts.groupBy(function(d) { return d.department; });
			return res;
		}
	});

	window.app.Distributors = Backbone.Collection.extend({
		model: window.app.Distributor,
		url: '/api/distributors',
		initialize: function initialize() {
			this.user = reqres.request('get:user');
		},
		parse: function parse(res) {
			var partners    = this.user.get('partners'),
				competitors = this.user.get('competitors'),
				releasesGroup,
				actualDistributors,
				notActualDistributors;

			_.each(res.list, function(distributor) {
				if (_.contains(partners, distributor.id)) {
					distributor.relations = 'partner';
				} else if (_.contains(competitors, distributor.id)) {
					distributor.relations = 'competitor';
				}
			});

			releasesGroup = res.list.groupBy(function(distributor) {
				if (distributor.count_releases) {
					return 'haveReleases';
				} else {
					return 'haveNotReleases';
				}
			});

			actualDistributors = releasesGroup.haveReleases.sortBy(function(distributor) {
				return distributor.name.full || distributor.name.short;
			});

			notActualDistributors = releasesGroup.haveNotReleases.sortBy(function(distributor) {
				return distributor.name.full || distributor.name.short;
			});

			return res.list = [].concat(actualDistributors, notActualDistributors);
		}
	});

	window.app.DistributorsWrapView = Backbone.View.extend({
		id: 'distributors',
		template: '#distributor_wrapper',
		filterTemplate: '#distributor_item',
		events: {
			'click .list.scroll .item:not(".active")': /* istanbul ignore next */ function(e) {
				_gaq.push(['_trackEvent', 'Дистрибьюторы', 'Выбор дистрибьютора']);
				$(e.currentTarget).addClass('active').siblings('.active').removeClass('active');
				this.showDistributor($(e.currentTarget).data('id'));
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
		},
		loadDistributors: function loadDistributors() {
			var self = this;
			Backbone.trigger('loader', 'show');
			this.collection.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
					self.renderFilter().showDistributor(self.options.distributor).$('.list.scroll').scrollTo('.active');
				}
			});
		},
		renderFilter: function renderFilter() {
			var template = Backbone.TemplateCache.get(this.filterTemplate);
			this.$('.list.scroll').html(template({
				distributors: this.collection.toJSON()
			}));
			return this;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());

			if (!this.collection.models.length) {
				this.loadDistributors();
			} else {
				this.renderFilter().showDistributor(this.options.distributor);
			}

			this.addSearching();

			return this;
		},
		showDistributor: function showDistributor(id) {
			if (this.infoView) this.infoView.remove().$el.empty();

			var model = this.collection.find({id: id}) || this.collection.at(0);
			this.infoView = new window.app.DistributorInfoView({model: model});

			this.$('.list.scroll').find('[data-id="' + model.id + '"]').addClass('active');
			this.$('.info.scroll').html(this.infoView.render().el);

			Backbone.trigger('router:navigate', '/distributors/' + model.get('id'));

			return this;
		},
		addSearching: function addSearching() {
			var self = this;

			this.searchingView = new window.app.SearchingModuleView({
				animation: false,
				onSearchingFinish: function onSearchingFinish(value) {
					self.$('.list.scroll .item')
						.hide().end()
						.find('.long:containsLower(' + value + '), .short:containsLower(' + value + ')').closest('.item').show();
				}
			});

			this.$('.search').html(this.searchingView.render().el);
		},
		remove: function remove() {
			if (this.infoView) this.infoView.remove().$el.empty();
			if (this.searchingView) this.searchingView.remove();
			this.$el.empty();
			Backbone.View.prototype.remove.call(this);
			return this;
		}
	});

	// Описываем представлние для страницы дистрибьютера
	window.app.DistributorInfoView = Backbone.View.extend({
		className: 'item',
		template: '#distributor_info',
		events: {
			'click .to_dist': /* istanbul ignore next */ function(e) {
				e.preventDefault();
				var distributorID = parseInt($(e.currentTarget).attr('did'));
				Backbone.trigger('router:navigate', '/releases/distributor/' + distributorID);
			},
			'click .to_release': /* istanbul ignore next */ function(e) {
				e.preventDefault();
				var releaseID = parseInt($(e.currentTarget).attr('did'));

				Backbone.trigger('router:navigate', '/release/' + releaseID);
				_gaq.push(['_trackEvent', 'Дистрибьюторы', 'Переход на релиз']);
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});
});

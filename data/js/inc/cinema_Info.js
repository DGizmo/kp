$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.Cinema = Backbone.Model.extend({
		defaults: {
			cinema: {}
		},
		initialize: function initialize(attributes, options) {
			this.options = options || {};
		},
		url: function url() {
			return '/api/cinema/info/?cinema_id=' + reqres.request('get:user').getCinema().id;
		},
		parse: function parse(res) {
			if (res.cinema && res.cinema.halls) {
				res.cinema.halls_str = res.cinema.halls.length + _.declOfNum(res.cinema.halls.length, [' зал', ' зала', ' залов']);
				res.cinema.dd24_pro = res.cinema.dd24_pro || '';

				_.each(res.cinema.halls, function(hall) {
					if (typeof hall.installed !== 'undefined' && hall.installed !== null) {
						hall.installed = new Date(hall.installed).format('{dd}.{MM}.{yyyy}');
					} else {
						hall.installed = '';
					}

					hall.type3d = hall.type3d || '';
				});

				res.cinema.halls.sort(function(a, b) {
					return a.number - b.number;
				});
			}
			return res;
		}
	});

	window.app.CinemaInfoView = Backbone.View.extend({
		className: 'cinema-info-wrapper',
		template: '#cinema_info_tmpl',
		events: {
			'click .cinema-info-item__mistake': /* istanbul ignore next */ function() {
				commands.execute('show:feedback-popup');
			}
		},
		initialize: function initialize() {
			this.user  = reqres.request('get:user');
			this.model = new window.app.Cinema();

			this.listenTo(this.model, 'change',         this.renderCinema);
			this.listenTo(Backbone,   'cinema-changed', this.loadCinema);

			this.loadCinema();
		},
		loadCinema: function loadCinema() {
			if (this.user.isApproved()) {
				Backbone.trigger('loader', 'show');
				this.model.clear({silent: true}).fetch({
					success: function() {
						Backbone.trigger('loader', 'hide');
					}
				});
			} else {
				this.renderCinema();
			}
		},
		renderCinema: function renderCinema() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({
				approved: this.user.isApproved(),
				error:    this.model.get('error'),
				cinema:   this.model.get('cinema')
			}));
			return this;
		}
	});

});

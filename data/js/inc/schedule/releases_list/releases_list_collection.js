$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.WeekScheduleReleasesListCollection = Backbone.Collection.extend({
		initialize: function initialize() {
			reqres.setHandlers({
				'get:weekscheduleReleasesList': {
					callback: function() {
						return this;
					},
					context: this
				}
			});
			this.listenTo(Backbone, 'weekschedule:changeDate technology:save:done schedule:save:done user:cinema:settings:save cinema-changed', function() {
				this.fetch();
			});
			this.listenTo(Backbone, 'release:favourites', function(data) {
				if (_.find(this.models, function(release) { return data.id === parseInt(release.get('release_id')); })) this.fetch();
			});
			this.listenTo(Backbone, 'socket:schedule:update', function(data) {
				if (reqres.request('get:user').getCinema().id === data.cinema_id) this.fetch();
			});
			this.listenTo(Backbone, 'socket:releases:update', function(data) {
				var modelsIDs = _.map(this.models, function(release) { return parseInt(release.get('release_id')); });
				if (_.intersection(modelsIDs, data.release_id).length) this.fetch();
			});
			this.listenTo(Backbone, 'release:duration:changed', function(data) {
				var newDuration = _.property(data.chosen_duration)(data.duration) || data.duration.full || data.duration.clean;
				_.each(this.models, function(release) {
					if (data.id === parseInt(release.get('release_id'))) release.set({duration: newDuration});
				});
			});
		},
		url: function url() {
			return '/api/schedule/week/releases' +
				'?cinema_id=' + reqres.request('get:user').getCinema().id +
				'&week=' + reqres.request('get:weekschedule').get('currentWeek') +
				'&year=' + reqres.request('get:weekschedule').get('currentYear');
		},
		parse: function parse(response) {
			var releases       = response.releases.sortBy(function(release) { return release.release.title.ru; }),
				currentWeek    = reqres.request('get:weekschedule').get('currentWeek'),
				currentYear    = reqres.request('get:weekschedule').get('currentYear'),
				parsedReleases = [],
				actualReleases = [],
				identifiers    = [],
				releaseStart,
				releaseId;

			_.each(releases, function(release) {
				_.chain(release.technology)
					.sortBy(function(technologyItem) { return technologyItem.upgrades.length; })
					.each(function(technologyItem) {
						technologyItem.count = parseInt(technologyItem.count);
						technologyItem.format = (technologyItem.format.toUpperCase() === '35MM') ? '35mm' : technologyItem.format.toUpperCase();

						if (technologyItem.count) identifiers.push(release.release.id + technologyItem.format.toUpperCase() + technologyItem.upgrades.join(', ').toUpperCase());
						parsedReleases.push({
								title:      release.release.title.ru || release.release.title.en,
								release_id: release.release.id,
								count:      technologyItem.count,
								color:      reqres.request('get:weekschedule').COLOR[release.color] || 'gray',
								ad:         reqres.request('get:weekschedule').get('defaultCinemaAd'),
								age:        release.release.age,
								duration:   _.property(release.release.chosen_duration)(release.release.duration) || release.release.duration.full || release.release.duration.clean,
								formats:    technologyItem.format,
								upgrades:   technologyItem.upgrades.join(', ').toUpperCase() || ''
						});
					}, this);

				releaseStart = release.release.date.russia.start || release.release.date.russia.string;
				releaseId    = release.release.id;

				if (moment(releaseStart).weeks() === currentWeek && moment(releaseStart).year() === currentYear) {
					actualReleases.push(releaseId);
				}
			}, this);

			parsedReleases.push({
				title:      'Пустой сеанс',
				release_id: '0',
				color:      'gray',
				count:      0,
				age:        0,
				duration:   90,
				formats:    '2D',
				upgrades:   '',
				ad:         reqres.request('get:weekschedule').get('defaultCinemaAd')
			});

			reqres.request('get:weekschedule').set({
				releasesParams: this.parseReleasesParams(releases),
				actualReleases: actualReleases,
				identifiers:    identifiers
			});

			return parsedReleases;
		},
		parseReleasesParams: function parseReleasesParams(releases) {
			var releasesParams = [],
				releaseParams,
				searchingDuration,
				searchingHasCopy,
				searchingHasKdm;

			_.each(releases, function(release) {
				_.each(release.release.formats, function(format) {
					releaseParams          = {};
					releaseParams.id       = parseInt(release.release.id);
					releaseParams.format   = format;
					searchingHasCopy       = _.find(release.has_copy, function(item) { return item.format === format; });
					searchingHasKdm        = _.find(release.has_kdm, function(item) { return item.format === format; });

					releaseParams.adParams = _.map(release.advertizing, function(ad) {
						searchingDuration = _.find(ad.durations, function(item) { return item.format === format; });
						return {
							duration: searchingDuration ? searchingDuration.duration : ad.durations[0].duration,
							title:    ad.title,
							type:     ad.type,
							halls:    ad.halls
						};
					});

					releaseParams.hasCopy  = searchingHasCopy ? searchingHasCopy.halls : [];
					releaseParams.hasKdm   = searchingHasKdm ? searchingHasKdm.halls : [];

					releasesParams.push(releaseParams);
				});
			});

			return releasesParams;
		}
	});

});

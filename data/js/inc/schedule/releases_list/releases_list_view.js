$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DayScheduleReleasesListView = Backbone.View.extend({
		template: '#day_schedule_releases_list',
		events: {
			'click .change-display-mode': 'filterReleases'
		},
		initialize: function initialize() {
			this.subViews = [];
			this.render();

			this.listenTo(Backbone, 'weekschedule:week:open', function() {
				this.$('.item_film .count').hide();
			});

			this.listenTo(this.collection, 'sync', function() {
				this.render();
				Backbone.trigger('HallsWeek:filled');
			});
			this.listenTo(reqres.request('get:weekschedule'), 'change:filterReleases', this.renderPlannedReleases);
		},
		render: function render() {
			var template       = Backbone.TemplateCache.get(this.template);

			this.$el.html(template(reqres.request('get:weekschedule').attributes));

			this.afterRender();

			return this;
		},
		afterRender: function afterRender() {
			var self = this;
			this.renderPlannedReleases();

			this.releaseSearching = new window.app.SearchingModuleView({
				animation: false,
				onSearchingFinish: function onSearchingFinish(value) {
					if (reqres.request('get:weekschedule').get('filterReleases') && value.length) self.filterReleases();
					if (value.length < 3) return;
					$.get('/api/releases/filter/?schedule=1&q=' + value, function(releases) {
						self.renderSearchingReleases(releases.list);
					});
				}
			});

			this.$('.change-display-mode').before(this.releaseSearching.render().el);
		},
		renderPlannedReleases: function renderPlannedReleases() {
			_.invoke(this.subViews, 'remove');
			_.each(this.collection.models, function(release) {
				if (!reqres.request('get:weekschedule').get('filterReleases') || release.get('count') || release.get('release_id') === '0') {
					var releasesListItemView = new window.app.WeekScheduleReleasesListItemView({
							attributes: release.attributes,
							model:      release,
							defaultRelease: release.get('release_id') === '0' ? true : false
						});

					this.subViews.push(releasesListItemView);
					this.$('.list_films_wrap').append(releasesListItemView.render().el);
				}
			}, this);
		},
		renderSearchingReleases: function addSearchingReleases(releases) {
			releases = _.sortBy(releases, function(release) { return release.title.ru; });
			_.each(releases, function(release) {
				_.each(release.formats, function(format) {
					if (!reqres.request('get:weekschedule').get('filterReleases')) {
						var releaseAttributes = {
								title:      release.title.ru || release.title.en,
								release_id: release.id,
								count:      0,
								color:      'gray',
								ad:         reqres.request('get:weekschedule').get('defaultCinemaAd'),
								age:        release.age,
								duration:   _.property(release.chosen_duration)(release.duration) || release.duration.full || release.duration.clean,
								formats:    format,
								upgrades:   ''
							},
							foundReleaseView = new window.app.WeekScheduleReleasesListItemView({
								attributes: releaseAttributes
							});

						this.releaseSearching.subViews.push(foundReleaseView);
						this.$('.found-releases').append(foundReleaseView.render().el);
					}
				}, this);
			}, this);
		},
		filterReleases: function filterReleases() {
			_gaq.push(['_trackEvent', 'Расписание', 'Список релизов::Фильтр']);
			this.$('.change-display-mode').toggleClass('active');
			reqres.request('get:weekschedule').set({filterReleases: this.$('.change-display-mode').hasClass('active')});
			if (reqres.request('get:weekschedule').get('filterReleases')) this.releaseSearching.resetInput();
			$.cookie('weekScheduleFilterReleases', reqres.request('get:weekschedule').get('filterReleases'), {expires: 365, path: '/'});
			Backbone.trigger('hallDay:saveSeances HallsWeek:filled');
		},
		remove: function remove() {
			this.releaseSearching.remove();
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.WeekScheduleReleasesListView = window.app.DayScheduleReleasesListView.extend({
		template: '#week_schedule_releases_list',
		events: {
			'click .change-display-mode': 'filterReleases',
			'click .current_week_date': function(event) {
				var $target = $(event.currentTarget);
				$target
					.parent().find('input').datepicker($target.hasClass('open') ? 'hide' : 'show')
					.toggleClass('open');
			},
			'click .schedule_report': function() {
				_gaq.push(['_trackEvent', 'Расписание', 'Отчет::Открытие']);

				Backbone.trigger('router:navigate', 'schedule/report');
			},
			'click .clear_schedule': function() {
				if (reqres.request('get:user').isReadOnly() || (!$('.hall_list_wr .rel').length && !$('#halls_timeline_collection .film_item_timeline').length)) return;
				var result  = confirm('Вы уверены, что хотите очистить расписание текущей недели?'),
					halls   = [],
					seances;

				if (!result) return;

				_gaq.push(['_trackEvent', 'Расписание', 'Неделя::Удаление']);

				reqres.request('get:hallsWeek').each(function(hallWeek) {
					seances = hallWeek.fillingSeances([]);

					hallWeek.set({
						days_week: reqres.request('get:weekschedule').get('days_week'),
						week:      reqres.request('get:weekschedule').get('currentWeek'),
						year:      reqres.request('get:weekschedule').get('currentYear'),
						seances:   seances
					});

					halls.push({
						id:      hallWeek.get('id'),
						seances: seances
					});
				}, this);

				this.saveAllHalls(halls);
			},
			'click .export': function() {
				_gaq.push(['_trackEvent', 'Расписание', 'Экспорт в Excel']);
				Backbone.trigger('weekschedule:export');
			},
			'click .schedule_generation': function() {
				if (reqres.request('get:user').isReadOnly()) return;
				var result   = confirm('Существующее расписание будет заменено.'),
					self     = this;

				if (!result) return;

				_gaq.push(['_trackEvent', 'Расписание', 'Неделя::Генерация']);

				$.ajax({
					url: '/api/schedule/generate',
					type: 'get',
					dataType: 'json',
					data: ({
						cinema_id: reqres.request('get:user').getCinema().id,
						year:      reqres.request('get:weekschedule').get('currentYear'),
						week:      reqres.request('get:weekschedule').get('currentWeek')
					}),
					success: function(data) {
						var halls = self.parseGeneratedSchedule(data);

						self.saveAllHalls(halls);
					},
					error: function() {
						alert('Проблемы с интернет соединением!');
					}
				});
			},
			'click .bk': function() {
				_gaq.push(['_trackEvent', 'Расписание', 'Смена недели']);
				reqres.request('get:weekschedule').setDateParameters(moment(reqres.request('get:weekschedule').get('date').subtract(7, 'day')));
			},
			'click .ff': function() {
				_gaq.push(['_trackEvent', 'Расписание', 'Смена недели']);
				reqres.request('get:weekschedule').setDateParameters(moment(reqres.request('get:weekschedule').get('date').add(7, 'day')));
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template(reqres.request('get:weekschedule').attributes));

			this.afterRender();
			this.setDatePicker();

			return this;
		},
		setDatePicker: function setDatePicker() {
			var $datepickerInput = this.$('.current_week input'),
				$datepicker,
				newDate,
				diff;

			$datepickerInput.datepicker({
				weekStart: 4,
				language: 'ru',
				calendarWeeks: true,
				autoclose: true,
				maxViewMode: 'days',
				daysOfWeekDisabled: [0, 1, 2, 3, 5, 6],
				format: 'dd M yyyy',
				todayHighlight: true
			})
			.on('hide', _.bind(function() {
				newDate = moment($datepickerInput.datepicker('getDate'));
				diff = newDate.diff(reqres.request('get:weekschedule').get('date'), 'days');
				if (Boolean(diff + 6)) reqres.request('get:weekschedule').setDateParameters(moment($datepickerInput.datepicker('getDate')));
				$datepickerInput.removeClass('open');
			}, this))
			.on('show', function() {
				$datepicker = window.app.App.$el.children('.datepicker');
				$datepicker
					.css({
						left: parseInt($datepicker.css('left')) + 55,
						top: parseInt($datepicker.css('top')) + 70
					})
					.addClass('center')
					.find('.day.active').addClass('disabled');
			});
		},
		parseGeneratedSchedule: function parseGeneratedSchedule(schedule) {
			var dataHalls      = schedule.data.halls,
				halls          = [],
				cinemaSettings = reqres.request('get:user').getCinemaSettings(),
				cinemaAd       = parseInt(reqres.request('get:weekschedule').get('defaultCinemaAd'));

			reqres.request('get:hallsWeek').each(function(hallWeek) {
				var hallId        = parseInt(hallWeek.get('id')),
					currentHall   = _.find(dataHalls, function(dataHall) { return dataHall.id === hallId; }),
					hallsSettings = cinemaSettings ? cinemaSettings.halls.find(function(h) { return h.id === hallId; }) : '',
					hallOpen      = hallsSettings.defaultHallOpen || '10:00',
					hallPause     = parseInt(hallsSettings.defaultHallPause) || 10,
					dataReleases  = _.isUndefined(currentHall.seances[0].releases) ? [] : currentHall.seances[0].releases,
					seances       = [];

				dataReleases.each(function(release, index) {
					release.ad       = cinemaAd;
					release.pause    = hallPause;
					release.upgrades = (release.upgrades.length > 0) ? release.upgrades.join(', ').toUpperCase() : '';
					release.formats  = release.formats !== '35mm' ? release.formats.toUpperCase() : '35mm';
					release.adParams = hallWeek.getAdParams(release.formats, release.id);

					if (index === 0) {
						release.start = hallOpen;
					} else {
						var prevReleaseStart = reqres.request('get:weekschedule').timeToMinutes(dataReleases[index - 1].start),
							releaseDuration = dataReleases[index - 1].duration,
							releaseStart;

						if (reqres.request('get:weekschedule').get('schedule_rounding') === 'ceil') {
							releaseStart = Math.ceil((prevReleaseStart + cinemaAd + releaseDuration + hallPause) / 5) * 5;
						} else {
							releaseStart = Math.floor((prevReleaseStart + cinemaAd + releaseDuration + hallPause) / 5) * 5;
						}

						release.start = reqres.request('get:weekschedule').minutesToTime(releaseStart);
					}
				});

				seances = hallWeek.fillingSeances(dataReleases);

				hallWeek.set({
					seances: seances
				});

				halls.push({
					id:      hallId,
					seances: seances
				});

				hallWeek.trigger('refreshSeances');
				hallWeek.trigger('render_timeline');
			});

			return halls;
		},
		saveAllHalls: function saveAllHalls(halls) {
			var user = reqres.request('get:user');

			$.post('/api/schedule/week/v2/saveall', {
				tx:        user.get('tx'),
				cinema_id: user.getCinema().id,
				year:      reqres.request('get:weekschedule').get('currentYear'),
				week:      reqres.request('get:weekschedule').get('currentWeek'),
				halls:     $.toJSON(halls)
			}, function() {
				Backbone.trigger('HallsWeek:filled');
			}, 'json');
		},
		remove: function remove() {
			this.$('.current_week input').datepicker('remove');
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.WeekScheduleReleasesListItemView = Backbone.View.extend({
		className: 'item_film',
		template: '#week_schedule_releases_list_item',
		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {defaultRelease: false});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				release  = $.extend({}, this.attributes);

			release.actualReleases = reqres.request('get:weekschedule').get('actualReleases');
			release.defaultRelease = this.options.defaultRelease;

			this.$el.html(template(release));

			if (!reqres.request('get:user').isReadOnly()) this.drag();

			return this;
		},
		drag: function drag() {
			var weekSchedule = reqres.request('get:weekschedule');
			this.$el.draggable({
				helper: 'clone',
				cursor: 'move',
				opacity: 0.7,
				containment: 'document',
				start: function(event, ui) {
					$(this).draggable({scope: reqres.request('get:weekschedule').get('user_watch')});

					if (weekSchedule.get('user_watch') !== 'timeline') return;
					var scaleSegment = weekSchedule.get('scaleSegment'),
						adDuration   = parseInt($(ui.helper).attr('ad')),
						adWidth      = (adDuration / 5) * scaleSegment,
						filmDuration = parseInt($(ui.helper).attr('duration')),
						filmFormat   = $(ui.helper).attr('formats');

					$(ui.helper).find('.badg_cer, .count').remove();
					$(ui.helper).find('.release_info').text(filmFormat).css('left', '0px');

					if (reqres.request('get:weekschedule').get('schedule_rounding') === 'ceil') {
						filmDuration = Math.ceil((filmDuration + adDuration) / 5) * scaleSegment - 10 - adWidth;
					} else {
						filmDuration = Math.floor((filmDuration + adDuration) / 5) * scaleSegment - 10 - adWidth;
					}

					$(ui.helper).find('p').css({
						width: filmDuration + 'px'
					});
					$(ui.helper).find('.ad_time').css({
						height: 55,
						width: adWidth
					});

					$(ui.helper).css({
						width:        filmDuration + 'px',
						height:       '45px',
						borderRadius: '0px',
						paddingLeft:  5 + adWidth
					});
				},
				drag: function(event, ui) {
					if (weekSchedule.get('user_watch') !== 'timeline') return;
					var scaleSegment      = weekSchedule.get('scaleSegment'),
						adDuration        = parseInt($(ui.helper).attr('ad')),
						adWidth           = (adDuration / 5) * scaleSegment,
						leftPosition      = ui.position.left - 281,
						addedFilmPosition = Math.ceil(leftPosition / scaleSegment) * scaleSegment,
						addedFilmStart    = weekSchedule.minutesToTime(((addedFilmPosition + scaleSegment * 8 * 12) / scaleSegment) * 5);

					$('#day_timeline_wr .position_marker_start').css({
						left: leftPosition
					});
					$('#day_timeline_wr .position_time')
						.offset({
							top: ui.position.top + 85,
							left: ui.offset.left + 5 + adWidth
						})
						.text(addedFilmStart);

					$('#day_timeline_wr .position_marker_end').css({
						left: leftPosition + $(ui.helper).width() + 11 + adWidth
					});
				},
				stop: function() {
					commands.execute('set:timeline:markersPosition');
				}
			});
		}
	});

});

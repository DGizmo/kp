$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReleaseHallWeekView = Backbone.View.extend({
		className: 'rel',
		template: '#release_hall_week',
		events: {
			contextmenu: 'showContextMenu',
			click: function(event) {
				if (!$(event.target).is('.remove, .fa-comment')) this.openDayPopup(event);
			},
			'click .fa-comment': function(event) {
				event.stopPropagation();
				_gaq.push(['_trackEvent', 'Расписание', 'Комментарий::Изменение']);

				this.commentPopover = new window.app.CommentPopoverView({
					top:  event.pageY,
					left: event.pageX + 1,

					comment: this.$el.attr('comment'),
					onSave: _.bind(function onSave(comment) {
						this.$el.attr('comment', comment);
						this.model.trigger('save:release:comment');
					}, this)
				});
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};

			var adParams    = this.options.release.adParams,
				advertising = _.filter(adParams, function(ad) { return ad.type === 'advertize'; }),
				commercials = _.filter(adParams, function(ad) { return ad.type === 'commercials'; }),
				adDuration  = 0;

			this.adTitle = advertising.length || commercials.length ? '' : 'План рекламы отсутствует';

			if (advertising.length) {
				this.adTitle = this.adTitle + '<u>Дополнительные трейлеры (' + advertising.length + ')</u>:<br>';
				_.each(advertising, function(ad) {
					this.adTitle = this.adTitle + '- ' + ad.title + '<br>';
					adDuration   = adDuration + ad.duration;
				}, this);
			}

			if (commercials.length) {
				this.adTitle = this.adTitle + '<u>Коммерческая реклама (' + commercials.length + ')</u>:<br>';
				_.each(advertising, function(ad) {
					this.adTitle = this.adTitle + '-' + ad.title + '<br>';
					adDuration   = adDuration + ad.duration;
				}, this);
			}

			if (reqres.request('get:weekschedule').get('schedule_rounding') === 'ceil') {
				adDuration = Math.ceil(adDuration / 300) * 5;
			} else {
				adDuration = Math.floor(adDuration / 300) * 5;
			}

			if (adDuration > this.attributes.ad) this.$el.attr('ad', adDuration);

			this.$el.data({minAdDuration: adDuration});
		},
		render: function render() {
			var template           = Backbone.TemplateCache.get(this.template),
				newRelease         = _.find(reqres.request('get:weekscheduleReleasesList').models, function(r) {
					return r.get('release_id') === this.options.release.id;
				}, this),
				newReleaseDuration = newRelease ? newRelease.get('duration') : 'undefined',
				extModel           = $.extend({}, this.options.release);

			extModel.color                = this.attributes.color;
			extModel.comment              = this.attributes.comment;
			extModel.missingPlaning       = this.attributes.missingPlaning;
			extModel.relTitle             = this.options.release.title;
			extModel.problem              = (extModel.missingPlaning || !newRelease) && reqres.request('get:weekschedule').get('filterReleases') && parseInt(this.options.release.id);
			extModel.changedDurationClass = (parseInt(this.options.release.duration) !== newReleaseDuration && newRelease) ? 'changed_duration' : '';
			extModel.showingFormats       = (this.options.release.formats === '2D' && this.attributes.upgrades === '') ? '' : this.options.release.formats + ' ' + this.attributes.upgrades;
			extModel.ad                   = this.attributes.ad;
			extModel.accessContent        = reqres.request('get:user').accessTo('kdmContent');
			extModel.age                  = newRelease ? newRelease.get('age') : null;

			this.$el.html(template(extModel));

			this.afterRender(extModel);

			return this;
		},
		afterRender: function afterRender() {
			this.$el
				.addClass(this.attributes.color)
				.data({
					adParams: this.options.release.adParams,
					has_copy: this.options.release.has_copy,
					has_kdm:  this.options.release.has_kdm
				});

			this.$('.rel_time')
				.attr('title', this.adTitle)
				.tipsy({
					html:    true,
					gravity: 'e'
				});
		},
		showContextMenu: function showContextMenu(event) {
			event.preventDefault();

			var items = [
				{
					title: 'Редактировать',
					iconClass: 'fa fa-pencil',
					action: function() {
						_gaq.push(['_trackEvent', 'Расписание', 'День::Открытие конт. меню']);
						this.openDayPopup(event, 'unlocked');
					},
					context: this
				},
				{
					title: 'Комментировать',
					iconClass: 'fa fa-comment',
					action: function() {
						if (this.$el.attr('comment').length) {
							_gaq.push(['_trackEvent', 'Расписание', 'Комментарий::Изменение']);
						} else {
							_gaq.push(['_trackEvent', 'Расписание', 'Комментарий::Добавление']);
						}

						this.openCommentPopover(event);
					},
					context: this
				}
			];

			if (reqres.request('get:user').isMultiplex()) {
				items.push({
					title: 'Копировать расписание',
					iconClass: 'fa fa-files-o',
					action: function() {
						this.hallCopy(event);
					},
					context: this
				});
			}

			this.setClickedDay(event);

			this.contextMenu = new window.app.ContextMenuView({
				top:  event.pageY,
				left: event.pageX + 1,
				items: items
			});
		},
		setClickedDay: function setClickedDay(e) {
			var $target           = this.$el,
				weekScheduleModel = reqres.request('get:weekschedule');

			$target.closest('.hall_item')
				.find('.hall_day').each(function(id, hallDay) {
					if (($(hallDay).offset().left + $(hallDay).width()) > e.clientX) {
						weekScheduleModel.set({ clickedDay: parseInt($(hallDay).attr('day_number'))});
						return false;
					}
				});
		},
		hallCopy: function hallCopy() {
			Backbone.trigger('weekschedule:hall:copy:open', this.model.get('week'), this.model.get('id'), this.model.get('year'), this.model.get('days_week')[reqres.request('get:weekschedule').get('clickedDay')].date);
		},
		openDayPopup: function openDayPopup(e, lockMode) {
			var $target           = this.$el,
				weekScheduleModel = reqres.request('get:weekschedule'),
				nextFullDay       = $target.closest('.hall_day').nextAll('.full:first').attr('day_number') || 7;

			this.setClickedDay(e);

			_gaq.push(['_trackEvent', 'Расписание', 'День::Открытие']);

			weekScheduleModel.set({
				chosen_day:          parseInt($target.closest('.hall_day').attr('day_number')),
				current_hall_number: parseInt($target.closest('.hall_item').find('.hall_item__hall').attr('hall')),
				current_hall_title:  $target.closest('.hall_item').find('.hall_item__hall').attr('hall_title'),
				nextFullDay:         parseInt(nextFullDay),
				fromDay:             parseInt($target.closest('.hall_day').attr('day_number')),
				lockMode:            lockMode || null
			});

			Backbone.trigger('weekschedule:day-popup:open');
		},
		openCommentPopover: function openCommentPopover(event) {
			var releaseIndex     = this.$el.index(),
				clickedDay       = reqres.request('get:weekschedule').get('clickedDay'),
				clickedReleaseEl = this.$el.closest('.hall_item')
					.find('.hall_day[day_number="' + clickedDay + '"] .rel')
					.eq(releaseIndex);

			this.commentPopover = new window.app.CommentPopoverView({
				top:  event.pageY,
				left: event.pageX + 1,

				comment: clickedReleaseEl.attr('comment'),
				onSave: _.bind(function onSave(comment) {
					clickedReleaseEl.attr('comment', comment);
					this.model.trigger('save:release:comment');
				}, this)
			});
		}
	});

	window.app.ReleaseHallDayView = window.app.ReleaseHallWeekView.extend({
		template: '#release_hall_day',
		className: 'release',
		events: {},
		afterRender: function afterRender() {
			this.$el
				.attr('title', '')
				.data({
					adParams: this.options.release.adParams,
					has_copy: this.options.release.has_copy,
					has_kdm:  this.options.release.has_kdm
				});

			this.$('.rel_day_ad')
				.attr('title', this.adTitle)
				.tipsy({
					html:    true,
					gravity: 'n'
				});
		}
	});

	window.app.ReleaseHallTimelineView = window.app.ReleaseHallWeekView.extend({
		template: '#release_hall_timeline',
		className: 'film_item_timeline',
		events: {},
		afterRender: function afterRender(extModel) {
			this.$el
				.addClass(this.options.release.chosen)
				.addClass(extModel.changedDurationClass)
				.data({
					adTitle:  this.adTitle,
					adParams: this.options.release.adParams,
					has_copy: this.options.release.has_copy,
					has_kdm:  this.options.release.has_kdm
				});
		}
	});

	window.app.ReleaseHallReportView = Backbone.View.extend({
		emptyTemplate: '#empty_release_hall_report',
		template: '#release_hall_report',
		tagName: 'tr',
		initialize: function(options) {
			this.options = options || {};
		},
		render: function() {
			var group    = this.options.releasesGroup,
				extModel = group ?
					{
						id:       group[0].id,
						title:    group[0].title,
						formats:  group[0].formats,
						upgrades: group[0].upgrades,
						duration: reqres.request('get:weekschedule').minutesToTime(group[0].duration).slice(1, 5),
						starts:   _.map(group, 'start')
					} :
					null,
				template = extModel ?
					Backbone.TemplateCache.get(this.template)(extModel) :
					Backbone.TemplateCache.get(this.emptyTemplate);

			this.$el.html(template);

			return this;
		}
	});

});

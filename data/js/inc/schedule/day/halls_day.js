$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.HallDayView = Backbone.View.extend({
		template:  '#hall_day_tmpl',
		tagName:   'div',
		className: 'hall_day',
		events: {
			'click .remove': function(e) {
				_gaq.push(['_trackEvent', 'Расписание', 'День::Удаление']);

				var firstStart = this.$('.release:first').attr('start');

				$(e.currentTarget).closest('.release').remove();

				this.$('.release:first').attr({
					start: firstStart
				});

				this.calcReleaseStart();

				if (!this.$('.release').length) {
					this.$('.helper_block').show();
				}
			},
			'click .minus_start, .plus_start': function(event) {
				_gaq.push(['_trackEvent', 'Расписание', 'День::Начало сеансов']);

				var $target           = $(event.currentTarget),
					firstReleaseStart = reqres.request('get:weekschedule').timeToMinutes(this.$('.release:first').attr('start')),
					lastReleaseEnd    = reqres.request('get:weekschedule').timeToMinutes(this.$('.release:last .rel_day_end').text());

				if ((firstReleaseStart + $target.data('minutes-change')) < 480) {
					alert('Начало показа релиза затрагивает конец предыдущего дня');
				} else if ((lastReleaseEnd + $target.data('minutes-change')) > 1860) {
					alert('Конец показа релиза затрагивает начало следующего дня');
				} else {
					firstReleaseStart = this.model.collection.weekSchedule.minutesToTime(firstReleaseStart + $target.data('minutes-change'));

					this.$('.release:first')
						.attr({
							start: firstReleaseStart
						})
						.find('.start_value').text(firstReleaseStart);

					this.calcReleaseStart();
				}
			},
			'click .plus_pause, .minus_pause': function(e) {
				_gaq.push(['_trackEvent', 'Расписание', 'День::Начало сеансов']);

				var $target        = $(e.currentTarget),
					minutesChange  = $target.data('minutes-change'),
					lastReleaseEnd = this.model.collection.weekSchedule.timeToMinutes(this.$('.release:last .rel_day_end').text()),
					releasePause   = parseInt($target.closest('.release').attr('pause')) + minutesChange;

				if ((lastReleaseEnd + minutesChange) > 1860) {
					alert('Конец показа релиза затрагивает начало следующего дня');
				} else if (releasePause < 0) {
					return;
				} else {
					$target.closest('.release')
						.attr({
							pause: releasePause
						})
						.find('.pause_value').text(releasePause + ' мин.');

					this.calcReleaseStart();
				}
			},
			'click .plus_ad, .minus_ad': function(e) {
				_gaq.push(['_trackEvent', 'Расписание', 'День::Реклама']);

				var $target        = $(e.currentTarget),
					minutesChange  = $target.data('minutes-change'),
					lastReleaseEnd = reqres.request('get:weekschedule').timeToMinutes(this.$('.release:last .rel_day_end').text()),
					releaseAd      = parseInt($target.closest('.release').attr('ad')) + minutesChange;

				if ($target.closest('.release').data('minAdDuration') > releaseAd) {
					Backbone.trigger('weekschedule:message', 'Нельзя выставить длительность рекламного блока меньше длительности запланированной рекламы.');
				} else if ((lastReleaseEnd + minutesChange) > 1860) {
					alert('Конец показа релиза затрагивает начало следующего дня');
				} else if (releaseAd < 0) {
					return;
				} else {
					$target.closest('.release').
						attr({ad: releaseAd})
						.find('.ad_value').text(releaseAd + ' мин.');

					this.calcReleaseStart();
				}
			},
			'click .repair_duration': function(e) {
				var needAlert = false;

				_gaq.push(['_trackEvent', 'Расписание', 'День::Починка длительностей']);

				$(e.currentTarget).hide();

				this.$el.find('.changed_duration').each(function(id, el) {
					var curRelId = $(el).closest('.release').attr('release_id'),
						actualDuration = $('.item_film[release_id=' + curRelId + ']').attr('duration');

					if (actualDuration === '') {
						needAlert = true;
					} else {
						$(el).removeClass('changed_duration').text(actualDuration + ' мин.');
						$(el).closest('.release').attr({
							duration: actualDuration
						});
					}
				});

				this.calcReleaseStart();

				if (needAlert) {
					$(e.currentTarget).show();
					alert('У некоторых релизов отсутствует длительность.');
				}
			}
		},
		initialize: function initialize() {
			this.subViews = [];
			this.listenTo(Backbone, 'hallDay:saveSeances', this.saveSeances);
			this.listenTo(Backbone, 'HallsWeek:filled', this.render);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			if (reqres.request('get:user').isReadOnly()) this.$el.addClass('no-rights');

			this.$el.html(template(this.model.attributes));
			this.afterRender();

			return this;
		},
		afterRender: function afterRender() {
			var self = this,
				releaseAttributes;

			_.each(self.model.get('seances')[self.model.collection.weekSchedule.get('chosen_day')].releases, function(release) {
				releaseAttributes = self.model.parseRelease(release);

				var releaseHallDayVew = new window.app.ReleaseHallDayView({
					release:    release,
					model:      self.model,
					attributes: releaseAttributes
				});

				self.$('.wr_release_day').append(releaseHallDayVew.render().el);

				self.subViews.push(releaseHallDayVew);
			});

			this.drop();
			this.sort();

			this.$('.wr_release_day').height($('.sidebar').height() - 186);
			if (this.$('.changed_duration').length) this.$('.repair_duration').show();

			if (!self.$('.release').length) {
				self.$('.helper_block').show();
			} else {
				self.calcReleaseStart();
			}
		},
		drop: function drop() {
			var self = this;

			this.$('.hall_content').droppable({
				accept: '.item_film',
				hoverClass: 'drop-hover',
				scope: 'day',
				drop: function(event, ui) {
					_gaq.push(['_trackEvent', 'Расписание', 'День::Добавление']);

					var hallId            = self.model.get('id'),
						hallFormats       = self.model.get('formats'),
						cinemaSetting     = reqres.request('get:user').getCinemaSettings(),
						hallsSettings     = cinemaSetting ? cinemaSetting.halls.find(function(h) {return h.id === hallId;}) : '',
						checkCopyAndKdm   = self.model.checkCopyAndKdm($(ui.draggable).attr('formats'), $(ui.draggable).attr('release_id')),
						release           = {
							has_copy: checkCopyAndKdm.has_copy,
							has_kdm:  checkCopyAndKdm.has_kdm,
							duration: $(ui.draggable).attr('duration'),
							formats:  $(ui.draggable).attr('formats'),
							upgrades: $(ui.draggable).attr('upgrades'),
							pause:    hallsSettings.defaultHallPause || 10,
							adParams: self.model.getAdParams($(ui.draggable).attr('formats'), $(ui.draggable).attr('release_id')),
							ad:       parseInt($(ui.draggable).attr('ad')),
							title:    $(ui.draggable).attr('title'),
							id:       $(ui.draggable).attr('release_id'),
							start:    hallsSettings.defaultHallOpen || '10:00'
						},
						releaseAttributes = self.model.parseRelease(release),
						releaseHallDayVew,
						$addedRelease,
						addedReleaseStart,
						addedReleaseAd,
						addedReleaseDuration,
						addedReleasePause,
						addedReleaseEnd;

					if (!new RegExp(release.formats, 'i').test(hallFormats.join('')) || !release.formats) {
						alert('Зал не поддерживает формат данного релиза.');
					} else if (!release.duration) {
						alert('У релиза нет длительности.');
					} else {
						releaseHallDayVew = new window.app.ReleaseHallDayView({
							release:    release,
							model:      self.model,
							attributes: releaseAttributes
						});

						$(this).find('.wr_release_day').append(releaseHallDayVew.render().el);

						self.calcReleaseStart();

						$addedRelease        = $(this).find('.release').last(),
						addedReleaseStart    = $addedRelease.attr('start'),
						addedReleaseAd       = parseInt($addedRelease.attr('ad')),
						addedReleaseDuration = parseInt($addedRelease.attr('duration')),
						addedReleasePause    = parseInt($addedRelease.attr('pause')),
						addedReleaseEnd      = 0;
						addedReleaseStart    = self.model.collection.weekSchedule.timeToMinutes(addedReleaseStart);

						if (reqres.request('get:weekschedule').get('schedule_rounding') === 'ceil') {
							addedReleaseEnd = Math.ceil((addedReleaseStart + addedReleasePause + addedReleaseAd + addedReleaseDuration) / 5) * 5;
						} else {
							addedReleaseEnd = Math.floor((addedReleaseStart + addedReleasePause + addedReleaseAd + addedReleaseDuration) / 5) * 5;
						}

						if (addedReleaseEnd > 1860) { // минут в 7 часах утра
							$addedRelease.find('.remove').trigger('click');

							alert('Конец показа релиза затрагивает начало следующего дня');

							return;
						}

						if (self.$el.find('.helper_block').is(':visible')) {
							self.$el.find('.helper_block').hide();
						}
					}
				}
			});
		},
		sort: function sort() {
			var self = this;
			this.$('.wr_release_day').sortable({
				axis: 'y',
				start: function() {
					self.dayStart = $(this).find('.release:first').attr('start');
				},
				update: function() {
					_gaq.push(['_trackEvent', 'Расписание', 'День::Перетягивание']);
					$(this).find('.release:first')
						.attr('start', self.dayStart)
						.find('.start_value').text(self.dayStart);
					self.calcReleaseStart();
				}
			});
		},
		calcReleaseStart: function calcReleaseStart() {
			var self = this;

			this.$('.release').each(function(id, rel) {
				var releasePause     = parseInt($(rel).attr('pause')),
					$nextRel         = $(rel).next(),
					releaseStart     = $(rel).attr('start'),
					releaseAd        = parseInt($(rel).attr('ad')),
					releaseDuration  = parseInt($(rel).attr('duration')),
					nextReleaseStart = $nextRel.attr('start'),
					releaseEnd;

				releaseStart = self.model.collection.weekSchedule.timeToMinutes(releaseStart);
				releaseEnd   = self.model.collection.weekSchedule.minutesToTime(releaseStart + releaseDuration + releaseAd);

				if (reqres.request('get:weekschedule').get('schedule_rounding') === 'ceil') {
					nextReleaseStart = self.model.collection.weekSchedule.minutesToTime(Math.ceil((releaseStart + releaseDuration + releasePause + releaseAd) / 5) * 5);
				} else {
					nextReleaseStart = self.model.collection.weekSchedule.minutesToTime(Math.floor((releaseStart + releaseDuration + releasePause + releaseAd) / 5) * 5);
				}

				$(rel).find('.rel_day_end').text(releaseEnd);
				$nextRel.attr('start', nextReleaseStart);
				$nextRel.find('.start_value').text(nextReleaseStart);
			});
		},
		saveSeances: function saveSeances() {
			var self         = this,
				seances      = self.model.get('seances'),
				nextFullDay  = self.model.collection.weekSchedule.get('nextFullDay') || 7,
				fromDay      = self.model.collection.weekSchedule.get('fromDay'),
				prevSeances  = JSON.stringify(self.model.get('seances')),
				prevSchedule = JSON.stringify(self.model.collection.weekSchedule.get('schedule')),
				i            = 0;

			_.each(seances, function(oneSeance) {
				if (i >= fromDay && i < nextFullDay) {
					oneSeance.releases = [];

					if (self.$el.find('.release').length === 0 && i !== 0) {
						oneSeance.releases = seances[i - 1].releases;
					} else {
						self.$el.find('.release').each(function() {
							oneSeance.releases.push({
								id:       $(this).attr('release_id'),
								formats:  $(this).attr('formats'),
								upgrades: $(this).attr('upgrades'),
								title:    $(this).find('.rel_day_title').attr('release_title'),
								duration: $(this).attr('duration'),
								start:    $(this).attr('start'),
								pause:    $(this).attr('pause'),
								ad:       $(this).attr('ad'),
								adParams: $(this).data('adParams') || null,
								has_copy: $(this).data('has_copy') || 0,
								has_kdm:  $(this).data('has_kdm') || 0,
								comment:  $(this).attr('comment')
							});
						});
					}
				}
				i++;
			});

			this.model.trigger('hall:day-popup:change');

			$.ajax({
				url: '/api/schedule/week/v2/set',
				type: 'post',
				dataType: 'json',
				timeout: 5000,
				data: ({
					tx:        reqres.request('get:user').get('tx'),
					cinema_id: reqres.request('get:user').getCinema().id,
					year:      self.model.get('year'),
					week:      self.model.get('week'),
					hall_id:   self.model.get('id'),
					seance:    JSON.stringify(self.model.get('seances'))
				}),
				error: function() {
					self.model.set('seances', JSON.parse(prevSeances));
					self.model.collection.weekSchedule.set('schedule', JSON.parse(prevSchedule));
					Backbone.trigger('weekschedule:message', 'Проблемы с интернет соединением!');
					Backbone.trigger('HallsWeek:filled');
				}
			});
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

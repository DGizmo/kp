$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};
	window.app.DayScheduleView = Backbone.View.extend({
		template: '#day_schedule_tmpl',
		className: 'week_settings',
		events: {
			'click .exit': function() {
				$.arcticmodal('close');
			},
			'click .fa-lock': function(e) {
				if (reqres.request('get:user').isReadOnly()) return;
				_gaq.push(['_trackEvent', 'Расписание', 'День::Разбитие промежутка']);

				$(e.currentTarget).removeClass('fa-lock').addClass('fa-unlock');

				var self       = this,
					WEEKDAY    = this.model.WEEKDAY,
					clickedDay = parseInt(this.model.get('clickedDay'));

				this.model.set({
					nextFullDay: clickedDay + 1,
					fromDay:     clickedDay
				});

				this.$el.find('.cur_date').text('Зал ' + this.model.get('current_hall_title') + ' (' + this.model.get('days_week')[clickedDay].day_of_month + ' ' + this.model.get('days_week')[clickedDay].month + ', ' + WEEKDAY[Date.create(self.model.get('days_week')[clickedDay].date).format('{weekday}')] + ')');
			},
			'click .fa-unlock': function(e) {
				if (reqres.request('get:user').isReadOnly()) return;
				$(e.currentTarget).removeClass('fa-unlock').addClass('fa-lock');

				var nextFullDay = $('.hall_day[hall_title=' + this.model.get('current_hall_title') + ']').nextAll('.full:first').attr('day_number'),
					self      = this,
					WEEKDAY   = this.model.WEEKDAY,
					chosenDay = this.model.get('chosen_day');

				if (!nextFullDay) nextFullDay = 7;

				this.model.set({
					nextFullDay: nextFullDay,
					fromDay:     chosenDay
				});

				this.$el.find('.cur_date').text('Зал ' + this.model.get('current_hall_title') + ' (' + this.model.get('days_week')[chosenDay].day_of_month + ' ' + this.model.get('days_week')[chosenDay].month + ', ' + WEEKDAY[Date.create(self.model.get('days_week')[chosenDay].date).format('{weekday}')] + ' - ' + this.model.get('days_week')[nextFullDay - 1].day_of_month + ' ' + this.model.get('days_week')[nextFullDay - 1].month + ', ' + WEEKDAY[Date.create(self.model.get('days_week')[nextFullDay - 1].date).format('{weekday}')] + ')');
			}
		},
		initialize: function initialize() {
			var clickedDay = parseInt(this.model.get('clickedDay'));

			if (this.model.get('lockMode') === 'unlocked') {
				this.model.set({
					nextFullDay: clickedDay + 1,
					fromDay:     clickedDay
				});
			}
			this.subViews = [];
			this.model.set({user_watch: 'day'});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.WEEKDAY = this.model.WEEKDAY;
			extModel.COLOR = this.model.COLOR;

			this.$el.html(template(extModel));

			this.afterRender();

			return this;
		},
		afterRender: function afterRender() {
			var hall = _.find(reqres.request('get:hallsWeek').models, function(hallWeek) {
					return parseInt(hallWeek.get('number')) === parseInt(this.model.get('current_hall_number'));
				}, this),
				hallDayView = new window.app.HallDayView({
					model: hall
				});

			this.subViews.push(new window.app.DayScheduleReleasesListView({collection: reqres.request('get:weekscheduleReleasesList'), el: this.$('.list_films')}), hallDayView);
			this.$('.day_schedule_content').append(hallDayView.render().el);
			this.$('.day_schedule_content, .list_films').height($('.sidebar').height() - 136);
			this.$('.wr_release_day').height($('.sidebar').height() - 186);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			Backbone.trigger('remove:tipsy');
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

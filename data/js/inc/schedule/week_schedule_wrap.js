$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.WeekScheduleWrapView = Backbone.View.extend({
		id: 'week_wr',
		className: 'week_schedule',
		template: '#week_schedule_wrap',
		initialize: function initialize(options) {
			this.options = options || {};
			this.subViews = [];

			if (!window.app.App.weekSchedule) {
				window.app.App.weekSchedule = new window.app.WeekSchedule();
				window.app.App.weekScheduleReleasesList = new window.app.WeekScheduleReleasesListCollection();
				window.app.App.hallsWeek = new window.app.HallsWeek(window.app.App.weekSchedule);
			}

			this.model = window.app.App.weekSchedule;
			this.releasesList = window.app.App.weekScheduleReleasesList;

			this.model.set({user_watch: this.options.user_watch});

			this.listenTo(this.model, 'sync', function() {
				this.$('.schedule_blocker').hide();
			});

			this.listenTo(this.model, 'request', function() {
				this.$('.schedule_blocker').show();
			});

			this.listenTo(Backbone, 'weekschedule:message',           this.createMessage);
			this.listenTo(Backbone, 'weekschedule:export',            this.createExportPopup);
			this.listenTo(Backbone, 'weekschedule:timeline:open',     this.openTimeline);
			this.listenTo(Backbone, 'weekschedule:day-popup:open',    this.openDayPopup);
			this.listenTo(Backbone, 'weekschedule:week:open',         this.openWeek);
			this.listenTo(Backbone, 'weekschedule:report:open',       this.openReport);
			this.listenTo(Backbone, 'weekschedule:hall:copy:open',    this.copyHallOpen);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model);

			extModel.userType = reqres.request('get:user').get('type');
			extModel.currentCinema = reqres.request('get:user').getCinema();

			this.$el.html(template(extModel));

			if (this.model.get('schedule')) {
				this.afterRender();
			} else {
				this.model.setDateParameters(moment());
				this.listenToOnce(this.model, 'sync', this.afterRender);
			}

			return this;
		},
		afterRender: function afterRender() {
			var self = this;

			if (this.model.get('user_watch') === 'report') {
				this.openReport();
			} else if (this.model.get('user_watch') === 'timeline') {
				this.openTimeline();
			} else {
				this.openWeek();
			}

			if (this.model.get('user_watch') !== 'report') {
				this.subViews.push(new window.app.WeekScheduleReleasesListView({collection: this.releasesList, el: this.$('.list_films')}));

				this.$('.item_week').scroll(function() {
					self.scrollFunction();
				});
			}
		},
		scrollFunction: function scrollFunction() {
			var self = this;

			this.$('.ui-draggable-dragging').css({
				'margin-top': this.$('.item_week').scrollTop() - this.$('.ui-draggable-dragging').data('startScroll')
			});

			this.$('.rel').addClass('hover_off');

			setTimeout(function() {
				self.$('.rel').removeClass('hover_off');
			}, 300);
		},
		createMessage: function createMessage(message) {
			this.$('.week_popup').html('<span class="message">' + message + '</span><br><span class="done">Ок</span>');

			this.$('.week_popup').arcticmodal({
				beforeOpen: function(data, el) {
					el.addClass('message_schedule');
				},
				afterOpen: function(data, el) {
					el.find('.done').click(function() {
						$.arcticmodal('close');
					});
				},
				afterClose: function(data, el) {
					el.empty().removeClass('message_schedule');
				}
			});
		},
		copyHallOpen: function copyHallOpen(week, hallId, year, singleDate) {
			var view = new window.app.copyingScheduleCinemasWrapperView({
					week: week,
					hallId: hallId,
					singleDate: singleDate,
					year: year
				});

			this.$('.week_popup').prepend(view.render().$el);

			view.$el.arcticmodal({
				beforeOpen: function(data) {
					$(data.body).addClass('center');
				},
				afterClose: function() {
					view.remove();
				}
			});
		},
		openDayPopup: function openDayPopup() {
			var self = this,
				view = new window.app.DayScheduleView({model: this.model});

			this.$('.week_popup').prepend(view.render().$el);

			this.$('.week_popup').arcticmodal({
				beforeClose: function() {
					if (!reqres.request('get:user').isReadOnly()) {
						Backbone.trigger('hallDay:saveSeances');
					}
				},
				afterClose: function() {
					view.remove();
					self.model.set({user_watch: self.model.previous('user_watch')});
				}
			});
		},
		createExportPopup: function createExportPopup() {
			var self = this;
			this.exportWeekSchedule = new window.app.ExportWeekScheduleView({date: this.model.get('date')});

			this.$('.week_popup').prepend(this.exportWeekSchedule.render().$el);
			/* istanbul ignore next */
			this.$('.week_popup').arcticmodal({
				beforeOpen: function(data, el) {
					el.addClass('export_schedule');
				},
				afterClose: function(data, el) {
					el.removeClass('export_schedule');
					self.exportWeekSchedule.remove();
				}
			});
		},
		openTimeline: function openTimeline() {
			window.app.controller.navigate('/schedule/timeline', {trigger: false});

			var view = new window.app.TimelineView({model: this.model});
			this.$('.item_week').html(view.render().el).addClass('gray');
			this.subViews.push(view);
		},
		openWeek: function openWeek() {
			window.app.controller.navigate('/schedule', {trigger: false});

			var view = new window.app.WeekScheduleView({model: this.model});
			this.$('.item_week').html(view.render().el).removeClass('gray');
			this.subViews.push(view);
		},
		openReport: function openReport() {
			window.app.controller.navigate('/schedule/report', {trigger: false});
			_.invoke(this.subViews, 'remove');
			this.$('.item_week, .list_films').remove();

			var view = new window.app.ReportScheduleView({model: this.model});
			this.$el.append(view.render().el);
			this.subViews.push(view);
		},
		remove: function remove() {
			this.$('.item_week').unbind('scroll');
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

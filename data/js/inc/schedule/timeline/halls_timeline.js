$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.HallTimelineView = Backbone.View.extend({
		className: 'films_timeline',
		template: '#hall_timeline_tmpl',
		events: {
			'mouseover .film_item_timeline': function(e) {
				if (reqres.request('get:user').isReadOnly()) {
					$(e.currentTarget).css('cursor', 'default');
					$(e.currentTarget).find('.remove').hide();
				}
			},
			'mousedown .remove': function(event) {
				if (reqres.request('get:user').isReadOnly()) return;
				_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Удаление']);

				Backbone.trigger('timeline:release-block:hide');
				this.removeDragBox();

				$(event.currentTarget).closest('.film_item_timeline')
					.prev().remove().end()
					.next().remove().end()
					.remove();

				commands.execute('set:timeline:markersPosition');
				this.buildingFilmsBlock();
				this.saveSeances();
			},
			mousedown: function() {
				this.canRemoveDragBox = false;
			},
			mouseup: function() {
				this.canRemoveDragBox = true;
			},
			'click .film_item_timeline': 'createDragBox',
			'mouseenter .film_item_timeline': 'createDragBox',
			'mouseleave .film_item_timeline': 'removeDragBox',
			'mousedown .film_item_timeline': 'detectCursorPosition'
		},
		initialize: function initialize(options) {
			this.options          = options || {};
			this.subViews         = [];
			this.dragBox          = 'block';
			this.canRemoveDragBox = true;

			this.listenTo(this.model.collection, 'halls-timeline:drag-one-film:enable', function() {
				this.dragBoxSwitch('oneFilm');
			});

			this.listenTo(this.model.collection, 'halls-timeline:drag-one-film:disable', function() {
				this.dragBoxSwitch('block');
			});

			this.listenTo(this.model, 'hall:day-popup:change', function() {
				this.render();
				commands.execute('set:timeline:markersPosition');
			});
		},
		render: function render() {
			var self     = this,
				template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template(this.model.attributes));

			this.afterRender();
			this.drop();

			setTimeout(function() { // Починка длительностей timeline
				self.buildingFilmsBlock();

				if (!self.$el.find('.changed_duration').length) {
					$('#timeline .repair_changed_films[hall_number="' + self.model.get('number') + '"]').hide();
				} else {
					$('#timeline .repair_changed_films[hall_number="' + self.model.get('number') + '"]').click(function(e) {
						var scaleSegment = self.model.collection.weekSchedule.get('scaleSegment'),
							needAlert = false;

						$(e.currentTarget).hide();

						self.$('.changed_duration').each(function(id, changedFilm) {
							var curRelId = $(changedFilm).attr('release_id'),
								actualDuration = $('#week_wr .item_film[release_id=' + curRelId + ']').attr('duration'),
								filmWidth,
								filmActualWidth,
								filmWidthDif,
								filmStart,
								end,
								filmEnd,
								step;

							if (actualDuration === '') {
								needAlert = true;
							} else {
								$(changedFilm).removeClass('changed_duration');
								$(changedFilm).attr({
									duration: actualDuration
								});

								filmWidth       = $(changedFilm).width();
								filmActualWidth = self.roundFiveFold(parseInt($(changedFilm).attr('duration')), scaleSegment);
								filmWidthDif    = filmActualWidth - filmWidth;
								filmStart       = window.app.App.weekSchedule.timeToMinutes($(changedFilm).find('.film_start_timeline').text());
								end             = Math.ceil(((filmStart + parseInt($(changedFilm).attr('duration')) + parseInt($(changedFilm).attr('ad'))) / 5) * 5);
								filmEnd         = window.app.App.weekSchedule.minutesToTime(filmStart + parseInt($(changedFilm).attr('duration')) + parseInt($(changedFilm).attr('ad')));
								step            = (filmWidthDif / scaleSegment) * 5;

								$(changedFilm)
									.attr({
										end: end
									})
									.css({
										width: filmActualWidth
									})
									.find('.film_end_timeline').text(filmEnd);

								if (step) {
									$(changedFilm).nextUntil('.half_films_timeline').each(function(id, el) {
										$(el).css({
											left: $(el).position().left + filmWidthDif
										});
										self.calcFilmsTime(el, step);
									});
								}
							}
						});

						self.saveSeances();

						if (needAlert) {
							$(e.currentTarget).show();
							Backbone.trigger('weekschedule:message', 'У некоторых релизов отсутствует длительность');
						}
					});
				}
			}, 0);

			return this;
		},
		afterRender: function afterRender() {
			var self = this,
				releaseAttributes;

			_.each(self.model.get('seances')[self.model.get('chosen_day')].releases, function(release) {
				releaseAttributes = self.model.parseRelease(release);

				var releaseHallTimelineVew = new window.app.ReleaseHallTimelineView({
					release:    release,
					model:      self.model,
					attributes: releaseAttributes
				});

				self.$el.find('.half_films_timeline').before(releaseHallTimelineVew.render().el);

				self.subViews.push(releaseHallTimelineVew);
			});
		},
		buildingFilmsBlock: function buildingFilmsBlock() {
			var self         = this,
				weekSchedule = reqres.request('get:weekschedule'),
				$films       = this.$('.film_item_timeline'),
				scaleSegment = weekSchedule.get('scaleSegment');

			self.$el.find('.ad_timeline, .pause_timeline').remove();

			$films.each(function(id, film) {
				var ad               = parseInt($(film).attr('ad')),
					adDuration       = self.roundFiveFold($(film).attr('ad'), scaleSegment),
					filmItemDuration = parseInt($(film).attr('duration')),
					filmItemStart    = self.model.collection.weekSchedule.timeToMinutes($(film).attr('start')),
					filmItemEnd      = filmItemStart + filmItemDuration + ad,
					$prevFilm        = id ? $films.eq(id - 1) : [],
					prevFilmEnd      = id ? parseInt($prevFilm.attr('end')) : 0,
					prevFilmPause    = id ? parseInt($prevFilm.attr('pause')) : 0,
					end              = id ? prevFilmEnd + prevFilmPause + ad + filmItemDuration : filmItemEnd,
					pauseDuration    = ($(film).attr('pause') / 5) * scaleSegment,
					nextFilmStart    = weekSchedule.minutesToTime(self.roundFiveFold(end + parseInt($(film).attr('pause'))));

				end              = self.roundFiveFold(end);
				filmItemEnd      = weekSchedule.minutesToTime(filmItemEnd); // Переводим время конца фильма в формат НН:мм
				filmItemDuration = self.roundFiveFold(filmItemDuration, scaleSegment);

				$films.eq(id + 1).attr('start', nextFilmStart);
				$films.eq(id + 1).find('.film_start_timeline').text(nextFilmStart);

				if (id) {
					filmItemStart = parseInt($prevFilm.css('left')) + $prevFilm.width() + adDuration + $prevFilm.next().width();
				} else {
					filmItemStart = (filmItemStart / 5) * scaleSegment + adDuration - scaleSegment * 8 * 12; // Шкала времени начинается с 8 утра
				}

				$(film)
					.css({
						width: filmItemDuration,
						left: filmItemStart
					})
					.attr({
						end: end
					})
					.before('<div class="ad_timeline" title="' + $(film).data('adTitle') + '" style="width:' + adDuration + 'px; left:' + (filmItemStart - adDuration) + 'px;"></div>')
					.after('<div class="pause_timeline" style="width:' + pauseDuration + 'px; left:' + (filmItemStart + filmItemDuration) + 'px;"></div>')
					.find('.film_end_timeline').text(filmItemEnd);

				$(film).prev().tipsy({
					html:    true,
					gravity: 'e',
					delayIn: 300
				});

				$(film).find('.film_time_wrap').attr('title', $(film).attr('start') + ' - ' + filmItemEnd);
			});

			this.pauseDrop();
		},
		detectCursorPosition: function detectCursorPosition(e) {
			if (!$(e.target).hasClass('remove')) {
				var self = this,
					$curFilm = $(e.currentTarget);

				setTimeout(function() {
					$curFilm.trigger('click');

					self.options.parentView.$el
						.find('.chosen_ad').removeClass('chosen_ad').end()
						.find('.chosen_film').removeClass('chosen_film').end()
						.find('.prev_film').removeClass('prev_film').end();

					$curFilm.addClass('chosen_film');

					$curFilm.closest('.films_drag_box').prevAll('.film_item_timeline:first').addClass('prev_film');

					commands.execute('timeline:releaseBlock:showChosenFilm', self);
					commands.execute('set:timeline:markersPosition');

					$(document).unbind('keydown');
					$(document).unbind('keyup');
					$(document).keydown(function(e) {
						if (!reqres.request('get:user').isReadOnly()) {
							if (e.keyCode === 39) {
								e.preventDefault();
								self.moveRight();
							}

							if (e.keyCode === 37) {
								e.preventDefault();
								self.moveLeft();
							}

							if (e.keyCode === 40) {
								e.preventDefault();
								self.switchDown();
							}

							if (e.keyCode === 38) {
								e.preventDefault();
								self.switchTop();
							}
						}
					});

					$(document).keyup(function(e) {
						if (reqres.request('get:user').isReadOnly()) return;
						if (e.keyCode === 39 || e.keyCode === 37) {
							e.preventDefault();
							self.saveSeances();
						}
					});
				}, 0);
			}
		},
		moveRight: function moveRight() {
			var self          = this,
				$chosenFilm   = this.options.parentView.$('.chosen_film'),
				scaleSegment  = this.model.collection.weekSchedule.get('scaleSegment'),
				prevFilmPause = parseInt(this.options.parentView.$('.prev_film').attr('pause')),
				lastFilmEnd   = this.model.collection.weekSchedule.timeToMinutes($chosenFilm.closest('.films_timeline').find('.film_item_timeline:last .film_end_timeline').text());

			if (lastFilmEnd + 5 > 1860) {
				this.saveSeances();
				Backbone.trigger('weekschedule:message', 'Конец показа релиза затрагивает начало следующего дня');
			} else {
				this.removeDragBox();

				$chosenFilm.prev().nextUntil('.half_films_timeline').andSelf().each(function(id, el) {
					$(el).css({
						left: $(el).position().left + scaleSegment
					});
				});

				if (!$chosenFilm.closest('.films_timeline').find('.film_item_timeline:first').hasClass('chosen_film')) {
					$chosenFilm.prevAll('.pause_timeline:first').width($chosenFilm.prevAll('.pause_timeline:first').width() + scaleSegment);

					this.options.parentView.$('.prev_film').attr({
						pause: prevFilmPause + 5
					});
				}

				$chosenFilm.nextUntil('.half_films_timeline').andSelf().each(function(id, el) { // Пересчет времени фильмов
					self.calcFilmsTime(el, 5);
				});

				commands.execute('timeline:releaseBlock:showChosenFilm', self);
				commands.execute('set:timeline:markersPosition');
			}
		},
		moveLeft: function moveLeft() {
			var self            = this,
				prevFilmPause   = parseInt(this.options.parentView.$('.prev_film').attr('pause')),
				$chosenFilm     = this.options.parentView.$('.chosen_film'),
				scaleSegment    = this.model.collection.weekSchedule.get('scaleSegment'),
				chosenFilmStart = this.model.collection.weekSchedule.timeToMinutes($chosenFilm.attr('start'));

			if (chosenFilmStart - 5 < 480) {
				this.saveSeances();
				Backbone.trigger('weekschedule:message', 'Начало показа релиза затрагивает конец предыдущего дня');
			} else if (prevFilmPause !== 5) {
				this.removeDragBox();

				$chosenFilm.prev().nextUntil('.half_films_timeline').andSelf().each(function(id, el) {
					$(el).css({
						left: $(el).position().left - scaleSegment + 'px'
					});
				});

				$chosenFilm.prevAll('.pause_timeline:first').width($chosenFilm.prevAll('.pause_timeline:first').width() - scaleSegment);
				this.options.parentView.$('.prev_film').attr({
					pause: prevFilmPause - 5
				});

				$chosenFilm.nextUntil('.half_films_timeline').andSelf().each(function(id, el) { // Пересчет времени фильмов
					self.calcFilmsTime(el, -5);
				});
			} else if (!$chosenFilm.closest('.films_timeline').find('.film_item_timeline:first').hasClass('chosen_film')) {
				this.removeDragBox();

				this.options.parentView.$('.chosen_film').removeClass('chosen_film');
				this.options.parentView.$('.prev_film').addClass('chosen_film').removeClass('prev_film');
				this.options.parentView.$('.chosen_film').prevAll('.film_item_timeline:first').addClass('prev_film');
			}

			commands.execute('timeline:releaseBlock:showChosenFilm', self);
			commands.execute('set:timeline:markersPosition');
		},
		switchDown: function switchDown() {
			var $chosenFilm = this.options.parentView.$('.chosen_film'),
				$nextFilms  = $chosenFilm.closest('.films_timeline').next();

			while ($nextFilms.find('.film_item_timeline').length === 0 && $nextFilms.hasClass('films_timeline')) {
				$nextFilms = $nextFilms.next();
			}

			if ($chosenFilm.length && $nextFilms.find('.film_item_timeline').length) {
				$nextFilms.find('.film_item_timeline:first').trigger('mousedown');
			}
		},
		switchTop: function switchTop() {
			var $chosenFilm = this.options.parentView.$('.chosen_film'),
				$prevFilms  = $chosenFilm.closest('.films_timeline').prev();

			while ($prevFilms.find('.film_item_timeline').length === 0 && $prevFilms.hasClass('films_timeline')) {
				$prevFilms = $prevFilms.prev();
			}

			if ($chosenFilm.length && $prevFilms.find('.film_item_timeline').length) {
				$prevFilms.find('.film_item_timeline:first').trigger('mousedown');
			}
		},
		calcFilmsTime: function calcFilmsTime(el, step) {
			if (!($(el).hasClass('film_item_timeline'))) return;

			var filmEnd = this.model.collection.weekSchedule.timeToMinutes($(el).find('.film_end_timeline').text()) + step,
				filmStart = this.model.collection.weekSchedule.timeToMinutes($(el).find('.film_start_timeline').text()) + step;

			$(el).attr({end: this.roundFiveFold(filmEnd)});
			filmEnd     = this.model.collection.weekSchedule.minutesToTime(filmEnd);
			filmStart   = this.model.collection.weekSchedule.minutesToTime(filmStart);
			$(el)
				.attr({start: filmStart})
				.find('.film_end_timeline').text(filmEnd).end()
				.find('.film_start_timeline').text(filmStart).end()
				.find('.film_time_wrap').attr('title', filmStart + ' - ' + filmEnd);
		},
		createDragBox: function createDragBox(e) {
			this.options.parentView.$('.film_item_timeline[identifier="' + $(e.currentTarget).attr('identifier') + '"]').addClass('same_film');

			if (!this.options.parentView.$('.films_drag_box').length && !this.options.parentView.$('.films_timeline').hasClass('drop-hover') && !reqres.request('get:user').isReadOnly()) {
				var $curAd = $(e.currentTarget).closest('.film_item_timeline').prev(),
					startPosition = parseInt($curAd.css('left')),
					dragBoxWidth = 0,
					$dragBoxElements;

				if (this.dragBox === 'oneFilm') {
					if ($curAd.nextAll('.ad_timeline').length) {
						$dragBoxElements = $curAd.nextUntil('.ad_timeline').andSelf();
					} else {
						$dragBoxElements = $curAd.nextUntil('.half_films_timeline').andSelf();
					}
				} else {
					$dragBoxElements = $curAd.nextUntil('.half_films_timeline').andSelf();
				}

				$dragBoxElements.css({
					position: 'relative',
					left:     ''
				});

				$dragBoxElements.each(function(id, el) {
					dragBoxWidth = dragBoxWidth + $(el).width();
				});

				$dragBoxElements.wrapAll('<div class="films_drag_box"></div>');

				this.options.parentView.$('.films_drag_box').css({
					top:      '0px',
					display:  'inline-block',
					position: 'absolute',
					overflow: 'visible',
					left:     startPosition + 'px',
					cursor:   'move',
					height:   '54px',
					width:    dragBoxWidth + 'px'
				});

				this.dragTimeline();
			}

			if (e.type === 'click') commands.execute('set:timeline:advertizingVisibility', e, $(e.currentTarget));
		},
		removeDragBox: function removeDragBox() {
			this.options.parentView.$('.same_film').removeClass('same_film');

			if (!this.options.parentView.$('.films_drag_box').hasClass('ui-draggable-dragging') && this.options.parentView.$('.films_drag_box').length >= 1 && !reqres.request('get:user').isReadOnly()) {
				var $curAd = this.options.parentView.$('.films_drag_box').find('.ad_timeline:first'),
					startPosition = parseInt($curAd.closest('.films_drag_box').css('left'));

				$curAd.css({
					position: 'absolute',
					left: startPosition + 'px'
				});

				$curAd.nextUntil('.half_films_timeline').each(function(id, el) {
					var prevElWidth = parseInt($(el).prev().css('width')),
						prevElPosition = parseInt($(el).prev().css('left')),
						elPosition = prevElWidth + prevElPosition;

					$(el).css({
						position: 'absolute',
						left: elPosition + 'px'
					});
				});

				$curAd.unwrap();
			}
		},
		dragBoxSwitch: function dragBoxSwitch(mode) {
			var $dragBox    = this.$el.find('.films_drag_box'),
				$targetFilm = this.$el.find('.films_drag_box').find('.film_item_timeline:first');

			this.dragBox = mode;

			if ($dragBox.length === 1 && this.canRemoveDragBox) {
				this.removeDragBox();
				$targetFilm.trigger('click');
			}
		},
		calcDragBoxDuration: function calcDragBoxDuration(dragBox) {
			var self            = this,
				dragBoxDuration = 0;

			$(dragBox).find('.film_item_timeline').each(function() {
				dragBoxDuration = self.roundFiveFold(dragBoxDuration + parseInt($(this).attr('duration')) + parseInt($(this).attr('ad')) + parseInt($(this).attr('pause')));
			});

			return dragBoxDuration;
		},
		dragTimeline: function dragTimeline() {
			if (reqres.request('get:user').isReadOnly()) return;
			var self = this,
				scaleSegment = this.model.collection.weekSchedule.get('scaleSegment');

			this.$el.find('.films_drag_box').draggable({
				opacity: 0.7,
				zIndex: 10,
				grid: [scaleSegment, 55],
				scope: 'timeline',
				create: function() {
					self.startPositionX = parseInt($(this).css('left'));
					// self.startPositionY = parseInt($(this).css('top'));

					var lastFilmEnd = $(this).get(0).getBoundingClientRect().right,
						x1,
						x2,
						y1,
						y2;

					if ($(this).offset().left <= $('#hour08').closest('.hour_timeline').offset().left) {
						x1 = $(this).offset().left;
					} else {
						x1 = $('#hour08').closest('.hour_timeline').offset().left;
					}

					if (lastFilmEnd >= $('#right_timeline_border').offset().left) {
						x2 = $(this).offset().left;
					} else {
						x2 = $('#right_timeline_border').offset().left - $('.films_drag_box').width() + $('.films_drag_box').find('.pause_timeline:last').width();
					}

					y1 = self.options.parentView.$el.offset().top;
					y2 = self.options.parentView.$el.get(0).getBoundingClientRect().bottom - 55;

					$(this).draggable('option', 'containment', [x1, y1, x2, y2]);
				},
				start: function(event, ui) {
					ui.position.left = self.startPositionX;

					self.model.collection.weekSchedule.set({draggingHall: self});

					if ($(this).prevAll().length) {
						_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Перетягивание части блока']);
					} else {
						_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Перетягивание всего блока']);
					}

					commands.execute('set:timeline:advertizingVisibility', {}, ui.helper.find('.film_item_timeline').first());

					$(window).unbind('keydown');
					$(window).unbind('keyup');
				},
				drag: function(event, ui) {
					var currentPositionX = ui.position.left,
						startPositionX   = self.startPositionX,
						diffPositionX    = currentPositionX - startPositionX,
						scaleSegment     = self.model.collection.weekSchedule.get('scaleSegment'),
						pauseWidth       = $(this).find('.pause_timeline:first').width(),
						adWidth          = $(this).find('.ad_timeline:first').width(),
						$currentFilms    = self.options.parentView.$('.drop-hover'),
						step,
						$lastPause;

					if ($currentFilms.find('.films_drag_box').length) {
						$lastPause = $(this).prev();
					} else {
						$lastPause = $currentFilms.find('.pause_timeline:last');
					}

					if (diffPositionX) {
						step = (diffPositionX / scaleSegment) * 5;

						if (self.dragBox === 'block') {
							$lastPause.css({
								width: currentPositionX - parseInt($lastPause.css('left'))
							});

							$lastPause.prev().attr({pause: ($lastPause.width() / scaleSegment) * 5});
						}

						$(this).find('.film_item_timeline').each(function(filmId, film) { // Пересчет времени фильмов
							self.calcFilmsTime(film, step);
						});

						self.startPositionX = ui.position.left;

						commands.execute('set:timeline:markersPosition', {
							start: ui.position.left,
							end:   ui.position.left + $(this).find('.film_item_timeline:first').width() + pauseWidth + adWidth
						});
					}
				}
			});
		},
		createReleasesViews: function createReleasesViews(filmsDragBox) {
			var releasesViews = [],
				releaseAttributes,
				checkCopyAndKdm,
				releaseView,
				release;

			_.each($(filmsDragBox).find('.film_item_timeline'), function(film, id) {
				checkCopyAndKdm = this.model.checkCopyAndKdm($(film).attr('formats'), $(film).attr('release_id')),
				release = {
					has_copy: checkCopyAndKdm.has_copy,
					has_kdm:  checkCopyAndKdm.has_kdm,
					duration: parseInt($(film).attr('duration')),
					formats:  $(film).attr('formats'),
					upgrades: $(film).attr('upgrades'),
					pause:    $(film).attr('pause'),
					adParams: reqres.request('get:weekschedule').get('draggingHall') === this ? $(film).data('adParams') : this.model.getAdParams($(film).attr('formats'), $(film).attr('release_id')),
					ad:       parseInt($(film).attr('ad')),
					title:    $(film).find('.film_title_timeline').attr('release_title'),
					id:       $(film).attr('release_id'),
					start:    $(film).attr('start'),
					comment:  $(film).attr('comment'),
					chosen:   id === 0 ? 'chosen_film' : ''
				};

				releaseAttributes = this.model.parseRelease(release);

				releaseView = new window.app.ReleaseHallTimelineView({
					release:    release,
					model:      this.model,
					attributes: releaseAttributes
				});

				releasesViews.push(releaseView);
			}, this);

			return releasesViews;
		},
		roundFiveFold: function roundFiveFold(value, number) {
			var roundedMothod = reqres.request('get:weekschedule').get('schedule_rounding');
			return Math[roundedMothod](value / 5) * (number || 5);
		},
		drop: function drop() {
			var self         = this,
				weekSchedule = reqres.request('get:weekschedule');

			this.$el.droppable({
				tolerance: 'pointer',
				hoverClass: 'drop-hover',
				scope: 'timeline',
				accept: function(el) {
					return el.hasClass('item_film') || el.hasClass('films_drag_box');
				},
				over: function(event, ui) {
					var id = $(this).index(),
						$lastPause;

					$('#timeline .right_added_block').find('.added_films_timeline').eq(id).addClass('drop-hover');

					if (ui.helper.hasClass('films_drag_box') && self.dragBox === 'block') {
						if (reqres.request('get:weekschedule').get('draggingHall') === self) {
							$lastPause = ui.helper.prev();
						} else {
							$lastPause = $(this).find('.pause_timeline:last');
						}

						$lastPause.css({
							width: ui.position.left - parseInt($lastPause.css('left'))
						});
					}
				},
				out: function(event, ui) {
					var id = $(this).index(),
						$lastPause;

					$('#timeline .right_added_block').find('.added_films_timeline').eq(id).removeClass('drop-hover');

					if (ui.helper.hasClass('films_drag_box') && self.dragBox === 'block') {
						if (reqres.request('get:weekschedule').get('draggingHall') === self) {
							$lastPause = ui.helper.prev();
						} else {
							$lastPause = $(this).find('.pause_timeline:last');
						}

						$lastPause.css({
							width: 0
						});

						$lastPause.prev().attr({
							pause: 0
						});
					}
				},
				drop: function(event, ui) {
					$('#timeline .right_added_block').find('.added_films_timeline').eq($(this).index()).removeClass('drop-hover');

					var $dropEl           = $(this),
						hallId            = parseInt(self.model.get('id')),
						hallFormats       = self.model.get('formats'),
						uiPosition        = ui.position.left - 281,
						addedFilmStart    = $('#day_timeline_wr .position_time').text(),
						dropPosition      = false,
						cinemaSetting     = reqres.request('get:user').getCinemaSettings(),
						hallsSettings     = cinemaSetting ? cinemaSetting.halls.find(function(h) {return h.id === hallId;}) : '',
						checkCopyAndKdm   = self.model.checkCopyAndKdm(ui.helper.attr('formats'), ui.helper.attr('release_id')),
						release           = {
							has_copy: checkCopyAndKdm.has_copy,
							has_kdm:  checkCopyAndKdm.has_kdm,
							duration: parseInt(ui.helper.attr('duration')),
							formats:  ui.helper.attr('formats'),
							upgrades: ui.helper.attr('upgrades'),
							pause:    hallsSettings.defaultHallPause || 10,
							adParams: self.model.getAdParams(ui.helper.attr('formats'), ui.helper.attr('release_id')),
							ad:       parseInt($(ui.draggable).attr('ad')),
							title:    ui.helper.attr('title'),
							id:       ui.helper.attr('release_id'),
							start:    addedFilmStart
						},
						releaseHallTimelineVew,
						releaseAttributes,
						blockDuration,
						firstFilmStart,
						releasesViews,
						$mark;

					if (ui.helper.hasClass('films_drag_box')) {
						$(ui.helper).detach();

						$dropEl.find('.film_item_timeline').each(function(id, film) {
							if ($(film).position().left > ui.position.left) {  // Вставляем блок фильмов между фильмами
								$mark = $(film).prev();
								dropPosition  = true;
								blockDuration = self.calcDragBoxDuration(ui.helper);

								firstFilmStart = weekSchedule.timeToMinutes($(film).attr('start')) - blockDuration;
								firstFilmStart = weekSchedule.minutesToTime(firstFilmStart);

								return false;
							}
						});

						$(ui.helper).find('.film_item_timeline:first').attr('start', firstFilmStart);
						releasesViews = self.createReleasesViews(ui.helper);

						if (!dropPosition) $mark = $dropEl.find('.half_films_timeline');

						_.each(releasesViews, function(releaseView) {
							$mark.before(releaseView.render().el);
							self.subViews.push(releaseView);
						});

					} else if (!new RegExp(release.formats, 'i').test(hallFormats.join('')) || !release.formats) {
						Backbone.trigger('weekschedule:message', 'Зал не поддерживает формат данного релиза');
					} else if (!release.duration) {
						Backbone.trigger('weekschedule:message', 'У релиза нет длительности');
					} else { // Если добавление фильма идет из боковой колонки
						_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Добавление']);

						$dropEl.find('.film_item_timeline').each(function(id, film) {
							if ($(film).position().left > uiPosition) {  // Вставляем блок фильмов между фильмами
								$mark = $(film).prev();
								dropPosition  = true;
								blockDuration = self.roundFiveFold(release.duration + release.pause + release.ad);

								release.start = weekSchedule.timeToMinutes($(film).attr('start')) - blockDuration;
								release.start = weekSchedule.minutesToTime(release.start);

								return false;
							}
						});

						releaseAttributes = self.model.parseRelease(release);

						releaseHallTimelineVew = new window.app.ReleaseHallTimelineView({
							release:    release,
							model:      self.model,
							attributes: releaseAttributes
						});

						if (!dropPosition) $mark = $dropEl.find('.half_films_timeline');

						$mark.before(releaseHallTimelineVew.render().el);
					}

					$(ui.helper).remove();
					Backbone.trigger('timeline:release-block:hide');

					if (reqres.request('get:weekschedule').get('draggingHall') && reqres.request('get:weekschedule').get('draggingHall') !== self) reqres.request('get:weekschedule').get('draggingHall').buildingFilmsBlock();
					self.buildingFilmsBlock();

					self.options.parentView.$el.trigger('mouseenter');

					setTimeout(function() {
						var firstFilmStart = self.model.collection.weekSchedule.timeToMinutes($dropEl.find('.film_item_timeline:first').attr('start'), 'early'),
							lastFilmEnd = $dropEl.find('.film_item_timeline:last').attr('end');

						if (firstFilmStart < 480 || lastFilmEnd > 1860) {
							if (firstFilmStart < 480) {
								Backbone.trigger('weekschedule:message', 'Начало показа релиза затрагивает конец предыдущего дня');
							} else {
								Backbone.trigger('weekschedule:message', 'Конец показа релиза затрагивает начало следующего дня');
							}

							if (reqres.request('get:weekschedule').get('draggingHall') && reqres.request('get:weekschedule').get('draggingHall') !== self) reqres.request('get:weekschedule').get('draggingHall').render();
							self.render();
						} else {
							if (reqres.request('get:weekschedule').get('draggingHall') && reqres.request('get:weekschedule').get('draggingHall') !== self) reqres.request('get:weekschedule').get('draggingHall').saveSeances();
							self.saveSeances();
						}

						self.model.collection.weekSchedule.set({draggingHall: null});

						commands.execute('timeline:releaseBlock:showChosenFilm', self);
						commands.execute('set:timeline:markersPosition');
						commands.execute('set:timeline:advertizingVisibility', {}, $dropEl.find('.film_item_timeline').first());
					}, 0);
				}
			});
		},
		pauseDrop: function pauseDrop() {
			this.$el.find('.pause_timeline').droppable({
				tolerance: 'pointer',
				hoverClass: 'pause_hover',
				accept: function(el) {
					return el.hasClass('item_film') || el.hasClass('films_drag_box');
				}
			});
		},
		saveSeances: function saveSeances() {
			var self = this;

			Backbone.trigger('timeline:check-releases');
			Backbone.trigger('timeline:check-pauses');

			clearTimeout(self.timer);

			self.timer = setTimeout(function() {
				var seances      = self.model.get('seances'),
					chosen_day   = self.model.get('chosen_day'),
					nextFullDay  = self.model.get('nextFullDay') || 7,
					prevSeances  = JSON.stringify(self.model.get('seances')),
					prevSchedule = JSON.stringify(self.model.collection.weekSchedule.get('schedule')),
					i            = 0;

				_.each(seances, function(oneSeance) {
					if (i >= chosen_day && i < nextFullDay) {
						oneSeance.releases = [];
						self.$el.find('.film_item_timeline').each(function() {
							oneSeance.releases.push({
								id:       $(this).attr('release_id'),
								formats:  $(this).attr('formats'),
								title:    $(this).find('.film_title_timeline').attr('release_title'),
								upgrades: $(this).attr('upgrades'),
								duration: $(this).attr('duration'),
								start:    $(this).attr('start'),
								pause:    $(this).attr('pause'),
								ad:       $(this).attr('ad'),
								adParams: $(this).data('adParams') || null,
								has_copy: $(this).data('has_copy'),
								has_kdm:  $(this).data('has_kdm'),
								comment:  $(this).attr('comment')
							});
						});
					}
					i++;
				});

				$.ajax({
					url: '/api/schedule/week/v2/set',
					type: 'post',
					dataType: 'json',
					timeout: 5000,
					data: ({
						tx:        reqres.request('get:user').get('tx'),
						cinema_id: reqres.request('get:user').getCinema().id,
						year:      self.model.get('year'),
						week:      self.model.get('week'),
						hall_id:   self.model.get('id'),
						seance:    JSON.stringify(self.model.get('seances'))
					}),
					error: function() {
						self.model.set('seances', JSON.parse(prevSeances));
						self.model.collection.weekSchedule.set('schedule', JSON.parse(prevSchedule));
						Backbone.trigger('weekschedule:message', 'Проблемы с интернет соединением!');
						Backbone.trigger('HallsWeek:filled');
					}
				});
			}, 500);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.HallsTimelineView = Backbone.View.extend({
		events: {
			mouseenter: function() {
				var self = this;

				$(window).keydown(function(event) {
					if (event.keyCode === 18) {
						self.collection.trigger('halls-timeline:drag-one-film:enable');
					}
				});

				$(window).keyup(function(event) {
					if (event.keyCode === 18) {
						self.collection.trigger('halls-timeline:drag-one-film:disable');
					}
				});
			},
			mouseleave: function() {
				this.collection.trigger('halls-timeline:drag-one-film:disable');

				$(window).unbind('keydown');
				$(window).unbind('keyup');
			}
		},
		initialize: function initialize() {
			this.subViews = [];
			this.render();
			this.listenTo(Backbone, 'HallsWeek:filled', this.remove);
		},
		render: function render() {
			var self = this;

			this.collection.each(function(hallTimeline) {
				var hallTimelineView = new window.app.HallTimelineView({
					model: hallTimeline,
					parentView: self
				});
				self.$el.append(hallTimelineView.render().el);
				self.subViews.push(hallTimelineView);
			});

			return this;
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

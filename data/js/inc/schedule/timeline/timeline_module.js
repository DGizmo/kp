$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.TimelineModule = Backbone.Model.extend({
		defaults: {
			syncProgress: true
		},
		initialize: function initialize(id, week, year, filledPercent, freeTime) {
			var self = this;

			this.set({ // Для формирования урлы
				cinema_id: id,
				week:      week,
				year:      year,
				open:      true
			});

			this.fetch({
				success: function() {
					var user               = reqres.request('get:user'),
						curCinema          = user.getCinema(),
						cinemaSettings     = user.getCinemaSettings(),
						scheduleRounding   = cinemaSettings ? cinemaSettings.schedule_rounding : 'ceil';

					window.app.App.hallsTimelineModule     = new window.app.HallsTimelineModule();
					window.app.App.timelineModuleView      = new window.app.TimelineModuleView({
						hallsTimelineModule: window.app.App.hallsTimelineModule,
						model: self
					});
					window.app.App.hallsTimelineModuleView = new window.app.HallsTimelineModuleView({collection:window.app.App.hallsTimelineModule});

					window.app.App.hallsTimelineModule.schedule_rounding = scheduleRounding;

					window.app.App.hallsTimelineModule.add(curCinema.halls.sort(function(a, b) {
						return a.number - b.number;
					}));

					self.set({
						cinema_title:      curCinema.title.ru || curCinema.title.en,
						cinema_city:       curCinema.city.title.ru || curCinema.city.title.en,
						minutes_remaining: freeTime,
						filled_percent:    filledPercent
					});

					window.app.App.hallsTimelineModule.timelineModule = self;
					window.app.App.hallsTimelineModule.fillingHalls();
					$('.timeline_module_popup').prepend(window.app.App.timelineModuleView.render().$el);

					$('.timeline_module_popup').arcticmodal({ // TODO: сделать нормальный рендер
						beforeOpen: function() {
							$('.timeline_module_popup').css({
								opacity: 0
							});
						},
						afterOpen: function() {
							$('.timeline_module_popup').css({
								opacity: 1
							});
						},
						afterClose: function() {
							window.app.App.timelineModuleView.remove();

							if (window.app.App.hallsTimelineModule.scheduleEditSeances) {
								window.app.App.hallsTimelineModule.scheduleEditSeances.remove().$el.empty();
							}
							window.app.App.timelineModule.set({
								open: false
							});
						}
					});

					self.syncProgress = false;
				}
			});
		},
		url: function url() {
			return '/api/schedule/generate/?cinema_id=' + this.get('cinema_id') + '&week=' + this.get('week') + '&year=' + this.get('year');
		},
		parse: function parse(response) {
			return response.data;
		},
		timeToMinutes: function timeToMinutes(time, early) {
			if (time) {
				var parts = time.split(':');

				switch (parts[0]) {
					case '00':
						parts[0] = 24;
					break;
					case '01':
						parts[0] = 25;
					break;
					case '02':
						parts[0] = 26;
					break;
					case '03':
						parts[0] = 27;
					break;
					case '04':
						parts[0] = 28;
					break;
					case '05':
						if (early === 'early') {
							parts[0] = 5;
						} else {
							parts[0] = 29;
						}
					break;
					case '06':
						if (early === 'early') {
							parts[0] = 6;
						} else {
							parts[0] = 30;
						}
					break;
					case '07':
						if (early === 'early') {
							parts[0] = 7;
						} else {
							parts[0] = 31;
						}
					break;
				}

				return parseInt(parts[0]) * 60 + parseInt(parts[1]);
			}
		},
		minutesToTime: function minutesToTime(minutes) {
			var hours = Math.floor(minutes / 60),
				min = minutes - (hours * 60);

			if (hours >= 24) {
				hours = hours - 24;
			}

			if (hours < 10) {
				hours = '0' + hours;
			}

			if (min < 10) {
				min = '0' + min;
			}

			return hours + ':' + min;
		},
		update: function update() {
			if (!this.syncProgress) {
				var view = window.app.App.timelineModuleView,
					self = this;

				self.syncProgress = true;

				this.fetch({
					success: function() {
						var cinema_time = self.get('halls').length * 960,
							filled_time,
							minutes_remaining,
							filled_percent;

						if (self.get('duration')) {
							filled_time = self.get('duration');
						} else {
							filled_time = 0;
						}

						minutes_remaining = cinema_time - filled_time;
						filled_percent = Math.round((filled_time / cinema_time) * 100);

						self.set({
							minutes_remaining: minutes_remaining,
							filled_percent:    filled_percent
						});

						window.app.App.hallsTimelineModule.fillingHalls();
						view.render();

						self.syncProgress = false;
					}
				});
			}
		}
	});

	window.app.TimelineModuleView = Backbone.View.extend({
		tagName:  'div',
		id:       'timeline_module',
		template: '#timeline_module_tmpl',
		events: {
			'click .arcticmodal-close': function() {
				$.arcticmodal('close');
			},
			click: function(e) {
				if (window.app.App.hallsTimelineModule.scheduleEditSeances && !$(e.target).hasClass('film_item_timeline')) {
					window.app.App.hallsTimelineModule.scheduleEditSeances.remove().$el.empty();
				}
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.listenTo(Backbone, 'window-resized', this.windowResized);
		},
		windowResized: function windowResized() {
			var hallsCollection = window.app.App.hallsTimelineModule,
				timelineLength = $(document).width() - 170,
				hourLength = Math.floor(timelineLength / 240),
				minuteLength = hourLength,
				prevScaleSegment = hallsCollection.scaleSegment,
				widthDif;

			hallsCollection.scaleSegment = minuteLength;
			// Задаем размеры всех элементов в зависимости от полученных начальных данных
			// 1. Для шкалы времени
			$('#day_timeline_module_wr, #day_timeline_module_wr .day_timeline, #day_timeline_module_wr .films_timeline').css('width', (hourLength * 240) + 'px');
			$('#timeline_module .hour_timeline, #timeline_module .minute_timeline, #timeline_module .half_hour_timeline').css('width', (hourLength - 1) + 'px');

			widthDif = $(document).width() - $('#day_timeline_module_wr .day_timeline').width();

			$('#timeline_module .right_added_block').css({
				width: widthDif - 70 + 'px'
			});

			$('#timeline_module').css({
				width: widthDif + $('#day_timeline_module_wr').width() + 'px'
			});

			$('#halls_timeline_collection .film_item_timeline, #halls_timeline_collection .ad_timeline, #halls_timeline_collection .pause_timeline').each(function(id, el) {
				$(el).css({
					width: ($(el).width() / prevScaleSegment) * hallsCollection.scaleSegment,
					left:  ($(el).position().left / prevScaleSegment) * hallsCollection.scaleSegment
				});
			});

			$('#halls_timeline_collection .film_time_wrap, #halls_timeline_collection .film_title_timeline').each(function(id, el) {
				$(el).css({
					width: ($(el).width() / prevScaleSegment) * hallsCollection.scaleSegment
				});

			});

			$('#day_timeline_module_wr .position_marker_start, #day_timeline_module_wr .position_marker_end').css({
				left: '-81px'
			});
		},
		render: function render() {
			var self     = this,
				template = Backbone.TemplateCache.get(this.template);

			this.$el.css({
				opacity: 0
			});

			this.$el.html(template({
				hallsTimelineModule: this.options.hallsTimelineModule,
				model: this.model.attributes
			}));

			setTimeout(function() {
				// Задаем начальные данные в зависимости от масштаба
				var timelineLength = $(document).width() - 170,
					hourLength = Math.floor(timelineLength / 240),
					minuteLength = hourLength,
					dayTimelineHeight,
					widthDif;

				window.app.App.hallsTimelineModule.scaleSegment = minuteLength;
				// Задаем размеры всех элементов в зависимости от полученных начальных данных
				// 1. Для шкалы времени
				$('#day_timeline_module_wr, #day_timeline_module_wr .day_timeline, #day_timeline_module_wr .films_timeline').css('width', (hourLength * 240) + 'px');
				$('#timeline_module .hour_timeline, #timeline_module .minute_timeline, #timeline_module .half_hour_timeline').css('width', (hourLength - 1) + 'px');

				widthDif = $(document).width() - $('#day_timeline_module_wr .day_timeline').width();

				$('#timeline_module .right_added_block').css({
					width: widthDif - 70 + 'px'
				});

				$('#timeline_module').css({
					width: widthDif + $('#day_timeline_module_wr').width() + 'px'
				});
				// В зависимости от количества залов задаем размер видимой части скролла
				dayTimelineHeight = window.app.App.hallsTimelineModule.length * 55 + 26;

				self.$el.find('#day_timeline_module_wr, .left_added_block, .right_added_block').css('height', dayTimelineHeight + 'px');
			}, 0);

			this.$el.find('.day_timeline').append(window.app.App.hallsTimelineModuleView.render().el);

			setTimeout(function() {
				// window.app.App.hallsTimelineModule.checkPauseLayout();

				self.$el.css({
					opacity: 1
				});
			}, 0);

			return this;
		}
	});
});

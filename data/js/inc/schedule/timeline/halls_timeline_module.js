$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.HallTimelineModule = Backbone.Model.extend({
		defaults: {},
		initialize: function initialize() {}
	});

	window.app.HallTimelineModuleView = Backbone.View.extend({
		className: 'films_timeline',
		template: '#hall_timeline_module_tmpl',
		events: {
			'mouseenter .film_item_timeline': function(e) {
				var currentIdentifier = $(e.currentTarget).attr('identifier');
				$('#halls_timeline_collection .film_item_timeline[identifier="' + currentIdentifier + '"]').addClass('same_film');
			},
			'mouseleave .film_item_timeline': function() {
				$('#halls_timeline_collection .same_film').removeClass('same_film');
			},
			'click .film_item_timeline': function(e) {
				e.stopPropagation();

				var self              = this,
					releaseId         = parseInt($(e.currentTarget).attr('release_id')),
					releaseIdentifier = $(e.currentTarget).attr('identifier'),
					cinemaId          = parseInt(this.model.collection.timelineModule.get('cinema_id')),
					week              = this.model.collection.timelineModule.get('week'),
					year              = this.model.collection.timelineModule.get('year'),
					$curFilm          = $(e.currentTarget);

				if (this.model.collection.scheduleEditSeances) {
					this.model.collection.scheduleEditSeances.remove();
				}

				$.ajax({
					url: '/api/schedule/get/',
					type: 'get',
					dataType: 'json',
					data: ({
						cinema_id:  cinemaId,
						release_id: releaseId,
						week:       week,
						year:       year
					}),
					success: function(data) {
						var halls = reqres.request('get:user').getCinema().halls,
							technology = data.weeks[0].technology,
							formatIndex,
							i,
							technologyIdentifier;

						for (i in technology) {
							technologyIdentifier = data.release_id + technology[i].format.toUpperCase() + technology[i].upgrades.join(', ').toUpperCase();

							if (technologyIdentifier === releaseIdentifier) {
								formatIndex = i;
							}
						}

						self.model.collection.scheduleEditSeances = new window.app.ScheduleEditSeancesView({
							technology: technology,
							format: $curFilm.attr('formats').toLowerCase(),
							upgrades: $curFilm.attr('upgrades').toLowerCase().split(', '),

							tx: reqres.request('get:user').get('tx'),
							date: {
								week: week,
								year: year
							},

							halls: halls,
							cinema_id: cinemaId,
							release_id: releaseId,

							parent: self,
							saveOnChange: true

						});

						$curFilm.closest('.arcticmodal-container_i2').append(self.model.collection.scheduleEditSeances.render().el).find('.halls-popup-top input').focus().select();
						self.model.collection.scheduleEditSeances.$el.css({
							top: parseInt($curFilm.offset().top) - 20,
							left: parseInt($curFilm.offset().left) + $curFilm.width(),
							boxShadow: '0 0 10px rgba(0,0,0,0.5)'
						});

						_gaq.push(['_trackEvent', 'Репертуарное планирование', 'Предварительное расписание', 'Открытие окна росписи']);
					},
					error: function() {
						alert('Проблемы с интернет соединением!');
					}
				});
			}
		},
		initialize: function initialize() {
			var self = this;

			this.listenToOnce(Backbone, 'technology:save:done', function() {
				this.remove();
				self.model.collection.timelineModule.update();
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				self = this;

			this.$el.css('opacity', 0);
			this.$el.html(template(this.model.attributes));

			setTimeout(function() {
				self.buildingFilmsBlock();
				self.$el.css('opacity', 1);
			}, 0);

			return this;
		},
		buildingFilmsBlock: function buildingFilmsBlock() {
			var self = this,
				$films = self.$el.find('.film_item_timeline'),
				scaleSegment = self.model.collection.scaleSegment;

			self.$el.find('.ad_timeline, .pause_timeline').remove();

			$films.each(function(id, film) {
				var ad               = parseInt($(film).attr('ad')),
					adDuration       = ($(film).attr('ad') / 5) * scaleSegment,
					filmItemDuration = parseInt($(film).attr('duration')),
					filmItemStart    = self.model.collection.timelineModule.timeToMinutes($(film).attr('start')),
					filmItemEnd      = filmItemStart + filmItemDuration,
					end,
					$prevFilm,
					pauseDuration,
					disabledAd,
					nextFilmStart,
					prevFilmEnd,
					prevFilmPause;

				if (id !== 0) {
					$prevFilm = $films.eq(id - 1);

					prevFilmEnd   = parseInt($prevFilm.attr('end'));
					prevFilmPause = parseInt($prevFilm.attr('pause'));

					if (self.model.collection.schedule_rounding === 'ceil') {
						end = Math.ceil((prevFilmEnd + prevFilmPause + ad + filmItemDuration) / 5) * 5;
					} else {
						end = Math.floor((prevFilmEnd + prevFilmPause + ad + filmItemDuration) / 5) * 5;
					}
				} else {
					if (self.model.collection.schedule_rounding === 'ceil') {
						end = Math.ceil((filmItemEnd + ad) / 5) * 5;
					} else {
						end = Math.floor((filmItemEnd + ad) / 5) * 5;
					}
				}

				pauseDuration = ($(film).attr('pause') / 5) * scaleSegment;
				disabledAd = '';
				nextFilmStart = self.model.collection.timelineModule.minutesToTime(end + parseInt($(film).attr('pause')));

				$films.eq(id + 1).attr('start', nextFilmStart);
				$films.eq(id + 1).find('.film_start_timeline').text(nextFilmStart);

				filmItemEnd = self.model.collection.timelineModule.minutesToTime(filmItemEnd + parseInt($(film).attr('ad'))); // Переводим время конца фильма в формат НН:мм

				if (self.model.collection.schedule_rounding === 'ceil') {
					filmItemDuration = Math.ceil(filmItemDuration / 5) * scaleSegment; // Округляем продолжительность фильма
				} else {
					filmItemDuration = Math.floor(filmItemDuration / 5) * scaleSegment; // Округляем продолжительность фильма
				}

				if (id === 0) {
					filmItemStart = (filmItemStart / 5) * scaleSegment + adDuration - scaleSegment * 8 * 12; // Шкала времени начинается с 8 утра

					$(film).css({
						width: filmItemDuration,
						left: filmItemStart
					});
				} else {
					filmItemStart = parseInt($prevFilm.css('left')) + $prevFilm.width() + adDuration + $prevFilm.next().width();

					$(film).css({
						width: filmItemDuration,
						left: filmItemStart
					});
				}

				$(film).attr({
						end: end
					})
					.before('<div class="ad_timeline ' + disabledAd + '" style="width:' + adDuration + 'px; left:' + (filmItemStart - adDuration) + 'px;"></div>')
					.after('<div class="pause_timeline" style="width:' + pauseDuration + 'px; left:' + (filmItemStart + filmItemDuration) + 'px;"></div>')
					.find('.film_end_timeline').text(filmItemEnd);

				$(film).find('.film_time_wrap').attr('title', $(film).attr('start') + ' - ' + filmItemEnd);
			});
		}
	});

	window.app.HallsTimelineModule = Backbone.Collection.extend({
		model: window.app.HallTimelineModule,
		fillingHalls: function fillingHalls() {
			var halls    = this.timelineModule.get('halls'),
				cinemaId = parseInt(this.timelineModule.get('cinema_id'));

			this.each(function(hallTimelineModule) {
				var seances       = [],
					hallId        = parseInt(hallTimelineModule.get('id')),
					cinemaSetting = reqres.request('get:user').getCinemaSettings(cinemaId) || false,
					hallsSettings = cinemaSetting ? cinemaSetting.halls.find(function(h) {return h.id === hallId;}) : '',
					i;

				seances.push({releases: []});

				for (i in halls) {
					if (halls[i].id === hallId && halls[i].seances[0]) {
						seances = halls[i].seances;
					}
				}

				hallTimelineModule.set({
					days_week:    this.timelineModule.get('days_week'),
					week:         this.timelineModule.get('currentWeek'),
					year:         this.timelineModule.get('currentYear'),
					seances:      seances,
					defaultAd:    cinemaSetting ? cinemaSetting.defaultCinemaAd : 0,
					defaultPause: hallsSettings.defaultHallPause || 10,
					defaultStart: hallsSettings.defaultHallOpen || '10:00'
				});
			}, this);
		},
		checkPauseLayout: function checkPauseLayout() { // Проверка совпадения пауз
			var starts = _.uniq($('#halls_timeline_collection .film_item_timeline').map(function() {
					return $(this).attr('start');
				})),
				ends = _.uniq($('#halls_timeline_collection .film_item_timeline').map(function() {
					return $(this).attr('end');
				}));

			_.each(starts, function(oneStart) {
				var $curFilm = $('#halls_timeline_collection .film_item_timeline[start="' + oneStart + '"]');

				if ($curFilm.length > 1) {
					$curFilm.each(function(id, film) {
						$(film).find('.film_start_timeline').addClass('red_time');
						$(film).prev().addClass('matched_ad');
						$(film).prev().prev().addClass('red_pause');
					});
				} else {
					$curFilm.find('.film_start_timeline').removeClass('red_time');
					$curFilm.prev().removeClass('matched_ad');
					$curFilm.prev().prev().removeClass('red_pause');
				}
			});

			_.each(ends, function(oneEnd) {
				var $curFilm = $('#halls_timeline_collection .film_item_timeline[end="' + oneEnd + '"]');

				if ($curFilm.length > 1) {
					$curFilm.each(function(id, film) {
						$(film).find('.film_end_timeline').addClass('red_time');
						$(film).next().addClass('red_pause');
					});
				} else {
					if (!$curFilm.next().next().hasClass('matched_ad')) {
						$curFilm.next().removeClass('red_pause');
					}
					$curFilm.find('.film_end_timeline').removeClass('red_time');
				}
			});
		}
	});

	window.app.HallsTimelineModuleView = Backbone.View.extend({
		id: 'halls_timeline_collection',
		events: {},
		initialize: function initialize() {},
		render: function render() {
			this.$el.empty();

			this.collection.each(function(hallTimelineModule) {
				var hallTimelineModuleView = new window.app.HallTimelineModuleView({
					model: hallTimelineModule
				});
				this.$el.append(hallTimelineModuleView.render().el);
			}, this);

			return this;
		}
	});
});

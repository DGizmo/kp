$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReleaseBlockTimelineView = Backbone.View.extend({
		className: 'release_block',
		template: '#release_block_timeline',
		chosenFilm: {
			adStart: '',
			adEnd:   '',
			adDuration: '',
			hallTitle: '',
			filmTitle: '',
			filmShow: '',
			filmDuration: '',
			pauseStart: '',
			pauseEnd: '',
			pauseDuration: ''
		},
		events: {
			'click .ad_pl, .ad_mi': function changeAd(event) {
				_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Реклама']);

				var $chosenFilm   = this.hallView.$('.chosen_film'),
					minutesChange = $(event.currentTarget).data('minutes-change'),
					lastFilmEnd   = this.hallView.$('.film_item_timeline:last').attr('end');

				if ($chosenFilm.data('minAdDuration') > this.chosenFilm.adDuration + minutesChange) {
					Backbone.trigger('weekschedule:message', 'Нельзя выставить длительность рекламного блока меньше длительности запланированной рекламы.');
				} else if (lastFilmEnd === '1860' && minutesChange > 0) {
					Backbone.trigger('weekschedule:message', 'Конец показа релиза затрагивает начало следующего дня');
				} else {
					$chosenFilm.attr({
						ad: this.chosenFilm.adDuration + minutesChange
					});
					this.update();
				}
			},
			'click .pause_pl, .pause_mi': function changePause(event) {
				_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Перерыв']);

				var $chosenFilm   = this.hallView.$('.chosen_film'),
					minutesChange = $(event.currentTarget).data('minutes-change'),
					lastFilmEnd   = this.hallView.$('.film_item_timeline:last').attr('end');

				if (lastFilmEnd === '1860' && minutesChange > 0) {
					Backbone.trigger('weekschedule:message', 'Конец показа релиза затрагивает начало следующего дня');
				} else {
					$chosenFilm.attr({
						pause: this.chosenFilm.pauseDuration + minutesChange
					});
					this.update();
				}
			},
			'click .delete_film': function deleteFilm() {
				this.hallView.$('.chosen_film').find('.remove').trigger('mousedown');
				commands.execute('set:timeline:markersPosition');
			}
		},
		initialize: function initialize() {
			commands.setHandlers({
				'timeline:releaseBlock:showChosenFilm': {
					callback: function(hallTimelineView) {
						this.hallView = hallTimelineView;
						this.showChosenFilm();
					},
					context: this
				}
			});

			if (reqres.request('get:user').isReadOnly()) this.$el.addClass('no-rights');

			this.render();
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.chosenFilm));
			return this;
		},
		showChosenFilm: function showChosenFilm() {
			if (!this.hallView.$('.chosen_film').length) return;

			var $chosenFilm   = this.hallView.$('.chosen_film'),
				filmDuration  = parseInt($chosenFilm.attr('duration')),
				pauseDuration = parseInt($chosenFilm.attr('pause')),
				adDuration    = parseInt($chosenFilm.attr('ad')),
				adStart       = $chosenFilm.attr('start'),
				adEnd         = reqres.request('get:weekschedule').timeToMinutes(adStart) + adDuration,
				pauseStart    = adEnd + filmDuration,
				pauseEnd      = pauseStart + pauseDuration;

			this.chosenFilm = {
				adStart:       adStart,
				adEnd:         reqres.request('get:weekschedule').minutesToTime(adEnd),
				adDuration:    adDuration,
				hallTitle:     this.hallView.model.get('title'),
				filmTitle:     this.hallView.$('.chosen_film .film_title_timeline').attr('title'),
				filmShow:      filmDuration + adDuration,
				filmDuration:  filmDuration,
				pauseStart:    reqres.request('get:weekschedule').minutesToTime(pauseStart),
				pauseEnd:      reqres.request('get:weekschedule').minutesToTime(pauseEnd),
				pauseDuration: pauseDuration
			};

			this.render();
			this.$el.show();
		},
		update: function update() {
			this.hallView.buildingFilmsBlock();
			this.hallView.saveSeances();
			this.showChosenFilm();
			commands.execute('set:timeline:markersPosition');
		}
	});

});

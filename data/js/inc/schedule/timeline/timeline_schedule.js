$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.TimelineView = Backbone.View.extend({
		template:  '#timeline_schedule_tmpl',
		tagName:   'div',
		id:        'timeline',
		className: 'timeline',
		events: {
			'click .hall_timeline': function(e) {
				if ($(e.target).hasClass('repair_changed_films')) return;
				_gaq.push(['_trackEvent', 'Расписание', 'День::Открытие']);

				var curHall     = reqres.request('get:hallsWeek').find(function(hallWeek) {return parseInt(hallWeek.get('number')) === parseInt($(e.currentTarget).attr('hall_number'));}),
					nextFullDay = curHall.get('nextFullDay') || 7;

				this.model.set({
					current_hall_number: parseInt($(e.currentTarget).attr('hall_number')),
					current_hall_title:  $(e.currentTarget).attr('hall_title'),
					nextFullDay:         nextFullDay,
					clickedDay:          this.model.get('chosen_day'),
					fromDay:             this.model.get('chosen_day')
				});

				Backbone.trigger('weekschedule:day-popup:open');
			},
			'click .week_switch': function() {
				$(document).unbind('keydown');
				$(document).unbind('keyup');
				this.remove();
				Backbone.trigger('weekschedule:week:open');
			},
			'click .prev_day_ico': function() {
				_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Переключение дня']);
				var chosenDay = parseInt(this.model.get('chosen_day')) - 1,
					nextFullDay;

				this.model.set({chosen_day: chosenDay});
				reqres.request('get:hallsWeek').each(function(hallWeek) {
					if (hallWeek.get('seances')[chosenDay].releases !== hallWeek.get('seances')[chosenDay + 1].releases) {
						nextFullDay = chosenDay + 1;
						hallWeek.set({nextFullDay: nextFullDay});
					}
					hallWeek.set({chosen_day: chosenDay});
				});

				this.render();
			},
			'click .next_day_ico': function() {
				_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Переключение дня']);
				var chosenDay = parseInt(this.model.get('chosen_day')) + 1,
					i         = 1,
					nextFullDay;

				this.model.set({chosen_day: chosenDay});
				reqres.request('get:hallsWeek').each(function(hallWeek) {
					while ((chosenDay + i) < 7 && hallWeek.get('seances')[chosenDay].releases === hallWeek.get('seances')[chosenDay + i].releases) {
						nextFullDay = chosenDay + i;
						hallWeek.set({nextFullDay: nextFullDay});
						i++;
					}
					hallWeek.set({
						chosen_day:  chosenDay
					});
				});

				this.render();
			},
			'click .fa-lock': function(e) {
				if (!reqres.request('get:user').isReadOnly()) {
					_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Разбитие промежутка']);

					$(e.currentTarget).removeClass('fa-lock').addClass('fa-unlock');

					reqres.request('get:hallsWeek').each(function(element) {
						element.set({
							nextFullDay: parseInt(window.app.App.weekSchedule.get('chosen_day')) + 1
						});
					});
				}
			},
			'click .fa-unlock': function(e) {
				if (!reqres.request('get:user').isReadOnly()) {
					$(e.currentTarget).removeClass('fa-unlock').addClass('fa-lock');

					reqres.request('get:hallsWeek').each(function(element, index) {
						var nextFullDay = $('#week_wr').find('.hall_item').eq(index).find('.hall_day').eq(window.app.App.weekSchedule.get('chosen_day')).nextAll('.full:first').attr('day_number');

						if (!nextFullDay) {
							nextFullDay = 7;
						}

						element.set({
							nextFullDay: nextFullDay
						});
					});
				}
			},
			'click #off_ads': 'setAdvertizingVisibility',
			'contextmenu .hall_timeline': 'showContextMenu'
		},
		initialize: function initialize() {
			this.subViews = [];
			this.model.set({user_watch: 'timeline'});

			this.listenTo(Backbone, 'window-resized', this.windowResized);
			this.listenTo(Backbone, 'timeline:check-pauses', this.checkPauseLayout);
			this.listenTo(Backbone, 'timeline:check-releases', this.calcReleasesCount);
			this.listenTo(Backbone, 'timeline:release-block:hide', function() {
				this.$('.release_block').hide();
			});
			this.listenTo(Backbone, 'HallsWeek:filled', this.render);

			reqres.setHandlers({
				'get:timeline:offAdsState': {
					callback: function() {
						return this.$('#off_ads').prop('checked');
					},
					context: this
				}
			});

			commands.setHandlers({
				'set:timeline:markersPosition': {
					callback: function(coordinates) {
						this.setMarkersPosition(coordinates);
					},
					context: this
				},
				'set:timeline:advertizingVisibility': {
					callback: function(e, $block) {
						this.setAdvertizingVisibility(e, $block);
					},
					context: this
				}
			});
		},
		render: function render() {
			Backbone.trigger('loader', 'show');
			this.$el.css('opacity', 0);
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes),
				self     = this;

			extModel.COLOR     = this.model.COLOR;
			extModel.hallsWeek = reqres.request('get:hallsWeek');

			this.$el.html(template(extModel));

			this.afterRender();
			setTimeout(function() {
				self.checkPauseLayout();
				self.calcReleasesCount();
				self.$el.css('opacity', 1);
				Backbone.trigger('loader', 'hide');
			}, 0);

			return this;
		},
		afterRender: function afterRender() {
			// Задаем начальные данные в зависимости от масштаба
			var dayTimelineHeight = reqres.request('get:hallsWeek').length * 55 + 26; // 55 - высота строки зала; 26 - высота временной сетки;

			this.setBlocksWidthFromScale();
			this.setHalls();
			// В зависимости от количества залов задаем размер видимой части скролла
			this.$('#day_timeline_wr, .left_added_block, .right_added_block').css('height', dayTimelineHeight + 'px');
			// Переключатель дня: скрываем нужные стрелочки если стоим на краю недели
			if (parseInt(this.model.get('chosen_day')) === 0) this.$('.prev_day_ico').hide();
			if (parseInt(this.model.get('chosen_day')) === 6) this.$('.next_day_ico').hide();

			this.subViews.push(
				new window.app.HallsTimelineView({
					collection: reqres.request('get:hallsWeek'),
					el: this.$('#halls_timeline_collection')
				}),
				new window.app.ReleaseBlockTimelineView({
					el: this.$('.release_block')
				})
			);
		},
		showContextMenu: function showContextMenu(e) {
			if (!reqres.request('get:user').isMultiplex()) return;

			e.preventDefault();
			var $target = $(e.currentTarget);

			this.contextMenu = new window.app.ContextMenuView({
				top:  e.pageY || $target.offset().top + ($target.height() / 2),
				left: e.pageX + 1 || $target.offset().left + ($target.width() / 2) + 1,
				items: [
					{
						title: 'Копировать расписание',
						iconClass: 'fa fa-files-o',
						action: function() {
							Backbone.trigger('weekschedule:hall:copy:open', this.model.get('currentWeek'), parseInt($target.attr('hall_id')), this.model.get('currentYear'), this.model.get('days_week')[this.model.get('chosen_day')].date);
						},
						context: this
					}
				]
			});
		},
		setAdvertizingVisibility: function setAdvertizingVisibility(e, $block) {
			var $timeline = $('#halls_timeline_collection');

			if (reqres.request('get:timeline:offAdsState') && _.isUndefined($block)) {
				_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Скрыты рекламные блоки']);

				$timeline.find('.ad_timeline').addClass('disabled_ad');
				$timeline.find('.chosen_film').prev().addClass('chosen_ad');

				$timeline.find('.film_item_timeline').each(function() {
					var adWidth = $(this).prev().width();

					$(this).find('.film_title_timeline, .film_format_timeline').css({
						marginLeft: 8 - adWidth
					});

					$(this).find('.film_time_wrap, .film_start_marker').css({
						marginLeft: -adWidth
					});

					$(this).find('.film_time_wrap').width($(this).find('.film_time_wrap').width() + adWidth);
					$(this).find('.film_title_timeline').width($(this).find('.film_title_timeline').width() + adWidth);
				});

				this.$el.find('.ad_mi, .ad_pl').css({
					opacity: 0,
					cursor:  'default'
				});
			} else if (reqres.request('get:timeline:offAdsState') && $block) {
				$timeline.find('.ad_timeline').addClass('disabled_ad');
				$timeline.find('.chosen_film').prev().addClass('chosen_ad');

				$block.closest('.films_timeline').find('.film_item_timeline').each(function() {
					var adWidth = $(this).prev().width();

					$(this).find('.film_title_timeline, .film_format_timeline').css({
						marginLeft: 8 - adWidth
					});

					$(this).find('.film_start_marker, .film_time_wrap').css({
						marginLeft: -adWidth
					});
				});

				this.$el.find('.ad_mi, .ad_pl').css({
					opacity: 0,
					cursor:  'default'
				});
			} else if (!reqres.request('get:timeline:offAdsState') && _.isUndefined($block)) {
				$timeline.find('.ad_timeline').removeClass('disabled_ad');
				$timeline.find('.chosen_film').prev().removeClass('chosen_ad');

				$timeline.find('.film_item_timeline').each(function() {
					var adWidth = $(this).prev().width();

					$(this).find('.film_title_timeline, .film_format_timeline').css({
						marginLeft: 8
					});

					$(this).find('.film_time_wrap, .film_start_marker').css({
						marginLeft: 0
					});

					$(this).find('.film_time_wrap').width($(this).find('.film_time_wrap').width() - adWidth);
					$(this).find('.film_title_timeline').width($(this).find('.film_title_timeline').width() - adWidth);

				});

				this.$el.find('.ad_mi, .ad_pl').css({
					opacity: 1,
					cursor:  'pointer'
				});
			}
		},
		setMarkersPosition: function setMarkersPosition(coordinates) {
			var scaleSegment = reqres.request('get:weekschedule').get('scaleSegment'),
				$chosenFilm  = this.$('.chosen_film');

			if ($chosenFilm.length) {
				coordinates = coordinates ? coordinates : {
					start: $chosenFilm.prev().offset().left - this.$('.day_timeline').offset().left,
					end:   $chosenFilm.next().offset().left + ($chosenFilm.attr('pause') / 5) * scaleSegment - this.$('.day_timeline').offset().left
				};

				this.$('.position_marker_start').css({
					left: coordinates.start
				});

				this.$('.position_marker_end').css({
					left: coordinates.end
				});
			} else {
				this.$('.position_marker_start, .position_marker_end').css({left: '-81px'});
			}

			this.$('.position_time').css({top: '-80px'});
		},
		setBlocksWidthFromScale: function setBlocksWidthFromScale() {
			// Задаем начальные данные в зависимости от масштаба
			var timelineLength = $(document).width() - 309,
				hourLength     = Math.floor(timelineLength / 240),
				minuteLength   = hourLength,
				widthDif       = $(document).width() - 346 - this.$('#day_timeline_wr').width() + this.model.get('scrollWidth'); // 346 - сумма ширин блоков перед началом timeline

			this.model.set({scaleSegment: minuteLength});
			// Задаем размеры всех элементов в зависимости от полученных начальных данных
			// 1. Для шкалы времени
			this.$('#day_timeline_wr').css('width', (hourLength * 240) + 'px');
			this.$('.hour_timeline, .minute_timeline, .half_hour_timeline').css('width', (hourLength - 1) + 'px');
			this.$('.right_added_block').css({width: widthDif + 'px'});
		},
		setHalls: function setHalls() {
			var chosenDay = parseInt(this.model.get('chosen_day')),
				i;

			reqres.request('get:hallsWeek').each(function(hallWeek) {
				i = 1;
				hallWeek.set({
					chosen_day:  chosenDay,
					nextFullDay: chosenDay + 1
				});

				while ((chosenDay + i) < 7 && JSON.stringify(hallWeek.get('seances')[chosenDay].releases) === JSON.stringify(hallWeek.get('seances')[chosenDay + i].releases)) {
					i++;
					hallWeek.set({nextFullDay: chosenDay + i});
				}
			});
		},
		windowResized: function windowResized() {
			var prevScaleSegment = this.model.get('scaleSegment'),
				self             = this;

			this.setBlocksWidthFromScale();

			this.$('.film_item_timeline, .ad_timeline, .pause_timeline').each(function(id, el) {
				$(el).css({
					width: ($(el).width() / prevScaleSegment) * self.model.get('scaleSegment'),
					left:  ($(el).position().left / prevScaleSegment) * self.model.get('scaleSegment')
				});
			});

			this.$('.film_time_wrap, .film_title_timeline').each(function(id, el) {
				$(el).css({
					width: ($(el).width() / prevScaleSegment) * self.model.get('scaleSegment')
				});
			});

			this.setMarkersPosition();
		},
		calcReleasesCount: function calcReleasesCount() {
			var self = this;
			$('.list_films .item_film').each(function(id, film) {
				var releaseId       = $(film).attr('release_id'),
					releaseFormat   = $(film).attr('formats'),
					releaseUpgrades = $(film).attr('upgrades'),
					currentCount    = self.$('.film_item_timeline[identifier="' + releaseId + releaseFormat + releaseUpgrades + '"]').length,
					commonCount     = $(film).attr('count');

				if (currentCount > commonCount) {
					$(film).find('.current_count').addClass('excess');
				} else {
					$(film).find('.current_count').removeClass('excess');
				}

				$(film).find('.current_count').text(currentCount);
				$(film).find('.count').show();
			});
		},
		checkPauseLayout: function checkPauseLayout() { // Проверка совпадения пауз
			var starts = _.uniq(this.$('.film_item_timeline').map(function() { return $(this).attr('start'); })),
				ends   = _.uniq(this.$('.film_item_timeline').map(function() { return $(this).attr('end'); })),
				$curFilm;

			_.each(starts, function(oneStart) {
				$curFilm = this.$('.film_item_timeline[start="' + oneStart + '"]');

				if ($curFilm.length > 1) {
					$curFilm.each(function(id, film) {
						$(film).find('.film_start_timeline').addClass('red_time');
						$(film).prev().addClass('matched_ad');
						$(film).prev().prev().addClass('red_pause');
					});
				} else {
					$curFilm.find('.film_start_timeline').removeClass('red_time');
					$curFilm.prev().removeClass('matched_ad');
					$curFilm.prev().prev().removeClass('red_pause');
				}
			}, this);

			_.each(ends, function(oneEnd) {
				$curFilm = this.$('.film_item_timeline[end="' + oneEnd + '"]');

				if ($curFilm.length > 1) {
					$curFilm.each(function(id, film) {
						$(film).find('.film_end_timeline').addClass('red_time');
						$(film).next().addClass('red_pause');
					});
				} else {
					if (!$curFilm.next().next().hasClass('matched_ad')) {
						$curFilm.next().removeClass('red_pause');
					}
					$curFilm.find('.film_end_timeline').removeClass('red_time');
				}
			}, this);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

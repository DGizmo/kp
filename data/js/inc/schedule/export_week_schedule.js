$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ExportWeekScheduleView = Backbone.View.extend({
		className: 'export_popup',
		template: '#export_week_schedule',
		events: {
			'click .done': 'export',
			'focus input': function(event) {
				$(event.currentTarget).removeClass('error');
			}
		},
		initialize: function initialize(options) {
			this.options = options || {date: moment()};
		},
		export: function() {
			var $startDate = this.$el.find('.date_input_from'),
				$endDate   = this.$el.find('.date_input_to'),
				startDate  = $startDate.datepicker('getDate'),
				endDate    = $endDate.datepicker('getDate'),
				$email     = this.$el.find('.email');

			$.each([$startDate, $endDate, $email], function(i, $input) {
				$input.removeClass('error');
			});

			if (!startDate.isValid() || !(/\d{2}\.\d{2}\.\d{4}/.test($startDate.val()))) {
				$startDate.addClass('error');
				$.growl.error({message: 'Неверный формат даты' });
				return;
			}

			if (!($endDate.val())) {
				$endDate.val($startDate.val());
				endDate = startDate;
			} else if (!endDate.isValid() || !(/\d{2}\.\d{2}\.\d{4}/.test($endDate.val()))) {
				$endDate.addClass('error');
				$.growl.error({message: 'Неверный формат даты' });
				return;
			}

			if (startDate > endDate) {
				$startDate.addClass('error');
				$.growl.error({message: 'Дата начала периода не должна быть позже Даты конца периода' });
				return;
			}

			if ($email.val().length > 0 && $email[0].validity.valid !== true) {
				$email.addClass('error');
				$.growl.error({message: 'Введите корректный E-mail' });
				return;
			}

			$.arcticmodal('close');
			this._download(startDate.format('{yyyy}-{MM}-{dd}'), endDate.format('{yyyy}-{MM}-{dd}'), $email.val());
		},
		_download: function _download(startDate, endDate, email) {
			location.href = window.location.origin +
				'/export/schedule/?cinema_id=' + reqres.request('get:user').getCinema().id +
				'&start='  + startDate +
				'&end='    + endDate +
				'&email='  + email +
				'&layout=' + this.$el.find('.layout').val();
		},
		render: function render() {
			var template      = Backbone.TemplateCache.get(this.template),
				cinemaNetwork = reqres.request('get:user').getCinema().cinema_network || {},
				mirage        = parseInt(cinemaNetwork.id) === 257 ? true : false,
				date          = this.options.date;

			this.$el.html(template({
				mirage:   mirage,
				fromDate: date.weekday(0).format('DD.MM.YYYY'),
				toDate:   date.weekday(6).format('DD.MM.YYYY')
			}));

			this.$el.find('.date_input').each(function() {
				$(this).datepicker({
					weekStart: 4,
					language: 'ru',
					calendarWeeks: true,
					autoclose: true,
					format: 'dd.mm.yyyy'
				});
			});

			return this;
		}
	});

});

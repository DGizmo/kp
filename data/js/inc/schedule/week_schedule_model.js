$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.WeekSchedule = Backbone.Model.extend({
		defaults: {
			days_week:         [],
			currentWeek:       moment().weeks(),
			currentMonth:      moment().month(),
			currentYear:       moment().year(),
			date:              moment(),
			hall_list:         [],
			changedHall:       0,
			chosen_day:        0,
			clickedDay:        0,
			defaultCinemaAd:   0,
			schedule_rounding: 'ceil',
			user_watch:        'week',
			lockMode:          null,
			filterReleases:    _.isUndefined($.cookie('weekScheduleFilterReleases')) || $.cookie('weekScheduleFilterReleases') === 'true' ? true : false,
			copyMode:          'simple'
		},
		WEEKDAY: {
			'понедельник': 'Пн',
			'вторник':     'Вт',
			'среда':       'Ср',
			'четверг':     'Чт',
			'пятница':     'Пт',
			'суббота':     'Сб',
			'воскресенье': 'Вс',
			0:             'ЧТ',
			1:             'ПТ',
			2:             'СБ',
			3:             'ВС',
			4:             'ПН',
			5:             'ВТ',
			6:             'СР'
		},
		COLOR: {
			0: 'gray',
			1: 'green',
			2: 'orange',
			3: 'blue',
			4: 'pink'
		},
		initialize: function initialize() {
			reqres.setHandlers({
				'get:weekschedule': {
					callback: function() {
						return this;
					},
					context: this
				}
			});

			this.listenTo(Backbone, 'socket:schedule_week', function(data) {
				if (reqres.request('get:user').getCinema().cinema_id === data.cinema_id) this.update();
			});

			this.listenTo(Backbone, 'user:cinema:settings:save', this.setCinemaParameters);
			this.listenTo(Backbone, 'weekschedule:changeDate cinema-changed', this.update);
		},
		url: function url() {
			return '/api/v3/schedule/week?week=' +
				this.get('currentWeek') + '&year=' +
				this.get('currentYear') + '&cinema_id=' +
				reqres.request('get:user').getCinema().id;
		},
		parse: function parse(response) {
			if (response.schedule[0]) {
				_.each(response.schedule[0].halls, function(hall) {
					_.each(hall.seances, function(seance) {
						seance.releases = _.filter(seance.releases, function(release) {
							return Boolean(release.formats && release.id);
						});
					});
				});
			}

			return response;
		},
		timeToMinutes: function timeToMinutes(time, early) {
			if (!time) return;
			var parts = _.map(time.split(':'), function(part) { return parseInt(part); });

			if (_.contains(_.range(0, 8), parts[0])) parts[0] = early !== 'early' ? parts[0] + 24 : parts[0];

			return parts[0] * 60 + parts[1];
		},
		minutesToTime: function minutesToTime(minutes) {
			var hours = Math.floor(minutes / 60),
				min = minutes - (hours * 60);

			if (hours >= 24) hours = hours - 24;
			if (hours < 10)  hours = '0' + hours;
			if (min < 10)    min = '0' + min;

			return hours + ':' + min;
		},
		setDateParameters: function setDateParameters(date) {
			var currentWeek  = date.weeks(),
				currentYear  = date.year(),
				currentMonth = date.month(),
				days_week    = [],
				i;

			for (i = 0; i < 7; i++) {
				days_week.push({
					date:         date.weekday(i).format('YYYY-MM-DD'),
					month:        date.weekday(i).format('MMM'),
					day_of_month: date.weekday(i).format('D'),
					day_of_week:  i
				});
			}

			this.set({
				date:         date,
				currentWeek:  currentWeek,
				currentMonth: currentMonth,
				currentYear:  currentYear,
				days_week:    days_week
			});

			Backbone.trigger('weekschedule:changeDate');
		},
		setCinemaParameters: function setCinemaParameters() {
			var currentCinSettings = reqres.request('get:user').getCinemaSettings();

			this.set({
				schedule_rounding: currentCinSettings ? currentCinSettings.schedule_rounding || 'ceil' : 'ceil',
				defaultCinemaAd:   currentCinSettings ? currentCinSettings.defaultCinemaAd || 0 : 0
			});
		},
		update: function update() {
			Backbone.trigger('loader', 'show');
			this.setCinemaParameters();

			this.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
				}
			});
		}
	});

});

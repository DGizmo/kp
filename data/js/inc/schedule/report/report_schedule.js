$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ReportScheduleView = Backbone.View.extend({
		template:  '#report_schedule_tmpl',
		className: 'report-schedule',
		events: {
			'click .btn-back': function() {
				window.history.back();
			},
			'click .next-interval': function() {
				this.currentDay = this.intervals[_.indexOf(this.intervals, this.currentDay) + 1];
				this.render();
			},
			'click .prev-interval': function() {
				this.currentDay = this.intervals[_.indexOf(this.intervals, this.currentDay) - 1];
				this.render();
			}
		},
		initialize: function initialize() {
			this.subViews   = [];
			this.intervals  = this.setIntervals();
			this.currentDay = 0;
			this.model.set({user_watch: 'report'});

			this.listenTo(Backbone, 'HallsWeek:filled', this.render);
		},
		render: function render() {
			var template        = Backbone.TemplateCache.get(this.template),
				extModel        = $.extend({}, this.model.attributes),
				currentDayIndex = _.indexOf(this.intervals, this.currentDay),
				nextDayIndex    = _.indexOf(this.intervals, this.currentDay) + 1,
				currentDate     = _.find(this.model.get('days_week'), function(day) {
					return day.day_of_week === this.intervals[currentDayIndex];
				}, this),
				nextDate        = _.find(this.model.get('days_week'), function(day) {
					return day.day_of_week === this.intervals[nextDayIndex] - 1;
				}, this) || this.model.get('days_week')[6];

			if (this.intervals[currentDayIndex] === this.intervals[nextDayIndex] - 1 || this.intervals[currentDayIndex] === 6) {
				extModel.intervals = [
					_.property(this.intervals[currentDayIndex])(this.model.WEEKDAY),
					moment(currentDate.date, 'YYY-MM-DD').format(' (D MMM)')
				].join('');
			} else {
				extModel.intervals = [
					_.property(this.intervals[currentDayIndex])(this.model.WEEKDAY),
					moment(currentDate.date, 'YYY-MM-DD').format(' (D MMM)'),
					' — ',
					_.property(this.intervals[nextDayIndex] - 1)(this.model.WEEKDAY) || 'СР',
					moment(nextDate.date, 'YYY-MM-DD').format(' (D MMM)')
				].join('');
			}

			this.$el.html(template(extModel));

			this.afterRender();

			return this;
		},
		afterRender: function afterRender() {
			this.subViews.push(new window.app.HallsReportView({
				collection: window.app.App.hallsWeek,
				el:         this.$('.halls-report-collection'),
				currentDay: this.currentDay
			}));
		},
		setIntervals: function setIntervals() {
			var days      = _.range(0, 6),
				intervals = [0],
				hallsIntervals;

			_.each(reqres.request('get:hallsWeek').models, function(hallWeek) {
				hallsIntervals = [0];
				_.each(days, function(day) {
					if (JSON.stringify(hallWeek.get('seances')[day].releases) !== JSON.stringify(hallWeek.get('seances')[day + 1].releases))
						hallsIntervals.push(day + 1);
				});

				intervals = _.union(intervals, hallsIntervals);
			});

			return intervals.sort();
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.HallReportView = Backbone.View.extend({
		template: '#hall_report_tmpl',
		tagName: 'tr',
		className: 'hall-report',
		initialize: function(options) {
			this.options  = options || {};
		},
		render: function() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.releasesNumber = this.options.releasesNumber;

			this.$el.html(template(extModel));

			return this;
		},
		remove: function() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.HallsReportView = Backbone.View.extend({
		events: {
			'mouseenter td[data-identifier]': function(e) {
				this.$('td[data-identifier="' + $(e.currentTarget).data('identifier') + '"]').addClass('marked');
			},
			'mouseleave td[data-identifier]': function(e) {
				this.$('td[data-identifier="' + $(e.currentTarget).data('identifier') + '"]').removeClass('marked');
			}
		},
		initialize: function(options) {
			this.options = options || {};
			this.subViews = [];
			this.render();
			this.listenTo(Backbone, 'HallsWeek:filled', this.remove);
		},
		render: function() {
			this.collection.each(function(hallWeek) {
				var releasesGroups = _.values(_.groupBy(hallWeek.get('seances')[this.options.currentDay].releases, function(release) {
						return release.id + release.formats.toUpperCase() + release.upgrades;
					}, this)) || [],
					releasesNumber = releasesGroups.length ? releasesGroups.length + 1 : 2,
					hallReportView = new window.app.HallReportView({
						model: hallWeek,
						releasesNumber: releasesNumber
					}),
					releaseHallReportView;

				this.$el.append(hallReportView.render().el);
				this.subViews.push(hallReportView);

				_.each(releasesGroups, function(group) {
					releaseHallReportView = new window.app.ReleaseHallReportView({
						model:         hallWeek,
						releasesGroup: group
					});
					this.subViews.push(releaseHallReportView);
					this.$el.append(releaseHallReportView.render().el);
				}, this);

				if (!releasesGroups.length) {
					releaseHallReportView = new window.app.ReleaseHallReportView({model: hallWeek});
					this.subViews.push(releaseHallReportView);
					this.$el.append(releaseHallReportView.render().el);
				}
			}, this);

			return this;
		},
		remove: function() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.HallWeekView = Backbone.View.extend({
		className: 'hall_item clearfix',
		template: '#hall_week_tmpl',
		events: {
			'mouseover .rel': function(e) {
				if (reqres.request('get:user').isReadOnly()) {
					$(e.currentTarget).css('cursor', 'default');
					$(e.currentTarget).find('.remove').hide();
				}
			},
			'click .remove': function(e) { // Удаление релиза дня из модели зала
				if (reqres.request('get:user').isReadOnly()) return;
				_gaq.push(['_trackEvent', 'Расписание', 'Неделя::Удаление']);

				var self             = this,
					releaseIndex     = $(e.currentTarget).closest('.rel').index(),
					$currentHallDay  = $(e.currentTarget).closest('.hall_day'),
					$prevFullHallDay = $currentHallDay.prevAll('.full:first'),
					$nextFullHallDay = $currentHallDay.nextAll('.full:first'),
					maxRelNumber     = 0,
					copyReleases;

				this.dayStart = $(e.currentTarget).closest('.release_day').find('.rel:first').attr('start');

				if ($currentHallDay.find('.release_day .rel').length === 1) {
					$currentHallDay.removeClass('full');
					this.dayStart = $prevFullHallDay.find('.release_day .rel:first').attr('start');
					if ($nextFullHallDay && !$nextFullHallDay.find('.rel').length) $nextFullHallDay.removeClass('full');

					copyReleases = $prevFullHallDay.length ? this.model.get('seances')[parseInt($prevFullHallDay.attr('day_number'))].releases : [];

					$currentHallDay.nextUntil('.full').andSelf().each(function(id, hallDay) {
						self.model.get('seances')[parseInt($(hallDay).attr('day_number'))].releases = copyReleases;
					});

					this.render();
				} else {
					$currentHallDay.nextUntil('.full').andSelf().each(function(id, hallDay) {
						$(hallDay).find('.release_day .rel').eq(releaseIndex).remove();
					});

					this.$el.find('.full').each(function() { // Определяем надо ли удалять пустую строчку
						var $rel = $(this).find('.release_day .rel');

						if ($rel.length > maxRelNumber) {
							maxRelNumber = $rel.length;
						}
					});

					this.$el.find('.hall_day').each(function() { // И удаляем при выполнении условия
						var $day = $(this).find('.item_day');

						if ($day.length > 3 && (maxRelNumber + 1) < $day.length) {
							$day.last().remove();
						}
					});

					$currentHallDay.nextUntil('.full').andSelf().each(function(id, hallDay) {
						$(hallDay).find('.release_day .rel').first().attr('start', self.dayStart);
						$(hallDay).find('.release_day .rel').first().find('.rel_time').text(self.dayStart);
						self.calcReleaseStart($(hallDay).find('.release_day .rel'));
					});
				}

				this.saveSeances();
			},
			'mouseenter .release_day.ui-state': 'sort',
			'click .hall_item__hall__right__copy': 'hallCopy',
			contextmenu: 'showContextMenu'
		},
		initialize: function initialize() {
			this.subViews = [];

			this.listenTo(this.model, 'hall:day-popup:change', this.render);

			this.listenTo(this.model.collection, 'halls:copying-day:enable', function() {
				this.$('.release_day:not(.ui-draggable-dragging)').each(function() {
					$(this)
						.draggable('enable')
						.draggable('option', 'cursorAt', {left: Math.round($(this).width() / 2), top: 15});
				});
			});

			this.listenTo(this.model.collection, 'halls:copying-day:disable', function() {
				this.$('.release_day:not(.ui-draggable-dragging)').draggable('disable');
			});

			this.listenTo(this.model, 'save:release:comment', function() {
				this.saveSeances();
				this.listenToOnce(this.model, 'hall:saved', function() {
					this.render();
				});
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.allowHallsCopy = reqres.request('get:user').isMultiplex();

			this.$el.html(template(extModel));

			this.afterRender();

			return this;
		},
		afterRender: function afterRender() {
			var self = this,
				releaseAttributes;

			self.$el.find('.hall_day').each(function(id, hallDay) {
				_.each(self.model.get('seances')[id].releases, function(release) {
					releaseAttributes = self.model.parseRelease(release);

					var releaseHallWeekVew = new window.app.ReleaseHallWeekView({
						release:    release,
						model:      self.model,
						attributes: releaseAttributes
					});

					$(hallDay).find('.release_day').append(releaseHallWeekVew.render().el);

					self.subViews.push(releaseHallWeekVew);
				});
				self.calcReleaseStart($(hallDay).find('.release_day').find('.rel'));
			});

			this.setItemDayNumber();
			this.dragDays();
			this.drop();
			this.checkFullDay();
		},
		hallCopy: function hallCopy() {
			Backbone.trigger('weekschedule:hall:copy:open', this.model.get('week'), this.model.get('id'), this.model.get('year'));
		},
		showContextMenu: function showContextMenu(e) {
			if (!reqres.request('get:user').isMultiplex()) return;

			e.preventDefault();
			var $target = $(e.currentTarget);

			if ($(e.target).closest('.rel, .hall_item__hall').length) return;

			this.contextMenu = new window.app.ContextMenuView({
				top:  e.pageY || $target.offset().top + ($target.height() / 2),
				left: e.pageX + 1 || $target.offset().left + ($target.width() / 2) + 1,
				items: [
					{
						title: 'Копировать расписание',
						iconClass: 'fa fa-files-o',
						action: function() {
							Backbone.trigger('weekschedule:hall:copy:open', this.model.get('week'), this.model.get('id'), this.model.get('year'), this.model.get('days_week')[parseInt($(e.target).closest('.hall_day').attr('day_number'))].date);
						},
						context: this
					}
				]
			});
		},
		calcReleaseStart: function calcReleaseStart(releasesDay) {
			var self = this;

			releasesDay.each(function(id, rel) {
				var releasePause = parseInt($(rel).attr('pause')),
					$nextRel = $(rel).next(),
					releaseStart = $(rel).find('.rel_time').text(),
					releaseAd = parseInt($(rel).attr('ad')),
					releaseDuration = parseInt($(rel).attr('duration')),
					nextReleaseStart = $nextRel.attr('start');

				releaseStart = self.model.collection.weekSchedule.timeToMinutes(releaseStart);

				if (reqres.request('get:weekschedule').get('schedule_rounding') === 'ceil') {
					nextReleaseStart = self.model.collection.weekSchedule.minutesToTime(Math.ceil((releaseStart + releaseDuration + releasePause + releaseAd) / 5) * 5);
				} else {
					nextReleaseStart = self.model.collection.weekSchedule.minutesToTime(Math.floor((releaseStart + releaseDuration + releasePause + releaseAd) / 5) * 5);
				}

				$nextRel.attr('start', nextReleaseStart);
				$nextRel.find('.rel_time').text(nextReleaseStart);
			});
		},
		createReleasesViews: function createReleasesViews(el) {
			var releasesViews = [],
				releaseAttributes,
				checkCopyAndKdm,
				releaseView,
				release;

			_.each($(el).find('.rel'), function(film) {
				checkCopyAndKdm = this.model.checkCopyAndKdm($(film).attr('formats'), $(film).attr('release_id')),
				release = {
					has_copy: checkCopyAndKdm.has_copy,
					has_kdm:  checkCopyAndKdm.has_kdm,
					duration: parseInt($(film).attr('duration')),
					formats:  $(film).attr('formats'),
					upgrades: $(film).attr('upgrades'),
					pause:    $(film).attr('pause'),
					adParams: reqres.request('get:weekschedule').get('draggingHall') === this ? $(film).data('adParams') : this.model.getAdParams($(film).attr('formats'), $(film).attr('release_id')),
					ad:       parseInt($(film).attr('ad')),
					title:    $(film).find('.rel_title').attr('release_title'),
					id:       $(film).attr('release_id'),
					start:    $(film).attr('start')
				};

				releaseAttributes = this.model.parseRelease(release);

				releaseView = new window.app.ReleaseHallWeekView({
					release:    release,
					model:      this.model,
					attributes: releaseAttributes
				});

				releasesViews.push(releaseView);
			}, this);

			return releasesViews;
		},
		dragDays: function dragDays() {
			if (reqres.request('get:user').isReadOnly()) return;
			var self = this;

			this.$('.release_day').draggable({
				helper:      'clone',
				cursor:      'move',
				disabled:    true,
				opacity:     1,
				zIndex:      3,
				scope:       'week',
				start: function(event, ui) {
					if (!$(this).children().length) return false;
					self.model.collection.weekSchedule.set('draggingHall', self);

					$(ui.helper).data('startScroll', $(ui.helper).closest('.item_week').scrollTop());
					_.each($(ui.helper).find('.rel'), function(release) {
						$(release)
							.css({width: '100%'})
							.show();
					});
				},
				drag: function(event, ui) {
					var viewPort        = $(ui.helper).closest('.item_week'),
						viewPortBorders = viewPort.get(0).getBoundingClientRect(),
						currentScroll   = viewPort.scrollTop(),
						topBorder       = viewPortBorders.top + 60,
						botBorder       = viewPortBorders.bottom - 60;

					if ($(ui.helper).offset().top < topBorder) {
						viewPort.scrollTop(currentScroll - 20);
					} else if (($(ui.helper).offset().top + $(ui.helper).height()) > botBorder) {
						viewPort.scrollTop(currentScroll + 20);
					}
				}
			});
		},
		dropDays: function dropDays(dropEl, dragEl) {
			var releasesViews = this.createReleasesViews(dragEl);

			$(dropEl).next()
				.addClass('full')
				.find('.release_day .rel').show();

			$(dropEl).find('.release_day').empty();

			_.each(releasesViews, function(releaseView) {
				$(dropEl).find('.release_day').append(releaseView.render().el);
				this.subViews.push(releaseView);
			}, this);
		},
		setReleasesBlocksLength: function setReleasesBlocksLength() {
			var releaseLength = 1;

			this.$el.find('.full').each(function(fullHallId, fullHallDay) { // Задаем длину блока релиза
				$(fullHallDay).nextUntil('.full').each(function() {
					releaseLength++;
				});

				$(fullHallDay).find('.release_day').find('.rel').css({
					width: 'calc(100% * ' + releaseLength + ' + ' + (6 * (releaseLength - 1)) + 'px)'
				});

				releaseLength = 1;
			});
		},
		drop: function drop() {
			var self        = this,
				hallId      = this.model.get('id'),
				hallFormats = this.model.get('formats');

			this.$el.find('.hall_day').droppable({
				hoverClass: 'drop-hover',
				tolerance: 'pointer',
				scope: 'week',
				accept: function(el) {
					var dayNumber = $(this).find('.release_day').attr('number');
					if (reqres.request('get:weekschedule').get('draggingHall') === self && el.attr('number') === dayNumber)
						return false;
					if (el.hasClass('item_film') || el.hasClass('release_day'))
						return el;
				},
				drop: function(event, ui) {
					_gaq.push(['_trackEvent', 'Расписание', 'Неделя::Добавление']);

					var $self             = $(this),
						cinemaSetting     = reqres.request('get:user').getCinemaSettings(),
						hallsSettings     = cinemaSetting ? cinemaSetting.halls.find(function(h) {return h.id === hallId;}) : '',
						checkCopyAndKdm   = self.model.checkCopyAndKdm($(ui.draggable).attr('formats'), $(ui.draggable).attr('release_id')),
						release           = {
							has_copy: checkCopyAndKdm.has_copy,
							has_kdm:  checkCopyAndKdm.has_kdm,
							duration: $(ui.draggable).attr('duration'),
							formats:  $(ui.draggable).attr('formats'),
							upgrades: $(ui.draggable).attr('upgrades'),
							pause:    hallsSettings.defaultHallPause || 10,
							adParams: self.model.getAdParams($(ui.draggable).attr('formats'), $(ui.draggable).attr('release_id')),
							ad:       parseInt($(ui.draggable).attr('ad')),
							title:    $(ui.draggable).attr('title'),
							id:       $(ui.draggable).attr('release_id'),
							start:    hallsSettings.defaultHallOpen || '10:00'
						},
						releaseAttributes,
						releaseHallWeekVew,
						$addedRelease,
						addedReleaseStart,
						addedReleaseDuration,
						addedReleaseAd,
						addedReleasePause,
						addedReleaseEnd;

					if ($(ui.draggable).hasClass('release_day')) {
						self.dropDays(this, ui.draggable);
					} else if (!new RegExp(release.formats, 'i').test(hallFormats.join('')) || !release.formats) {
						$(ui.helper).remove();
						Backbone.trigger('weekschedule:message', 'Зал не поддерживает формат данного релиза');
					} else if (!release.duration) {
						$(ui.helper).remove();
						Backbone.trigger('weekschedule:message', 'У релиза нет длительности');
					} else {
						releaseAttributes = self.model.parseRelease(release);

						$(this).nextUntil('.full').andSelf().each(function(id, hallDay) {
							if (!$self.hasClass('full')) $(hallDay).find('.release_day').empty();

							releaseHallWeekVew = new window.app.ReleaseHallWeekView({
								release:    release,
								model:      self.model,
								attributes: releaseAttributes
							});

							self.subViews.push(releaseHallWeekVew);
							$(hallDay).find('.release_day').append(releaseHallWeekVew.render().el);

							if (id !== 0) releaseHallWeekVew.$el.hide();

							self.calcReleaseStart($(hallDay).find('.release_day .rel'));
						});
					}

					$(this).addClass('full');

					if ($(this).find('.release_day').html() !== $(ui.draggable).html()) {
						while ($(this).find('.item_day').length <= $(this).find('.release_day').find('.rel').length) {
							$(this).closest('.hall_item').find('.hall_day').append('<div class="item_day" />');
						}
					}

					self.setReleasesBlocksLength();

					$addedRelease        = $(this).find('.release_day .rel').last();
					addedReleaseStart    = $addedRelease.find('.rel_time').text();
					addedReleaseDuration = parseInt($addedRelease.attr('duration'));
					addedReleaseAd       = parseInt($addedRelease.attr('ad'));
					addedReleasePause    = parseInt($addedRelease.attr('pause'));
					addedReleaseEnd      = 0;

					addedReleaseStart = self.model.collection.weekSchedule.timeToMinutes(addedReleaseStart);
					if (reqres.request('get:weekschedule').get('schedule_rounding') === 'ceil') {
						addedReleaseEnd = Math.ceil((addedReleaseStart + addedReleasePause + addedReleaseDuration + addedReleaseAd) / 5) * 5;
					} else {
						addedReleaseEnd = Math.floor((addedReleaseStart + addedReleasePause + addedReleaseDuration + addedReleaseAd) / 5) * 5;
					}

					if (addedReleaseEnd > 1860) { // минут в 7  часах утра
						$addedRelease.find('.remove').trigger('click');

						Backbone.trigger('weekschedule:message', 'Конец показа релиза затрагивает начало следующего дня');

						return;
					}

					self.saveSeances();
				}
			});
		},
		sort: function sort() {
			if (reqres.request('get:user').isReadOnly()) return;
			var self = this;

			this.$el.find('.release_day.ui-state').sortable({
				axis: 'y',
				cursor: 'move',
				helper: 'clone',
				start: function() {
					self.dayStart = $(this).closest('.release_day').find('.rel:first').attr('start');
				},
				update: function() {
					_gaq.push(['_trackEvent', 'Расписание', 'Неделя::Перетягивание']);

					var $firstRel  = $(this).find('.rel').first(),
						dayNumber  = parseInt($(this).closest('.hall_day').attr('day_number')),
						seance     = self.model.get('seances')[dayNumber];

					$firstRel.attr('start', self.dayStart);
					$firstRel.find('.rel_time').text(self.dayStart);

					self.calcReleaseStart($(this).find('.rel'));
					seance.releases = [];
					$(this).find('.rel').each(function() {
						seance.releases.push({
							id:       $(this).attr('release_id'),
							formats:  $(this).attr('formats'),
							upgrades: $(this).attr('upgrades'),
							title:    $(this).find('.rel_title').attr('release_title'),
							duration: $(this).attr('duration'),
							start:    $(this).find('.rel_time').text(),
							pause:    $(this).attr('pause'),
							ad:       $(this).attr('ad'),
							adParams: $(this).data('adParams') || null,
							has_copy: $(this).data('has_copy') || 0,
							has_kdm:  $(this).data('has_kdm') || 0,
							comment:  $(this).attr('comment')
						});
					});

					$(this).closest('.full').nextUntil('.full').each(function(id) {
						self.model.get('seances')[dayNumber + id + 1].releases = seance.releases;
					});

					self.render();
					self.saveSeances();
				}
			});
		},
		checkFullDay: function checkFullDay() {
			var hallDays = this.$el.find('.hall_day'),
				seances  = this.model.get('seances');

			hallDays.each(function(id, hallDay) {
				var i           = 1,
					nextHallDay = hallDays.eq(id + i);

				if (id && JSON.stringify(seances[id].releases) === JSON.stringify(seances[id - 1].releases)) return true;

				$(hallDay)
					.addClass('full')
					.find('.rel').show();

				while ((id + i) < 7 && JSON.stringify(seances[id].releases) === JSON.stringify(seances[id + i].releases)) {
					$(nextHallDay).find('.release_day .rel').hide();

					i++;
					nextHallDay = hallDays.eq(id + i);
				}

				$(hallDay).find('.release_day .rel').css({
					width: 'calc(100% * ' + i + ' + ' + (6 * (i - 1)) + 'px)'
				});

				id = id + i;
			});
		},
		saveSeances: function saveSeances() {
			var self = this;

			clearTimeout(self.timer);

			self.timer = setTimeout(function() {
				var seances      = self.model.get('seances'),
					prevSeances  = JSON.stringify(self.model.get('seances')),
					prevSchedule = JSON.stringify(self.model.collection.weekSchedule.get('schedule')),
					i            = 0;

				_.each(seances, function(oneSeance) {
					oneSeance.releases = [];
					self.$el.find('.hall_day').eq(i).find('.release_day .rel').each(function() {
						oneSeance.releases.push({
							id:       $(this).attr('release_id'),
							formats:  $(this).attr('formats'),
							upgrades: $(this).attr('upgrades'),
							title:    $(this).find('.rel_title').attr('release_title'),
							duration: $(this).attr('duration'),
							start:    $(this).find('.rel_time').text(),
							pause:    $(this).attr('pause'),
							ad:       $(this).attr('ad'),
							adParams: $(this).data('adParams') || null,
							has_copy: $(this).data('has_copy') || 0,
							has_kdm:  $(this).data('has_kdm') || 0,
							comment:  $(this).attr('comment')
						});
					});
					i++;
				});

				$.ajax({
					url: '/api/schedule/week/v2/set',
					type: 'post',
					dataType: 'json',
					timeout: 5000,
					data: ({
						tx:        reqres.request('get:user').get('tx'),
						cinema_id: reqres.request('get:user').getCinema().id,
						year:      self.model.get('year'),
						week:      self.model.get('week'),
						hall_id:   self.model.get('id'),
						seance:    JSON.stringify(self.model.get('seances'))
					}),
					success: function() {
						self.model.trigger('hall:saved');
					},
					error: function() {
						self.model.set(seances, JSON.parse(prevSeances));
						self.model.collection.weekSchedule.set({schedule: JSON.parse(prevSchedule)});
						self.render();
						Backbone.trigger('weekschedule:message', 'Проблемы с интернет соединением!');
						Backbone.trigger('HallsWeek:filled');
					}
				});
			});
		},
		setItemDayNumber: function setItemDayNumber() {
			var itemDayNumber = 3;

			this.$el.find('.hall_day').each(function(id, hallDay) {
				var relNumber = 0;

				$(hallDay).find('.release_day .rel').each(function() {
					relNumber++;
				});

				if (relNumber >= itemDayNumber) {
					itemDayNumber = relNumber + 1;
				}
			});

			this.$el.find('.hall_day').each(function(id, hallDay) {
				for (var i = 1; i <= itemDayNumber; i++) {
					$(hallDay).append('<div class="item_day" />');
				}
			});
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.HallsWeekView = Backbone.View.extend({
		events: {
			mouseenter: function() {
				var self = this;

				$(window).keydown(function(event) {
					if (event.keyCode === 18) {
						self.collection.trigger('halls:copying-day:enable');
					}
				});

				$(window).keyup(function(event) {
					if (event.keyCode === 18) {
						self.collection.trigger('halls:copying-day:disable');
					}
				});
			},
			mouseleave: function() {
				$(window).unbind('keydown');
				$(window).unbind('keyup');
			}
		},
		initialize: function initialize() {
			this.subViews = [];
			this.render();
			this.listenTo(Backbone, 'HallsWeek:filled', this.remove);
		},
		render: function render() {
			this.$el.empty();

			this.collection.each(function(hallWeek) {
				var hallWeekView = new window.app.HallWeekView({model: hallWeek});

				this.$el.append(hallWeekView.render().el);

				this.subViews.push(hallWeekView);
			}, this);

			return this;
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

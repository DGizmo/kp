$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.WeekScheduleView = Backbone.View.extend({
		template: '#week_schedule_tmpl',
		events: {
			'mouseover .item_film': function(e) {
				if (reqres.request('get:user').isReadOnly()) {
					$(e.currentTarget).css('cursor', 'default');
				}
			},
			'click .timeline_switch': function(e) {
				_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Открытие']);
				var chosenDay = parseInt($(e.currentTarget).attr('chosen_day'));

				this.model.set({chosen_day: chosenDay});
				this.remove();

				Backbone.trigger('weekschedule:timeline:open', chosenDay);
			}
		},
		initialize: function initialize() {
			this.subViews = [];
			this.listenTo(Backbone, 'HallsWeek:filled', this.render);
			this.model.set({user_watch: 'week'});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.COLOR   = this.model.COLOR;
			extModel.WEEKDAY = this.model.WEEKDAY;

			this.$el.html(template(extModel));

			this.afterRender();

			return this;
		},
		afterRender: function afterRender() {
			var scrollWidth = _.getScrollBarWidth();

			this.$el.find('.head_day_wr').css({right: scrollWidth});

			this.subViews.push(new window.app.HallsWeekView({
				collection: reqres.request('get:hallsWeek'),
				el: this.$('.hall_list_wr')
			}));
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

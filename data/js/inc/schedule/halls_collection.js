$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.HallWeek = Backbone.Model.extend({
		defaults: {},
		initialize: function initialize() {},
		fillingSeances: function fillingSeances(releases) {
			var daysWeek = this.collection.weekSchedule.get('days_week'),
				seances  = [],
				i;

			for (i in daysWeek) {
				seances.push({
					date:     daysWeek[i].date,
					releases: releases
				});
			}

			return seances;
		},
		getAdParams: function getAdParams(format, releaseId) {
			var hallNumber     = this.get('number'),
				releaseParams  = _.find(reqres.request('get:weekschedule').get('releasesParams'), function(release) {
					return parseInt(release.id) === parseInt(releaseId) && release.format === format;
				}),
				adParams = null;

			if (releaseParams) {
				adParams = _.filter(releaseParams.adParams, function(ad) {
					if (_.indexOf(ad.halls, hallNumber) !== -1) return true;
				});
			}

			return adParams;
		},
		checkCopyAndKdm: function checkCopyAndKdm(format, releaseId) {
			var hallId         = this.get('id'),
				releaseParams  = _.find(reqres.request('get:weekschedule').get('releasesParams'), function(release) {
					return parseInt(release.id) === parseInt(releaseId) && release.format === format;
				}),
				hasCopy = 0,
				hasKdm  = 0;

			if (releaseParams) {
				if (_.indexOf(releaseParams.hasCopy, hallId) !== -1) hasCopy = 1;
				if (_.indexOf(releaseParams.hasKdm, hallId) !== -1) hasKdm = 1;
			}

			return {
				has_copy: hasCopy,
				has_kdm:  hasKdm
			};
		},
		parseRelease: function parseRelease(release) {
			var attributes      = {},
				relDuration     = release.duration,
				relUpgrades     = release.upgrades ? release.upgrades : '',
				identifier      = release.id + release.formats.toUpperCase() + relUpgrades,
				title           = [release.title, release.formats, relUpgrades].join(' '),
				missingPlaning  = '',
				newRelease      = _.find(reqres.request('get:weekscheduleReleasesList').models, function(r) {
					return r.get('release_id') === release.id;
				}),
				checkRepertoire = _.contains(this.collection.weekSchedule.get('identifiers'), identifier);

			if (newRelease && !checkRepertoire && reqres.request('get:weekschedule').get('filterReleases') && parseInt(release.id)) {
				missingPlaning = ['Не указано количество сеансов на этой неделе для', release.title, release.formats, relUpgrades].join(' ');
			} else if (!newRelease && reqres.request('get:weekschedule').get('filterReleases') && parseInt(release.id)) {
				missingPlaning = [release.title, release.formats, relUpgrades, 'отсутствует в прокате на этой неделе'].join(' ');
			}

			attributes.identifier     = identifier;
			attributes.color          = newRelease ? newRelease.get('color') : 'gray';
			attributes.title          = missingPlaning ? missingPlaning : title;
			attributes.ad             = release.ad;
			attributes.formats        = release.formats;
			attributes.upgrades       = relUpgrades;
			attributes.release_id     = release.id;
			attributes.duration       = relDuration;
			attributes.pause          = release.pause;
			attributes.start          = release.start;
			attributes.missingPlaning = missingPlaning;
			attributes.comment        = release.comment || '';

			return attributes;
		}
	});

	window.app.HallsWeek = Backbone.Collection.extend({
		model: window.app.HallWeek,
		initialize: function initialize(weekSchedule) {
			this.weekSchedule = weekSchedule;

			this.listenTo(weekSchedule, 'sync', this.fillingHalls);

			reqres.setHandlers({
				'get:hallsWeek': {
					callback: function() {
						return this;
					},
					context: this
				}
			});
		},
		fillingHalls: function fillingHalls() {
			this.grabHalls();
			this.each(function(hallWeek) {
				var seances  = hallWeek.fillingSeances([]),
					hallId   = hallWeek.get('id'),
					halls,
					day;

				if (this.weekSchedule.get('schedule').length > 0) {
					halls = this.weekSchedule.get('schedule')[0].halls;

					_.each(halls, function(hall) {
						if (hall.id === hallId) {
							seances = _.map(seances, function(seance) {
								day = _.findWhere(hall.seances, {date: seance.date});
								if (day) seance.releases = day.releases;
								return seance;
							});
						}
					});
				}

				if (!hallWeek.get('title')) hallWeek.set({title: hallWeek.get('number').toString()});

				hallWeek.set({
					days_week: this.weekSchedule.get('days_week'),
					week:      this.weekSchedule.get('currentWeek'),
					year:      this.weekSchedule.get('currentYear'),
					seances:   seances
				});
			}, this);

			Backbone.trigger('HallsWeek:filled');
		},
		grabHalls: function grabHalls() {
			var currentCinema = reqres.request('get:user').getCinema(),
				halls = [];

			_.each(currentCinema.halls.sort(function(a, b) {
				return a.number - b.number;
			}), function(hall) {
				halls.push(hall);
			}, this);

			this.set(halls);
		}
	});

});

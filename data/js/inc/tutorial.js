$(function() {
	'use strict';
	window.app.tour = new Tour({
		name: 'tour',
		version: '0.11',
		backdrop: true,
		onEnd: function onEnd(tour) {
			if (tour.getCurrentStep() === 17) {
				_gaq.push(['_trackEvent', 'Обучение', 'Репертуар: Успешное завершение']);
			} else {
				_gaq.push(['_trackEvent', 'Обучение', 'Репертуар: Прервано']);
			}
			$.arcticmodal('close');
			$('.repertoire_wrap__head .duration:eq(1)').html(tour.prevText);
			if (tour.empty) {
				$('.repertoire_wrap__body>.row.release:eq(0)')
					.removeClass('release')
					.find('.week:eq(0)').removeClass('colored').addClass('add_new_release')
					.find('.cell').removeClass('color blue');
			}

			if (window.tourPlaningPopup) {
				window.tourPlaningPopup.remove();
				window.tourAddReleasePopupView = undefined;
			}

			if (window.tourAddReleasePopupView) {
				window.tourAddReleasePopupView.remove();
				window.tourAddReleasePopupView = undefined;
			}
		},
		template: '<div class="popover tour-tour"> <div class="arrow"></div> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-tour-group"> <button class="btn-tour btn-tour-sm btn-tour-default" data-role="prev">« Назад</button> <button class="btn-tour btn-tour-sm btn-tour-default" data-role="next">Далее »</button> </div> <button class="btn-tour btn-tour-sm btn-tour-default" data-role="end">Закончить</button> </div> </div>',
		debug: false,
		storage: false,
		orphan: true,
		empty: false,
		steps: [{
			element: '[data-tour="planning"]',
			placement: 'right',
			title: '1',
			content: 'Раздел, который поможет держать под контролем Ваши планы проката!',
			onNext: function() {
				Backbone.trigger('router:navigate', '/planning');
			}
		}, {
			element: '[data-tour="show-planning-list"]',
			placement: 'right',
			title: '2',
			content: 'Список фильмов запланированных для показа в выбранном кинотеатре'
		}, {
			element: '[data-tour="show-planning-week"]',
			placement: 'bottom',
			title: '3',
			content: 'Репертуар может составляться как на текущий месяц, так и на будущие',
			onNext: function(tour) {
				$('#my_repertoire_wr').scrollTo(0, 400);

				if (!$('.repertoire_wrap__body>.row.release:first .week:not(.prolong_release):first').length) {
					tour.empty = true;
					window.app.tour._options.steps[8].element = '.repertoire_wrap__body>.row:eq(0) .week:eq(1)';
					$('.repertoire_wrap__body>.row:eq(0)')
						.addClass('release')
						.find('.week:eq(0)').removeClass('add_new_release')
						.find('.cell').addClass('color blue');
				}

			}
		}, {
			element: '.repertoire_wrap__body>.row.release:first .week:not(.prolong_release):first',
			placement: 'bottom',
			title: '4',
			content: 'Каждая цветная полоска - это фильм, число - общее количество сеансов, запланированных на этой неделе'
		}, {
			element: '.repertoire_wrap__head .add_week_release:eq(1)',
			placement: 'bottom',
			title: '5',
			content: 'Добавить новый фильм в репертуар можно двумя способами. Первый - эта кнопка',
			onNext: function() {
				$('#my_repertoire_wr').stop().scrollTo('.repertoire_wrap__body>.row:not(.release):first', 400, {
					offset: -200
				});
			},
			onPrev: function() {
				$('#my_repertoire_wr').scrollTo(0, 400);
			}
		}, {
			element: '.repertoire_wrap__body>.row:not(.release):first .week:first',
			placement: 'top',
			title: '6',
			content: 'Второй - навести на пустую строку и нажать плюсик',
			reflex: true,
			onPrev: function() {
				$('#my_repertoire_wr').scrollTo(0, 400);
			},
			onNext: function() {
				$('#my_repertoire_wr').scrollTo(0, 400);
				window.tourAddReleasePopupView = new window.app.TourAddReleasePopupView();
			}
		}, {
			element: '.my_add_popup input',
			placement: 'left',
			title: '7',
			content: 'В репертуар можно поставить любой фильм из нашей базы данных',
			onPrev: function() {
				$.arcticmodal('close');
				$('#my_repertoire_wr').stop().scrollTo('.repertoire_wrap__body>.row:not(.release):first', 400, {
					offset: -200
				});
			}
		}, {
			element: '.my_add_popup .tabs span:first',
			placement: 'left',
			title: '8',
			content: 'Или выбрать из списка фильмы, которые идут на текущей или предыдущей неделе',
			onNext: function() {
				$.arcticmodal('close');
				$('#my_repertoire_wr').stop().scrollTo('.repertoire_wrap__body .prolong_release:first', 400, {
					offset: -200
				});
			}
		}, {
			element: '.repertoire_wrap__body .prolong_release:first',
			placement: 'top',
			title: '9',
			content: 'После добавления релиза можно указать сколько недель он будет в прокате',
			onPrev: function() {
				$('#my_repertoire_wr').scrollTo(0, 400);
				window.tourAddReleasePopupView = new window.app.TourAddReleasePopupView();
			},
			onNext: function() {
				$('#my_repertoire_wr').scrollTo(0, 400);
			}
		}, {
			element: '',
			placement: 'top',
			title: '10',
			content: 'Теперь можно приступить к указанию количества сеансов',
			onPrev: function() {
				$('#my_repertoire_wr').stop().scrollTo('.repertoire_wrap__body .prolong_release:first', 400, {
					offset: -200
				});
			},
			onNext: function() {
				window.tourPlaningPopup = new window.app.TourPlaningPopupView();
			}
		}, {
			element: '.tour-popup .table>.item:first .week',
			placement: 'left',
			title: '11',
			content: 'Количество сеансов указывается для каждой недели...',
			onPrev: function() {
				$.arcticmodal('close');
			}
		}, {
			element: '.tour-popup .table>.head .format:eq(1)',
			placement: 'left',
			title: '12',
			content: '...и каждого формата отдельно'
		}, {
			element: '.tour-popup .new_format__top',
			placement: 'left',
			title: '13',
			content: 'Если основных форматов не хватает, можно завести свой собственный',
			onShow: function() {
				$('.tour-popup .new_format__top').click();
			},
			onHide: function() {
				$('.tour-popup .new_format__top').click();
			}
		}, {
			element: '.tour-popup .add',
			placement: 'left',
			title: '14',
			content: 'Кстати, добавлять недели проката можно и отсюда',
			onNext: function() {
				$('.tour-popup .active_status').click();
			}
		}, {
			element: '.tour-popup .choose',
			placement: 'right',
			title: '15',
			content: 'После того как все сделано можно указать статус фильма в прокате, чтобы сотрудники знали, что с ним можно работать',
			onNext: function(tour) {
				tour.prevText = $('.repertoire_wrap__head .duration:eq(1)').html();
				$.arcticmodal('close');
				if (!$('.repertoire_wrap__head .duration:eq(1)').find('.cinema_time').length) {
					$('.repertoire_wrap__head .duration:eq(1)').html('<span class="cinema_time">5760 </span><span class="rel_time low">-4240</span>');
				}
			},
			onPrev: function() {
				$('.tour-popup .choose .item.active').click();
			}
		}, {
			element: '.repertoire_wrap__head .duration:eq(1)',
			placement: 'bottom',
			title: '16',
			content: '<p>Для понимания того, сколько сеансов фильмов можно добавить в прокат, мы рассчитываем два числа.</p><p>Первое - сколько времени в день кинотеатр работает.</p><p>Второе - сколько времени осталось свободным, с учетом уже добавленных фильмов и количества сеансов.</p><p>Если второе число становится красным то, <b>скорее всего</b>, запланированные сеансы не поместятся в ежедневное расписание.</p>',
			onPrev: function(tour) {
				window.tourPlaningPopup = new window.app.TourPlaningPopupView();
				$('.tour-popup .active_status').click();
				$('.repertoire_wrap__head .duration:eq(1)').html(tour.prevText);
			},
			onNext: function(tour) {
				$('.tour-popup .active_status').click();
				$('.repertoire_wrap__head .duration:eq(1)').html(tour.prevText);
			}
		}, {
			element: '.sidebar .feedback',
			placement: 'top',
			title: '17',
			content: '<p>Пишите нам!</p><p>Например, можно сообщить время работы своего кинотеатра — тогда мы сможем точнее рассчитывать остаток свободного времени в кинотеатре после составления репертуара.</p>',
			onPrev: function() {
				if (!$('.repertoire_wrap__head .duration:eq(1)').find('.cinema_time').length) {
					$('.repertoire_wrap__head .duration:eq(1)').html('<span class="cinema_time">5760 </span><span class="rel_time low">-4240</span>');
				}
			}
		}, {
			element: '.restart-tour',
			placement: 'right',
			title: '18',
			content: 'Удачного репертуарного планирования! Если возникнут вопросы, всегда можно нажать сюда и вновь пройтись по шагам процесса.'
		}]
	});

	window.app.readOnlyTour = new Tour({
		name: 'readOnlyTour',
		backdrop: true,
		onEnd: function onEnd(tour) {
			if (tour.getCurrentStep() === 17) {
				_gaq.push(['_trackEvent', 'Обучение', 'Репертуар: Успешное завершение']);
			} else {
				_gaq.push(['_trackEvent', 'Обучение', 'Репертуар: Прервано']);
			}
			$.arcticmodal('close');
			$('.repertoire_wrap__head .duration:eq(1)').html(tour.prevText);
			if (tour.empty) {
				$('.repertoire_wrap__body>.row.release:eq(0)')
					.removeClass('release')
					.find('.week:eq(0)').removeClass('colored').addClass('add_new_release')
					.find('.cell').removeClass('color blue');
			}
		},
		template: '<div class="popover tour-tour"> <div class="arrow"></div> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-tour-group"> <button class="btn-tour btn-tour-sm btn-tour-default" data-role="prev">« Назад</button> <button class="btn-tour btn-tour-sm btn-tour-default" data-role="next">Далее »</button> </div> <button class="btn-tour btn-tour-sm btn-tour-default" data-role="end">Закончить</button> </div> </div>',
		debug: false,
		storage: false,
		orphan: true,
		empty: false,
		steps: [{
			element: '[data-tour="planning"]',
			placement: 'right',
			title: '1',
			content: 'Раздел, который поможет держать под контролем Ваши планы проката!',
			onNext: function() {
				Backbone.trigger('router:navigate', '/planning');
			}
		}, {
			element: '[data-tour="show-planning-list"]',
			placement: 'right',
			title: '2',
			content: 'Список фильмов запланированных для показа в выбранном кинотеатре'
		}, {
			element: '[data-tour="show-planning-week"]',
			placement: 'bottom',
			title: '3',
			content: 'Репертуар может составляться как на текущий месяц, так и на будущие',
			onNext: function(tour) {
				$('#my_repertoire_wr').scrollTo(0, 400);

				if (!$('.repertoire_wrap__body>.row.release:first .week:not(.prolong_release):first').length) {
					tour.empty = true;
					window.app.tour._options.steps[8].element = '.repertoire_wrap__body>.row:eq(0) .week:eq(1)';
					$('.repertoire_wrap__body>.row:eq(0)')
						.addClass('release')
						.find('.week:eq(0)').removeClass('add_new_release')
						.find('.cell').addClass('color blue');
				}

			}
		}, {
			element: '.repertoire_wrap__body>.row.release:first .week:not(.prolong_release):first',
			placement: 'bottom',
			title: '4',
			content: 'Каждая цветная полоска - это фильм, число - общее количество сеансов, запланированных на этой неделе',
			onNext: function() {
				window.tourAddReleasePopupView = new window.app.TourAddReleasePopupView();
			}
		}, {
			element: '.my_add_popup input',
			placement: 'left',
			title: '7',
			content: 'В репертуар можно поставить любой фильм из нашей базы данных',
			onPrev: function() {
				$.arcticmodal('close');
			},
			onNext: function() {
				$.arcticmodal('close');
				window.tourPlaningPopup = new window.app.TourPlaningPopupView();
			}
		}, {
			element: '.tour-popup .table>.item:first .week',
			placement: 'left',
			title: '11',
			content: 'Количество сеансов указывается для каждой недели...',
			onPrev: function() {
				$.arcticmodal('close');
				window.tourAddReleasePopupView = new window.app.TourAddReleasePopupView();
			}
		}, {
			element: '.tour-popup .table>.head .format:eq(1)',
			placement: 'left',
			title: '12',
			content: '...и каждого формата отдельно'
		}, {
			element: '.tour-popup .new_format__top',
			placement: 'left',
			title: '13',
			content: 'Если основных форматов не хватает, можно завести свой собственный',
			onShow: function() {
				$('.tour-popup .new_format__top').click();
			},
			onHide: function() {
				$('.tour-popup .new_format__top').click();
			}
		}, {
			element: '.tour-popup .add',
			placement: 'left',
			title: '14',
			content: 'Кстати, добавлять недели проката можно и отсюда',
			onNext: function() {
				$('.tour-popup .active_status').click();
			}
		}, {
			element: '.tour-popup .choose',
			placement: 'right',
			title: '15',
			content: 'После того как все сделано можно указать статус фильма в прокате, чтобы сотрудники знали, что с ним можно работать',
			onNext: function(tour) {
				tour.prevText = $('.repertoire_table .duration:eq(1)').html();
				$.arcticmodal('close');
				if (!$('.repertoire_table .duration:eq(1)').find('.cinema_time').length) {
					$('.repertoire_table .duration:eq(1)').html('<span class="cinema_time">5760 </span><span class="rel_time low">-4240</span>');
				}
			},
			onPrev: function() {
				$('.tour-popup .choose .item.active').click();
			}
		}, {
			element: '.repertoire_wrap__head .duration:eq(1)',
			placement: 'bottom',
			title: '16',
			content: '<p>Для понимания того, сколько сеансов фильмов можно добавить в прокат, мы рассчитываем два числа.</p><p>Первое - сколько времени в день кинотеатр работает.</p><p>Второе - сколько времени осталось свободным, с учетом уже добавленных фильмов и количества сеансов.</p><p>Если второе число становится красным то, <b>скорее всего</b>, запланированные сеансы не поместятся в ежедневное расписание.</p>',
			onPrev: function(tour) {
				window.tourPlaningPopup = new window.app.TourPlaningPopupView();
				$('.tour-popup .active_status').click();
				$('.repertoire_table .duration:eq(1)').html(tour.prevText);
			},
			onNext: function(tour) {
				$('.tour-popup .active_status').click();
				$('.repertoire_table .duration:eq(1)').html(tour.prevText);
			}
		}, {
			element: '.sidebar .feedback',
			placement: 'top',
			title: '17',
			content: '<p>Пишите нам!</p><p>Например, можно сообщить время работы своего кинотеатра — тогда мы сможем точнее рассчитывать остаток свободного времени в кинотеатре после составления репертуара.</p>',
			onPrev: function() {
				if (!$('.repertoire_table .duration:eq(1)').find('.cinema_time').length) {
					$('.repertoire_table .duration:eq(1)').html('<span class="cinema_time">5760 </span><span class="rel_time low">-4240</span>');
				}
			}
		}, {
			element: '.restart-tour',
			placement: 'right',
			title: '18',
			content: 'Удачного репертуарного планирования! Если возникнут вопросы, всегда можно нажать сюда и вновь пройтись по шагам процесса.'
		}]
	});

	window.app.TourPlaningPopupView = Backbone.View.extend({
		className: 'tour-popup',
		template: '#tour-planing-popup',
		events: {
			'click .exit': function() {
				$.arcticmodal('close');
				window.app.tour.end();
			},
			'click .add': function(e) {
				e.preventDefault();
			},
			'click .save': function() {
				$.arcticmodal('close');
			},
			'click .active_status': function(e) {
				var target = $(e.target),
					wrap   = target.closest('.choose_wrap'),
					choose = wrap.find('.choose'),
					items  = choose.find('.item'),
					popup  = wrap.closest('.change_my_popup');

				target.hide();
				choose.show();

				items.each(function() {
					var item   = $(this),
						text   = item.text(),
						status = item.data('status');

					item.click(function() {
						items.removeClass('active');
						item.addClass('active');

						target.attr('data-status', status).text(text);
						popup.attr('data-status', status);

						target.show();
						choose.hide();
					});
				});
			},
			'click .new_format__top': function(e) {
				var $parent          = $(e.currentTarget).closest('.new_format'),
					$block           = $parent.find('.add_format'),
					blockWidth       = $block.outerWidth(),
					$firstFormatItem = $block.find('.list .item').first(),
					$inputs          = $block.find('.technology input').not('#sub');

				$parent.toggleClass('active');

				$block.css({
					marginLeft: -(blockWidth / 2)
				});
				$block.toggleClass('visible');

				$firstFormatItem.addClass('active');

				$parent.siblings().find('.custom_format').each(function() {
					$(this).removeClass('visible');
				});
				$parent.siblings().find('.format_custom_block').each(function() {
					$(this).removeClass('active');
				});

				if ($firstFormatItem.data('format') === 'Public Video' || $firstFormatItem.data('format') === 'imax') {
					$inputs.attr('disabled', 'disabled').removeAttr('checked');
				}
			}
		},
		initialize: function initialize() {
			var self = this;
			$('body').prepend(this.render().el);
			this.$el.arcticmodal({
				afterClose: function() {
					self.remove();
				}
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());
			return this;
		},
		remove: function remove() {
			this.$el.empty();
			Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.TourAddReleasePopupView = Backbone.View.extend({
		className: 'tour-popup',
		template: '#tour-add-release-popup',
		initialize: function initialize() {
			var self = this;
			$('body').prepend(this.render().el);
			this.$el.arcticmodal({
				afterClose: function() {
					self.remove();
				}
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());
			return this;
		},
		remove: function remove() {
			this.$el.empty();
			Backbone.View.prototype.remove.call(this);
		}
	});

});

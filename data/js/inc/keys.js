$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.Key = Backbone.Model.extend({
		defaults: {
			cplid: '',
			created: null,
			dcp: {},
			hall: {},
			hidden: 0,
			path: '',
			play_server: null,
			release_date: null,
			release_id: null,
			release_title: {},
			title: '',
			valid_since: null,
			valid_till: null
		}
	});

	window.app.Keys = Backbone.Collection.extend({
		model: window.app.Key,
		url: '/api/kdm/all',
		initialize: function initialize() {
			this._releases = [];
		},
		comparator: function comparator(a, b) {
			return b.get('release_date') - a.get('release_date');
		},
		parse: function parse(res) {
			return _.chain(res.kdm)
				.filter(function(key) { return !key.hidden || !key.deleted; })
				.each(function(key) {
					key.cinema_id    = key.hall.cinema_id;
					key.release_date = moment(key.release_date);
					key.valid_since  = moment(key.valid_since);
					key.valid_till   = moment(key.valid_till);
				})
				.sortBy(function(release) { return -release.release_date; })
				.value();
		},
		getReleases: function getReleases(cinemaID) {
			if (!this._releases.length) {
				this._releases = _.chain(this.toJSON())
					.groupBy('release_id')
					.map(function(group) {
						var release = _.first(group);

						release.valid_since = group.min(function(key) { return key.valid_since;	}).valid_since;
						release.valid_till  = group.max(function(key) { return key.valid_till;  }).valid_till;
						release.keys_number = group.length;

						return release;
					})
					.sortBy(function(release) { return -release.release_date; })
					.value();
			}

			if (cinemaID) {
				return _.filter(this._releases, function(release) {
					return release.hall.cinema_id === cinemaID;
				});
			}

			return this._releases;
		},
		getKeys: function getKeys(cinemaID, releaseID) {
			var keys = cinemaID ? _.where(this.toJSON(), {cinema_id: cinemaID}) : this.toJSON();
			return _.where(keys, {release_id: releaseID || (keys.length && _.first(keys).release_id)});
		}
	});

	window.app.KeysWrapView = Backbone.View.extend({
		id:             'keys',
		template:       '#keys_wrapper',
		filterTemplate: '#keys_filter_tmpl',
		keysTemplate:   '#keys_tmpl',
		events: {
			'click .list.scroll .item:not(".active")': /* istanbul ignore next */ function(event) {
				var $target = $(event.currentTarget);
				$target.addClass('active').siblings().removeClass('active');
				this.renderKeys($target.data('release_id'));
			},
			'change .b-filter': /* istanbul ignore next */ function(event) {
				var cinemaID = parseInt($(event.currentTarget).val(), 10);
				this.cinemaFilter = cinemaID;
				this.renderReleases(cinemaID).renderKeys();
			},
			'click .kdm_table_item .url': /* istanbul ignore next */ function() {
				_gaq.push(['_trackEvent', 'Ключи', 'Скачали ключ']);
			}
		},
		initialize: function initialize() {
			this.user = reqres.request('get:user');

			this.cinemaFilter = '';

			if (this.user.isApproved()) {
				this.loadKDM();
			} else {
				this.$el.html(Backbone.TemplateCache.get('#unapprovedTemplate')({accessTo: 'ключам'}));
			}
		},
		loadKDM: function loadKDM() {
			var self = this;

			this.$el
				.find('.search .input').val('').end()
				.find('.list.scroll, .info.scroll').empty();

			if (this.collection.models.length) {
				this.renderView();
			} else {
				Backbone.trigger('loader', 'show');
				this.collection.fetch({
					success: function() {
						Backbone.trigger('loader', 'hide');
						self.renderView();
					}
				});
			}
		},
		renderView: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({
				currentFilter: this.cinemaFilter,
				cinemas: _.map(this.user.getCinemas(), function(cinema) {
					return {
						id: cinema.id,
						title: cinema.title.ru || cinema.title.en + ',' + cinema.city.title.ru || cinema.city.title.en
					};
				})
			})).find('.b-filter').styler();

			this.renderReleases().renderKeys().addSearching();

			return this;
		},
		renderReleases: function renderReleases(cinemaID) {
			var filterTemplate = Backbone.TemplateCache.get(this.filterTemplate);

			this.$el.find('.list.scroll')
				.html(filterTemplate({
					releases: this.collection.getReleases(cinemaID)
				}))
				.find('.item:first').addClass('active');

			return this;
		},
		renderKeys: function renderKeys(releaseID) {
			var keysTemplate = Backbone.TemplateCache.get(this.keysTemplate),
				user         = this.user,
				cinemas      = user.getCinemas(),
				keysGroups,
				sortedCinemas;

			keysGroups   = _.chain(this.collection.getKeys(this.cinemaFilter, releaseID)
				.sort(function(a, b) {
					var date = a.valid_since - b.valid_since;
					return date ? date : a.hall.number - b.hall.number;
				}))
				.map(function(key) {
					key.valid_since  = key.valid_since.format('DD.MM.YYYY, HH:mm');
					key.valid_till   = key.valid_till.format('DD.MM.YYYY, HH:mm');

					return key;
				})
				.groupBy('cinema_id')
				.value();

			sortedCinemas = _.chain(keysGroups).keys().sortByNat(function(cinemaID) {
					var cinema = user.getCinema(parseInt(cinemaID, 10));
					return cinema && cinema.title.ru;
				})
				.value();

			this.$el.find('.info.scroll').html(keysTemplate({
				keysGroups:      keysGroups,
				admin:           user.isAdmin(),
				accessToKdm:     user.accessTo('kdmContent'),
				cinemas:         cinemas,
				sortedCinemas:   sortedCinemas,
				showCinemaTitle: !this.cinemaFilter && user.isMultiplex() ? true : false
			}));

			return this;
		},
		addSearching: function addSearching() {
			var self = this;

			this.searching = new window.app.SearchingModuleView({
				onSearchingFinish: function onSearchingFinish(value) {
					self.$('#keys_wr .list .item')
						.hide().end()
						.find('.ru:containsLower(' + value + '), .en:containsLower(' + value + ')').closest('.item').show();
				}
			});
			this.$('.search').append(this.searching.render().el);

			return this;
		}
	});

});

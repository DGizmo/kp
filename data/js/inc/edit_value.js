$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.EditValueView = Backbone.View.extend({
		className: 'edit-value-popup',
		template: '#edit_value_tmpl',
		events: {
			mousewheel: 'mouseWheel',
			DOMMouseScroll: 'mouseWheel',
			click: function(event) {
				event.stopPropagation();
			},
			'click .edit-value-popup-top__input-plus': /* istanbul ignore next */ function() {
				this.changeInputCount(1);
			},
			'click .edit-value-popup-top__input-minus': /* istanbul ignore next */ function() {
				this.changeInputCount(-1);
			},
			'input .edit-value-popup-top__input': /* istanbul ignore next */ function() {
				this.changeInputCount();
			},
			'keydown .edit-value-popup__input': /* istanbul ignore next */ function(event) {
				// Allow: backspace, delete, tab, escape, enter
				if ($.inArray(event.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
					// Allow: Ctrl+A
					(event.keyCode === 65 && event.ctrlKey === true) ||
					// Allow: home, end, left, right
					(event.keyCode >= 35 && event.keyCode <= 40)) {
					// let it happen, don't do anything
					return;
				}
				// Ensure that it is a number and stop the keypress
				if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
					event.preventDefault();
				}
			},
			'click .edit-value-popup__save': 'saveValue'
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.onSave  = this.options.onSave;

			if (typeof this.onSave !== 'function')
				throw 'editValuePopover onSave undefined.';

			var self = this;
			$(document).on('keydown', /* istanbul ignore next */ function(event) {
				if (event.keyCode === 13) {
					self.changeInputCount().saveValue(event);
				}
			});

			this.listenTo(Backbone, 'body:click', this.remove);
		},
		mouseWheel: /* istanbul ignore next */ function mouseWheel(event) {
			var $input = this.$('.edit-value-popup-top__input:focus'),
				wheelDelta = event.originalEvent.detail ? event.originalEvent.detail * (-1) : event.originalEvent.wheelDelta;
			if ($input.length) {
				wheelDelta > 0 ? this.changeInputCount(1) : this.changeInputCount(-1);
			}
			event.preventDefault();
		},
		changeInputCount: /* istanbul ignore next */ function changeInputCount(value) {
			var $input = this.$('.edit-value-popup-top__input'),
				currentVal = +$input.val(),
				newVal = currentVal + (value || 0);

			if ((currentVal === 1 && value < 0) || (currentVal === 500 && value > 0)) {
				return this;
			} else if (newVal.length === 0 || newVal < 1 || newVal > 500 || !_.isFinite(newVal)) {
				$.growl.error({ title: 'Ошибка', message: 'Вводимое значение должно быть числом лежащим в диапазоне от 1 до 500' });
				$input.val(1);
			} else {
				$input.val(newVal);
			}

			return this;
		},
		saveValue: function saveValue(event) {
			event.preventDefault();
			this.onSave(parseInt(this.$('.edit-value-popup-top__input').val(), 10));
			this.remove();
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template({value: this.options.value}));
			return this;
		},
		remove: function remove() {
			$(document).off('keydown');
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

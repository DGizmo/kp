$(function() {
	'use strict';
	window.app.HallsHeaderView = Backbone.View.extend({
		className: 'halls_head',
		template: '#halls_header_tmpl',
		initialize: function initialize(options) {
			document.title = 'Планирование по залам - Киноплан';
			_gaq.push(['_trackEvent', 'Репертуарное планирование', 'Планирование']);

			this.innerViews    = [];
			this.options       = options || {};
			this.$wrapper      = this.options.parent.$el.find('#halls .cinema_schedule');
			this.calendar      = this.options.calendar;
			this.fullHallsMode = reqres.request('get:repertoire:view').fullHallsMode;

			this.$wrapper.append('<div class="halls_body" screen-page="1"/>');

			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this, 'remove', this.removeView);

			this.listenTo(this.options.parent.repertoire, 'add', function(model) {
				this.changeModels(model);
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.halls = reqres.request('get:user').getCinema().halls.sortBy(function(n) {
				return n.number;
			});

			this.$wrapper.prepend(this.$el.html(template(extModel)));

			if (this.fullHallsMode && extModel.halls.length > 7) {
				this.$el.find('.list .item').css({
					width: 'calc(100% / ' + extModel.halls.length + ')'
				});
			}

			this.setModelData(this.$wrapper.children('.halls_body'));

			if (reqres.request('get:app:state') === 'planning:halls') {
				if (reqres.request('get:user').getCinema().halls.length > 7) {
					if (this.fullHallsMode) {
						this.options.parent.$el.find('.filter').children('.full_halls_mode').show().siblings('.screen, .btn_export, .full_weeks_mode_wrapper').hide();
					} else {
						this.options.parent.$el.find('.filter').children('.screen, .full_halls_mode').show().siblings('.btn_export, .full_weeks_mode_wrapper').hide();
					}
				} else {
					this.options.parent.$el.find('.filter').children('.screen, .full_halls_mode, .btn_export, .full_weeks_mode_wrapper').hide();
				}
			}

			return this;
		},
		setModelData: function setModelData(hallsBody) {
			var self          = this,
				modelMonth    = this.model.get('currentMonth'),
				modelYear     = this.model.get('currentYear'),
				range         = Date.range(Date.create().set({month: modelMonth, year: modelYear, day: 1}).beginningOfMonth(), Date.create().set({month: modelMonth, year: modelYear, day: 1}).endOfMonth()),
				line          = [],
				currentCinema = reqres.request('get:user').getCinema(),
				$activeTab    = this.options.parent.$el.find('.filter .repertoire_sections .active');

			range.every('weekday', function(weekday) {
				var day     = weekday,
					dayName = Date.create(weekday).format('{weekday}'),
					date;

				if (dayName === 'четверг') {
					date = Date.create(day).format('{d}');

					line.push({
						date:               date,
						currentMonth:       modelMonth,
						currentYear:        modelYear,
						leftPosition:       142,
						leftObjectPosition: 0
					});
				}
			});

			this.model.set({
				line:  line
			}, { silent: true });

			if ($activeTab.hasClass('section_halls')) {
				if (currentCinema.halls.length > 7) {
					this.options.parent.$el.find('.filter .screen').show();
				} else {
					this.options.parent.$el.find('.filter .screen').hide();
				}
			}

			hallsBody.empty();

			this.weeksLength = line.length;

			_.each(line, function(weekDay) {
				var length    = line.length,
					weekModel = new window.app.HallsPlanning();

				weekModel.set({
					currentMonth: this.model.get('currentMonth'),
					currentYear:  this.model.get('currentYear'),
					date:         weekDay.date,
					halls:        currentCinema.halls,
					line:         weekDay,
					weekLength:   length
				});

				this.addOneLine(hallsBody, weekModel);
			}, this);

			setTimeout(function() {
				_.each(reqres.request('get:repertoire:view').repertoire.models, function(model) {
					if (model.get('release_id')) {
						self.listenTo(model, 'change', function() {
							self.changeModels(model);
						});
						self.changeModels(model);
					}
				});

				self.checkWeekHeight();
				self.checkHallsScroll();
				Backbone.trigger('loader', 'hide');
				Backbone.trigger('planning:screenBlocker', 'hide');
			}, 0);
		},
		addOneLine: function addOneLine(wrap, weekModel) {
			var Line = new window.app.HallsView({
				model: weekModel,
				parent: this
			});

			Line.parent = this;
			wrap.append(Line.render().el);

			this.innerViews.push(Line);
		},
		changeModels: function changeModels(m) {
			var $hallWeek     = this.$wrapper.find('.halls_body .week'),
				cinema_id     = m.get('cinema_id'),
				release       = m.get('release'),
				release_id    = m.get('release_id'),
				color         = m.get('color'),
				currentCinema = reqres.request('get:user').getCinema(),
				self          = this;

			_.each(m.get('weeks'), function(week) {
				var count      = week.count,
					weekNumber = parseInt(week.number),
					technology = week.technology;

				_.each(technology, function(format) {
					var $week    = $hallWeek.filter('[data-week="' + weekNumber + '"]'),
						relModel,
						$sections,
						hallsList,
						$notHallReleaseSections,
						upgradesRel,
						formatRel,
						$list,
						section;

					$.fn.repertoireReleases = function() {
						relModel = new window.app.OneReleaseModel({
							cinema_id:     cinema_id,
							release:       release,
							release_id:    release_id,
							status:        color,
							count:         count,
							week:          weekNumber,
							technology:    format,
							cinema_rights: currentCinema.rights_id
						});

						this.find('.release').each(function() {
							var $oneRepRelease  = $(this),
								releaseId       = parseInt($oneRepRelease.data('id')),
								releaseFormat   = $oneRepRelease.data('format'),
								releaseUpgrades = $oneRepRelease.data('upgrades');

							if (releaseId === release_id && releaseFormat === format.format && releaseUpgrades === format.upgrades.join(', ')) {
								$oneRepRelease.remove();
							}
						});

						format.count_hall = format.count_hall || 0;

						section = new window.app.SectionView({
							model: relModel,
							parent: self
						});

						this.append(section.render().el);
					};

					if (format.hall_id) {
						format.count_hall = format.count_hall || 0;

						hallsList               = format.hall_id;
						$notHallReleaseSections = $week.find('.section');
						upgradesRel             = format.upgrades.join(', ');
						formatRel               = format.format;

						_.each(_.keys(hallsList), function(hall) {
							relModel = new window.app.OneReleaseModel({
								cinema_id:     cinema_id,
								release:       release,
								release_id:    release_id,
								status:        color,
								count:         count,
								week:          weekNumber,
								technology:    format,
								cinema_rights: currentCinema.rights_id,
								hall_id:       parseInt(hall)
							});

							var section = new window.app.SectionView({
									model: relModel,
									parent: self
								}),
								$section          = $week.find('.section').filter('[data-hall-id="' + hall + '"]'),
								$inSectionRelease = $section.children('.release').filter(function() {
									return parseInt($(this).data('id')) === release_id && $(this).data('upgrades') === upgradesRel && $(this).data('format') === formatRel;
								}),
								$matchRelease,
								$toBefore;

							$section.find('.repertoire__list').children('.release').filter(function() {
								return parseInt($(this).data('id')) === release_id && $(this).data('upgrades') === upgradesRel && $(this).data('format') === formatRel;
							}).remove();

							if ($inSectionRelease.length) {
								$matchRelease = $inSectionRelease;
								$toBefore     = $matchRelease.next();

								$matchRelease.remove();

								$toBefore.before(section.render().el);
							} else {
								$section.find('.repertoire__list').before(section.render().el);
							}

							$notHallReleaseSections = $notHallReleaseSections.not('[data-hall-id="' + hall + '"]');
						});

						$notHallReleaseSections.find('.release').filter(function() {
							return parseInt($(this).data('id')) === release_id && $(this).data('upgrades') === upgradesRel && $(this).data('format') === formatRel;
						}).remove();

						$list = $notHallReleaseSections.find('.repertoire__list');

						$list.repertoireReleases();
					} else {
						$sections = $week.find('.section .repertoire__list');

						$sections.repertoireReleases();
					}
				});
			});

			$hallWeek.each(function() {
				var $oneWeek         = $(this),
					$oneWeekSections = $oneWeek.find('.section'),
					id,
					$release;

				$oneWeekSections.each(function() {
					if ($(this).children('.release').length === 1) {
						$release = $(this).children('.release');
						id       = $release.data('id');

						$(this).children('.one-release-poster').addClass('active').css({
							backgroundImage: 'url(https://nas.dcp24.ru/img/movie/' + id + '_big.jpg)'
						}).attr('title', $release.attr('comment'));

						$(this).children('.one-release-status-count').addClass('active').attr('data-status', $release.find('.status').data('status'));

						if ($release.data('format') !== '2d' || $release.data('upgrades').length) {
							$(this).children('.one-release-format')
									.text($release.data('format') + ($release.data('upgrades').length ? '+' : ''))
									.attr('data-status', $release.find('.status').data('status'))
									.addClass('active');
						}
					} else {
						$(this).children('.one-release-poster, .one-release-status-count, .one-release-format').removeClass('active');
					}
				});
			});
		},
		checkHallsScroll: function checkHallsScroll() {
			var $wrap = this.$el.closest('.cinema_schedule'),
				scrollWidth;

			function getScrollBarWidth() {
				var inner = document.createElement('p'),
					outer,
					w1,
					w2;

				inner.style.width = '100%';
				inner.style.height = '200px';

				outer = document.createElement('div');
				outer.style.position = 'absolute';
				outer.style.top = '0px';
				outer.style.left = '0px';
				outer.style.visibility = 'hidden';
				outer.style.width = '200px';
				outer.style.height = '150px';
				outer.style.overflow = 'hidden';
				outer.appendChild (inner);

				document.body.appendChild(outer);
				w1 = inner.offsetWidth;
				outer.style.overflow = 'scroll';
				w2 = inner.offsetWidth;
				if (w1 === w2) w2 = outer.clientWidth;
				document.body.removeChild (outer);

				return (w1 - w2);
			}

			scrollWidth = getScrollBarWidth();

			if (scrollWidth > 0) {
				$wrap.css({
					width: 'calc(100% + ' + scrollWidth + 'px)'
				});
			}
		},
		checkWeekHeight: function checkWeekHeight(week) {
			var $hallsBody = this.$wrapper.children('.halls_body'),
				self = this,
				$weeks = week || $hallsBody.find('.week');

			$weeks.each(function() {
				var $oneWeek          = $(this),
					$oneWeekSection   = $oneWeek.find('.section'),
					maxSectionsHeight = 0;

				$oneWeekSection.each(function() {
					var $oneSection = $(this),
						sectionChildsHeight = 0;

					$oneSection.children('.release, .add_release').each(function() {
						sectionChildsHeight = sectionChildsHeight + $(this).outerHeight();
					});

					if (maxSectionsHeight < sectionChildsHeight) {
						maxSectionsHeight = sectionChildsHeight;
					}
				});

				if (maxSectionsHeight >= 160) {
					$oneWeek.css({
						minHeight: maxSectionsHeight + 'px'
					});
					$hallsBody.scrollTo(0);
				} else if (self.$wrapper.find('.halls_body').height() / self.weeksLength > 140) {
					$oneWeek.css({
						minHeight: 'calc(100% / ' + self.weeksLength + ' - 1px)'
					});
				} else {
					$oneWeek.css({
						minHeight: '140px'
					});
				}
			});
		},
		removeView: function removeView() {
			_.each(this.innerViews, function(oneView) {
				oneView.remove();
			});

			this.$wrapper.children('.halls_body').remove();

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.HallsView = Backbone.View.extend({
		className: 'week',
		template: '#halls_line_tmpl',
		events: {
			contextmenu: 'showContextMenu',
			'click .repertoire_viewer': function(e) {
				var $target           = $(e.currentTarget),
					$week             = $target.closest('.week'),
					$repList          = $week.find('.list'),
					maxSectionsHeight = 0;

				$target.toggleClass('active');
				$repList.toggleClass('active');

				if (!$repList.hasClass('active')) {
					$repList.find('.one-release-poster.ex_active, .one-release-status-count.ex_active, .one-release-format.ex_active').removeClass('ex_active').addClass('active');
					$repList.find('.release.added').each(function() {
						var $repRelease        = $(this),
							$repReleaseSection = $repRelease.closest('.section'),
							$newRelease        = $repRelease.removeClass('added');

						$repReleaseSection.find('.repertoire__list').before($newRelease);
					});

					$repList.find('.section').each(function() {
						var $oneSection = $(this),
							sectionChildsHeight = 0;

						$oneSection.children('.release, .add_release').each(function() {
							sectionChildsHeight = sectionChildsHeight + $(this).outerHeight();
						});

						if (maxSectionsHeight < sectionChildsHeight) {
							maxSectionsHeight = sectionChildsHeight;
						}
					});

					if (maxSectionsHeight > 160) {
						$week.css({
							'min-height': maxSectionsHeight + 'px'
						});
					} else {
						$week.css({
							'min-height': '160px'
						});
					}
				} else {
					$repList.find('.one-release-poster.active, .one-release-status-count.active, .one-release-format.active').removeClass('active').addClass('ex_active');

					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Режим Fast']);

					$repList.find('.section').each(function() {
						var $oneSection = $(this),
							sectionChildsHeight = 0;

						$oneSection.find('.release').each(function() {
							sectionChildsHeight = sectionChildsHeight + $(this).outerHeight();
						});

						if (maxSectionsHeight < sectionChildsHeight) {
							maxSectionsHeight = sectionChildsHeight;
						}
					});

					if (maxSectionsHeight > 160) {
						$week.css({
							'min-height': maxSectionsHeight + 'px'
						});
					}
				}

				this.checkRelasesCountInSections();
			},
			'click .add_release': function(e) {
				var $target  = $(e.currentTarget),
					$section = $target.closest('.section'),
					week     = parseInt($section.closest('.list').data('week')),
					cinema   = parseInt($section.closest('.list').data('cinema')),
					hall     = parseInt($section.data('hall-id'));

				if (week && cinema && hall) {
					this.showSettings($section, cinema, hall, week);
				}

				_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Табл::Добавление релиза']);
			},
			'click .one-release-poster': function(e) {
				$(e.currentTarget).siblings('.release').trigger('click');
			},
			'contextmenu .one-release-poster': function(e) {
				e.preventDefault();
				$(e.currentTarget).siblings('.release').trigger('contextmenu');
			},
			'mouseenter .one-release-poster': function(e) {
				if (this.model.get('cinema_rights') !== 1 && this.model.get('cinema_rights') !== 2) {
					$(e.currentTarget).siblings('.one-release-status-count').addClass('full-size');
				}
			},
			'mouseleave .section': function(e) {
				$(e.currentTarget).find('.one-release-status-count').removeClass('full-size');
			},
			'click .one-release-status-count.full-size': function(e) {
				$(e.currentTarget).removeClass('full-size').siblings('.add_release').trigger('click');
			}
		},
		initialize: function initialize(options) {
			var	fullDate      = Date.create().set({month: this.model.get('currentMonth'), day: this.model.get('date'), year: this.model.get('currentYear')}),
				currentCinema = reqres.request('get:user').getCinema();

			this.options  = options || {};
			this.$wrapper = options.parent.$wrapper;

			this.model.set({
				week:          fullDate.getWeek(),
				cinema_rights: currentCinema.rights_id,
				cinemaId:      currentCinema.id
			});

			this.listenTo(Backbone, 'planning:halls:hallCopy', function(week, hallId) {
				if (week === this.model.get('week')) {
					this.hallCopy({}, week, hallId);
				}
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.halls = this.model.get('halls').sortBy(function(n) {
				return n.number;
			});

			this.$el.html(template(extModel));

			if (this.$wrapper.find('.halls_body').height() / this.model.get('weekLength') > 140) {
				this.$el.css({
					minHeight: 'calc(100% / ' + this.model.get('weekLength') + ' - 1px)'
				});
			}

			this.$el.attr('data-week', this.model.get('week'));

			if (this.options.parent.fullHallsMode && extModel.halls.length > 7) {
				this.$el.find('.list .section').css({
					width: 'calc(100% / ' + extModel.halls.length + ')'
				});
			}

			return this;
		},
		checkRelasesCountInSections: function checkRelasesCountInSections() {
			this.$el.find('.list .section').each(function() {
				var $release = $(this).children('.release'),
					id;

				if ($release.length === 1) {
					id = $release.data('id');

					$(this).children('.one-release-poster').addClass('active').css({
						backgroundImage: 'url(https://nas.dcp24.ru/img/movie/' + id + '_big.jpg)'
					}).attr('title', $release.attr('comment'));

					$(this).children('.one-release-status-count').addClass('active').attr('data-status', $release.find('.status').data('status'));

					if ($release.data('format') !== '2d' || $release.data('upgrades').length) {
						$(this).children('.one-release-format')
								.text($release.data('format') + ($release.data('upgrades').length ? '+' : ''))
								.attr('data-status', $release.find('.status').data('status'))
								.addClass('active');
					}
				} else {
					$(this).children('.one-release-poster, .one-release-status-count, .one-release-format').removeClass('active');
				}
			});
		},
		showSettings: function showSettings(section, cinema, hall, week) {
			var sort       = $(section).find('.release').last().data('sort') + 1 || 0,
				year       = this.model.get('currentYear'),
				prevDate   = moment(0, 'HH').year(year).day(4).week(week).add(-1, 'week'),
				$hallsList = this.$wrapper.find('.halls_head .list .item'),
				hallIndex  = $(section).index(),
				hallTitle  = $hallsList.eq(hallIndex).find('.title').data('hall-title'),
				modelClone = this.model.clone(),
				hall_id    = $(section).data('hall-id'),
				$schedule  = this.$wrapper;

			modelClone.set({
				hall_title: hallTitle,
				hall_id: hall_id
			});

			this.settings = new window.app.relaseScheduleView({
				model: modelClone,
				parent: this.options.parent
			});

			$schedule.prepend('<div class="release_popup" />');
			$schedule.children('.release_popup').append(this.settings.render().$el);
			$schedule.children('.release_popup').arcticmodal({
				beforeOpen: function(data, el) {
					var popup = $(el).find('.cinema_schedule__popup');

					popup.find('.top').show();
				},
				afterOpen: function(data, el) {
					var popup = $(el).find('.cinema_schedule__popup'),
						$input = popup.find('.search .input'),
						currentWeekBlock = el.find('.short_list .releases').eq(0),
						prevWeekBlock = el.find('.short_list .releases').eq(1),
						$hallsList = $schedule.find('.halls_head .list .item'),
						hallIndex = $(section).index(),
						hallTitle = $hallsList.eq(hallIndex).find('.title').data('hall-title'),
						$shortSections = popup.find('.short_list .sections'),
						$searchResults = popup.find('.search .results'),
						addKeyFunction;

					popup.find('.search .input').focus();

					$.get('/api/releases/filter/?full=1&last_week=1&week=' + week + '&year=' + year + '&cinema_id=' + reqres.request('get:user').getCinema().id, function(res) {
						var r = res.list,
							week_length,
							i,
							relDate,
							relDatePrevDay,
							date,
							f;

						for (i in r) {
							relDate        = Date.create(r[i].date);
							relDatePrevDay = Date.create(r[i].date).addDays(-1).format('{yyyy}');

							if (relDate.format('{yyyy}') === relDatePrevDay) {
								date = relDate.format('{dd} {month} {yyyy}');

								if (date === 'Invalid Date') {
									date = r[i].date;
								}
							} else {
								date = r[i].date;
							}

							for (f in r[i].formats) {
								currentWeekBlock.append('<div class="item js_hall_release" data-id="' + r[i].id + '"><span class="date">' + date + '</span><span class="title">' + r[i].title.ru + '</span><span class="formats">' + r[i].formats[f] + '</span></div>');
							}
						}

						week_length = r.length;

						if (week_length > 0) {
							popup.find('.search').addClass('match');

							currentWeekBlock.addClass('visible');
						}
					});

					$.get('/api/releases/filter/?full=1&last_week=1&week=' + prevDate.week() + '&year=' + prevDate.year() + '&cinema_id=' + reqres.request('get:user').getCinema().id, function(res) {
						var r = res.list,
							i,
							relDate,
							relDatePrevDay,
							date,
							f;

						for (i in r) {
							relDate        = Date.create(r[i].date);
							relDatePrevDay = Date.create(r[i].date).addDays(-1).format('{yyyy}');

							if (relDate.format('{yyyy}') === relDatePrevDay) {
								date = relDate.format('{dd} {month} {yyyy}');

								if (date === 'Invalid Date') {
									date = r[i].date;
								}
							} else {
								date = r[i].date;
							}

							for (f in r[i].formats) {
								prevWeekBlock.append('<div class="item js_hall_release" data-id="' + r[i].id + '"><span class="date">' + date + '</span><span class="title">' + r[i].title.ru + '</span><span class="formats">' + r[i].formats[f] + '</span></div>');
							}
						}
					});

					popup.attr('data-hall', hall);
					popup.attr('data-hall-title', hallTitle);
					popup.attr('data-cinema', cinema);
					popup.attr('data-week', week);
					popup.attr('data-sort', sort);

					addKeyFunction = function() {
						$(document).unbind('keydown');
						$(document).keydown(function(e) {
							var key = e.keyCode,
								inputVal = $input.val().replace(/(^\s+|\s+$)/g, ''),
								$visibleReleases,
								$selected,
								$newSelected,
								newSelectedToTop,
								currentScroll,
								selectedPositionFull,
								currentViewPlace;

							if (key === 13) {
								_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Клавиатура::Enter']);
								if ($shortSections.closest('.short_list').is(':visible') && $shortSections.find('.visible .selected').length) {
									$shortSections.find('.visible .selected').trigger('click');
								} else if ($searchResults.is(':visible') && $searchResults.find('.selected').length) {
									$searchResults.find('.selected').trigger('click');
								}
							}

							if (inputVal.length <= 2 || $searchResults.find('.js_my_release').length === 0) {
								if (key === 40) {
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Клавиатура::Курсор']);
									$input.blur();

									$visibleReleases = $shortSections.find('.visible');
									$selected = $visibleReleases.find('.selected');

									if (!$selected.length) {
										$visibleReleases.find('.js_hall_release').first().addClass('selected');
									} else {
										if ($selected.next().length) {
											$newSelected = $selected.next();
											newSelectedToTop = Math.round($newSelected.position().top);
											currentScroll = $visibleReleases.scrollTop();

											selectedPositionFull = newSelectedToTop + $newSelected.outerHeight();
											currentViewPlace = $visibleReleases.outerHeight() + currentScroll;

											$selected.removeClass('selected');
											$selected.next().addClass('selected');

											if (selectedPositionFull > currentViewPlace) {
												$visibleReleases.scrollTo(currentScroll + $newSelected.outerHeight());
											}
										}
									} // вниз
								} else if (key === 38) {
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Клавиатура::Курсор']);

									$visibleReleases = $shortSections.find('.visible');
									$selected = $visibleReleases.find('.selected');

									if ($selected.length) {
										if ($selected.prev().length) {
											$input.blur();

											$newSelected = $selected.prev();
											newSelectedToTop = Math.round($newSelected.position().top);
											currentScroll = $visibleReleases.scrollTop();

											$selected.removeClass('selected');
											$selected.prev().addClass('selected');

											if (currentScroll > newSelectedToTop) {
												$visibleReleases.scrollTo($newSelected);
											}
										} else {
											$selected.removeClass('selected');
											$input.focus();
										}
									} // вверх
								} else if (key === 39) { // вправо
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Клавиатура::Курсор']);

									$visibleReleases = $shortSections.find('.visible');
									$selected = $visibleReleases.find('.selected');

									if ($selected.length && $visibleReleases.index() === 0) {
										$visibleReleases.removeClass('visible').siblings().addClass('visible');
										$visibleReleases.closest('.short_list').find('.tabs .item').removeClass('active').last().addClass('active');

										if (!$visibleReleases.siblings().children('.selected').length) {
											$visibleReleases.siblings().children().first().addClass('selected');
										}
									}
								} else if (key === 37) { // влево
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Клавиатура::Курсор']);

									$visibleReleases = $shortSections.find('.visible');
									$selected = $visibleReleases.find('.selected');

									if ($selected.length && $visibleReleases.index() === 1) {
										$visibleReleases.removeClass('visible').siblings().addClass('visible');
										$visibleReleases.closest('.short_list').find('.tabs .item').removeClass('active').first().addClass('active');

										if (!$visibleReleases.siblings().children('.selected').length) {
											$visibleReleases.siblings().children().first().addClass('selected');
										}
									}
								}
							} else if ($searchResults.find('.js_my_release').length > 0) {
								if (key === 40) {
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Клавиатура::Курсор']);
									e.preventDefault();
									$input.blur();

									$visibleReleases = $searchResults;
									$selected = $visibleReleases.find('.selected');

									if (!$selected.length) {
										$visibleReleases.find('.js_my_release').first().addClass('selected');
									} else {
										if ($selected.next().length) {
											$newSelected = $selected.next();
											newSelectedToTop = Math.round($newSelected.position().top);
											currentScroll = $visibleReleases.scrollTop();

											selectedPositionFull = newSelectedToTop + $newSelected.outerHeight();
											currentViewPlace = $visibleReleases.outerHeight() + currentScroll;

											$selected.removeClass('selected');
											$newSelected.addClass('selected');

											if (selectedPositionFull > currentViewPlace) {
												if (newSelectedToTop === $visibleReleases.outerHeight()) {
													$visibleReleases.scrollTo(currentScroll + $newSelected.outerHeight());
												} else {
													$visibleReleases.scrollTo(currentScroll - (currentViewPlace - selectedPositionFull));
												}
											} else if (selectedPositionFull < currentViewPlace && currentScroll !== 0) {
												$visibleReleases.scrollTo(currentScroll + $newSelected.outerHeight());
											}
										}
									} // вниз
								} else if (key === 38) {
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Клавиатура::Курсор']);

									$visibleReleases = $searchResults;
									$selected = $visibleReleases.find('.selected');

									if ($selected.length) {
										if ($selected.prev().length) {
											$input.blur();

											$newSelected = $selected.prev();
											newSelectedToTop = Math.round($newSelected.position().top);
											currentScroll = $visibleReleases.scrollTop();

											$selected.removeClass('selected');
											$newSelected.addClass('selected');

											if (currentScroll > newSelectedToTop) {
												$visibleReleases.scrollTo($newSelected);
											}
										} else {
											$selected.removeClass('selected');
											$input.focus();
										}
									} // вверх
								}
							}
						});
					};

					addKeyFunction();

					window.app.App.$el.children('.arcticmodal-container').on('mouseenter', addKeyFunction);
				},
				afterClose: function(data, el) {
					el.remove();
					$schedule.closest('#halls').trigger('mouseenter');
				}
			});
		},
		showContextMenu: function showContextMenu(e) {
			if (!reqres.request('get:user').isMultiplex()) return;

			e.preventDefault();
			var $target = $(e.currentTarget);

			if ($(e.target).closest('.release, .calendar').length || !$(e.target).children('.release').length) return;

			this.contextMenu = new window.app.ContextMenuView({
				top:  e.pageY || $target.offset().top + ($target.height() / 2),
				left: e.pageX + 1 || $target.offset().left + ($target.width() / 2) + 1,
				items: [
					{
						title: 'Копировать репертуар',
						iconClass: 'fa fa-files-o',
						action: function() {
							this.hallCopy(e);
						},
						context: this
					}
				]
			});
		},
		hallCopy: function hallCopy(e, week, hallId) {
			var view = new window.app.copyingScheduleCinemasWrapperView({
					week: week || this.model.get('week'),
					hallId: hallId || $(e.target).closest('.section').data('hall-id'),
					year: this.model.get('currentYear'),
					source: 'planning:hall:week'
				});

			this.$wrapper.prepend(view.render().$el);

			view.$el.arcticmodal({
				beforeOpen: function(data) {
					$(data.body).addClass('center');
				},
				afterClose: function() {
					view.remove();
				}
			});
		}
	});

	window.app.SectionView = Backbone.View.extend({
		className: 'release',
		template: '#halls_section_tmpl',
		events: {
			contextmenu: 'showContextMenu',
			click: function(e) {
				var $target = $(e.currentTarget),
					data    = {
						release_id: this.model.get('release_id'),
						cinema_id:  this.model.get('cinema_id'),
						week:       this.model.get('week')
					},
					clicked_hall,
					modelHalls,
					hallObject,
					techHalls;

				if ($(e.target).hasClass('comment')) {
					e.stopPropagation();
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Комментарий::Изменение']);

					this.openCommentPopover(e);
					return;
				}

				if ($target.closest('.repertoire__list').length && !$target.hasClass('added')) {
					clicked_hall = $target.closest('.section').data('hall-id');

					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Fast::Добавление']);

					$target.addClass('added');

					if (this.model.get('technology').hall_id) {
						modelHalls = _.clone(this.model.get('technology').hall_id);

						modelHalls[clicked_hall] = modelHalls[clicked_hall] || 0;

						this.model.get('technology').hall_id = modelHalls;

						data.technology = this.model.get('technology');

						Backbone.trigger('repertoire:release:technology:change', data);
					} else {
						hallObject = {};

						hallObject[clicked_hall] = 0;

						this.model.get('technology').hall_id = hallObject;

						data.technology = this.model.get('technology');

						Backbone.trigger('repertoire:release:technology:change', data);
					}
				} else if ($target.closest('.repertoire__list').length && $target.hasClass('added')) {
					clicked_hall = $target.closest('.section').data('hall-id');

					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Fast::Удаление']);

					$target.removeClass('added');

					techHalls = this.model.get('technology').hall_id;

					delete techHalls[clicked_hall];

					this.model.get('technology').hall_id = techHalls;

					data.technology = this.model.get('technology');

					Backbone.trigger('repertoire:release:technology:change', data);
				}

				if (!$target.closest('.repertoire__list').length) {
					if ($target.hasClass('clicked')) {
						$target.removeClass('clicked').closest('.section').find('.add_release').removeClass('visible');
						$target.closest('.halls_body').find('.backlight').removeClass('backlight');
					} else {
						_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Таблица::Клик на релиз']);

						$target.closest('.halls_body').find('.release, .add_release').removeClass('clicked visible');
						$target.addClass('clicked').closest('.section').find('.add_release').addClass('visible');
					}

					$target.trigger('seanceInfo');
				}
			},
			seanceInfo: function(e) {
				var $target      = $(e.currentTarget),
					release      = $target.closest('.release'),
					section      = release.closest('.section'),
					week         = parseInt(section.closest('.list').data('week')),
					cinema       = parseInt(section.closest('.list').data('cinema')),
					hall         = parseInt(section.data('hall-id')),
					relId        = parseInt(release.data('id')),
					sort         = parseInt(release.data('sort')) || 0,
					$hallsList   = this.$wrapper.find('.halls_head .list .item'),
					hallIndex    = section.index(),
					hallTitle    = $hallsList.eq(hallIndex).find('.title').data('hall-title'),
					$allReleases = this.$wrapper.find('.halls_body .section').children('.release'),
					$currentInfo,
					$seance;

				if (!$target.closest('.repertoire__list').length) {
					this.model.set({
						hall_title: hallTitle,
						hall_id: hall
					});

					this.settings = new window.app.relaseSeanceInfo({
						model: this.model,
						parent: this.options.parent
					});

					$currentInfo = this.$wrapper.children('.seance_info');

					if (!release.hasClass('clicked')) {
						$currentInfo.remove();
						return;
					}

					$currentInfo.remove();

					this.$wrapper.append(this.settings.render().$el);

					$seance = this.$wrapper.children('.seance_info');

					$seance.attr({
						'data-cinema': cinema,
						'data-hall':   hall,
						'data-week':   week,
						'data-sort':   sort,
						'data-id':     relId
					});

					$allReleases.removeClass('backlight');
					$allReleases.filter('[data-id="' + relId + '"]').not($target).addClass('backlight');
				}
			},
			changeSeanceCount: function(e, key) {
				var $target     = $(e.currentTarget),
					$section    = $target.closest('.section'),
					cinemaRight = this.model.get('cinema_rights'),
					data        = {},
					$viewer,
					hallId,
					hall_id,
					$repList,
					$release,
					id;

				if (cinemaRight !== 1 && cinemaRight !== 2 && (key === 8 || key === 46)) {
					e.preventDefault();
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Клавиатура::Удаление']);

					if (!$target.hasClass('seance__input') && !$target.hasClass('textarea')) {
						hallId   = parseInt($target.closest('.section').data('hall-id'));
						hall_id  = this.model.get('technology').hall_id;
						$repList = $target.closest('.section').find('.repertoire__list');

						delete hall_id[hallId];

						$target.removeClass('clicked');
						this.$wrapper.children('.halls_body').find('.backlight').removeClass('backlight');

						if ($target.prev().length) {
							$target.prev().trigger('click');
						} else if ($target.next().hasClass('release')) {
							$target.next().trigger('click');
						} else if ($target.nextAll().hasClass('add_release')) {
							$target.nextAll('.add_release').addClass('clicked');

							this.$wrapper.children('.seance_info').remove();

							this.$wrapper.children('.halls_body').find('.backlight').removeClass('backlight');
						}

						data.release_id = this.model.get('release_id');
						data.cinema_id  = this.model.get('cinema_id');
						data.week       = this.model.get('week');
						data.technology = this.model.get('technology');

						Backbone.trigger('repertoire:release:technology:change', data);

						$repList.append($target);
						$target.removeClass('backlight').find('.status').text(0);

						$target.find('.comment').remove();
						$target.find('.name').removeClass('short');

						$viewer = $repList.closest('.week').find('.repertoire_viewer');

						if ($viewer.hasClass('active')) {
							$viewer.trigger('click');
						} else {
							this.options.parent.checkWeekHeight();
						}

						if ($section.children('.release').length === 1) {
							$release = $section.children('.release');
							id       = $release.data('id');

							$section.children('.one-release-poster').addClass('active').css({
								backgroundImage: 'url(https://nas.dcp24.ru/img/movie/' + id + '_big.jpg)'
							}).attr('title', $release.attr('comment'));

							$section.children('.one-release-status-count').addClass('active').attr('data-status', $release.find('.status').data('status'));

							if ($release.data('format') !== '2d' || $release.data('upgrades').length) {
								$section.children('.one-release-format')
										.text($release.data('format') + ($release.data('upgrades').length ? '+' : ''))
										.attr('data-status', $release.find('.status').data('status'))
										.addClass('active');
							}
						} else {
							$section.children('.one-release-poster, .one-release-status-count, .one-release-format').removeClass('active');
						}
					}
				}
			}
		},
		initialize: function initialize(options) {
			this.options  = options || {};
			this.$wrapper = this.options.parent.$wrapper;
			this.options.parent.innerViews.push(this);
		},
		render: function render() {
			var template         = Backbone.TemplateCache.get(this.template),
				extModel         = $.extend({}, this.model.attributes),
				releaseStartDate = moment(this.model.get('release').date.russia.start).startOf('week'),
				parentModel      = this.options.parent.model.attributes,
				currentDate      = moment().year(parentModel.currentYear).month(parentModel.currentMonth).week(this.model.get('week')).startOf('week'),
				modelTechnology  = this.model.get('technology'),
				modelRelease     = this.model.get('release'),
				color            = modelRelease.color || '',
				upgradesNames;

			if (modelTechnology.upgrades.length) {
				upgradesNames = modelTechnology.upgrades.join(', ');
				upgradesNames = upgradesNames.toUpperCase();
			}

			if (modelTechnology.format) {
				if (modelTechnology.upgrades.length) {
					extModel.format = modelTechnology.format + '+';
				} else if (modelTechnology.format.toLowerCase() !== '2d') {
					extModel.format = modelTechnology.format.toLowerCase() === 'public video' ? 'pv' : modelTechnology.format;
				}
			}

			if (currentDate.isBefore(releaseStartDate)) {
				extModel.rental = 'П';
			} else {
				if (currentDate.diff(releaseStartDate, 'week') < 10) {
					extModel.rental = currentDate.diff(releaseStartDate, 'week') + 1;
				} else {
					extModel.rental = 'Э';
				}
			}

			extModel.title = modelRelease.title.ru;
			extModel.comment = modelTechnology.hall_id && modelTechnology.hall_id[this.model.get('hall_id')] ? (modelTechnology.hall_id[this.model.get('hall_id')] === 0 ? '' : modelTechnology.hall_id[this.model.get('hall_id')]) : '';

			this.$el.html(template(extModel));

			this.$el.attr({
				'data-id':       this.model.get('release_id'),
				'data-format':   modelTechnology.format,
				'data-upgrades': modelTechnology.upgrades.join(', '),
				'data-duration': modelRelease.duration.full || modelRelease.duration.clean,
				'data-seances':  modelTechnology.count,
				'data-color':    color,
				title:           modelRelease.title.ru + ': ' + modelTechnology.format.toUpperCase() + (upgradesNames ? upgradesNames : ''),
				comment:         extModel.comment
			});

			if (extModel.comment.length) {
				this.$('.name').addClass('short');
			} else {
				this.$('.name').removeClass('short');
			}

			return this;
		},
		showContextMenu: function showContextMenu(e) {
			e.preventDefault();
			var $target = $(e.currentTarget),
				items;

			if ($target.closest('.repertoire__list').length) return;

			items = [
				{
					title: 'Комментировать',
					iconClass: 'fa fa-comment',
					action: function() {
						if (this.$el.attr('comment').length) {
							_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Комментарий::Изменение']);
						} else {
							_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Комментарий::Добавление']);
						}

						this.openCommentPopover(e);
					},
					context: this
				}
			];

			if (reqres.request('get:user').isMultiplex()) {
				items.push({
					title: 'Копировать репертуар',
					iconClass: 'fa fa-files-o',
					action: function() {
						Backbone.trigger('planning:halls:hallCopy', this.model.get('week'), this.model.get('hall_id') || $(e.target).closest('.section').data('hall-id'));
					},
					context: this
				});
			}

			this.contextMenu = new window.app.ContextMenuView({
				top:  e.pageY || $target.offset().top + ($target.height() / 2),
				left: e.pageX + 1 || $target.offset().left + ($target.width() / 2) + 1,
				items: items
			});
		},
		openCommentPopover: function openCommentPopover(e) {
			var data = {
					release_id: this.model.get('release_id'),
					cinema_id:  this.model.get('cinema_id'),
					week:       this.model.get('week')
				},
				$target = $(e.target);

			this.commentPopover = new window.app.CommentPopoverView({
				top:  e.pageY || $target.offset().top + ($target.height() / 2),
				left: e.pageX + 1 || $target.offset().left + ($target.width() / 2) + 1,

				comment: this.$el.attr('comment'),
				onSave: _.bind(function onSave(comment) {
					this.$el.attr('comment', comment);
					this.model.get('technology').hall_id[this.$el.closest('.section').data('hall-id')] = comment;
					this.$el.siblings('.one-release-poster').attr('title', comment);

					data.technology = this.model.get('technology');
					Backbone.trigger('repertoire:release:technology:change', data);

					this.render();
				}, this)
			});
		}
	});

	window.app.relaseSeanceInfo = Backbone.View.extend({
		tagName: 'div',
		className: 'seance_info',
		template: '#halls_seance_popup',
		events: {
			'click .arcticmodal-close': function() {
				$.arcticmodal('close');
			},
			'click .remove': function() {
				this.$wrapper.find('.halls_body .clicked').trigger('changeSeanceCount', [8]);

				_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Панель::Удаление']);
			}
		},
		initialize: function initialize(options) {
			if (!this.model.has('release')) {
				this.model.set({
					release: []
				});
			}

			this.options  = options || {};
			this.$wrapper = this.options.parent.$wrapper;

			this.listenTo(this.model, 'remove', this.removeView);

			this.options.parent.innerViews.push(this);
		},
		render: function render() {
			var template      = Backbone.TemplateCache.get(this.template),
				extModel      = $.extend({}, this.model.attributes),
				weekStartDate = moment().week(this.model.get('week')).startOf('week'),
				weekEndDate   = moment().week(this.model.get('week')).endOf('week'),
				fromDate      = weekStartDate.date(),
				fromMonth     = weekStartDate.month(),
				endDate       = weekEndDate.date(),
				endMonth      = weekEndDate.month(),
				distributors  = this.model.get('release').distributors,
				endMonthText,
				dateRange,
				fullText,
				fromMonthText;

			if (fromDate < 10) {
				fromDate = '0' + fromDate;
			}

			if (endDate < 10) {
				endDate = '0' + endDate;
			}

			if (fromMonth !== endMonth) {
				fromMonthText = Date.create(weekStartDate.toString()).format('{month}');

				endMonthText = Date.create(weekEndDate.toString()).format('{month}');
				dateRange = fromDate + ' ' + fromMonthText + ' - ' + endDate + ' ' + endMonthText;
			} else {
				endMonthText = Date.create(weekEndDate.toString()).format('{month}');
				dateRange = fromDate + ' - ' + endDate + ' ' + endMonthText;
			}

			if (distributors.length > 1) {
				fullText = [];

				_.each(distributors, function(dist) {
					var name = dist.name.short || dist.name.full;

					fullText.push(name);
				});

				extModel.distText = fullText.join(', ');
			} else {
				extModel.distText = distributors[0].name.short || distributors[0].name.full;
			}

			extModel.dateRange = dateRange;
			extModel.dateStart = Date.create(this.model.get('release').date.russia.start).format('{dd} {month} {yyyy}');

			this.$el.html(template(extModel));

			return this;
		},
		removeView: function removeView() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.relaseScheduleView = Backbone.View.extend({
		tagName: 'div',
		className: 'schedule_settings',
		template: '#halls_popup',
		initialize: function initialize(options) {
			if (!this.model.has('release')) {
				this.model.set({
					release: []
				});
			}

			this.$wrapper = options.parent.$wrapper;
			this.parent   = options.parent;

			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.removeView);
		},
		events: {
			'click .arcticmodal-close': function() {
				$.arcticmodal('close');
			},
			'click .input': 'search',
			'focus .input': 'search',
			'click .js_hall_release': 'newReleaseAdd',
			'click .js_my_release': 'newReleaseAdd',
			'click .short_list .tabs .item': function(e) {
				var item  = $(e.currentTarget),
					index = item.index(),
					popup = this.$el;

				if (!item.hasClass('active')) {
					item.addClass('active').siblings().removeClass('active');
				}

				popup.find('.short_list .releases').eq(index).addClass('visible').siblings().removeClass('visible');

				if (popup.find('.short_list .releases').eq(index).find('.item').length) {
					popup.find('.top .search').addClass('match');
				} else {
					popup.find('.short_list .releases').eq(index).removeClass('visible');
					popup.find('.top .search').removeClass('match');
				}
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));

			return this;
		},
		search: function search(e) {
			var input      = $(e.currentTarget),
				popup      = this.$el.children(),
				$shortList = popup.find('.short_list');

			input.unbind('textchange');

			input.one('textchange', function() {
				_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Попап::Поиск релиза']);
			});

			input.bind('textchange', function() {
				var parent        = input.closest('.search'),
					results_block = parent.find('.results'),
					value         = input.val().toLowerCase().replace(/(^\s+|\s+$)/g, '');

				parent.find('.remove_text').click(function() {
					input.val('').focus();
					$(this).hide();
					results_block.empty().hide();

					$shortList.show();
				});

				if (value.length > 0) {
					parent.find('.remove_text').show();
				} else {
					parent.find('.remove_text').hide();
				}

				if (value.length > 2) {
					results_block.scrollTo(0);

					$.get('/api/releases/filter/?q=' + value, function(res) {
						results_block.empty();

						var r            = res.list,
							resultsNum   = r.length,
							getRandomInt = function(min, max) {
								return Math.floor(Math.random() * (max - min + 1)) + min;
							},
							relDate,
							relDatePrevDay,
							date,
							f,
							i;

						for (i in r) {
							relDate        = Date.create(r[i].date);
							relDatePrevDay = Date.create(r[i].date).addDays(-1).format('{yyyy}');

							if (relDate.format('{yyyy}') === relDatePrevDay) {
								date = relDate.format('{dd} {month} {yyyy}');

								if (date === 'Invalid Date') {
									date = r[i].date;
								}
							} else {
								date = r[i].date;
							}

							for (f in r[i].formats) {
								if (r[i].cover && r[i].title.en) {
									results_block.append('<div class="item js_my_release" data-id="' + r[i].id + '"><div class="left"><img src="' + r[i].cover + '" alt="" class="poster"></div><div class="right"><span class="ru"><span class="format">' + r[i].formats[f] + '</span> ' + r[i].title.ru + '</span><span class="en">' + r[i].title.en + '</span><div class="info"><span class="date">' + date + ',</span><span class="dist">' + r[i].distributors + ',</span><span class="age">' + r[i].age + '+</span></div></div></div>');
								} else if (r[i].cover && !r[i].title.en) {
									results_block.append('<div class="item js_my_release" data-id="' + r[i].id + '"><div class="left"><img src="' + r[i].cover + '" alt="" class="poster"></div><div class="right"><span class="ru"><span class="format">' + r[i].formats[f] + '</span> ' + r[i].title.ru + '</span><span class="en">&nbsp;</span><div class="info"><span class="date">' + date + ',</span><span class="dist">' + r[i].distributors + ',</span><span class="age">' + r[i].age + '+</span></div></div></div>');
								} else if (!r[i].cover && r[i].title.en) {
									results_block.append('<div class="item js_my_release" data-id="' + r[i].id + '"><div class="left"><i class="poster_ico gradient_' + getRandomInt(1, 8) + '"></i></div><div class="right"><span class="ru"><span class="format">' + r[i].formats[f] + '</span> ' + r[i].title.ru + '</span><span class="en">' + r[i].title.en + '</span><div class="info"><span class="date">' + date + ',</span><span class="dist">' + r[i].distributors + ',</span><span class="age">' + r[i].age + '+</span></div></div></div>');
								} else {
									results_block.append('<div class="item js_my_release" data-id="' + r[i].id + '"><div class="left"><i class="poster_ico gradient_' + getRandomInt(1, 8) + '"></i></div><div class="right"><span class="ru"><span class="format">' + r[i].formats[f] + '</span> ' + r[i].title.ru + '</span><span class="en">&nbsp;</span><div class="info"><span class="date">' + date + ',</span><span class="dist">' + r[i].distributors + ',</span><span class="age">' + r[i].age + '+</span></div></div></div>');
								}
							}
						}

						if (resultsNum > 0) {
							$shortList.hide();
							results_block.show();
						} else {
							$shortList.show();
							results_block.empty().hide();
						}
					}, 'json');
				} else {
					$shortList.show();
					results_block.empty().hide();
				}
			});
		},
		newReleaseAdd: function newReleaseAdd(e) {
			var item         = $(e.target).closest('.item'),
				id           = item.data('id'),
				format       = item.find('.formats').text().toLowerCase() || item.find('.format').text().toLowerCase(),
				popup        = item.closest('.cinema_schedule__popup'),
				week         = popup.data('week'),
				year         = this.model.get('currentYear'),
				month        = parseInt(moment().week(week).startOf('week').month().format('M')) + 1,
				hallId       = popup.data('hall'),
				$currentWeek = this.$wrapper.find('.halls_body .week').filter('[data-week="' + week + '"]'),
				section      = $currentWeek.find('.section').filter('[data-hall-id="' + hallId + '"]'),
				goPost       = 1,
				self         = this,
				$releaseToAdd,
				$repReleaseSection,
				$newRelease,
				$releaseToProlong,
				releaseModel,
				lastWeek;

			Backbone.trigger('loader', 'show');
			Backbone.trigger('planning:screenBlocker', 'show');

			if (section.find('.release').length) {
				section.find('.release').each(function() {
					var $release = $(this);

					if ($release.data('id') === id && $release.data('format').toLowerCase() === format && $release.data('upgrades').length <= 0) {
						if ($release.parent().hasClass('repertoire__list')) {
							goPost = 2;
							$releaseToAdd = $release;
						} else {
							goPost = 0;
						}

						return false;
					} else {
						$currentWeek.siblings().find('.release').each(function() {
							var $siblingRelease = $(this);

							if (parseInt($siblingRelease.data('id')) === id) {
								goPost = 3;

								return false;
							}
						});
					}
				});
			} else {
				$currentWeek.siblings().find('.release').each(function() {
					var $siblingRelease = $(this);

					if ($siblingRelease.data('id') === id) {
						goPost = 3;
					}
				});
			}

			if (goPost === 0) {
				$.arcticmodal('close');
				Backbone.trigger('loader', 'hide');
				Backbone.trigger('planning:screenBlocker', 'hide');
			} else if (goPost === 2) {
				$releaseToAdd.trigger('click');

				$repReleaseSection = $releaseToAdd.closest('.section');
				$newRelease        = $releaseToAdd.removeClass('added');

				$repReleaseSection.find('.repertoire__list').before($newRelease);

				this.$wrapper.children('.halls_body').find('.clicked, .ex_clicked').removeClass('clicked, ex_clicked');

				$newRelease.trigger('click');

				if (section.children('.release').length === 1) {
					id = $newRelease.data('id');

					section.children('.one-release-poster').addClass('active').css({
						backgroundImage: 'url(https://nas.dcp24.ru/img/movie/' + id + '_big.jpg)'
					}).attr('title', $newRelease.attr('comment'));

					section.children('.one-release-status-count').addClass('active').attr('data-status', $newRelease.find('.status').data('status'));

					if ($newRelease.data('format') !== '2d' || $newRelease.data('upgrades').length) {
						section.children('.one-release-format')
								.text($newRelease.data('format') + ($newRelease.data('upgrades').length ? '+' : ''))
								.attr('data-status', $newRelease.find('.status').data('status'))
								.addClass('active');
					}
				} else {
					section.children('.one-release-poster, .one-release-status-count, .one-release-format').removeClass('active');
				}

				self.parent.checkWeekHeight();

				$.arcticmodal('close');
				Backbone.trigger('loader', 'hide');
				Backbone.trigger('planning:screenBlocker', 'hide');
			} else if (goPost === 3) {

				releaseModel = reqres.request('get:repertoire:view').repertoire.findWhere({release_id: id});
				lastWeek     = _.last(releaseModel.get('weeks'));

				_.each(_.range(+lastWeek.number + 1, week + 1), function(number) {
					var newDate = moment(0, 'H').year(lastWeek.year).startOf('week').week(lastWeek.number).add(1, 'weeks'),
						technology = releaseModel.createNewTechnology();

					if (lastWeek.days && lastWeek.days.length) lastWeek.days = _.range(lastWeek.days[0] || 1, 8);

					if (number === week) {
						_.findWhere(technology, {format: format}).hall_id[hallId] = 0;
					}

					releaseModel.get('weeks').push({
						count: 0,
						year: newDate.year(),
						number: number,
						technology: technology
					});
				}, this);

				this.listenToOnce(releaseModel, 'sync', function() {
					this.parent.changeModels(releaseModel);

					$currentWeek.find('.section').filter('[data-hall-id="' + hallId + '"]').find('.release').each(function() {
						var $prolongRelease = $(this);

						if (parseInt($prolongRelease.data('id')) === id && $prolongRelease.data('format').toLowerCase() === format && $prolongRelease.data('upgrades').length <= 0) {
							$releaseToProlong = $prolongRelease;
						}
					});

					$releaseToProlong.trigger('click');

					$repReleaseSection = $releaseToProlong.closest('.section');
					$newRelease        = $releaseToProlong.removeClass('added');

					$repReleaseSection.find('.repertoire__list').before($newRelease);

					this.$wrapper.children('.halls_body').find('.clicked, .ex_clicked').removeClass('clicked, ex_clicked');

					$newRelease.trigger('click');

					if (section.children('.release').length === 1) {
						id = $newRelease.data('id');

						section.children('.one-release-poster').addClass('active').css({
							backgroundImage: 'url(https://nas.dcp24.ru/img/movie/' + id + '_big.jpg)'
						}).attr('title', $newRelease.attr('comment'));

						section.children('.one-release-status-count').addClass('active').attr('data-status', $newRelease.find('.status').data('status'));

						if ($newRelease.data('format') !== '2d' || $newRelease.data('upgrades').length) {
							section.children('.one-release-format')
									.text($newRelease.data('format') + ($newRelease.data('upgrades').length ? '+' : ''))
									.attr('data-status', $newRelease.find('.status').data('status'))
									.addClass('active');
						}
					} else {
						section.children('.one-release-poster, .one-release-status-count, .one-release-format').removeClass('active');
					}

					this.parent.checkWeekHeight();
					reqres.request('get:repertoire:view').networkCollection.fetch();

					$.arcticmodal('close');
					Backbone.trigger('loader', 'hide');
					Backbone.trigger('planning:screenBlocker', 'hide');
				});

				releaseModel.save();
			} else if (goPost === 1) {
				$.post('/api/schedule/add', {
					tx:         reqres.request('get:user').get('tx'),
					cinema_id:  reqres.request('get:user').getCinema().id,
					release_id: id,
					week:       week,
					year:       year,
					add:        1,
					month:      month
				}, function(r) {
					$.arcticmodal('close');

					if (r.ok) {
						r.release.at = 0;

						var rep                = reqres.request('get:repertoire:view').repertoire,
							models             = rep.models,
							releases           = _.filter(models, function(id) {
								return id.attributes.release_id !== null;
							}),
							releasesOfWeek     = _.filter(releases, function(release) {
								var thisWeek = _.filter(release.attributes.weeks, function(oneWeek) {
									return parseInt(oneWeek.number) === week;
								});

								if (thisWeek.length) {
									return thisWeek;
								}
							}),
							lastOfWeek         = _.last(releasesOfWeek),
							index;

						if (lastOfWeek) {
							index = _.indexOf(models, lastOfWeek) + 1;
						} else {
							index = releases.length;
						}

						rep.add(r.release, {
							at: index
						});

						Backbone.trigger('planning:add', {favourite_id: id});

						self.$wrapper.find('.halls_body .week').filter('[data-week="' + week + '"]').find('.section').filter('[data-hall-id="' + hallId + '"]').find('.release').each(function() {
							var $oneReleaseInSection = $(this),
								oneReleaseId         = parseInt($oneReleaseInSection.data('id')),
								oneReleaseFormat     = $oneReleaseInSection.data('format').toLowerCase(),
								$repReleaseSection,
								$newRelease;

							if (oneReleaseId === id && oneReleaseFormat === format) {
								$oneReleaseInSection.trigger('click');

								$repReleaseSection = $oneReleaseInSection.closest('.section');
								$newRelease        = $oneReleaseInSection.removeClass('added');

								$repReleaseSection.find('.repertoire__list').before($newRelease);

								self.$wrapper.find('.halls_body').find('.clicked, .ex_clicked').removeClass('clicked, ex_clicked');

								$newRelease.trigger('click');

								if (section.children('.release').length === 1) {
									id = $newRelease.data('id');

									section.children('.one-release-poster').addClass('active').css({
										backgroundImage: 'url(https://nas.dcp24.ru/img/movie/' + id + '_big.jpg)'
									}).attr('title', $newRelease.attr('comment'));

									section.children('.one-release-status-count').addClass('active').attr('data-status', $newRelease.find('.status').data('status'));

									if ($newRelease.data('format') !== '2d' || $newRelease.data('upgrades').length) {
										section.children('.one-release-format')
												.text($newRelease.data('format') + ($newRelease.data('upgrades').length ? '+' : ''))
												.attr('data-status', $newRelease.find('.status').data('status'))
												.addClass('active');
									}
								} else {
									section.children('.one-release-poster, .one-release-status-count, .one-release-format').removeClass('active');
								}

								self.parent.checkWeekHeight();
							}
						});

						reqres.request('get:repertoire:view').networkCollection.fetch();
					}
					Backbone.trigger('loader', 'hide');
					Backbone.trigger('planning:screenBlocker', 'hide');
				}, 'json');
			}
		},
		removeView: function removeView() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

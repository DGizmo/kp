$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.Repertoire = Backbone.Model.extend({
		defaults: {
			sort: null,
			weeks: [],
			release_id: null,
			release: {
				title: {},
				formats: []
			},
			f_formats: [],
			cinema_rights: null,
			release_start_date: null,
			key: false,
			color: 3,
			addRel: false
		},
		initialize: function initialize() {
			this.rel_id    = this.attributes.release_id;
			this.cinema_id = this.attributes.cinema_id;
		},
		url: function url() {
			return '/api/schedule/get/?cinema_id=' + this.cinema_id + '&release_id=' + this.rel_id;
		},
		addWeek: function addWeek() {
			var lastWeek      = this.get('weeks').last(),
				newDate       = moment(0, 'H').year(lastWeek.year).startOf('week').week(lastWeek.number).add(1, 'weeks');

			if (lastWeek.days && lastWeek.days.length) lastWeek.days = _.range(lastWeek.days[0] || 1, 8);

			this.attributes.weeks.push({
				count: 0,
				year: newDate.year(),
				number: newDate.week(),
				technology: this.createNewTechnology()
			});

			this.trigger('change');
			return this;
		},
		createNewTechnology: function createNewTechnology() {
			var newTechnology = $.extend(true, [], this.get('weeks')[0].technology);

			_.each(newTechnology, function(tech) {
				tech.count = 0;
				tech.hall_id = {};
			});

			return newTechnology;
		},
		deleteWeek: function deleteWeek(weekNum, yearNum) {
			var allWeeks    = this.get('weeks'),
				deletedWeek = _.find(allWeeks, function(week) {
					return week.number === weekNum && week.year === yearNum;
				});

			if (allWeeks.length === 1) return false;

			allWeeks.splice(allWeeks.indexOf(deletedWeek), 1);

			this.trigger('change');
			return this;
		},
		changeFormat: function changeFormat(format, upgrades, newUpgrades, action) {
			var self = this;

			switch (action) {

				case 'add':
					_.each(this.get('weeks'), function(week) {
						if ((!newUpgrades.length && format !== 'imax' && format !== 'luxe' && format !== 'Public Video') || self._findTechnology(week, format, newUpgrades)) return;
						week.technology.push({ format: format, upgrades: newUpgrades, count: 0 });
					});
				break;

				case 'save':
					if (!newUpgrades.length) return;
					_.each(this.get('weeks'), function(week) {
						if (
						_.some(week.technology, function(technology) {
							return (self._concatFormat(technology.format, technology.upgrades) === self._concatFormat(format, newUpgrades));
						})) return;
						self._findTechnology(week, format, upgrades).upgrades = newUpgrades;
					});
				break;

				case 'remove':
					if (!upgrades.length) return;
					_.each(this.get('weeks'), function(week) {
						week.technology.splice(week.technology.indexOf(self._findTechnology(week, format, upgrades)), 1);
						week.count = _.reduce(week.technology, function(sum, item) {
							return sum + item.count;
						}, 0);
					});
				break;
			}

			this.sortByFormat(this.get('weeks'));
			this.trigger('change');
		},
		_concatFormat: function _concatFormat(format, upgrades) {
			if (format === 'Public Video') {
				return format + upgrades.join(',').toLowerCase();
			} else {
				return (format + upgrades.join(',')).toLowerCase();
			}
		},
		_findTechnology: function _findTechnology(week, format, upgrades) {
			return _.find(week.technology, function(technology) {
				return (this._concatFormat(technology.format, technology.upgrades) === this._concatFormat(format, upgrades));
			}, this);
		},
		save: function save(tx) {
			var self = this,
				data = {
					release_id: this.get('release_id'),
					color:      this.get('color'),
					cinema_id:  this.get('cinema_id'),
					weeks:      $.toJSON(this.get('weeks'))
				};
			if (tx) data.tx = tx;
			$.post('/api/schedule/save', data)
				.done(function(res) {
					if (res.ok) {
						Backbone.trigger('schedule:save:done', {
							release_id: self.get('release_id'),
							color:      self.get('color'),
							cinema_id:  self.get('cinema_id')
						});
					}
				});

			return this;
		},
		sortByFormat: function sortByFormat(weeks) {
			_.each(weeks, function(week) {
				week.technology.sort(function(a, b) {
					if (a.format < b.format) return -1;
					if (a.format > b.format) return 1;
					return 0;
				});

				var tmp = _.find(week.technology, function(t) { return t.format === '35mm'; });

				if (tmp) {
					week.technology.splice(_.indexOf(week.technology, tmp), 1);
					week.technology.unshift(tmp);
				}

			});
		},
		parse: function parse(res) {
			_.each(_.flatten(_.pluck(res.weeks, 'technology')), function(technology) {
				technology.format = technology.format.toLowerCase();
			});

			res.weeks.sort(function(a, b) {
				var year = a.year - b.year;
				return year ? year : a.number - b.number;
			});

			// TODO typeof week.number - "string"? ШТОА
			_.each(res.weeks, function(week) {
				week.number = parseInt(week.number);
				week.year = parseInt(week.year);
			});

			this.sortByFormat(res.weeks);
			return res;
		}
	});

	window.app.NetworkModel = Backbone.Model.extend({
		defaults: {
			cinema_id: null,
			year: null,
			week: null
		},
		initialize: function initialize() {
			this.cinema_id = this.attributes.cinema_id;
			this.year = this.attributes.year;
			this.week = this.attributes.week ? this.attributes.week.number : null;
		},
		url: function url() {
			var data = '/api/schedule/network/one/?cinema_id=' + this.cinema_id + '&year=' + this.year + '&week=' + this.week;

			return data;
		}
	});

	window.app.HallsPlanning = Backbone.Model.extend({
		defaults: {
			halls:        [],
			days:         [],
			date:         Date.create(),
			currentYear:  null,
			currentMonth: null
		},
		initialize: function initialize() {
			var dateCalendar = new window.app.CreateCalendar();

			this.set({
				days:          window.app.App.repview.model.get('days') || dateCalendar.days,
				currentYear:   window.app.App.repview.model.get('currentYear') || dateCalendar.current.year,
				currentMonth:  !_.isUndefined(window.app.App.repview.model.get('currentMonth')) ? window.app.App.repview.model.get('currentMonth') : dateCalendar.current.month
			});
		}
	});

	window.app.OneReleaseModel = Backbone.Model.extend({
		defaults: {
			release:      {},
			release_id:   null,
			color:        3,
			week:         null,
			cinema_id:    null,
			hall_id:      null,
			count:        null,
			format_count: null,
			format:       null
		}
	});

	window.app.RepertoireCollection = Backbone.Collection.extend({
		model:         window.app.Repertoire,
		parentview:    {},
		duration:      {},
		headView:      false,
		changedCinema: false,
		initialize: function initialize() {
			var self = this;

			this.listenTo(Backbone, 'repertoire:release:technology:change', function(data) {
				var model = this.findWhere({
						release_id: data.release_id,
						cinema_id: data.cinema_id
					}),
					technology = data.technology,
					week = data.week,
					changedWeek,
					changedTechnology;

				changedWeek = _.find(model.get('weeks'), function(oneWeek) {
					return parseInt(oneWeek.number) === week;
				});

				changedTechnology = _.find(changedWeek.technology, function(oneTech) {
					return oneTech.upgrades === technology.upgrades && oneTech.format === technology.format;
				});

				changedTechnology.hall_id = technology.hall_id;

				model.save(reqres.request('get:user').get('tx'));
			});

			this.listenTo(Backbone, 'technology:save:done', function(data) {
				var model = this.findWhere({
					release_id: data.release_id,
					cinema_id: data.cinema_id
				}),
				currentWeek;

				if (model) {
					currentWeek = _.find(model.get('weeks'), function(week) { return parseInt(week.number) === data.week; });
					currentWeek.technology = data.technology;
					currentWeek.count = _.reduce(currentWeek.technology, function(sum, item) { return sum + item.count; }, 0);

					this.trigger('change', model);
					model.trigger('change');
				}

				this.updateHeaderCount();

			});

			this.listenTo(Backbone, 'schedule:save:done', function(data) {
				var model = this.findWhere({ release_id: data.release_id, cinema_id: data.cinema_id });
				if (model) model.set('color', data.color);
				this.updateHeaderCount();
			});

			this.listenTo(Backbone, 'socket:schedule_halls', function() {
				this.changeCinema();
			});

			this.listenTo(Backbone, 'release:favourites', function(releaseStart) {
				var matchedWeek = this.parentview.model.get('currentWeeks').find(function(week) { return week.value === releaseStart.week; });
				if (matchedWeek && this.parentview.model.get('currentYear') === releaseStart.year) this.update();
			});

			this.listenTo(Backbone, 'release:color:changed', function(data) {
				var model = this.findWhere({release_id: data.id});

				if (model) {
					model.get('release').color = data.color;
					model.trigger('change');
				}
			});

			this.listenTo(Backbone, 'release:duration:changed', function(data) {
				var model = this.findWhere({release_id: data.id});
				if (model) {
					model.get('release').duration        = data.duration;
					model.get('release').chosen_duration = data.chosen_duration;
					model.trigger('change');
					this.updateHeaderCount();
				}
			});

			this.listenTo(Backbone, 'cinema-changed', function() {
				if (Backbone.history.fragment === 'planning') {
					if (this.parentview.activeTab !== 'network') {
						this.changeCinema('change');

						Backbone.trigger('loader', 'show');
						Backbone.trigger('planning:screenBlocker', 'show');
						Backbone.trigger('planning:mechanicBlocker', 'hide');
						this.parentview.$el.find('.seance_info').remove();
					} else {
						this.checkCinemaHallsLength('change');
						this.parentview.needRepertoireUpdate = true;
						this.parentview.currentView.checkNetworkVisibleCinema();
					}
				} else {
					this.changeCinema('change');
				}
			});

			this.listenTo(Backbone, 'socket:favourite', function() {
				if (Backbone.history.fragment === 'planning') {
					if (!this.headView.startUpdate || this.headView.startUpdate !== 1) {
						this.headView.startUpdate = 1;
						this.headView.listenToOnce(this, 'sync', function() {
							self.headView.startUpdate = 0;
						});

						Backbone.trigger('loader', 'show');
						Backbone.trigger('planning:screenBlocker', 'show');
						Backbone.trigger('planning:mechanicBlocker', 'hide');
						this.headView.$el.find('.seance_info').remove();

						this.changeCinema('favourite');
					}
				} else {
					this.update('silent');
				}
			});

			this.listenTo(Backbone, 'socket:repertoire:update', function(data) {
				var self = this,
					modelFirstTab;

				if (data && data.schedule_id && (reqres.request('get:user').getCinema().id === parseInt(data.cinema_id))) {
					modelFirstTab = this.findWhere({release_id: data.schedule_id });

					if (modelFirstTab) {
						modelFirstTab.fetch({
							success: function() {
								if (self.parentview.activeTab === 'cinema') {
									self.updateHeaderCount();
								} else if (self.parentview.activeTab === 'halls') {
									reqres.request('get:repertoire:view').currentView.checkWeekHeight();
								}
							}
						});
					}
				}
			});

			this.listenTo(Backbone, 'releaseCard:model:change', function(data) {
				this.findWhere({release_id: data.id}).fetch();
			});
		},
		url: function url() {
			return '/api/schedule/' +
				this.parentview.model.get('date').getFullYear() + '/' +
				(this.parentview.model.get('date').getMonth() + 1) + '/?' +
				($.param(this.parentview.model.get('currentWeeks'))) +
				'&cinema_id=' + reqres.request('get:user').getCinema().id;
		},
		parse: function parse(r) { // функция парсинга результата, нам нужен только массив, а не бессмысленные комментарии
			this.duration = r.duration;

			return r.schedule.sort(function(a, b) {
					var date = +_.first(a.weeks).year - +_.first(b.weeks).year;
					return date ? date : +_.first(a.weeks).number - +_.first(b.weeks).number;
				});
		},
		headCount: function headCount() {
			if (this.headView) {
				if (_.isEmpty(this.duration)) {
					this.headView.model.set('duration', {});
					this.headView.model.trigger('change');
				} else {
					this.headView.model.set('duration', this.duration);
					this.headView.render();
				}
			}
		},
		setParent: function setParent(v) {
			this.parentview = v;
		},
		checkCinemaHallsLength: function checkCinemaHallsLength(param) {
			var $lastTab      = this.parentview.$el.find('.filter .repertoire_sections .item').last(),
				currentCinema = reqres.request('get:user').getCinema();

			if (param !== 'favourite') {
				if (currentCinema.halls.length === 1) {
					$lastTab.addClass('hide');

					if ($lastTab.hasClass('active')) $lastTab.prev().trigger('click');
				} else {
					$lastTab.removeClass('hide');
				}
			}
		},
		changeCinema: function changeCinema(param) {
			if (param === 'change') this.changedCinema = true;

			this.checkCinemaHallsLength(param);
			this.update(param);

			this.parentview.model.set({cinema: reqres.request('get:user').getCinema()});
		},
		update: function update(param) {
			var self = this,
				user = reqres.request('get:user');

			Backbone.trigger('header:cinemaSelect:activeToggle', 'block');

			this.fetch({
				success: function() {
					var count,
						missCount;

					Backbone.trigger('planning:mechanicBlocker', 'hide');

					if (user.get('type') === 'mechanic' && (!user.get('cinema_rights').length || !user.hasRights())) {
						self.remove(self.models);

						for (count = 0; count < 32; count++) {
							self.add(new window.app.Repertoire());
						}

						Backbone.trigger('planning:mechanicBlocker', 'show');
					} else {
						missCount = 32 - self.length;

						for (count = 0; count < missCount; count++) {
							self.add(new window.app.Repertoire());
						}
					}

					if (param !== 'silent') {
						self.createCinemaHeader(param);
					}

					Backbone.trigger('header:cinemaSelect:activeToggle', 'unblock');

					self.parentview.enableButton = true;

					Backbone.trigger('loader', 'hide');
					Backbone.trigger('planning:screenBlocker', 'hide');
				}
			});
		},
		createCinemaHeader: function createCinemaHeader(param, state) {
			if (state) {
				if (state === 'planning:cinema') {
					this.headView = new window.app.RepertoireHeaderView({
						model: this.parentview.model,
						collection: this
					});
					this.headCount();
					this.parentview.currentView = this.headView;
				} else if (state === 'planning:halls') {
					this.parentview.$el.find('#halls').show().siblings('.planning_section').hide();
					this.parentview.$el.find('.repertoire_sections .section_halls').addClass('active').siblings().removeClass('active');

					window.app.App.hallsview = new window.app.HallsHeaderView({
						model: new window.app.HallsPlanning(),
						parent: this.parentview,
						calendar: this.parentview.calendar
					});
					this.parentview.currentView = window.app.App.hallsview;
					this.parentview.currentView.render().el;

					this.parentview.activeTab = 'halls';
				} else if (state === 'planning:network') {
					window.app.App.networkview = new window.app.NetworkHeaderView({
						model:    new window.app.HallsPlanning(),
						parent:   this.parentview,
						cinemas:  reqres.request('get:user').getCinemas(),
						calendar: this.parentview.calendar
					});

					this.parentview.currentView = window.app.App.networkview;
					this.parentview.currentView.render().el;

					this.parentview.activeTab = 'network';
					this.parentview.$el.find('#network').show().siblings('.planning_section').hide();
					this.parentview.$el.find('.repertoire_sections .section_network').addClass('active').siblings().removeClass('active');
				}
			} else {
				if (this.parentview.currentView || param === 'favourite' || this.changedCinema) {
					this.parentview.currentView.trigger('remove');
				}

				if (this.parentview.activeTab === 'cinema') {
					this.headView = new window.app.RepertoireHeaderView({
						model: this.parentview.model,
						collection: this
					});
					this.headCount();
					this.parentview.currentView = this.headView;
				} else if (this.parentview.activeTab === 'halls') {
					this.parentview.currentView.trigger('remove');
					this.parentview.currentView.initialize({
						parent: this.parentview
					});
					this.parentview.currentView.render().el;
				} else if (this.parentview.activeTab === 'network') {
					this.parentview.currentView.initialize({
						parent: this.parentview
					});
					this.parentview.currentView.render().el;
				}
			}

			this.changedCinema = false;
		},
		updateHeaderCount: function updateHeaderCount() {
			var durations = {};

			_.each(this.models, function(oneModel) {
				var oneModelData = oneModel.attributes,
					time;

				if (oneModelData.release_id) {
					time = _.property(oneModelData.release.chosen_duration)(oneModelData.release.duration) ||
						oneModelData.release.duration.full ||
						oneModelData.release.duration.clean || 100;

					_.each(oneModelData.weeks, function(week) {
						var count               = week.count,
							number              = week.number,
							weekReleaseDuration = time * count;

						durations[number] = (durations[number] || 0) + weekReleaseDuration;
					});
				}
			});

			this.duration = durations;
			this.headView.model.set('duration', durations);
		}
	});

	window.app.NetworkRepertoireCollection = Backbone.Collection.extend({
		model:      Backbone.Model,
		parentview: {},
		duration:   {},
		headView:   {},
		initialize: function initialize() {
			this.listenTo(Backbone, 'technology:save:done schedule:save:done socket:favourite release:duration:changed', function() {
				this.parentview.$el.find('.network_body .repertoire_viewer.active').trigger('click');
				this.fetch();
			});

			this.listenTo(Backbone, 'socket:repertoire:update', function(data) {
				if (data && data.cinema_id && reqres.request('get:user').isMultiplex()) {
					this.parentview.$el.find('.network_body .repertoire_viewer.active').trigger('click');
					this.fetch();
				}
			});
		},
		url: function url() {
			var date  = this.parentview.model.get('date'),
				range = Date.range(Date.create().set({month: date.getMonth(), year: date.getFullYear()}).beginningOfMonth(), Date.create().set({month: date.getMonth(), year: date.getFullYear()}).endOfMonth()),
				line  = [];

			range.every('weekday', function(weekday) {
				if (Date.create(weekday).format('{weekday}') === 'четверг') {
					line.push({
						name:  'week',
						value: moment(Date.create(weekday)).weeks()
					});
				}
			});

			return '/api/schedule/network/'  +
				date.getFullYear()    + '/'  +
				(date.getMonth() + 1) + '/?' +
				($.param(line))  +
				'&cinema_id=all';
		},
		parse: function parse(res) {
			var arr  = [];

			_.each(res.list, function(cinema) {
				_.each(cinema.weeks, function(weekInCinema) {
					var section      = {},
						releasesList = weekInCinema.releases.sortBy(function(release) {
							return release.title;
						});

					weekInCinema.releases = releasesList;

					section.cinema_id = cinema.cinema_id;
					section.week      = weekInCinema;
					section.year      = this.parentview.model.get('currentYear');
					section.duration  = weekInCinema.duration;

					arr.push(section);
				}, this);
			}, this);

			return arr;
		},
		setParent: function setParent(parent, repertoire) {
			this.parentview = parent;
			this.listenToOnce(repertoire, 'sync', function() {
				this.listenTo(this.parentview.model, 'change:date', function() {
					this.fetch();
				});
			});
		},
		addRelease: function addRelease(week, fromCinema, targetCinema, id, format, upgrades, param) {
			fromCinema   = parseInt(fromCinema);
			targetCinema = parseInt(targetCinema);
			week         = parseInt(week);
			id           = parseInt(id);

			Backbone.trigger('loader', 'show');

			var self             = this,
				year             = this.parentview.model.get('currentYear'),
				oldCinemaData    = this.where({cinema_id: fromCinema}),
				newCinemaData    = this.where({cinema_id: targetCinema}),
				oldReleaseCount  = 0,
				oldReleaseToCopy = {},
				newModel,
				newRelease;

			_.each(oldCinemaData, function(oldModel) {
				if (parseInt(oldModel.get('week').number) === week) {
					_.each(oldModel.get('week').releases, function(oldRelease) {
						if (oldRelease.id === id) {
							oldReleaseToCopy = oldRelease;
							_.each(oldRelease.technology, function(oldTech) {
								var oldUpgrades = oldTech.upgrades;

								if (oldUpgrades.length > 0) {
									oldUpgrades = oldUpgrades.join(', ');

									if (oldTech.format === format && oldUpgrades === upgrades) {
										oldReleaseCount = oldTech.count;
									}
								} else if (oldTech.format === format) {
									oldReleaseCount = oldTech.count;
								}
							});
						}
					});
				}
			});

			if (param === 'replace') {
				newModel   = newCinemaData.find(function(model) { return parseInt(model.get('week').number) === week; });
				newRelease = newModel.get('week').releases.find(function(release) { return release.id === id; });

				_.each(newRelease.technology, function(newTech) {
					delete newTech.releaseId;
					delete newTech.releaseColor;
					delete newTech.releaseTitle;
					if (_.isUndefined(upgrades)) {
						if (newTech.format === format && newTech.upgrades.length === 0) {
							newTech.count = oldReleaseCount;
						}
					} else {
						var formatUpgrades = newTech.upgrades.join(', ');

						if (newTech.format === format && formatUpgrades === upgrades) {
							newTech.count = oldReleaseCount;
						}
					}
				});

				$.post('/api/schedule/change_count', {
					tx:         reqres.request('get:user').get('tx'),
					release_id: id,
					cinema_id:  targetCinema,
					week:       week,
					year:       year,
					technology: $.toJSON(newRelease.technology)
				}, function(r) {
					if (r.ok) {
						newModel.set({
							duration: r.duration
						});

						if (targetCinema === reqres.request('get:user').getCinema().id) {
							var neededModel = window.app.App.repview.repertoire.findWhere({release_id: id});

							if (neededModel) {
								neededModel.fetch({
									success: function() {
										window.app.App.repview.repertoire.updateHeaderCount();
									}
								});
							}
						}
					}
					Backbone.trigger('loader', 'hide');
				}, 'json');
			} else if (param === 'new') {
				oldReleaseToCopy = JSON.stringify(oldReleaseToCopy);
				oldReleaseToCopy = JSON.parse(oldReleaseToCopy);

				_.each(oldReleaseToCopy.technology, function(copyTech) {
					delete copyTech.releaseId;
					delete copyTech.releaseColor;
					delete copyTech.releaseTitle;
					copyTech.count_hall = 0;
					copyTech.hall_id = {};

					var newUpgrades = copyTech.upgrades;

					if (_.isUndefined(upgrades)) {
						if (newUpgrades.length > 0) {
							copyTech.count = 0;
						} else if (copyTech.format !== format) {
							copyTech.count = 0;
						}
					} else {
						if (newUpgrades.length > 0) {
							newUpgrades = newUpgrades.join(', ');

							if (copyTech.format !== format && newUpgrades !== upgrades) {
								copyTech.count = 0;
							}
						} else {
							copyTech.count = 0;
						}
					}
				});

				$.post('/api/schedule/addone', {
					tx:         reqres.request('get:user').get('tx'),
					release_id: id,
					cinema_id:  targetCinema,
					week:       week,
					year:       year,
					technology: $.toJSON(oldReleaseToCopy.technology)
				}, function(r) {
					if (r.ok) {
						self.fetch();

						if (targetCinema === reqres.request('get:user').getCinema().id) {
							reqres.request('get:repertoire:view').repertoire.update('silent');
						}
					}
					Backbone.trigger('loader', 'hide');
				}, 'json');
			}
		}
	});

	window.app.RepertoireView = Backbone.View.extend({
		id: 'halls_schedule',
		template: '#repertoire_full',
		events: {
			'click .repertoire_wrap__head .week': function(e) {
				if ($(e.target).hasClass('add_week_release')) {
					return;
				}

				var cinema_id        = reqres.request('get:user').getCinema().id,
					week             = $(e.currentTarget).data('week'),
					year             = $(e.currentTarget).data('year'),
					occupancyPercent = $(e.currentTarget).data('occupancy_percent'),
					freeTime         = $(e.currentTarget).data('free_time');

				window.app.App.timelineModule = new window.app.TimelineModule(cinema_id, week, year, occupancyPercent, freeTime);

				_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Открытие предварительного расписания']);
			},
			'click .repertoire_sections .item': function(e) {
				if ($(e.currentTarget).hasClass('active')) return;

				Backbone.trigger('loader', 'show');
				Backbone.trigger('popover:hide');

				var item         = $(e.currentTarget),
					$halls       = this.$el.children('#halls'),
					$repertoire  = this.$el.children('#repertoire'),
					$network     = this.$el.children('#network'),
					$restartTour = this.$el.find('.restart-tour'),
					$filter      = this.$el.find('.filter'),
					self         = this,
					cinemas;

				Backbone.trigger('planning:screenBlocker', 'show');

				if (item.hasClass('section_cinema')) {
					$restartTour.show();
				} else {
					$restartTour.hide();
				}

				item.addClass('active').siblings().removeClass('active');

				if (item.hasClass('section_cinema')) {
					this.activeTab = 'cinema';
					window.app.App.State = 'planning:cinema';

					$repertoire.show();
					$halls.hide();
					$network.hide();

					setTimeout(function() {
						self.repertoire.headView = new window.app.RepertoireHeaderView({
							model:      self.model,
							collection: self.repertoire
						});
						self.switchView(self.repertoire.headView);
					}, 0);

					$filter.find('.nt-sidebar-toggle, .btn_export, .full_weeks_mode_wrapper').show();
					$filter.find('.screen, .full_halls_mode').hide();

					$repertoire.find('#my_repertoire_wr').focus();
				} else if (item.hasClass('section_halls')) {
					this.activeTab = 'halls';
					window.app.App.State = 'planning:halls';

					$repertoire.hide();
					$halls.show();
					$network.hide();

					setTimeout(function() {
						window.app.App.hallsview = new window.app.HallsHeaderView({
							model: new window.app.HallsPlanning(),
							parent: self,
							calendar: self.calendar
						});
						self.switchView(window.app.App.hallsview);
					}, 0);

					$filter.find('.nt-sidebar-toggle, .btn_export, .full_weeks_mode_wrapper').hide();

					if (reqres.request('get:user').getCinema().halls.length > 7) {
						if (this.fullHallsMode) {
							$filter.find('.full_halls_mode').show();
							$filter.find('.screen').hide();
						} else {
							$filter.find('.screen, .full_halls_mode').show();
						}
					} else {
						$filter.find('.screen').hide();
					}
				} else if (item.hasClass('section_network')) {
					this.activeTab = 'network';
					window.app.App.State = 'planning:network';

					$repertoire.hide();
					$halls.hide();
					$network.show();

					cinemas = reqres.request('get:user').getCinemas();

					setTimeout(function() {
						window.app.App.networkview = new window.app.NetworkHeaderView({
							model:   new window.app.HallsPlanning(),
							cinemas: cinemas,
							parent: self,
							calendar: self.calendar
						});
						self.switchView(window.app.App.networkview);
					}, 0);

					$filter.find('.nt-sidebar-toggle, .btn_export, .full_weeks_mode_wrapper, .full_weeks_mode_wrapper').hide();
					$filter.find('.screen, .full_halls_mode').hide();

					if (cinemas.length > 7) {
						$filter.find('.screen').show();
					} else {
						$filter.find('.screen').hide();
					}
				}

				if (this.needRepertoireUpdate) {
					Backbone.trigger('loader', 'show');
					Backbone.trigger('planning:screenBlocker', 'show');
					Backbone.trigger('planning:mechanicBlocker', 'hide');
					this.$el.find('.seance_info').remove();

					this.needRepertoireUpdate = false;
					this.repertoire.changeCinema('change');
				}
			},
			'click .restart-tour': function(e) {
				e.preventDefault();
				var user = reqres.request('get:user');
				if (user.get('type') !== 'cinema') return false;

				$.cookie('tour_ver', window.app.tour._options.version, { expires: 365 });

				if (user.getCinema().rights === ('read' || 'rw') ? true : false) {
					window.app.readOnlyTour.restart();
				} else {
					window.app.tour.restart();
				}

				_gaq.push(['_trackEvent', 'Обучение', 'Репертуар: Запуск']);
			},
			'click .my_date .next': function(e) {
				e.preventDefault();
				if (!this.enableButton) return;
				this.enableButton = false;
				this.changeDate(1);

				this.$el.find('.seance_info').remove();
			},
			'click .my_date .prev': function(e) {
				e.preventDefault();
				if (!this.enableButton) return;
				this.enableButton = false;
				this.changeDate(-1);

				this.$el.find('.seance_info').remove();
			},
			'click .screen .arrow_left': function(e) {
				var $target = $(e.target),
					$section = $target.closest('.filter').find('.repertoire_sections .active'),
					head,
					body,
					page,
					objects,
					item,
					itemWidth,
					currentLeft,
					newLeft,
					itemWidthObj,
					currentLeftObj,
					newLeftObj;

				if ($section.hasClass('section_halls')) {
					head = this.$el.find('.halls_head .list');
					body = this.$el.find('.halls_body');
				} else if ($section.hasClass('section_network')) {
					head = this.$el.find('.network_head .list');
					body = this.$el.find('.network_body');
				}

				page    = parseInt(body.attr('screen-page'));
				objects = body.find('.list .section');
				item    = head.find('.item');

				if (item.length > 7) {
					itemWidth      = item.first().outerWidth();
					currentLeft    = parseInt(head.css('left'));
					newLeft        = currentLeft + itemWidth;
					itemWidthObj   = objects.first().outerWidth();
					currentLeftObj = parseInt(objects.closest('.list').css('left'));
					newLeftObj     = currentLeftObj + itemWidthObj;

					if (body.attr('screen-page') > 1) {
						head.css({
							left: newLeft
						});

						objects.closest('.list').css({
							left: newLeftObj
						});

						body.attr({
							'screen-page': page - 1
						});
					}
				}

				if (this.activeTab === 'halls') {
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Переключение залов']);
				} else if (this.activeTab === 'network') {
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По сети', 'Переключение кинотеатров']);
				}
			},
			'click .screen .arrow_right': function(e) {
				var $target = $(e.target),
					$section = $target.closest('.filter').find('.repertoire_sections .active'),
					head,
					body,
					page,
					objects,
					item,
					itemWidth,
					itemNum,
					currentLeft,
					maxLeft,
					newLeft,
					itemWidthObj,
					currentLeftObj,
					maxLeftObj,
					newLeftObj;

				if ($section.hasClass('section_halls')) {
					head = this.$el.find('.halls_head .list');
					body = this.$el.find('.halls_body');
				} else if ($section.hasClass('section_network')) {
					head = this.$el.find('.network_head .list');
					body = this.$el.find('.network_body');
				}

				page    = parseInt(body.attr('screen-page'));
				objects = body.find('.list .section');
				item    = head.find('.item');

				if (item && item.length > 7) {
					itemWidth      = item.first().outerWidth();
					itemNum        = item.length;
					currentLeft    = parseInt(head.css('left'));
					maxLeft        = (itemNum - 6) * itemWidth - ($section.hasClass('section_network') ? 142 : 113);
					newLeft        = currentLeft - itemWidth;
					itemWidthObj   = objects.first().width();
					currentLeftObj = parseInt(objects.closest('.list').css('left'));
					maxLeftObj     = (itemNum - 7) * itemWidthObj;
					newLeftObj     = currentLeftObj - itemWidthObj - 1;

					if (Math.abs(newLeft) < maxLeft) {
						head.css({
							left: newLeft
						});

						body.attr({
							'screen-page': page + 1
						});
					}

					if (Math.abs(newLeftObj) < maxLeftObj) {
						objects.closest('.list').css({
							left: newLeftObj
						});
					}
				}

				if (this.activeTab === 'halls') {
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Переключение залов']);
				} else if (this.activeTab === 'network') {
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По сети', 'Переключение кинотеатров']);
				}
			},
			'click .btn_export': function() {
				var self          = this,
					exportPlaning = new window.app.ExportPlaningView({model: this.model});

				this.$el.prepend(exportPlaning.render().$el);

				exportPlaning.$el.arcticmodal({
					beforeOpen: function(data, el) {
						el.css({opacity:0});
					},
					afterOpen: function(data, el) {
						var $exportWrap = el,
							currentPickerDate = moment(self.model.get('date')).startOf('month').format('MMMM YYYY');

						currentPickerDate = currentPickerDate.charAt(0).toUpperCase() + currentPickerDate.slice(1);

						$exportWrap.css({opacity:1}).find('input').each(function() {
							$(this).datepicker({
								autoclose: true,
								language: 'ru',
								startDate: currentPickerDate,
								endDate: moment(self.model.get('date')).startOf('month').add(11, 'month').toDate(),
								minViewMode: 'months',
								format: 'MM yyyy'
							});
						});
					},
					afterClose: function(data, el) {
						el.remove();
					}
				});

				_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Экспорт в Excel']);
			},
			'mouseenter #halls, #network': function(e) {
				var $target    = $(e.currentTarget),
					$exClicked = $target.find('.ex_clicked'),
					network    = false,
					self       = this,
					releaseSelectMove,
					postAnalytic;

				if ($target.context.id === 'network') {
					network = true;
				}

				if ($exClicked.length) {
					if ($exClicked.hasClass('add_release')) {
						$exClicked.removeClass('ex_clicked').addClass('clicked visible');
					} else {
						$exClicked.removeClass('ex_clicked').addClass('clicked').siblings('.add_release').addClass('visible');
					}

					$target.removeClass('mouseleave');
				}

				postAnalytic = function(key) {
					var text;

					if (key === 40) {
						text = 'Клавиатура::вниз';
					} else if (key === 38) {
						text = 'Клавиатура::вверх';
					} else if (key === 37) {
						text = 'Клавиатура::влево';
					} else if (key === 39) {
						text = 'Клавиатура::вправо';
					}

					if (network) {
						_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По сети', text]);
					} else {
						_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', text]);
					}
				};

				releaseSelectMove = function(e) {
					e = e || window.event;

					if (e.keyCode === 8 || e.keyCode === 46) {
						e.preventDefault();
					}

					var $clickedRelease = $target.find('.section .clicked'),
						$currentWeek,
						sectionIndex,
						$currentSection,
						clickedIndex,
						$currentSectionObject,
						currentPage,
						$nextWeekSection,
						$next,
						$prev,
						$body,
						$leftSection,
						$leftSectionObjects,
						prevPage,
						$rightSection,
						$rightSectionObjects,
						nextPage;

					$clickedRelease.trigger('changeSeanceCount', [e.keyCode]);

					if (e.keyCode === 40 && $clickedRelease.length) { // стрелка вниз
						e.preventDefault();

						postAnalytic(e.keyCode);

						if ($clickedRelease.next().hasClass('release')) {
							$next = $clickedRelease.next();

							$clickedRelease.removeClass('clicked');

							$next.addClass('clicked');

							if (!network) {
								$next.trigger('seanceInfo');

								if ($next.offset().top + 20 > self.$el.find('.seance_info').offset().top) {
									$next.closest('.halls_body').scrollTo($next, {axis:'y'});
								}
							} else {
								if ($next.offset().top + 20 > $next.closest('.network_wrap').offset().top + $next.closest('.network_body').height()) {
									$next.closest('.network_wrap').scrollTo($next, {offset: {top: -53}, axis: 'y'});
								}
							}
						} else if ($clickedRelease.nextAll('.add_release').length) {
							$next = $clickedRelease.nextAll('.add_release');

							$clickedRelease.removeClass('clicked');
							$next.addClass('clicked');

							if ($next.offset().top + 20 > self.$el.find('.seance_info').offset().top) {
								$next.closest('.halls_body').scrollTo($next, {axis:'y'});
							}

							self.$el.find('.seance_info').remove();

							$clickedRelease.closest('.halls_body').find('.backlight').removeClass('backlight');
						} else {
							$currentWeek = $clickedRelease.closest('.week');
							sectionIndex = $clickedRelease.closest('.section').index();

							if ($currentWeek.next().length) {
								$nextWeekSection = $currentWeek.next().find('.section').eq(sectionIndex);

								$clickedRelease.closest('.section').find('.add_release').removeClass('visible');

								if ($nextWeekSection.children('.release, .add_release').length) {
									$clickedRelease.removeClass('clicked');
									$nextWeekSection.children('.release, .add_release').first().addClass('clicked');

									if (!network) {
										$nextWeekSection.find('.add_release').addClass('visible');
									}
								} else if (network && $nextWeekSection.children('.network_list').find('.release').not('.ui-draggable-disabled').length) {
									$clickedRelease.removeClass('clicked');
									$nextWeekSection.children('.network_list').find('.release').first().addClass('clicked');
								}

								if ($nextWeekSection.find('.clicked.release').length) {
									if (!network) {
										$nextWeekSection.find('.clicked.release').trigger('seanceInfo');
									}

									if (!network) {
										if ($nextWeekSection.find('.clicked').offset().top + 20 > $nextWeekSection.closest('.cinema_schedule').offset().top + $nextWeekSection.closest('.halls_body').height()) {
											$nextWeekSection.closest('.cinema_schedule').scrollTo($nextWeekSection.find('.clicked'), {axis:'y'});
										}
									} else {
										if ($nextWeekSection.find('.clicked').offset().top + 20 > $nextWeekSection.closest('.network_wrap').offset().top + $nextWeekSection.closest('.network_body').height()) {
											$nextWeekSection.closest('.network_wrap').scrollTo($nextWeekSection.find('.clicked'), {offset: {top: -53}, axis: 'y'});
										}
									}
								} else {
									if (!network) {
										if ($nextWeekSection.find('.clicked').offset().top + 20 > $nextWeekSection.closest('.cinema_schedule').offset().top + $nextWeekSection.closest('.halls_body').height()) {
											$nextWeekSection.closest('.cinema_schedule').scrollTo($nextWeekSection.find('.clicked'), {axis:'y'});
										}

										self.$el.find('.seance_info').remove();
									}

									$nextWeekSection.closest('.halls_body').find('.backlight').removeClass('backlight');
								}
							}
						}
					} else if (e.keyCode === 38 && $clickedRelease.length) { // стрелка вверх
						e.preventDefault();

						postAnalytic(e.keyCode);

						if ($clickedRelease.prevAll('.release').length) {
							if ($clickedRelease.prev().hasClass('repertoire__list')) {
								$prev = $clickedRelease.closest('.section').children('.release').last();
								$body = $prev.closest('.halls_body');

								$clickedRelease.removeClass('clicked');
								$prev.addClass('clicked');

								if (!network) {
									$prev.trigger('seanceInfo');

									if ($prev.offset().top < $body.offset().top) {
										$prev.closest('.cinema_schedule').scrollTo($prev, {axis:'y'});
									}
								}
							} else {
								$prev = $clickedRelease.prev();
								$body = $prev.closest('.halls_body');

								$clickedRelease.removeClass('clicked');
								$prev.addClass('clicked');

								if (!network) {
									$prev.trigger('seanceInfo');

									if ($prev.offset().top < $body.offset().top) {
										$prev.closest('.cinema_schedule').scrollTo($prev, {axis:'y'});
									}
								} else {
									if ($prev.offset().top < $prev.closest('.network_body').offset().top) {
										$prev.closest('.network_wrap').scrollTo($prev, {offset: {top: -53}, axis: 'y'});
									}
								}
							}
						} else {
							$currentWeek = $clickedRelease.closest('.week');
							sectionIndex = $clickedRelease.closest('.section').index();

							if ($currentWeek.prev().length) {
								$nextWeekSection = $currentWeek.prev().find('.section').eq(sectionIndex);

								$clickedRelease.closest('.section').find('.add_release').removeClass('visible');

								if (!network) {
									$clickedRelease.removeClass('clicked');
									$nextWeekSection.find('.add_release').addClass('clicked visible');
									if ($nextWeekSection.find('.clicked').offset().top < $nextWeekSection.closest('.cinema_schedule').offset().top) {
										$nextWeekSection.closest('.cinema_schedule').scrollTo(0, {axis:'y'});
									}

									self.$el.find('.seance_info').remove();
								} else {
									if (!$nextWeekSection.find('.network_list .release').length) return;
									$clickedRelease.removeClass('clicked');
									$nextWeekSection.find('.network_list .release').not('.ui-draggable-disabled').last().addClass('clicked');
									if ($nextWeekSection.find('.clicked').offset().top < $nextWeekSection.closest('.network_wrap').offset().top) {
										$nextWeekSection.closest('.network_wrap').scrollTo($nextWeekSection, {offset: {top: -53}, axis: 'y'});
									}
								}

								$nextWeekSection.closest('.halls_body').find('.backlight').removeClass('backlight');
							}
						}
					} else if (e.keyCode === 37 && $clickedRelease.length) { // стрелка влево
						e.preventDefault();

						postAnalytic(e.keyCode);

						$currentSection = $clickedRelease.closest('.section');
						clickedIndex    = $clickedRelease.index();

						if ($clickedRelease.hasClass('add_release')) {
							clickedIndex = clickedIndex - 1;
						}

						if ($currentSection.prev().hasClass('section')) {
							$leftSection          = $currentSection.prev();
							$leftSectionObjects   = network ? $leftSection.find('.network_list .release').not('.ui-draggable-disabled') : $leftSection.children('.release, .add_release');
							prevPage              = parseInt($leftSection.index() + 1);
							$currentSectionObject = network ? $currentSection.find('.network_list .release').not('.ui-draggable-disabled') : $currentSection.children('.release, .add_release');
							currentPage           = parseInt($leftSection.closest('.halls_body').attr('screen-page')) || parseInt($leftSection.closest('.network_body').attr('screen-page'));

							if ($leftSectionObjects.length) {
								$currentSection.find('.add_release').removeClass('visible');
								$clickedRelease.removeClass('clicked');
								$leftSection.children('.add_release').addClass('visible');
							}

							if ($currentSectionObject.length === $leftSectionObjects.length) {
								$leftSectionObjects.eq(clickedIndex).addClass('clicked');
							} else if ($currentSectionObject.length > $leftSectionObjects.length) {
								if ($clickedRelease.hasClass('add_release')) {
									$leftSectionObjects.last().addClass('clicked');
								} else {
									if ($leftSectionObjects.eq(clickedIndex).length) {
										$leftSectionObjects.eq(clickedIndex).addClass('clicked');
									} else {
										$leftSectionObjects.last().addClass('clicked');
									}
								}
							} else if ($currentSectionObject.length < $leftSectionObjects.length) {
								if ($clickedRelease.hasClass('add_release')) {
									$leftSectionObjects.eq(clickedIndex).addClass('clicked');
								} else {
									$leftSectionObjects.eq(clickedIndex).addClass('clicked');
								}
							}

							if ($leftSectionObjects.eq(clickedIndex).hasClass('release') && !network) {
								$leftSectionObjects.eq(clickedIndex).trigger('seanceInfo');
							} else {
								self.$el.find('.seance_info').remove();
								$leftSection.closest('.halls_body').find('.backlight').removeClass('backlight');
							}
						}
					} else if (e.keyCode === 39 && $clickedRelease.length) { // стрелка вправо
						e.preventDefault();

						postAnalytic(e.keyCode);

						$currentSection = $clickedRelease.closest('.section');
						clickedIndex    = $clickedRelease.index();

						if ($clickedRelease.hasClass('add_release')) {
							clickedIndex = clickedIndex - 1;
						}

						if ($currentSection.next().find('.add_release').length || !$currentSection.next().hasClass('empty_section')) {
							$rightSection         = $currentSection.next();
							$rightSectionObjects  = network ? $rightSection.find('.network_list .release').not('.ui-draggable-disabled') : $rightSection.children('.release, .add_release');
							nextPage              = parseInt($rightSection.index() - 5);
							$currentSectionObject = network ? $currentSection.find('.network_list .release').not('.ui-draggable-disabled') : $currentSection.children('.release, .add_release');
							currentPage           = parseInt($rightSection.closest('.halls_body').attr('screen-page')) || parseInt($rightSection.closest('.network_body').attr('screen-page'));

							if ($rightSectionObjects.length) {
								$currentSection.find('.add_release').removeClass('visible');
								$clickedRelease.removeClass('clicked');
								$rightSection.children('.add_release').addClass('visible');
							}

							if ($currentSectionObject.length === $rightSectionObjects.length) {
								$rightSectionObjects.eq(clickedIndex).addClass('clicked');
							} else if ($currentSectionObject.length > $rightSectionObjects.length) {
								if ($clickedRelease.hasClass('add_release')) {
									$rightSectionObjects.last().addClass('clicked');
								} else {
									if ($rightSectionObjects.eq(clickedIndex).length) {
										$rightSectionObjects.eq(clickedIndex).addClass('clicked');
									} else {
										$rightSectionObjects.last().addClass('clicked');
									}
								}
							} else if ($currentSectionObject.length < $rightSectionObjects.length) {
								if ($clickedRelease.hasClass('add_release')) {
									$rightSectionObjects.eq(clickedIndex).addClass('clicked');
								} else {
									$rightSectionObjects.eq(clickedIndex).addClass('clicked');
								}
							}

							if ($rightSectionObjects.eq(clickedIndex).hasClass('release') && !network) {
								$rightSectionObjects.eq(clickedIndex).trigger('seanceInfo');
							} else {
								self.$el.find('.seance_info').remove();
								$rightSection.closest('.halls_body').find('.backlight').removeClass('backlight');
							}
						}
					}

					if (e.keyCode === 13 && $clickedRelease.length) {
						e.preventDefault();

						if ($clickedRelease.hasClass('add_release')) {
							$clickedRelease.trigger('click');
						} else {
							$clickedRelease.trigger('seanceInfo');
						}
					}
				};

				$(document).unbind('keydown');
				$(document).keydown(releaseSelectMove);
			},
			'mouseleave #halls, #network': function(e) {
				var $target         = $(e.currentTarget),
					$clickedRelease = $target.find('.section .clicked');

				if ($clickedRelease.hasClass('add_release')) {
					$clickedRelease.removeClass('clicked visible').addClass('ex_clicked');
				} else {
					$clickedRelease.removeClass('clicked').addClass('ex_clicked').siblings('.add_release').removeClass('visible');
				}

				$target.addClass('mouseleave');

				$(document).unbind('keydown');
				// $(document).unbind('keypress');
			},
			'mouseenter #repertoire': function() {
				var self = this,
					monthChange = function(e) {
						e = e || window.event;

						if (e.keyCode === 39) { //стрелка вправо
							if (!self.enableButton) return;
							self.enableButton = false;
							window.app.App.$el.children('.tipsy').remove();
							self.changeDate(1);

							self.$el.find('.seance_info').remove();
						} else if (e.keyCode === 37) { //стрелка влево
							if (!self.enableButton) return;
							self.enableButton = false;
							window.app.App.$el.children('.tipsy').remove();
							self.changeDate(-1);

							self.$el.find('.seance_info').remove();
						}

					};

				$(document).unbind('keydown');
				$(document).keydown(monthChange);
			},
			'mouseleave #repertoire': function() {
				$(document).unbind('keydown');
			},
			'mouseenter .filter': function() {
				$(document).unbind('keydown');
			},
			'click .my_date__date': function(e) {
				$(e.currentTarget).closest('.my_date').children('.my_date__date--input').datepicker('hide');
			},
			'mouseenter .my_date__date--input, .my_date__date': function(e) {
				$(e.currentTarget).closest('.my_date').children('.my_date__date').addClass('hover');
			},
			'mouseleave .my_date__date--input, .my_date__date': function(e) {
				$(e.currentTarget).closest('.my_date').children('.my_date__date').removeClass('hover');
			},
			'click .full_weeks_mode': function(e) {
				var $targetParent = $(e.currentTarget).closest('.full_weeks_mode_wrapper');

				if (this.monthWeeksStarts && $(e.currentTarget).hasClass('full_weeks_mode__enable') || !this.monthWeeksStarts && !$(e.currentTarget).hasClass('full_weeks_mode__enable')) return;

				if (!this.monthWeeksStarts) {
					this.monthWeeksStarts = true;
					$targetParent.addClass('active');
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Режим полных недель']);
				} else {
					this.monthWeeksStarts = false;
					$targetParent.removeClass('active');
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Режим классический']);
				}

				$.cookie('monthWeeksStarts', this.monthWeeksStarts, {expires: 365});

				this.calendar.monthWeeksStarts = this.monthWeeksStarts;
				this.changeDate(0, {weekStartsChange: true});
			},
			'click .full_halls_mode': function(e) {
				var $target = $(e.currentTarget),
					self    = this;

				Backbone.trigger('loader', 'show');

				if (!this.fullHallsMode) {
					this.fullHallsMode = true;
					$target.addClass('active').siblings('.screen').hide();
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Режим всех залов']);
				} else {
					this.fullHallsMode = false;
					$target.removeClass('active').siblings('.screen').show();
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По залам', 'Режим классический']);
				}

				setTimeout(function() {
					self.currentView.trigger('remove');

					self.currentView.initialize({
						parent: self
					});
					self.currentView.render().el;
				}, 300);
			}
		},
		initialize: function initialize(options) {
			Backbone.trigger('loader', 'show');
			this.options = options || {};
			this.options.user = reqres.request('get:user');

			this.enableButton = true;
			this.needRepertoireUpdate = false;
			this.currentView = null;
			this.datePickerShowed = false;
			this.fullHallsMode = false;
			this.monthWeeksStarts = false;
			this.activeTab = 'cinema';

			var user = this.options.user;

			if (_.isUndefined($.cookie('monthWeeksStarts')) || $.cookie('monthWeeksStarts') === 'true') {
				this.monthWeeksStarts = true;
			}

			if (user.getCinema().halls.length > 7) this.fullHallsMode = true;

			this.repertoire = this.options.repertoire;
			this.networkCollection = this.options.network;

			this.repertoire.setParent(this);
			this.networkCollection.setParent(this, this.repertoire);

			this.calendar = new window.app.CreateCalendar({monthWeeksStarts: this.monthWeeksStarts});
			this.model.set({
				days:         this.repertoire.parentviewModel ? this.repertoire.parentviewModel.get('days')         : this.calendar.days,
				check:        this.repertoire.parentviewModel ? this.repertoire.parentviewModel.get('check')        : this.calendar.check,
				date:         this.repertoire.parentviewModel ? this.repertoire.parentviewModel.get('date')         : Date.create(),
				currentMonth: this.repertoire.parentviewModel ? this.repertoire.parentviewModel.get('currentMonth') : this.calendar.current.month,
				currentYear:  this.repertoire.parentviewModel ? this.repertoire.parentviewModel.get('currentYear')  : this.calendar.current.year,
				currentWeeks: this.repertoire.parentviewModel ? this.repertoire.parentviewModel.get('currentWeeks') : this.calendar.weeks
			});

			this.listenTo(this, 'remove', this.removeView);

			this.listenToOnce(this.repertoire, 'sync', function() {
				this.$el.find('#my_repertoire_wr').focus();
			});

			this.model.set({
				cinema:       user.getCinema(),
				cinemasCount: user.getCinemas().length,
				duration: this.repertoire.duration
			});

			if (this.repertoire.models.length) {
				this.listenToOnce(window.app.controller, 'route:planning',  function() {
					this.currentView = null;
					this.repertoire.createCinemaHeader('change', reqres.request('get:app:state'));
				});

				Backbone.trigger('loader', 'hide');
			}

			reqres.setHandlers({
				'get:repertoire:view': {
					callback: function() {
						return this;
					},
					context: this
				}
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.userType         = reqres.request('get:user').get('type');
			extModel.monthWeeksStarts = this.monthWeeksStarts;
			extModel.fullHallsMode    = this.fullHallsMode;

			this.$el.html(template(extModel));

			this.setDatePicker();

			this.screenBlocker = this.$el.children('.repertoire_blocked');
			this.listenTo(Backbone, 'planning:screenBlocker', function(action) {
				this.screenBlocker[action]();
			});

			this.mechanicBlocker = this.$el.children('.mechanic_blocked');
			this.listenTo(Backbone, 'planning:mechanicBlocker', function(action) {
				if (action === 'show') {
					this.mechanicBlocker.css('display', 'table');
				} else {
					this.mechanicBlocker.css('display', 'none');
				}
			});

			if (!this.repertoire.models.length) {
				this.repertoire.update();
			}

			return this;
		},
		setDatePicker: function setDatePicker() {
			var self = this,
				$date = this.$el.children('.filter').find('.my_date__date--input'),
				oldDate,
				newDate,
				diff,
				$datepicker;

			$date.datepicker({
				language: 'ru',
				weekStart: 4,
				autoclose: true,
				minViewMode: 1,
				format: 'mm yyyy',
				calendarWeeks: true
			})
			.on('hide', function() {
				newDate = moment(self.$el.children('.filter').find('.my_date__date--input').val(), 'MM YYYY');

				diff = newDate.diff(oldDate, 'months');

				self.datePickerShowed = false;

				self.changeDate(diff, {picker: true});
				$date.datepicker('update').removeClass('hide');
			})
			.on('show', function() {
				self.datePickerShowed = true;
				$datepicker = window.app.App.$el.children('.datepicker');

				$datepicker.css({
					left: parseInt($datepicker.css('left')) - 35,
					top: parseInt($datepicker.css('top')) - 1
				}).addClass('center');

				$date.addClass('hide');

				oldDate = moment($date.val(), 'MM YYYY');
			});
		},
		switchView: function switchView(view) {
			if (this.currentView !== null && this.currentView.cid !== view.cid) {
				this.currentView.trigger('remove');
			}

			this.currentView = view;

			view.render().el;
		},
		changeDate: function changeDate(action, option) {
			var date = moment(this.model.get('date')).add(action, 'month').toDate(),
				section = 'По кинотеатру';

			if (action !== 0 || (option && option.weekStartsChange)) {
				this.calendar.changeDate(date.getFullYear(), date.getMonth());

				this.model.set({
					date:         date,
					days:         this.calendar.days,
					currentMonth: this.calendar.current.month,
					currentYear:  this.calendar.current.year,
					currentWeeks: this.calendar.weeks
				});

				this.currentView.model.set({
					date:         date,
					days:         this.calendar.days,
					currentMonth: this.calendar.current.month,
					currentYear:  this.calendar.current.year,
					currentWeeks: this.calendar.weeks,
					calendar:     this.calendar
				});

				this.repertoire.parentviewModel = $.extend({}, this.model);

				this.currentView.trigger('remove');

				this.repertoire.changeCinema('change');

				Backbone.trigger('loader', 'show');
				Backbone.trigger('planning:screenBlocker', 'show');
				Backbone.trigger('planning:mechanicBlocker', 'hide');
				this.$el.find('.seance_info').remove();
			}

			if (reqres.request('get:app:state') === 'planning:cinema') {
				section = 'По кинотеатру';
			} else if (reqres.request('get:app:state') === 'planning:halls') {
				section = 'По залам';
			} else if (reqres.request('get:app:state') === 'planning:network') {
				section = 'По сети';
			}

			if (option && option.picker && action !== 0) {
				_gaq.push(['_trackEvent', 'Репертуарное планирование', section, 'Выбор определенного месяца']);
			} else if (!option) {
				_gaq.push(['_trackEvent', 'Репертуарное планирование', section, 'Переключение месяца']);
				this.$el.children('.filter').find('.my_date__date--input').val(moment(this.model.get('date')).format('MM YYYY')).datepicker('update');
			}

			this.$el.children('.filter').find('.my_date__date').text(date.getMonth().toMonthName() + ' ' + date.getFullYear());
		},
		removeView: function removeView() {
			this.$el.find('.my_date__date--input').datepicker('remove');

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ExportPlaningView = Backbone.View.extend({
		tagName:   'div',
		className: 'export_popup',
		template:  '#export_planing',
		initialize: function initialize() {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.removeView);
		},
		events: {
			'click .done': 'export',
			'focus input': function(event) {
				$(event.currentTarget).removeClass('error');
			}
		},
		export: function() {
			var $popup             = this.$el,
				$startDate         = $popup.find('#date'),
				startDate          = Date.create($startDate.val()),
				date               = moment($startDate.val(), 'MMMM YYYY').date(1),
				dateStart          = date.format('YYYY-MM-DD'),
				dateEnd            = date.endOf('month').format('YYYY-MM-DD'),
				singleCinemaExport = this.$('.layout').val() === 'current' ? true : false;

			$startDate.removeClass('error');

			if (!startDate.isValid() || !(/[а-яА-Я]\s\d{4}/.test($startDate.val()))) {
				$startDate.addClass('error');
				$.growl.error({message: 'Неверный формат даты' });
				return;
			}

			$.arcticmodal('close');

			location.href = window.location.origin +
				'/export/planning/' + (singleCinemaExport ? reqres.request('get:user').getCinema().id : '') +
				'?start=' + dateStart +
				'&end=' + dateEnd;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				currentPickerDate = moment(this.model.get('date')).startOf('month').format('MMMM YYYY');

			currentPickerDate = currentPickerDate.charAt(0).toUpperCase() + currentPickerDate.slice(1);

			this.$el.html(template(this.model.attributes));
			this.$el.find('#date').val(currentPickerDate);
			return this;
		},
		removeView: function removeView() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

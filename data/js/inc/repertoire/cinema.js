$(function() {
	'use strict';
	window.app.RepertoireHeaderView = Backbone.View.extend({
		className: 'repertoire_wrap__head',
		template: '#repertoire_header',
		events: {
			'click .add_week_release': 'add_release'
		},
		initialize: function initialize() {
			document.title = 'Мой репертуар - Киноплан';
			_gaq.push(['_trackEvent', 'Репертуарное планирование', 'Репертуар']);

			this.lineViews  = [];
			this.$wrapper   = this.collection.parentview.$el.find('#my_repertoire_wr');

			this.model.set({
				weekGroup: _.chain(this.model.get('days'))
					.groupBy(function(n) {
						return moment(n.date).weekYear().toString() + n.number_of_week;
					})
					.sortBy(function(group) {
						return group[0].day_of_month;
					})
					.value()
			}, {silent: true});

			this.listenTo(this.model, 'change', this.render);
			this.listenTo(Backbone, 'user:cinema:settings:save', this.render);
			this.listenTo(this, 'remove', this.removeView);

			this.$wrapper.append('<div class="repertoire_wrap__body"/>');

			var $releasesBody = this.$wrapper.children('.repertoire_wrap__body');

			this.listenTo(this.collection, 'add', function(model) {
				this.addOneRep(model, $releasesBody);
			});
			_.each(this.collection.models, function(model) {
				this.addOneRep(model, $releasesBody);
			}, this);
		},
		render: function render() {
			var template       = Backbone.TemplateCache.get(this.template),
				extModel       = $.extend({}, this.model.attributes),
				cinemaSettings = reqres.request('get:user').getCinemaSettings(),
				cinemaDayTime  = 960 * this.model.get('cinema').halls.length,
				startTime,
				endTime,
				weekendStartTime,
				weekendEndTime,
				dayTime,
				weekendTime;

			if (cinemaSettings) {
				startTime = parseInt(Date.create(cinemaSettings.cinemaOpen).format('{H}')) * 60 + parseInt(Date.create(cinemaSettings.cinemaOpen).format('{mm}'));
				endTime   = parseInt(Date.create(cinemaSettings.cinemaClose).format('{H}')) * 60 + parseInt(Date.create(cinemaSettings.cinemaClose).format('{mm}'));

				if (endTime < startTime) {
					endTime = 24 * 60 + endTime;
				}

				weekendStartTime = parseInt(Date.create(cinemaSettings.cinemaWeekendOpen).format('{H}')) * 60 + parseInt(Date.create(cinemaSettings.cinemaWeekendOpen).format('{mm}'));
				weekendEndTime   = parseInt(Date.create(cinemaSettings.cinemaWeekendClose).format('{H}')) * 60 + parseInt(Date.create(cinemaSettings.cinemaWeekendClose).format('{mm}'));

				if (weekendEndTime < weekendStartTime) {
					weekendEndTime = 24 * 60 + weekendEndTime;
				}

				dayTime     = endTime - startTime;
				weekendTime = weekendEndTime - weekendStartTime;

				if (endTime !== startTime || weekendEndTime !== weekendStartTime) {
					cinemaDayTime = Math.floor(((dayTime * 5 + weekendTime * 2) / 7) * this.model.get('cinema').halls.length);
				}
			}

			extModel.curYear = this.model.get('date').getFullYear();

			extModel.cinemaDayTime = cinemaDayTime;

			this.$wrapper.prepend(this.$el.html(template(extModel)));

			this.setNormalWidth();

			this.$el.show().find('.add_week_release').tipsy();

			Backbone.trigger('loader', 'hide');
			Backbone.trigger('planning:screenBlocker', 'hide');

			return this;
		},
		setNormalWidth: function setNormalWidth() {
			function getScrollBarWidth() {
				var inner = document.createElement('p'),
					outer,
					w1,
					w2;

				inner.style.width = '100%';
				inner.style.height = '200px';

				outer = document.createElement('div');
				outer.style.position = 'absolute';
				outer.style.top = '0px';
				outer.style.left = '0px';
				outer.style.visibility = 'hidden';
				outer.style.width = '200px';
				outer.style.height = '150px';
				outer.style.overflow = 'hidden';
				outer.appendChild (inner);

				document.body.appendChild(outer);
				w1 = inner.offsetWidth;
				outer.style.overflow = 'scroll';
				w2 = inner.offsetWidth;
				if (w1 === w2) w2 = outer.clientWidth;
				document.body.removeChild (outer);

				return (w1 - w2);
			}

			var scrollWidth = getScrollBarWidth(),
				$header     = this.$el.closest('.repertoire_wrap__head');

			$header.css({right: scrollWidth});
		},
		addOneRep: function addOneRep(model, wrapper) {
			var currentRepertoire = this.collection,
				extModel = $.extend({}, model),
				View,
				index;

			extModel.set({
				days: this.model.get('days'),
				check: this.model.get('check'),
				date: this.model.get('date'),
				cinema: this.model.get('cinema'),
				weekGroup: this.model.get('weekGroup')
			});

			View = new window.app.RepView({
				model: extModel,
				parent: this
			});

			View.parent = this;

			this.lineViews.push(View);

			if (model.get('release_id')) {
				index = _.indexOf(currentRepertoire.models, model);

				if (index !== 0) {
					wrapper.find('.row .left_side.linked').closest('.row').eq(index - 1).after(View.render().el);
				} else {
					wrapper.prepend(View.render().el);
				}
			} else {
				wrapper.append(View.render().el);
			}
		},
		add_release: function add_release(e) {
			var target   = $(e.currentTarget),
				block    = target.closest('.week') || target,
				week     = block.data('week'),
				year     = block.data('year'),
				prevDate = moment(0, 'HH').year(year).day(4).week(week).add(-1, 'week'),
				addPopup,
				popup;

			_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Шапка::Добавление релиза']);

			this.model.set('repertoire', this.repertoire);
			this.addRel = new window.app.CalendarViewPopup({
				model: this.model,
				parent: this
			});

			this.$wrapper.prepend('<div class="add_my_popup" />');

			addPopup = this.addRel;
			popup    = this.$wrapper.children('.add_my_popup');

			popup.append(addPopup.render().$el);

			popup.arcticmodal({
				afterOpen: function(data, el) {
					var currentWeekBlock = el.find('.short_list .releases').eq(0),
						prevWeekBlock    = el.find('.short_list .releases').eq(1),
						$input           = el.find('.list .input'),
						$shortSections   = el.find('.short_list .sections'),
						$searchResults   = el.find('.list .results'),
						addKeyFunction;

					$input.focus();

					$.get('/api/releases/filter/?full=1&last_week=1&week=' + week + '&year=' + year + '&cinema_id=' + reqres.request('get:user').getCinema().id, function(res) {
						var r = res.list,
							week_length = r.length,
							i,
							relDate,
							relDatePrevDay,
							date;

						for (i in r) {
							relDate        = Date.create(r[i].date);
							relDatePrevDay = Date.create(r[i].date).addDays(-1).format('{yyyy}');

							if (relDate.format('{yyyy}') === relDatePrevDay) {
								date = relDate.format('{dd} {month} {yyyy}');
								if (date === 'Invalid Date') {
									date = r[i].date;
								}
							} else {
								date = r[i].date;
							}

							currentWeekBlock.append('<div class="item js_my_release" data-id="' + r[i].id + '"><span class="date">' + date + '</span><span class="title">' + r[i].title.ru + '</span><span class="formats">' + r[i].formats.join(', ') + '</span></div>');
						}

						if (week_length > 0) {
							popup.find('.search').addClass('match');
							currentWeekBlock.addClass('visible');
						}
					});

					$.get('/api/releases/filter/?full=1&last_week=1&week=' + prevDate.week() + '&year=' + prevDate.year() + '&cinema_id=' + reqres.request('get:user').getCinema().id, function(res) {
						var r = res.list,
							i,
							relDate,
							relDatePrevDay,
							date;

						for (i in r) {
							relDate = Date.create(r[i].date);
							relDatePrevDay = Date.create(r[i].date).addDays(-1).format('{yyyy}');

							if (relDate.format('{yyyy}') === relDatePrevDay) {
								date = relDate.format('{dd} {month} {yyyy}');
								if (date === 'Invalid Date') {
									date = r[i].date;
								}
							} else {
								date = r[i].date;
							}

							prevWeekBlock.append('<div class="item js_my_release" data-id="' + r[i].id + '"><span class="date">' + date + '</span><span class="title">' + r[i].title.ru + '</span><span class="formats">' + r[i].formats.join(', ') + '</span></div>');
						}
					});

					popup.find('.my_add_popup').attr('data-week', week);
					popup.find('.my_add_popup').attr('data-year', year);

					addKeyFunction = function() {
						$(document).unbind('keydown');
						$(document).keydown(function(e) {
							var key = e.keyCode,
								inputVal = $input.val().replace(/(^\s+|\s+$)/g, ''),
								$visibleReleases,
								$selected,
								$newSelected,
								newSelectedToTop,
								currentScroll,
								selectedPositionFull,
								currentViewPlace;

							if (key === 13) {
								_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Клавиатура::Enter']);
								if ($shortSections.closest('.short_list').is(':visible') && $shortSections.find('.visible .selected').length) {
									$shortSections.find('.visible .selected').trigger('click');
								} else if ($searchResults.is(':visible') && $searchResults.find('.selected').length) {
									$searchResults.find('.selected').trigger('click');
								}
							}

							if (inputVal.length <= 2 || $searchResults.find('.js_my_release').length === 0) {
								if (key === 40) {
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Клавиатура::Курсор']);
									$input.blur();

									$visibleReleases = $shortSections.find('.visible');
									$selected = $visibleReleases.find('.selected');

									if (!$selected.length) {
										$visibleReleases.find('.js_my_release').first().addClass('selected');
									} else {
										if ($selected.next().length) {
											$newSelected = $selected.next();
											newSelectedToTop = Math.round($newSelected.position().top);
											currentScroll = $visibleReleases.scrollTop();
											selectedPositionFull = newSelectedToTop + $newSelected.outerHeight();
											currentViewPlace = $visibleReleases.outerHeight() + currentScroll;

											$selected.removeClass('selected');
											$selected.next().addClass('selected');

											if (selectedPositionFull > currentViewPlace) {
												$visibleReleases.scrollTo(currentScroll + $newSelected.outerHeight());
											}
										}
									} // вниз
								} else if (key === 38) {
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Клавиатура::Курсор']);

									$visibleReleases = $shortSections.find('.visible');
									$selected = $visibleReleases.find('.selected');

									if ($selected.length) {
										if ($selected.prev().length) {
											$input.blur();

											$newSelected = $selected.prev();
											newSelectedToTop = Math.round($newSelected.position().top);
											currentScroll = $visibleReleases.scrollTop();

											$selected.removeClass('selected').prev().addClass('selected');

											if (currentScroll > newSelectedToTop) {
												$visibleReleases.scrollTo($newSelected);
											}
										} else {
											$selected.removeClass('selected');
											$input.focus();
										}
									} // вверх
								} else if (key === 39) { // вправо
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Клавиатура::Курсор']);

									$visibleReleases = $shortSections.find('.visible');
									$selected = $visibleReleases.find('.selected');

									if ($selected.length && $visibleReleases.index() === 0) {
										$visibleReleases.removeClass('visible').siblings().addClass('visible');
										$visibleReleases.closest('.short_list').find('.tabs .item').removeClass('active').last().addClass('active');

										if (!$visibleReleases.siblings().children('.selected').length) {
											$visibleReleases.siblings().children().first().addClass('selected');
										}
									}
								} else if (key === 37) { // влево
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Клавиатура::Курсор']);

									$visibleReleases = $shortSections.find('.visible');
									$selected = $visibleReleases.find('.selected');

									if ($selected.length && $visibleReleases.index() === 1) {
										$visibleReleases.removeClass('visible').siblings().addClass('visible');
										$visibleReleases.closest('.short_list').find('.tabs .item').removeClass('active').first().addClass('active');

										if (!$visibleReleases.siblings().children('.selected').length) {
											$visibleReleases.siblings().children().first().addClass('selected');
										}
									}
								}
							} else if ($searchResults.find('.js_my_release').length > 0) {
								if (key === 40) {
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Клавиатура::Курсор']);
									$input.blur();

									$visibleReleases = $searchResults;
									$selected = $visibleReleases.find('.selected');

									if (!$selected.length) {
										$visibleReleases.find('.js_my_release').first().addClass('selected');
									} else {
										if ($selected.next().length) {
											$newSelected = $selected.next();
											newSelectedToTop = Math.round($newSelected.position().top);
											currentScroll = $visibleReleases.scrollTop();
											selectedPositionFull = newSelectedToTop + $newSelected.outerHeight();
											currentViewPlace = $visibleReleases.outerHeight() + currentScroll;

											$selected.removeClass('selected');
											$newSelected.addClass('selected');

											if (selectedPositionFull > currentViewPlace) {
												if (newSelectedToTop === $visibleReleases.outerHeight()) {
													$visibleReleases.scrollTo(currentScroll + $newSelected.outerHeight());
												} else {
													$visibleReleases.scrollTo(currentScroll - (currentViewPlace - selectedPositionFull));
												}
											} else if (selectedPositionFull < currentViewPlace && currentScroll !== 0) {
												$visibleReleases.scrollTo(currentScroll + $newSelected.outerHeight());
											}
										}
									} // вниз
								} else if (key === 38) {
									e.preventDefault();
									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Клавиатура::Курсор']);

									$visibleReleases = $searchResults;
									$selected = $visibleReleases.find('.selected');

									if ($selected.length) {
										if ($selected.prev().length) {
											$input.blur();

											$newSelected = $selected.prev();
											newSelectedToTop = Math.round($newSelected.position().top);
											currentScroll = $visibleReleases.scrollTop();

											$selected.removeClass('selected');
											$newSelected.addClass('selected');

											if (currentScroll > newSelectedToTop) {
												$visibleReleases.scrollTo($newSelected);
											}
										} else {
											$selected.removeClass('selected');
											$input.focus();
										}
									} // вверх
								}
							}
						});
					};

					addKeyFunction();

					el.closest('.arcticmodal-container').on('mouseenter', addKeyFunction);
				},
				afterClose: function() {
					popup.remove();
					$(document).unbind('keydown');
				}
			});
		},
		removeView: function removeView() {
			_.each(this.lineViews, function(releaseView) {
				releaseView.remove();
			});

			this.$el.empty();
			this.$wrapper.children('.repertoire_wrap__body').remove();

			this.remove();
		}
	});

	window.app.RepView = Backbone.View.extend({
		tagName: 'div',
		className: 'row',
		template: '#repertoire_one',
		events: {
			'mousedown .ui-resizable-se': function(e) {
				var $moveBlock = $(e.currentTarget).closest('.move_block'),
					$colors = $moveBlock.closest('.right_side').find('.color'),
					firstColorLeft = $colors.first().offset().left - 199;

				$moveBlock.css({
					left: firstColorLeft + 'px',
					width: $colors.length * ($colors.first().closest('.week').outerWidth() / $colors.first().closest('.week').data('days')) - 1
				});
			},
			'mouseup .ui-resizable-se': function(e) {
				var $moveBlock = $(e.currentTarget).closest('.move_block'),
					$colors = $moveBlock.closest('.right_side').find('.color'),
					lastColorLeft = $colors.last().offset().left - 199;

				if (!$moveBlock.closest('.row').hasClass('cell_resize')) {
					$moveBlock.css({
						left: lastColorLeft + 'px',
						width: ($colors.last().closest('.week').outerWidth() / $colors.last().closest('.week').data('days')) - 1
					});
				}
			},
			'click .linked': function(e) {
				e.preventDefault();
				var id = parseInt($(e.currentTarget).data('release'));

				if (e.ctrlKey || e.metaKey) {
					window.open(window.location.origin + '/release/' + id, '_blank');
				} else {
					Backbone.trigger('loader', 'show');
					Backbone.trigger('planning:mechanicBlocker', 'hide');
					Backbone.trigger('router:navigate', '/release/' + id);
					window.app.App.sidebar.$el.find('.list[page="releases"]').addClass('active').siblings('.active').removeClass('active');
				}
				Backbone.trigger('remove:tipsy');
			},
			'click .move_block': function(e) {
				$(e.delegateTarget).find('.color').first().trigger('click');
			},
			'click .color': function(e) {
				e.preventDefault();

				if (!this.resizeAction) {
					this.showSettings();
				}
			},
			'dblclick .prolong_release': function(e) {
				var self         = this,
					id           = this.model.get('release_id'),
					week         = +_.last(this.model.get('weeks')).number + 1,
					year         = moment(this.model.get('date')).year(),
					lastWeekDay  = $(e.currentTarget).prev().children('.color').first().index() + 1,
					newTechnology;

				_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Табл::Продление недели']);

				_.last(this.model.get('weeks')).days = _.range(lastWeekDay, 8);

				newTechnology = $.extend(true, [], _.first(this.model.get('weeks')).technology);

				_.each(newTechnology, function(tech) {
					tech.count = 0;
					tech.hall_id = {};
				});

				this.model.get('weeks').push({
					year:       year,
					number:     week,
					count:      0,
					days:       _.range(1, 8),
					technology: newTechnology
				});

				$.post('/api/schedule/save', {
					tx:         reqres.request('get:user').get('tx'),
					release_id: id,
					color:      self.model.get('color'),
					weeks:      $.toJSON(self.model.get('weeks')),
					cinema_id:  reqres.request('get:user').getCinema().id
				}, function(r) {
					if (r.ok) {
						self.model.set({weeks: r.release.weeks});

						reqres.request('get:repertoire:view').networkCollection.fetch();

						// TODO: изменилось. нужно переписать
						// if (window.app.App.weekScheduleView) {
						// 	window.app.App.weekScheduleView.updateWeekSchedule();
						// }
					}
				}, 'json');
			},
			'click .add_new_release': function(e) {
				_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Табл::Добавление релиза']);

				reqres.request('get:repertoire:view').currentView.add_release(e);
			}
		},
		initialize: function initialize(options) {
			this.release          = false;
			this.sufficientAccess = false;
			this.options          = options || {};
			this.parent           = options.parent;
			this.$wrapper         = this.parent.$wrapper;

			this.listenTo(this.model, 'change', function() {
				this.model.set({
					date: reqres.request('get:repertoire:view').model.get('date')
				});
				this.render();
			});

			if (this.model.get('weeks').length) {
				this.release = true;
			}

			if (!reqres.request('get:user').isRepertoireReadOnly(this.model.get('cinema').id)) {
				this.sufficientAccess = true;
			}
		},
		render: function render() { // рендеринг шаблона
			this.listenTo(this.model, 'remove', this.removeView);

			var template = Backbone.TemplateCache.get(this.template),
				self     = this,
				extModel = $.extend({}, this.model.attributes),
				release = this.model.get('release'),
				key = this.model.get('key');

			if (!_.isUndefined(release.date)) {
				if (moment(release.date.russia.preview).year() !== 1970 && (moment(release.date.russia.preview).month() === moment(this.model.get('date')))) {
					extModel.trueDateStart = release.date.russia.preview;
				} else {
					extModel.trueDateStart = release.date.russia.start;
				}
			}

			if (key) {
				extModel.valid_since = Date.create(key.valid_since) || '';
				extModel.valid_till  = Date.create(key.valid_till) || '';
			}

			if (this.release) {
				extModel.dist = _.map(release.distributors, function(distributor) {
					return distributor.name.full || distributor.name.short;
				});
			}

			if (!this.release && this.model.get('cinema')) {
				if (!reqres.request('get:user').isRepertoireReadOnly(this.model.get('cinema').id)) {
					extModel.addRel = 'add_new_release';
				}
			}

			extModel.COLOR = {
				0: 'gray',
				1: 'green',
				2: 'orange',
				3: 'blue',
				4: 'pink'
			};
			extModel.curYear = this.model.get('date').start && !_.isUndefined(this.model.get('date').start) ? moment(this.model.get('date').start).year() : this.model.get('date').getFullYear();

			this.$el.html(template(extModel)).show();

			setTimeout(function() {
				if (self.sufficientAccess) {
					self.setMoveBlock(self.$el);

					$(window).resize(function(e) {
						if (!e.timeStamp) {
							self.resetMoveBlock(self.$el);
						}
					});
				}

				if ($('html').hasClass('safari') && ($('html').hasClass('safari7') || $('html').hasClass('safari6'))) { // safari 6.0 - 7.0.6 math bug fix
					self.setSafariKey(self.$el);
				}

				self.$el.find('.week').each(function() {
					if ($(this).attr('original-title')) {
						$(this).tipsy({html: true});
					} else if ($(this).find('.color').length && $(this).find('.color').first().attr('original-title')) {
						$(this).find('.color').first().tipsy({html: true, gravity: $.fn.tipsy.autoNS});
					}
				});

				if (self.release) {
					self.$el.addClass('release');
					self.setAdd(self.$el);

					self.$el.find('.left_side.linked').tipsy({html: true, gravity: 'w'});
				}

				Backbone.trigger('remove:tipsy');
			}, 0);

			return this;
		},
		resetMoveBlock: function resetMoveBlock(line) {
			var colors = line.find('.color'),
				$firstDate = colors.first(),
				$lastDate = colors.last(),
				leftPosition,
				$firstColorWeek,
				width,
				$rightSideMove,
				gridX,
				startDate,
				lastDate;

			if (colors.length && $firstDate.is(':visible') && $lastDate.is(':visible')) {
				leftPosition    = $lastDate.offset().left - 199;
				$firstColorWeek = $firstDate.closest('.week');
				width           = ($firstColorWeek.outerWidth() / $firstColorWeek.data('days')) - 1;
				$rightSideMove  = line.find('.move_block');
				gridX           = $firstColorWeek.outerWidth() / $firstColorWeek.data('days');
				startDate       = moment(this.model.get('date')).date($firstDate.data('day')).format('X');
				lastDate        = moment(this.model.get('date')).week($lastDate.closest('.week').data('week')).weekday($lastDate.index()).format('X');

				if ($rightSideMove.length) {
					$rightSideMove
						.css({
							left: leftPosition,
							width: width + 'px'
						})
						.attr({
							'days-count': colors.length,
							'start-date': startDate,
							'last-date': lastDate
						});

					if ($rightSideMove.hasClass('ui-resizable')) {
						$rightSideMove.resizable('option', {
							grid: [gridX, 0]
						});
					}
				}
			}
		},
		setMoveBlock: function setMoveBlock(line) {
			var colors         = line.find('.color'),
				$firstDate     = colors.first(),
				$lastDate      = colors.last(),
				year           = parseInt($firstDate.closest('.week').data('year')),
				modelWeeks     = this.model.get('weeks'),
				self           = this,
				repertoireView = reqres.request('get:repertoire:view'),
				leftPosition,
				$firstColorWeek,
				width,
				$rightSideMove,
				gridX,
				startDate,
				lastDate,
				originalRight,
				resizeRight;

			if (colors.length && !line.find('.cell').last().hasClass('color')) {
				leftPosition    = $lastDate.offset().left - 199;
				$firstColorWeek = $firstDate.closest('.week');
				width           = ($firstColorWeek.outerWidth() / $firstColorWeek.data('days')) - 1;
				$rightSideMove  = line.find('.move_block');
				gridX           = $firstColorWeek.outerWidth() / $firstColorWeek.data('days');
				startDate       = moment(this.model.get('date')).date($firstDate.data('day')).format('X');
				lastDate        = moment(this.model.get('date')).week($lastDate.closest('.week').data('week')).weekday($lastDate.index()).format('X');

				if ($rightSideMove.length) {
					$rightSideMove
						.css({
							left: leftPosition,
							width: width + 'px'
						})
						.attr({
							'days-count': colors.length,
							'start-date': startDate,
							'last-date': lastDate
						});

					if ($rightSideMove.hasClass('ui-resizable')) {
						$rightSideMove.resizable('destroy');
					}

					$rightSideMove.resizable({
						grid: [gridX, 0],
						containment: 'parent',
						minHeight: $firstDate.outerHeight(),
						maxHeight: $firstDate.outerHeight(),
						start: function() {
							line.addClass('cell_resize');

							self.resizeAction = true;

							originalRight = parseInt($rightSideMove.css('left')) + parseInt($rightSideMove.outerWidth());
						},
						stop: function(event, ui) {
							resizeRight = parseInt($rightSideMove.css('left')) + parseInt($rightSideMove.outerWidth());

							var currentCount = $rightSideMove.attr('days-count'),
								moverCell = Math.round(Math.abs(resizeRight - originalRight) / gridX),
								w,
								endWeekDays,
								fullWeekDays = _.range(1, 8),
								newWeeksJson,
								newWeeks,
								lastWeekDay,
								oldLastWeek,
								lastWeek,
								firstWeek,
								newStart,
								newEnd,
								deletedCount,
								startNumber,
								firstWeekStartDay,
								newTechnology,
								firstNumber;

							if (ui.originalSize.width < ui.size.width) {
								lastDate = moment($rightSideMove.attr('last-date'), 'X').add(moverCell, 'days').format('X');

								$rightSideMove.attr('days-count', currentCount + moverCell);
							} else if (ui.originalSize.width > ui.size.width) {
								lastDate = moment($rightSideMove.attr('last-date'), 'X').add(-moverCell, 'days').format('X');

								$rightSideMove.attr('days-count', currentCount - moverCell);
							} else {
								$rightSideMove.attr('days-count', colors.length);
							}

							$rightSideMove.attr('last-date', lastDate);

							newWeeksJson = JSON.stringify(modelWeeks);
							newWeeks     = JSON.parse(newWeeksJson);
							lastWeekDay  = moment(lastDate, 'X').weekday() + 1;
							oldLastWeek  = parseInt(_.last(newWeeks).number);
							lastWeek     = moment(lastDate, 'X').week();
							firstWeek    = parseInt(_.first(newWeeks).number);
							newStart     = moment(startDate, 'X');
							newEnd       = moment(lastDate, 'X');

							if (lastWeek > oldLastWeek) { // появилась новая неделя
								firstWeekStartDay = newStart.weekday() + 1;

								if (!_.first(newWeeks).days || (_.first(newWeeks).days && !_.first(newWeeks).days.length)) {
									firstNumber = parseInt(_.first(newWeeks).number);

									firstWeekStartDay = 1;

									if (firstNumber === moment(self.model.get('release').date.russia.start).weeks()) {
										firstWeekStartDay = moment(self.model.get('release').date.russia.start).weekday() + 1; // т.к. отсчет с 0, а нам надо с 1
									}

									_.first(newWeeks).days = _.range(firstWeekStartDay, 8);
								}

								endWeekDays = _.range(1, newEnd.weekday() + 2);

								if (_.last(_.first(newWeeks).days) !== 7) {
									_.first(newWeeks).days = _.range(_.first(_.first(newWeeks).days), 8);
								}

								newTechnology = $.extend(true, [], _.first(self.model.get('weeks')).technology);

								_.each(newTechnology, function(tech) {
									tech.count = 0;
									tech.hall_id = {};
								});

								if (lastWeek - oldLastWeek === 1) { // добавлена 1 неделя
									if (newWeeks.length > 1) {
										_.each(_.rest(newWeeks), function(notFirstWeek) {
											notFirstWeek.days = fullWeekDays;
										});
									}

									newWeeks.push({
										year:       year,
										number:     lastWeek,
										count:      0,
										days:       endWeekDays,
										technology: newTechnology
									});
								} else if (lastWeek - oldLastWeek > 1) { // добавлено больше 1 недели
									if (newWeeks.length > 1) {
										_.last(newWeeks).days = fullWeekDays;
									}

									for (w = oldLastWeek + 1; w < lastWeek; w++) {
										newWeeks.push({
											year:       year,
											number:     w,
											count:      0,
											days:       fullWeekDays,
											technology: newTechnology
										});
									}

									newWeeks.push({
										year:       year,
										number:     lastWeek,
										count:      0,
										days:       endWeekDays,
										technology: newTechnology
									});
								}
							} else {
								if (lastWeek < oldLastWeek) {
									deletedCount = oldLastWeek - lastWeek;

									for (w = deletedCount; w > 0; w--) {
										newWeeks.splice(-1, 1);
									}
								}

								endWeekDays = _.range(1, newEnd.weekday() + 2);

								if (newWeeks.length > 1) {
									_.last(newWeeks).days = endWeekDays;
								} else {
									if (_.first(newWeeks).days && _.first(newWeeks).days.length) {
										startNumber = _.first(newWeeks).days[0];
									} else if (parseInt(_.first(newWeeks).number) === moment(self.model.get('release').date.russia.start).weeks()) {
										startNumber = moment(self.model.get('release').date.russia.start).day() + 4;
									} else {
										startNumber = newStart.weekday() + 1;
									}

									if (startNumber > 7) {
										startNumber = startNumber - 7;
									}

									_.first(newWeeks).number = firstWeek;
									_.first(newWeeks).days = _.range(startNumber, lastWeekDay + 1);
								}
							}

							$.post('/api/schedule/save', {
								tx:         reqres.request('get:user').get('tx'),
								release_id: self.model.get('release_id'),
								color:      self.model.get('color'),
								weeks:      $.toJSON(newWeeks),
								cinema_id:  reqres.request('get:user').getCinema().id
							}, function(r) {
								if (r.ok) {
									self.model.set({
										weeks: newWeeks
									});

									_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Табл::Продление недели растягиванием']);

									repertoireView.networkCollection.fetch();

									repertoireView.repertoire.updateHeaderCount();
									line.removeClass('cell_resize');
								}

								self.resizeAction = false;

								$rightSideMove.css({
									left: leftPosition,
									width: width + 'px'
								});
							}, 'json');
						}
					});
				}
			}
		},
		setSafariKey: function setSafariKey(line) {
			line.find('.week').each(function() {
				var $oneWeek  = $(this),
					$allCells = $oneWeek.find('.cell'),
					$lastCell = $allCells.last(),
					lostWidth = $oneWeek.width() - ($lastCell.width() * $allCells.length);

				if (lostWidth) {
					$lastCell.css({
						width: $lastCell.width() + lostWidth
					});
				}
			});
		},
		setAdd: function setAdd(line) {
			var userRights = !reqres.request('get:user').isRepertoireReadOnly(this.model.get('cinema').id);

			line.find('.week').each(function() {
				var $week = $(this);

				if (!$week.find('.color').length && !$week.prevAll().hasClass('prolong_release') &&
					!$week.hasClass('add_new_release') && userRights && $week.prevAll().find('.color').length) {
					$week.addClass('prolong_release').attr('original-title', 'Двойной клик, чтобы добавить неделю проката');

					$week.tipsy();
				}
			});
		},
		removeView: function removeView() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		},
		showSettings: function showSettings() {
			var view = new window.app.ScheduleView({
					model: new window.app.Repertoire($.extend(true, {}, this.model.toJSON())),
					parentModel: this.model
				});

			this.$wrapper.prepend(view.render().$el);

			view.$el.arcticmodal({
				beforeOpen: function(data, el) {
					el.closest('.arcticmodal-container_i2').addClass('center');
				},
				afterClose: function() {
					view.remove();
				}
			});
		}
	});

	window.app.CalendarViewPopup = Backbone.View.extend({
		tagName: 'div',
		className: 'calendar_popup',
		template: '#repertoire_calendar_popup',
		initialize: function initialize(options) {
			this.options = options || {};
			this.searchTemplate = Backbone.TemplateCache.get('#repertoire_search');

			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.removeView);
		},
		events: {
			'click .input': 'search',
			'focus .input': 'search',
			'click .js_my_release': function(e) {
				var release        = $(e.currentTarget),
					popup          = release.closest('.my_add_popup'),
					id             = release.data('id'),
					week           = popup.data('week'),
					year           = popup.data('year'),
					month          = +moment().week(week).startOf('week').month().format('M') + 1,
					goPost         = 1,
					repertoireView = reqres.request('get:repertoire:view');

				Backbone.trigger('loader', 'show');
				Backbone.trigger('planning:screenBlocker', 'show');

				if (this.options.parent.collection.where({release_id: id}).length) {
					goPost = 0;
				}

				if (!goPost) {
					$.arcticmodal('close');
					Backbone.trigger('loader', 'hide');
					Backbone.trigger('planning:screenBlocker', 'hide');
				} else {
					$.post('/api/schedule/add', {
						tx:         reqres.request('get:user').get('tx'),
						cinema_id:  reqres.request('get:user').getCinema().id,
						release_id: id,
						week:       week,
						year:       year,
						add:        1,
						month:      month
					}, function(r) {
						$.arcticmodal('close');

						if (r.ok) {
							r.release.at = 0;

							var rep    = repertoireView.repertoire,
								models = rep.models,
								releases = _.filter(models, function(id) {
									return id.attributes.release_id !== null;
								}),
								releasesOfWeek = _.filter(releases, function(release) {
									var thisWeek = _.filter(release.attributes.weeks, function(oneWeek) {
										return parseInt(oneWeek.number) === week;
									});

									if (thisWeek.length) return thisWeek;
								}),
								index = _.last(releasesOfWeek) ? _.indexOf(models, _.last(releasesOfWeek)) + 1 : releases.length,
								clon,
								clonArr = [];

							rep.add(r.release, {
								at: index
							});

							clon = rep.findWhere({release_id: r.release.release_id }).clone();

							_.each(clon.get('weeks'), function(oneWeek) {
								_.each(oneWeek.technology, function(technology) {
									if (parseInt(oneWeek.number) === week) {
										var tmpJson  = JSON.stringify(clon),
											tmp      = JSON.parse(tmpJson),
											weekJson = JSON.stringify(oneWeek),
											weekTmp  = JSON.parse(weekJson);

										tmp.week = weekTmp;
										tmp.week.technology = technology;
										tmp.week.technology.hall_id = {};
										clonArr.push(tmp);
									}
								});
							});

							repertoireView.networkCollection.add(clonArr);
							Backbone.trigger('planning:add', {favourite_id: id});
							repertoireView.networkCollection.fetch();
						}

						Backbone.trigger('loader', 'hide');
						Backbone.trigger('planning:screenBlocker', 'hide');
					}, 'json');
				}
			},
			'click .short_list .tabs .item': function(e) {
				var item  = $(e.currentTarget),
					index = item.index();

				if (!item.hasClass('active')) {
					item.addClass('active').siblings().removeClass('active');
				}

				this.$el.find('.short_list .releases').eq(index).addClass('visible').siblings().removeClass('visible');
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		},
		search: function search(e) {
			var self       = this,
				input      = $(e.currentTarget),
				popup      = input.closest('.my_add_popup'),
				$shortList = popup.find('.short_list'),
				getRandomInt = function(min, max) {
					return Math.floor(Math.random() * (max - min + 1)) + min;
				};

			input.unbind('textchange');

			input.one('textchange', function() {
				_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Попап::Поиск релиза']);
			});

			input.bind('textchange', function() {
				var parent        = input.closest('.list'),
					results_block = parent.find('.results'),
					value         = input.val().toLowerCase().replace(/(^\s+|\s+$)/g, '');

				parent.find('.remove_text').click(function() {
					input.val('').focus();
					$(this).hide();
					results_block.empty().hide();

					$shortList.show();
				});

				if (value.length > 0) {
					parent.find('.remove_text').show();
				} else {
					parent.find('.remove_text').hide();
				}

				if (value.length > 2) {
					$.get('/api/releases/filter/?q=' + value, function(res) {
						results_block
							.empty()
							.append(self.searchTemplate({
								results: res.list,
								getRandomInt: getRandomInt
							}));

						if (res.list.length) {
							$shortList.hide();
							results_block.show();
						} else {
							$shortList.show();
							results_block.hide();
						}
					}, 'json');
				} else {
					$shortList.show();
					results_block.empty().hide();
				}
			});
		},
		removeView: function removeView() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

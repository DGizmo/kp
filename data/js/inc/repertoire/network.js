$(function() {
	'use strict';

	window.app.NetworkHeaderView = Backbone.View.extend({
		template: '#network_header_tmpl',
		className: 'network_head',
		events: {
			click: function(e) {
				var $target = $(e.target);

				if (!$target.closest('.halls_list').length) {
					$target.closest('.network_wrap').find('.halls_list.visible').removeClass('visible');
				}
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.$wrapper = this.options.parent.$el;
			this.calendar = this.options.calendar;

			document.title = 'Планирование по сети - Киноплан';
			_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По сети']);

			this.innerViews = [];

			var $wrapper = this.$wrapper.find('#network .network_wrap'),
				$networkBody;

			$wrapper.append('<div class="network_body" screen-page="1"/>');

			$networkBody = $wrapper.find('.network_body');

			this.model.unset('days', {silent: true});
			this.model.unset('halls', {silent: true});
			this.model.unset('date', {silent: true});

			this.cinemas = this.options.cinemas || reqres.request('get:user').getCinemas();

			this.setModelData($networkBody);

			this.collection = window.app.App.Collections.network;
			this.listenTo(this.collection, 'add', this.addOneRelease);
			this.listenTo(this.collection, 'sync', function() {
				Backbone.trigger('loader', 'hide');
				Backbone.trigger('planning:screenBlocker', 'hide');
				this.checkWeekHeight();
			});

			this.listenTo(this, 'remove', this.removeView);
		},
		render: function render() {
			var template  = Backbone.TemplateCache.get(this.template),
				$wrap     = this.$wrapper.find('#network .network_wrap'),
				extModel  = $.extend({}, this.model.attributes),
				cinemaArr = [],
				cinemas   = this.model.get('cinemas').groupBy(function(n) {
					return n.halls.length;
				});

			_.each(_.values(cinemas), function(cinema) {
				var cinemasByTitle = cinema.sortBy(function(n) {
					return n.title.ru;
				});

				_.each(cinemasByTitle, function(oneCinema) {
					cinemaArr.push(oneCinema);
				});
			});

			extModel.cinemaArr = cinemaArr;

			$wrap.prepend(this.$el.html(template(extModel)));

			if (!this.collection.models.length && reqres.request('get:user').isMultiplex()) {
				this.collection.fetch();
			} else {
				_.each(this.collection.models, function(model) {
					this.addOneRelease(model);
				}, this);

				Backbone.trigger('loader', 'hide');
				Backbone.trigger('planning:screenBlocker', 'hide');
				this.checkWeekHeight();
			}

			return this;
		},
		setModelData: function setModelData(networkBody) {
			var self       = this,
				modelMonth = this.model.get('currentMonth'),
				modelYear  = this.model.get('currentYear'),
				range      = Date.range(Date.create().set({month: modelMonth, year: modelYear, day: 1}).beginningOfMonth(), Date.create().set({month: modelMonth, year: modelYear, day: 1}).endOfMonth()),
				line       = [],
				cinemas    = this.cinemas,
				$activeTab = this.$wrapper.find('.filter .repertoire_sections .active');

			range.every('weekday', function(weekday) {
				if (Date.create(weekday).format('{weekday}') === 'четверг') {
					var date = Date.create(weekday).format('{d}');
					line.push({
						date:               date,
						leftPosition:       142,
						leftObjectPosition: 0
					});
				}
			});

			this.model.set({
				line: line,
				cinemas: cinemas
			});

			if ($activeTab.hasClass('section_network')) {
				if (cinemas.length > 7) {
					this.$wrapper.find('.filter .screen').show();
				} else {
					this.$wrapper.find('.filter .screen').hide();
				}
			}

			this.$el.find('.list').css({
				left: 92
			});

			networkBody.empty().attr('screen-page', 1);

			_.each(line, function(weekDay) {
				var weekModel = new window.app.HallsPlanning();

				weekModel.unset('days', {silent: true});
				weekModel.unset('halls', {silent: true});

				weekModel.set({
					currentMonth: this.model.get('currentMonth'),
					currentYear:  this.model.get('currentYear'),
					date:         parseInt(weekDay.date),
					cinemas:      cinemas,
					weekLength:   line.length
				});

				this.addOneLine(weekModel);
			}, this);

			setTimeout(function() {
				self.checkHallsScroll();
				self.checkNetworkVisibleCinema(networkBody);
			}, 0);
		},
		addOneLine: function addOneLine(weekModel) {
			var Line = new window.app.NetworkView({
				model: weekModel,
				parent: this
			});

			this.$wrapper.find('#network .network_body').append(Line.render().el);

			this.innerViews.push(Line);
		},
		addOneRelease: function addOneRelease(model) {
			var $targetLine    = this.$wrapper.find('.network_body .week[data-week="' + model.get('week').number + '"] '),
				$targetSection = $targetLine.find('.section[data-cinema-id="' + model.get('cinema_id') + '"] '),
				section        = new window.app.NetworkSectionView({
					model: model,
					parent: this
				});

			$targetSection.find('.bottom').before(section.render().el);
		},
		checkHallsScroll: function checkHallsScroll() {
			var scrollWidth = _.getScrollBarWidth();

			if (scrollWidth) {
				this.$el.closest('.network_wrap').css({
					width: 'calc(100% + ' + scrollWidth + 'px)'
				});
			}
		},
		checkNetworkVisibleCinema: function checkNetworkVisibleCinema(networkBody) {
			var currentCinemaId = reqres.request('get:user').getCinema().id,
				$arrows         = this.$wrapper.find('.filter .screen'),
				$arrowLeft      = $arrows.find('.arrow_left'),
				$arrowRight     = $arrows.find('.arrow_right'),
				$network        = networkBody ? networkBody.closest('#network') : this.$wrapper.find('#network'),
				$networkBody    = networkBody ? networkBody : $network.find('.network_body'),
				$currentCinema  = $networkBody.find('.section').filter('[data-cinema-id="' + currentCinemaId + '"]'),
				cinemaIndex     = $currentCinema.index() + 1, // от 1
				lastCinemaIndex = $networkBody.find('.section').last().index() + 1, // от 1
				currentPage     = parseInt($networkBody.attr('screen-page')),
				neededClick,
				count;

			if ($networkBody.find('.week').first().find('.section').length > 7 && this.$wrapper.find('.repertoire_sections .section_network').hasClass('active')) {
				if (cinemaIndex > currentPage + 6) { // кинотеатр вне экрана справа
					if (cinemaIndex + 3 >= lastCinemaIndex) { // справа от кинотеатра нет еще 3х кинотеатров
						neededClick = lastCinemaIndex - currentPage - 6;
					} else { // справа от кинотеатра еще 3+ кинотеатра
						neededClick = cinemaIndex - 4;
					}

					for (count = 0; count < neededClick; count++) {
						$arrowRight.trigger('click');
					}
				} else if (cinemaIndex < currentPage) { // кинотеатр вне экрана слева
					if (cinemaIndex - 3 < 1) { // слева от кинотеатра нет еще 3х кинотеатров
						if (cinemaIndex === 2 || cinemaIndex === 3) cinemaIndex = 1;
						neededClick = currentPage - cinemaIndex;
					} else { // слева от кинотеатра еще 3+ кинотеатра
						neededClick = currentPage - (cinemaIndex - 3);
					}

					for (count = 0; count < neededClick; count++) {
						$arrowLeft.trigger('click');
					}
				}
			}

			$networkBody.find('.section').removeClass('active_cinema');
			$currentCinema.addClass('active_cinema');
		},
		checkWeekHeight: function checkWeekHeight() {
			this.$el.siblings('.network_body').find('.week').each(function() {
				var maxWeekReleasesCount = 0,
					sectionVisibleReleasesCount,
					neededMinHeight;

				$(this).find('.section').each(function() {
					sectionVisibleReleasesCount = $(this).find('.network_list').children('.release').length;

					if (maxWeekReleasesCount < sectionVisibleReleasesCount) {
						maxWeekReleasesCount = sectionVisibleReleasesCount;
					}
				});

				neededMinHeight = (maxWeekReleasesCount + 1) * 20;

				if (neededMinHeight > 160) {
					$(this).css({
						'min-height': neededMinHeight + 'px'
					});
				} else {
					$(this).css({
						'min-height': '160px'
					});
				}
			});
		},
		removeView: function removeView() {
			_.each(this.innerViews, function(oneView) {
				oneView.remove();
			});

			this.$wrapper.find('#network .network_body').remove();

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.NetworkView = Backbone.View.extend({
		className: 'week',
		template: '#network_line_tmpl',
		events: {
			click: function(e) {
				var $target = $(e.target);

				if (!$target.closest('.halls_list').length) {
					$target.closest('.network_wrap').find('.halls_list.visible').removeClass('visible');
				}
			},
			'mouseenter .section': function(e) {
				var $section = $(e.currentTarget);

				if (!$section.hasClass('no_rights') && $section.find('.release:visible').length) {
					$section.find('.bottom').css('display', 'block');
				}
			},
			'mouseleave .section': function(e) {
				var $section = $(e.currentTarget);

				$section.find('.bottom').css('display', 'none');
			},
			'click .generate_timeline': function(e) {
				var $target          = $(e.currentTarget),
					cinemaId         = $target.closest('.section').data('cinema-id'),
					week             = $target.closest('.week').data('week'),
					year             = this.model.get('currentYear'),
					occupancyPercent = $target.attr('occupancy_percent'),
					freeTime         = $target.attr('free_time');

				if (window.app.App.timelineModule && !window.app.App.timelineModule.get('open')) {
					window.app.App.timelineModule = new window.app.TimelineModule(cinemaId, week, year, occupancyPercent, freeTime);
				} else if (!window.app.App.timelineModule) {
					window.app.App.timelineModule = new window.app.TimelineModule(cinemaId, week, year, occupancyPercent, freeTime);
				}

				_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По сети', 'Открытие предварительного расписания']);
			},
			'click .copy_left': function(e) {
				var $target        = $(e.currentTarget),
					cinemaId       = $target.closest('.section').data('cinema-id'),
					targetCinema   = $target.closest('.section').prev().data('cinema-id'),
					week           = $target.closest('.week').data('week'),
					year           = this.model.get('currentYear'),
					userCinemas    = reqres.request('get:user').getCinemas(),
					fromTitle      = _.findWhere(userCinemas, {id: cinemaId}).title.ru + ', ' + _.findWhere(userCinemas, {id: cinemaId}).city.title.ru,
					targetTitle    = _.findWhere(userCinemas, {id: targetCinema}).title.ru + ', ' + _.findWhere(userCinemas, {id: targetCinema}).city.title.ru,
					areReady       = confirm('Копирование репертуара.\n\nРепертуар кинотеатра "' + fromTitle + '" будет скопирован в кинотеатр "' + targetTitle + '".\n\nВы уверены?'),
					repertoireView = reqres.request('get:repertoire:view');

				if (!areReady) return;

				Backbone.trigger('loader', 'show');

				$.post('/api/schedule/network/copy', {
					tx:             reqres.request('get:user').get('tx'),
					year:           year,
					week:           week,
					cinema_id:      targetCinema,
					cinema_id_from: cinemaId
				}, function(r) {
					if (r.ok) {
						repertoireView.networkCollection.fetch({
							success: function() {
								if (targetCinema === reqres.request('get:user').getCinema().id) {
									repertoireView.repertoire.update();
								}
							}
						});
					}
					Backbone.trigger('loader', 'hide');
				}, 'json');
			},
			'click .copy_right': function(e) {
				var $target        = $(e.currentTarget),
					cinemaId       = $target.closest('.section').data('cinema-id'),
					targetCinema   = $target.closest('.section').next().data('cinema-id'),
					week           = $target.closest('.week').data('week'),
					year           = this.model.get('currentYear'),
					userCinemas    = reqres.request('get:user').getCinemas(),
					fromTitle      = _.findWhere(userCinemas, {id: cinemaId}).title.ru + ', ' + _.findWhere(userCinemas, {id: cinemaId}).city.title.ru,
					targetTitle    = _.findWhere(userCinemas, {id: targetCinema}).title.ru + ', ' + _.findWhere(userCinemas, {id: targetCinema}).city.title.ru,
					areReady       = confirm('Копирование репертуара.\n\nРепертуар кинотеатра "' + fromTitle + '" будет скопирован в кинотеатр "' + targetTitle + '".\n\nВы уверены?'),
					repertoireView = reqres.request('get:repertoire:view');

				if (!areReady) return;

				Backbone.trigger('loader', 'show');

				$.post('/api/schedule/network/copy', {
					tx:             reqres.request('get:user').get('tx'),
					year:           year,
					week:           week,
					cinema_id:      targetCinema,
					cinema_id_from: cinemaId
				}, function(r) {
					if (r.ok) {
						repertoireView.networkCollection.fetch({
							success: function() {
								if (targetCinema === reqres.request('get:user').getCinema().id) {
									repertoireView.repertoire.update();
								}
							}
						});
					}
					Backbone.trigger('loader', 'hide');
				}, 'json');
			},
			'click .repertoire_viewer': function(e) {
				var $target          = $(e.currentTarget),
					$week            = $target.closest('.week'),
					$repList         = $week.find('.list'),
					maxWeekReleases,
					neededMinHeight;

				if ($repList.hasClass('active')) {
					$repList.find('.release.added').each(function() {
						var $repRelease        = $(this),
							$repReleaseSection = $repRelease.closest('.section'),
							$newRelease        = $repRelease.removeClass('added'),
							$list              = $repReleaseSection.find('.network_list');

						// $newRelease.draggable('enable');

						$list.append($newRelease);

						if ($list.hasClass('empty')) $list.removeClass('empty');
					});

					maxWeekReleases = 0;

					$repList.find('.section').each(function() {
						var $oneSection = $(this),
							neededReleases = $oneSection.find('.network_list').children('.release').length;

						if (maxWeekReleases < neededReleases) {
							maxWeekReleases = neededReleases;
						}
					});

					neededMinHeight = (maxWeekReleases + 1) * 20;

					if (neededMinHeight > 160) {
						$week.css({
							'min-height': neededMinHeight + 'px'
						});
					} else {
						$week.css({
							'min-height': '160px'
						});
					}
				} else {
					maxWeekReleases = 0;

					$repList.find('.section').each(function() {
						var $oneSection = $(this),
							allReleases = $oneSection.find('.release').length;

						if (maxWeekReleases < allReleases && !$oneSection.hasClass('no_rights')) {
							maxWeekReleases = allReleases;
						}
					});

					neededMinHeight = (maxWeekReleases + 1) * 20;

					if (neededMinHeight > 160) {
						$week.css({
							'min-height': neededMinHeight + 'px'
						});
					}
				}

				$target.toggleClass('active');
				$repList.toggleClass('active');
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.$wrapper = this.options.parent.$wrapper;

			var	fullDate = Date.create().set({
					month: this.model.get('currentMonth'),
					day:   this.model.get('date'),
					year:  this.model.get('currentYear')
				});

			this.model.set({
				week: fullDate.getWeek(),
				cinemas: reqres.request('get:user').getCinemas()
			});

			this.listenTo(this.model, 'remove', this.removeView);
		},
		render: function render() {
			var template  = Backbone.TemplateCache.get(this.template),
				extModel  = $.extend({}, this.model.attributes),
				cinemas   = this.model.get('cinemas').groupBy(function(n) { return n.halls.length; }),
				cinemaArr = [],
				self      = this;

			_.each(_.values(cinemas), function(cinema) {
				var cinemasByTitle = cinema.sortBy(function(n) {
					return n.title.ru;
				});

				_.each(cinemasByTitle, function(oneCinema) {
					cinemaArr.push(oneCinema);
				});
			});

			extModel.cinemaArr = cinemaArr;

			this.$el.html(template(extModel));

			this.$el.css({height: 'calc(100% / ' + this.model.get('weekLength') + ' - 1px)'});

			this.$el.attr('data-week', this.model.get('week'));

			setTimeout(function() {
				self.$el.find('.section').each(function() {
					if ($(this).hasClass('empty_section')) return;
					var $section       = $(this),
						repertoireView = reqres.request('get:repertoire:view');

					if ($section.hasClass('no_rights') && !$section.prev().hasClass('no_rights')) {
						$section.prev().addClass('no_copy_next');
					}

					if (reqres.request('get:user').isRepertoireReadOnly($section.data('cinema-id'))) return;

					$section.droppable({
						drop: function(event, ui) {
							var $targetSection = $(event.target),
								$releaseClone  = $(ui.draggable).clone(),
								fromCinema     = $(ui.draggable).closest('.section').data('cinema-id'),
								targetCinema   = $targetSection.data('cinema-id'),
								targetWeek     = $targetSection.closest('.week').data('week'),
								targetId       = $releaseClone.data('id'),
								targetFormat   = $releaseClone.data('format'),
								targetUpgrades = $releaseClone.data('upgrades'),
								$matchRelease  = $targetSection.find('.release').filter(function() {
									return $(this).data('id') === targetId && $(this).data('format') === targetFormat && $(this).data('upgrades') === targetUpgrades;
								});

							if (fromCinema !== targetCinema) {
								if ($matchRelease.length) {
									repertoireView.networkCollection.addRelease(targetWeek, fromCinema, targetCinema, targetId, targetFormat, targetUpgrades, 'replace');
								} else {
									repertoireView.networkCollection.addRelease(targetWeek, fromCinema, targetCinema, targetId, targetFormat, targetUpgrades, 'new');
								}
							}

							$(ui.draggable).closest('.section').find('.bottom').css('display', 'none');
						}
					});
				});
			}, 0);

			return this;
		},
		removeView: function removeView() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.NetworkSectionView = Backbone.View.extend({
		className: 'network_list',
		template: '#network_section_tmpl',
		events: {
			'click .release': function(e) {
				var $target = $(e.currentTarget);

				if ($target.find('.halls_list').hasClass('visible')) return;

				if ($target.closest('.zeros').length && !$target.hasClass('added')) {
					$target.addClass('added');
				} else if ($target.closest('.zeros').length && $target.hasClass('added')) {
					$target.removeClass('added');
				}

				if (!$target.closest('.zeros').length && $target.hasClass('clicked')) {
					$target.removeClass('clicked');
				} else if (!$target.closest('.zeros').length && !$target.hasClass('clicked')) {
					$target.closest('.network_body').find('.clicked').removeClass('clicked');
					$target.addClass('clicked');
				}

				$target.closest('.network_body').find('.halls_list.visible').removeClass('visible');
			},
			'dblclick .release': function(e) {
				var $target = $(e.currentTarget),
					$hallsList = $target.find('.halls_list');

				if ($target.parent().hasClass('zeros')) return;

				$target.closest('.network_body').find('.halls_list.visible').removeClass('visible');

				$hallsList.toggleClass('visible');
			},
			'click .one_hall .title': function(e) {
				var $target = $(e.currentTarget),
					$hall = $target.closest('.one_hall'),
					$check = $hall.find('.check');

				$check.trigger('click');
			},
			'click .save_halls': function(e) {
				Backbone.trigger('loader', 'show');

				var $save           = $(e.currentTarget),
					$wrap           = $save.closest('.halls_list'),
					$checked        = $wrap.find('input'),
					releaseId       = $save.closest('.release').data('id'),
					releaseFormat   = $save.closest('.release').data('format'),
					releaseUpgrades = $save.closest('.release').data('upgrades'),
					cinema_id       = this.model.get('cinema_id'),
					week            = parseInt(this.model.get('week').number),
					year            = this.model.collection.parentview.model.get('currentYear'),
					repertoireView  = reqres.request('get:repertoire:view'),
					currentTechnology,
					technologyToSave,
					currentHallCount,
					hall_id;

				_.each(this.model.get('week').releases, function(oneRelease) {
					if (oneRelease.id === releaseId) {
						_.each(oneRelease.technology, function(tech) {
							delete tech.releaseId;
							delete tech.releaseColor;
							delete tech.releaseTitle;

							if (_.isUndefined(releaseUpgrades)) {
								if (tech.format === releaseFormat) {
									currentTechnology = tech;
								}
							} else {
								var formatUpgrades = tech.upgrades.join(', ');

								if (tech.format === releaseFormat && formatUpgrades === releaseUpgrades) {
									currentTechnology = tech;
								}
							}
						});

						technologyToSave = oneRelease.technology;
					}
				});

				currentHallCount = parseInt(currentTechnology.count_hall) || 0;
				hall_id          = currentTechnology.hall_id || {};

				$checked.each(function() {
					var checkedHallId,
						uncheckedHallId,
						removedValue;

					if ($(this).prop('checked')) {
						checkedHallId = $(this).closest('.one_hall').data('id');

						if (_.isUndefined(hall_id[checkedHallId])) {
							hall_id[checkedHallId] = 0;
						}
					} else {
						uncheckedHallId = $(this).closest('.one_hall').data('id');

						if (_.isNumber(hall_id[uncheckedHallId])) {
							removedValue = parseInt(hall_id[uncheckedHallId]);

							delete hall_id[uncheckedHallId];

							currentTechnology.count_hall = currentHallCount - removedValue;
						}
					}
				});

				technologyToSave[0].hall_id = hall_id;

				$.post('/api/schedule/change_count', {
					tx:         reqres.request('get:user').get('tx'),
					release_id: releaseId,
					cinema_id:  cinema_id,
					week:       week,
					year:       year,
					technology: $.toJSON(technologyToSave)
				}, function(r) {
					if (r.ok) {
						if (cinema_id === reqres.request('get:user').getCinema().id) {
							var neededModel = repertoireView.repertoire.findWhere({release_id: releaseId});

							if (neededModel) {
								neededModel.fetch();
							}
						}

						// TODO: изменилось. нужно переписать
						// if (window.app.App.weekScheduleView) {
						// 	window.app.App.weekScheduleView.updateWeekSchedule();
						// }
					}

					$wrap.removeClass('visible');

					Backbone.trigger('loader', 'hide');
				}, 'json');
			},
			changeSeanceCount: function(e, key) {
				var $target        = $(e.currentTarget).find('.clicked'),
					release_id     = $target.data('id'),
					cinemaId       = this.model.get('cinema_id'),
					week           = $target.closest('.week').data('week'),
					format         = $target.data('format'),
					upgrades       = $target.data('upgrades'),
					self           = this,
					year           = this.model.collection.parentview.model.get('currentYear'),
					releaseModel   = this.model.get('week').releases.filter(function(release) { if (release.id === release_id) return release; }),
					technology     = releaseModel[0].technology,
					repertoireView = reqres.request('get:repertoire:view'),
					value,
					modelRelease;

				if (!reqres.request('get:user').isRepertoireReadOnly(cinemaId)) {
					value = 0;
					modelRelease = _.findWhere(self.model.get('week').releases, {id: release_id});

					if ((parseInt(key) <= 57 && parseInt(key) >= 48) || (parseInt(key) <= 105 && parseInt(key) >= 96)) {
						Backbone.trigger('loader', 'show');

						if (key === 48 || key === 96) {
							value = 0;
						} else if (key === 49 || key === 97) {
							value = 1;
						} else if (key === 50 || key === 98) {
							value = 2;
						} else if (key === 51 || key === 99) {
							value = 3;
						} else if (key === 52 || key === 100) {
							value = 4;
						} else if (key === 53 || key === 101) {
							value = 5;
						} else if (key === 54 || key === 102) {
							value = 6;
						} else if (key === 55 || key === 103) {
							value = 7;
						} else if (key === 56 || key === 104) {
							value = 8;
						} else if (key === 57 || key === 105) {
							value = 9;
						}

						_.each(modelRelease.technology, function(technology) {
							delete technology.releaseId;
							delete technology.releaseColor;
							delete technology.releaseTitle;
							if (_.isUndefined(upgrades)) {
								if (technology.format === format && !technology.upgrades.length) {
									technology.count = value;
								}
							} else {
								var formatUpgrades = technology.upgrades.join(', ');

								if (technology.format === format && formatUpgrades === upgrades) {
									technology.count = value;
								}
							}
						});

						$.post('/api/schedule/change_count', {
							tx:         reqres.request('get:user').get('tx'),
							release_id: release_id,
							cinema_id:  cinemaId,
							week:       week,
							year:       year,
							technology: $.toJSON(technology)
						}, function(r) {
							if (r.ok) {
								_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По сети', 'Клавиатура::Редактирование']);

								$target.find('.status').text(value);

								if (cinemaId === reqres.request('get:user').getCinema().id) {
									var neededModel = repertoireView.repertoire.findWhere({release_id: release_id});

									if (neededModel) {
										neededModel.fetch();
									}
								}

								// TODO: изменилось. нужно переписать
								// if (window.app.App.weekScheduleView) {
								// 	window.app.App.weekScheduleView.updateWeekSchedule();
								// }

								self.model.set({
									duration: r.duration
								}, {silent: true});

								self.calcWeekPercent(self.$el.closest('.section'));
							}
							Backbone.trigger('loader', 'hide');
						}, 'json');
					}

					if (key === 8 || key === 46) {
						Backbone.trigger('loader', 'show');

						e.preventDefault();

						value = 0;

						_.each(modelRelease.technology, function(technology) {
							if (_.isUndefined(upgrades)) {
								if (technology.format === format && !technology.upgrades.length) {
									technology.count = value;
								}
							} else {
								var formatUpgrades = technology.upgrades.join(', ');

								if (technology.format === format && formatUpgrades === upgrades) {
									technology.count = value;
								}
							}
						});

						$.post('/api/schedule/change_count', {
							tx:         reqres.request('get:user').get('tx'),
							release_id: release_id,
							cinema_id:  cinemaId,
							week:       week,
							year:       year,
							technology: $.toJSON(technology)
						}, function(r) {
							if (r.ok) {
								_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По сети', 'Клавиатура::Обнуление']);

								$target.find('.status').text(value);

								if (cinemaId === reqres.request('get:user').getCinema().id) {
									var neededModel = repertoireView.repertoire.findWhere({release_id: release_id});

									if (neededModel) {
										neededModel.fetch({
											success: function() {
												repertoireView.repertoire.updateHeaderCount();
											}
										});
									}
								}

								// TODO: изменилось. нужно переписать
								// if (window.app.App.weekScheduleView) {
								// 	window.app.App.weekScheduleView.updateWeekSchedule();
								// }

								self.model.set({
									duration: r.duration
								}, {silent: true});

								self.calcWeekPercent(self.$el.closest('.section'));

								$target.removeClass('clicked').closest('.section').find('.zeros').prepend($target);
							}
							Backbone.trigger('loader', 'hide');
						}, 'json');
					}
				}
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.$wrapper = this.options.parent.$wrapper;

			var	currentCinema = reqres.request('get:user').getCinema();

			this.model.set({
				cinema_rights: currentCinema.rights_id,
				cinema: {
					id: currentCinema.id,
					halls: currentCinema.halls,
					title: currentCinema.title
				}
			});

			this.listenTo(this.model, 'change', function() {
				if (this.model.changed.duration) {
					this.calcWeekPercent(this.$el.closest('.section'));
				}

				this.render();
			});
			this.listenTo(this.model, 'remove', this.removeView);

			window.app.App.networkview.innerViews.push(this);
		},
		render: function render() {
			var self     = this,
				template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes),
				releasesTechnologys = [],
				zeroCountReleasesTechnology = [];

			_.each(this.model.get('week').releases, function(release) {
				_.each(release.technology, function(tech) {
					tech.releaseTitle = release.title;
					tech.releaseId = release.id;
					tech.releaseColor = release.color;

					if (tech.count === 0) {
						zeroCountReleasesTechnology.push(tech);
					} else {
						releasesTechnologys.push(tech);
					}
				});
			});

			extModel.releasesTechnologys = releasesTechnologys;
			extModel.zeroCountReleasesTechnology = zeroCountReleasesTechnology;

			this.$el.html(template(extModel));

			setTimeout(function() {
				if (!self.$el.find('.release').length) self.$el.addClass('empty');

				self.calcWeekPercent(self.$el.closest('.section'));

				var $scheduleScreen,
					$arrowLeft,
					$arrowRight,
					rightIntervalId,
					leftIntervalId;

				if (reqres.request('get:user').isRepertoireReadOnly(self.model.get('cinema').id)) return;

				$scheduleScreen = self.$wrapper.find('.filter .screen');
				$arrowLeft      = $scheduleScreen.find('.arrow_left');
				$arrowRight     = $scheduleScreen.find('.arrow_right');

				function clickRight() {
					$arrowRight.trigger('click');
				}

				function clickLeft() {
					$arrowLeft.trigger('click');
				}

				self.$el.find('.release').draggable({
					helper: 'clone',
					cursor: 'move',
					zIndex: 5,
					containment: self.$el.closest('.list'),
					start: function(event, ui) {
						ui.helper.removeClass('clicked');
						self.$el.closest('.network_body').find('.repertoire_viewer.active').trigger('click');
					},
					stop: function() {
						clearInterval(rightIntervalId);
						clearInterval(leftIntervalId);
					},
					drag: function(event, ui) {
						var nowLeft        = Math.floor(ui.offset.left),
							leftPrevPoint  = 160,
							rightNextPoint = $(window).width() - 160;

						if (rightNextPoint <= nowLeft) {
							clearInterval(rightIntervalId);
							rightIntervalId = setInterval(clickRight, 500);
						} else {
							clearInterval(rightIntervalId);
						}

						if (leftPrevPoint >= nowLeft) {
							clearInterval(leftIntervalId);
							leftIntervalId = setInterval(clickLeft, 500);
						} else {
							clearInterval(leftIntervalId);
						}
					}
				});

				self.$el.find('.zeros .release').draggable('disable');
			}, 0);

			return this;
		},
		removeView: function removeView() {
			var $section = this.$el.closest('.section');

			$section.find('.zeros').empty();
			$section.find('.generate_timeline').html('0%');

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		},
		calcWeekPercent: function calcWeekPercent(section) {
			var model            = this.model,
				$targetSection   = section,

				cinemaSettings   = reqres.request('get:user').getCinemaSettings(model.get('cinema_id')),
				cinemaTime       = reqres.request('get:user').getCinema(model.get('cinema_id')).halls.length * 960,
				occupancyPercent = Math.round((model.get('duration') / cinemaTime) * 100),
				startTime,
				endTime,
				weekendStartTime,
				weekendEndTime,
				dayTime,
				weekendTime;

			if (cinemaSettings) {
				startTime = parseInt(Date.create(cinemaSettings.cinemaOpen).format('{H}')) * 60 + parseInt(Date.create(cinemaSettings.cinemaOpen).format('{mm}'));
				endTime   = parseInt(Date.create(cinemaSettings.cinemaClose).format('{H}')) * 60 + parseInt(Date.create(cinemaSettings.cinemaClose).format('{mm}'));

				if (endTime < startTime) {
					endTime = 24 * 60 + endTime;
				}

				weekendStartTime = parseInt(Date.create(cinemaSettings.cinemaWeekendOpen).format('{H}')) * 60 + parseInt(Date.create(cinemaSettings.cinemaWeekendOpen).format('{mm}'));
				weekendEndTime   = parseInt(Date.create(cinemaSettings.cinemaWeekendClose).format('{H}')) * 60 + parseInt(Date.create(cinemaSettings.cinemaWeekendClose).format('{mm}'));

				if (weekendEndTime < weekendStartTime) {
					weekendEndTime = 24 * 60 + weekendEndTime;
				}

				dayTime     = endTime - startTime;
				weekendTime = weekendEndTime - weekendStartTime;

				if (endTime !== startTime || weekendEndTime !== weekendStartTime) {
					cinemaTime = Math.floor(((dayTime * 5 + weekendTime * 2) / 7) * this.model.get('cinema').halls.length);
				}

				occupancyPercent = Math.round((model.get('duration') / cinemaTime) * 100);
			}

			$targetSection.find('.generate_timeline')
				.html(occupancyPercent + '%')
				.attr('occupancy_percent', occupancyPercent)
				.attr('free_time', cinemaTime - model.get('duration'));

			if (occupancyPercent > 100) {
				$targetSection.find('.generate_timeline').addClass('overdose');
			} else {
				$targetSection.find('.generate_timeline').removeClass('overdose');
			}
		}
	});
});

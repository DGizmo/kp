$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.UserUpdate = Backbone.Model.extend({
		defaults: {
			user:   '',
			type:   '',
			format: '',
			data:   ''
		}
	});

	window.app.UserUpdates = Backbone.Collection.extend({
		model: window.app.UserUpdate,
		initialize: function initialize() {
			this.user = reqres.request('get:user');
		},
		url: function url() {
			return '/api/history' + '?cinema_id=' + this.user.getCinema().id;
		},
		parse: function parse(r) {
			var currentUserID = this.user.id;
			_.each(r.history, function(item) {
				item.your = item.user_id === currentUserID ? true : false;
			});
			return r.history;
		}
	});

	window.app.UserUpdatesView = Backbone.View.extend({
		className: 'nt-sidebar-item nt-sidebar-item--user-updates',
		template: '#user_updates_box',
		toggleTemplate: _.template('<div class="nt-sidebar-toggle-icon icon-updates" data-nt-sidebar="user-updates" title="История изменений репертуарного плана."></div>'),
		initialize: function initialize() {
			$('.nt-sidebar').prepend(this.render().el);
			$('.nt-sidebar-toggle').prepend($(this.toggleTemplate()));

			this.updates = new window.app.UserUpdates();

			this.listenTo(this.updates, 'add', this.addOneNotify);
			this.listenTo(this.updates, 'reset', function(col, opts) {
				_.each(opts.previousModels, function(model) {
					model.trigger('remove');
				});
			});

			this.listenTo(Backbone, 'cinema-changed', this.loadUpdates);
			this.listenTo(Backbone, 'avatar-changed', this.changeCurrentUserAvatar);

			this.loadUpdates();
		},
		addOneNotify: function addOneNotify(notify) {
			var View = new window.app.userUpdateView({model: notify});
			View.parent = this;
			this.$el.find('#user_updates').append(View.render().el);
		},
		changeCurrentUserAvatar: function changeCurrentUserAvatar() {
			this.updates.fetch(); // TODO
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());
			return this;
		},
		loadUpdates: function loadUpdates() {
			this.updates.reset();
			this.$el.find('.nt-sidebar-user-updates-item').remove();

			this.$el.find('.loader').show();
			var self = this;
			this.updates.fetch({
				success: function() {
					self.$el.find('.loader').hide();
				}
			});
		}
	});

	window.app.userUpdateView = Backbone.View.extend({
		className: 'nt-sidebar-user-updates-item-wraper',
		template: '#user_updates_item',
		events: {
			'click .nt-sidebar-user-updates-item__name': 'showRelease'
		},
		initialize: function initialize() {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.remove);
		},
		showRelease: function showRelease(event) {
			event.preventDefault();
			Backbone.trigger('router:navigate', '/release/' + this.model.get('data').release_id);
			_gaq.push(['_trackEvent', 'Оповещения обновлений', 'Переход по ссылке']);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});
});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.KeygenReleasesListView = window.app.BookingReleasesListView.extend({
		id: 'keygens_wrap',
		template: '#keygen_releases_list_tmpl',
		initialize: function initialize() {
			this.subViews = [];

			this.collection = reqres.request('get:app:entity', 'bookingCollection');

			this.listenTo(this.collection, 'sync', function() {
				this.renderBookingReleases(this.collection.models);
			});
		},
		renderBookingReleases: function renderBookingReleases(models) {
			_.each(models, function(model) {
				var view = new window.app.KeygenReleaseView({model: model});
				this.subViews.push(view);
				this.$('#keygen_wr').append(view.render().el);
			}, this);
		}
	});

	window.app.KeygenReleaseView = window.app.BookingReleaseView.extend({
		template: '#keygen_release_tmpl',
		events: {
			'click .releases_body .info_item': function(e) {
				e.preventDefault();
				Backbone.trigger('router:navigate', $(e.currentTarget).data('href'));
			},
			'click .create_key': function(e) {
				e.preventDefault();
				e.stopPropagation();

				var model = new window.app.BookingProjectModel({
					project_id: $(e.currentTarget).data('project_id'),
					request_id: $(e.currentTarget).data('request_id')
				});

				Backbone.trigger('loader', 'show');
				model.fetch({
					success: _.bind(function() {
						Backbone.trigger('loader', 'hide');
						this.bookingProposalView = new window.app.BookingProposalView({
							model:      model,
							popupTitle: 'Создание ключей',
							cinemas:    new window.app.BookingProposalCinemas()
						});
					}, this)
				});
			}
		}
	});

});

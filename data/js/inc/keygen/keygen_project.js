$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.KeygenProjectView = Backbone.View.extend({
		id: 'keygen_item',
		template: '#keygen_project_tmpl',
		events: {
			'click .back a': function(e) {
				e.preventDefault();
				Backbone.trigger('router:navigate', '/keygen');
			},
			'click #release_check': function(e) {
				var $checkbox = $(e.currentTarget),
					filteredCinemas;

				if ($checkbox.prop('checked')) {
					filteredCinemas = _.map(this.$('.releases_body').children('.info_item:visible'), function(i) {
						return $(i).data('id');
					});

					_.chain(this.collection.models)
						.filter(function(cinema) { return _.contains(filteredCinemas, cinema.id); })
						.each(function(cinema) {
							var checkedHalls = _.map(cinema.get('halls'), function(hall) { return hall.id; });
							cinema.set({checkedHalls: checkedHalls});
						}, this);

				} else {
					$checkbox.prop('checked', false);
					this.collection.models.each(function(cinema) {cinema.set({checkedHalls: []}); });
				}
			},
			'click .action_key_head .key_extend': function() {
				var cinemas = _.filter(this.collection.models, function(cinema) {
						return cinema.get('checkedHalls').length;
					});

				if (cinemas.length) {
					this.bookingProposalView = new window.app.BookingProposalView({
						model:      this.model,
						popupTitle: 'Генерация ключей',
						cinemas:    new Backbone.Collection(cinemas)
					});
				} else {
					alert('Залы не выбраны!');
				}
			},
			'click .action_key_head .key_lock': function() {
				var self        = this,
					cinemas     = this.model.attributes.cinemas,
					info_form   = $('#info_form').serializeArray(),
					cinemas_id  = [],
					keys_id     = [],
					key_block   = 0,
					key_unblock = 0,
					message1    = '',
					message2    = '',
					halls_id    = [];

				_.each(info_form, function(r) {
					if (r.name === 'hall_id') {
						halls_id.push(+r.value);
					} else if (r.name === 'cinema_id') {
						cinemas_id.push(+r.value);
					}
				});

				_.each(cinemas_id, function(r) {
					_.each(cinemas, function(cinema) {
						if (+cinema.id === +r) {
							_.each(cinema.halls, function(hall) {
								halls_id.push(hall.id);
							});
						}
					});
				});

				_.each(_.union(halls_id), function(r) {
					_.each(cinemas, function(cinema) {
						_.each(cinema.halls, function(hall) {
							if (+hall.id === +r) {
								_.each(hall.keys, function(key) {
									key.blocked ? key_block++ : key_unblock++;
									keys_id.push(key.key_id);
								});
							}
						});
					});
				});

				if (key_unblock) {
					message1 = key_unblock + ' ' +
						_.declOfNum(key_unblock, ['ключ', 'ключа', 'ключей']) + ' ' +
						_.declOfNum(key_unblock, ['будет', 'будут', 'будут']) + ' ' +
						_.declOfNum(key_unblock, ['заблокирован', 'заблокированы', 'заблокированы']);
				}
				if (key_block) {
					message2 = key_block + ' ' +
						_.declOfNum(key_block, ['ключ', 'ключа', 'ключей']) + ' ' +
						_.declOfNum(key_block, ['будет', 'будут', 'будут']) + ' ' +
						_.declOfNum(key_block, ['разблокирован', 'разблокированы', 'разблокированы']);
				}

				if (key_block + key_unblock) {
					if (confirm(message1 + '\n' + message2)) {
						$.ajax({
							type: 'POST',
							url: '/api/booking/kdm_block',
							traditional: true,
							data: {id: keys_id}
						})
						.done(function() {
							$.growl.notice({title: 'Готово', message: ''});
							self.model.fetch();
						})
						.fail(function() {
							$.growl.error({title: 'Ошибка', message: 'Сбой подключения'});
						});
					}
				} else {
					alert('Пожалуйста, выберите ключи');
				}
			},
			'click .btn[data-action="open-popup"]': 'openBookingProposalPopup'
		},
		initialize: function initialize(projectID, requestID) {
			var self = this;

			this.model = new window.app.BookingProjectModel({project_id: projectID, request_id: requestID});
			this.collection = new Backbone.Collection([], {model: window.app.BookingProposalCinema});

			this.searchingView = new window.app.SearchingModuleView({
				animation: true,
				className: 'searching-module searching-module--right',
				onSearchingFinish: function onSearchingFinish(value) {
					var filtered = self.$('.releases_body').children('.info_item').hide().end()
						.find('.release_name')
						.children('.text:containsLower(' + value + ')').closest('.info_item').show().size();

					self.updateHeaderCheckbox(filtered);
				}
			});

			this.listenTo(Backbone, 'keygen:project-cinema:checked', this.updateHeaderCheckbox);
			this.fetchModel();
		},

		updateHeaderCheckbox: function updateHeaderCheckbox(filtered) {
			var $cinemas = this.$('.releases_body').children('.info_item'),
				$visibleItems = $cinemas.filter(':visible'),
				checked = $visibleItems
					.children('.release_check')
					.children('.release_item_check')
					.filter(function() { return $(this).prop('checked'); })
					.size();

			this.$('#release_check')
				.prop('checked', $visibleItems.length && ((filtered || $visibleItems.length) === checked));
		},
		fetchModel: function fetchModel() {
			Backbone.trigger('loader', 'show');
			this.model.fetch({
				success: _.bind(function(model) {

					this.collection.add(model.get('cinemas'));

					this.subViews = _.map(this.collection.models, function(model) {
						return new window.app.KeygenProjectCinemaView({
							attributes: {'data-id': model.id},
							model: model
						});
					});

					this.renderView();

					Backbone.trigger('loader', 'hide');
				}, this)
			});
		},
		renderView: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template(this.model.attributes));
			this.$('.filter').append(this.searchingView.render().el);

			_.each(this.subViews, function(view) {
				this.$('.releases_body').append(view.render().el);
			}, this);

			return this;
		},
		openBookingProposalPopup: function openBookingProposalPopup() {
			this.bookingProposalView = new window.app.BookingProposalView({
				model:      this.model,
				popupTitle: 'Создание ключей',
				cinemas:    reqres.request('get:app:entity', 'bookingProposalCinemas')
			});
		},
		remove: function remove() {
			this.$el.empty();
			_.invoke(this.subViews, 'remove');
			this.model.stopListening();
			this.collection.stopListening();
			this.searchingView.remove();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.KeygenProjectCinemaView = Backbone.View.extend({
		tagName: 'div',
		className: 'info_item',
		template: '#keygen_project_cinema_tmpl',
		kdm_status: {
			new:          'заявка создана',
			kdm_creating: 'генерируется',
			kdm_complete: 'сгенерирован',
			kdm_ready:    'проверен',
			kdm_error:    'ошибка',
			uploaded:     'загружен',
			deleted:      'удален'
		},
		email_status: {
			new:     'не отправлен',
			sending: 'отправляется',
			sent:    'отправлен',
			error:   'ошибка'
		},
		events: {
			'click .releases_cell': function(e) {
				if ($(e.target).hasClass('release_item_check')) return;
				this.showHallInfo = !this.showHallInfo;
				this.render();
				this.$el.toggleClass('open');
			},
			'click .release_item_check': function(e) {
				var checkedHalls = $(e.currentTarget).prop('checked') ? _.map(this.model.get('halls'), function(hall) {
					return hall.id;
				}) : [];

				this.model.set({checkedHalls: checkedHalls});
				Backbone.trigger('keygen:project-cinema:checked');
			},
			'click .hall_check': function() {
				var checkedHalls = [];

				_.each(this.$('.hall_check'), function(checkBox) {
					if ($(checkBox).prop('checked')) checkedHalls.push(parseInt($(checkBox).val()));
				});

				this.model.set({checkedHalls: checkedHalls});
				Backbone.trigger('keygen:project-cinema:checked');
			},
			'click .key_extend': function(e) {
				var checkedHalls = [],
					bookingProposalView;

				checkedHalls.push($(e.currentTarget).closest('.halls_body').data('hall-id'));
				this.model.set({checkedHalls: checkedHalls});

				bookingProposalView = new window.app.BookingProposalView({
					model:      reqres.request('get:booking-project'),
					popupTitle: 'Генерация ключей',
					cinemas:    new Backbone.Collection(this.model)
				});
			},
			'click .key_lock': function(e) {
				var keyID = $(e.currentTarget).closest('.action_key').data('key-id'),
					model   = this.model;

				$.post('/api/booking/kdm_block', {id: keyID})
					.done(function() {
						var key = _.chain(model.get('halls'))
							.pluck('keys').flatten()
							.findWhere({key_id: keyID})
							.value();

						key.blocked = !key.blocked;
						model.trigger('change');

						$.growl.notice({
							title: 'Готово',
							message: (key.blocked ? 'Ключ разблокирован' : 'Ключ заблокирован')
						});
					})
					.fail(function() {
						$.growl.error({title: 'Ошибка', message: 'Сбой подключения'});
					});
			}

		},
		initialize: function initialize() {
			this.showHallInfo = false;
			this.listenTo(this.model, 'change', this.render);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template({
				model:         this.model.attributes,
				showHallInfo:   this.showHallInfo,
				halls:         this.model.get('halls'),
				checkedHalls:  this.model.get('checkedHalls'),
				kdm_status:    this.kdm_status,
				email_status:  this.email_status,
				keys_emails:   this.model.getKeysEmails()
			}));
			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	var apiURL = '/api/v2';

	window.app.Message = Backbone.Model.extend({
		idAttribute: '_id',
		urlRoot: apiURL + '/messages',
		defaults: {
			created: null,
			text: '',
			ticket_id: null,
			user: {}
		},
		_text2link: function _text2link(text) {
			var exp = /(\b((https?|ftp|file):\/\/|(www))[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|,.]*[A-Z0-9+&@#\/%=~_|])/ig;
			return text.replace(exp, '<a href="$1" target="_blank">$1</a>');
		},
		_subStrAvatar: function _subStrAvatar(str) {
			return str.substring(str.indexOf('/img')) || '/img/avatar.png';
		},
		createTicket: function createTicket(type) {
			var self = this;

			Backbone.trigger('chat:loader', 'show');

			$.ajax({
				url: this.url() + '/ticket',
				type: 'PUT',
				dataType: 'json',
				data: JSON.stringify({type: type})
			})
			.done(function(res) {
				self.set('ticket_id', res.ticket_id);
				$.growl.ok({message: 'Тикет создан.'});
			})
			.fail(function() {
				$.growl.error({message: 'Что-то пошло не так. Попробуйте еще раз.'});
			})
			.always(function() {
				Backbone.trigger('chat:loader', 'hide');
			});
		},
		parse: function(model) {
			model.user.photo        = this._subStrAvatar(model.user.photo);
			model.user.photo_medium = this._subStrAvatar(model.user.photo_medium);
			model.text    = this._text2link(model.text);
			model.created = moment(model.created * 1000);

			return model;
		}
	});

	window.app.Messages = Backbone.Collection.extend({
		model: window.app.Message,
		initialize: function initialize() {
			this.listenTo(Backbone, 'socket:messages:delete', function(id) {
				_.invoke(this.findWhere({_id: id}), 'trigger', 'destroy');
			});

			this.listenTo(Backbone, 'socket:messages:add', function(message) {
				message._id = message._id && message._id.$oid;
				this.add(message, {parse: true});
			});
		},
		url: function url(history) {
			return apiURL + '/messages' + (history ? '?history=' + history : '');
		}
	});

	window.app.MessagesView = Backbone.View.extend({
		className: 'nt-sidebar-item nt-sidebar-item--chat',
		template: '#messages_box',
		toggleTemplate: _.template('<div class="nt-sidebar-toggle-icon icon-chat" data-nt-sidebar="chat" title="Общение с профессионалами кинопрокатной индустрии."><span id="chat-counter" class="nt-sidebar-toggle-icon__counter empty">0</span></div>'),
		count: 0,
		isOpen: false,
		events: {
			'click #chat-send': /* istanbul ignore next */ function(event) {
				event.preventDefault();
				_gaq.push(['_trackEvent', 'Чат', 'Отправка сообщения', 'Кнопка']);
				this.postMessage();
			},
			'keypress #chat-message': /* istanbul ignore next */ function(event) {
				$(event.currentTarget).autosize({
					callback:function() {
						$('#chatbox').css('height', $('.nt-sidebar-item--chat').outerHeight() - $('.nt-sidebar-chat-send').outerHeight() - 94);
					},
					append: ''
				});

				if (event.keyCode !== 13) { return; }
				var $textfield = this.$el.find('#chat-message');
				if (event.keyCode === 13 && event.shiftKey) {
					$textfield
						.val($textfield.val() + '\n')
						.trigger('autosize.resize');
					return false;
				}
				event.preventDefault();
				_gaq.push(['_trackEvent', 'Чат', 'Отправка сообщения', 'Enter']);
				this.postMessage();
			},
			'click .nt-sidebar-chat-send__download-photo': function() {
				_gaq.push(['_trackEvent', 'Чат', 'Добавить фото']);
				Backbone.trigger('user:profile:open:photo');
			}
		},
		initialize: function initialize(options) {
			var self = this;

			this.options = options;

			this.render().el;

			this.isOpen = 0;
			this.toUser = '';
			this.loader = this.$el.find('.loader');
			this.user   = reqres.request('get:user');

			this.messages = new window.app.Messages();
			this.messages.updated = false;

			this.messages.fetch({
				success: function(messages) {
					self.messages.updated = true;
					messages.each(function(model) {
						self.addOneMessage(model);
					});

					self.listenTo(self.messages, 'add', self.addOneMessage);
					self.listenTo(Backbone, 'window-resized', /* istanbul ignore next */ function() {
						if (self.isOpen) {
							self.$el.find('#chatbox').css('height', $('.nt-sidebar-item--chat').outerHeight() - $('.nt-sidebar-chat-send').outerHeight() - 94);
						}
					});
					self.listenTo(Backbone, 'chat-open', self.openChat);
					self.listenTo(Backbone, 'chat-close', function() {
						self.isOpen = false;
					});

					self.$el.find('#chatbox').on('scroll', _.bind(self.chatLoadMore, self)); // WTF

					$('.nt-sidebar').prepend(self.el);

					self.listenTo(Backbone, 'avatar-changed', self.changeCurrentUserAvatar);
				}

			});

			$('.nt-sidebar-toggle').prepend($(this.toggleTemplate()));

			this.listenTo(Backbone, 'chat:loader', function(action) {
				self.loader[action]();
			});

		},
		changeCurrentUserAvatar: function changeCurrentUserAvatar(avatar) {
			this.$el.children('#chatbox').removeClass('nt-sidebar-chat-box__no-photo');
			this.$el.find('.nt-sidebar-chat-send__download-photo').remove();

			_.chain(this.messages.models)
				.filter(function(model) {
					return model.get('user') && model.get('user')._id === this.user.id;
				}, this)
				.each(function(model) {
					model.set($.extend(model.get('user'), {
						photo_medium: avatar
					}));
				});
		},
		addOneMessage: function addOneMessage(mess) {
			var lastReadChat = this.user.get('last_read_chat'),
				View = new window.app.MessageView({model: mess, parent: this});

			this.$el.find('#chatbox').append(View.render().el).scrollTo(99999);

			if ((!this.isOpen) && (mess.attributes.user._id !== this.user.id) && (mess.attributes.created.unix() > lastReadChat)) {
				++this.count;
				$('.nt-sidebar-toggle').find('#chat-counter').removeClass('empty').text(this.count);
			}
		},
		addMoreMessages: function addMoreMessages(messages) {
			var self         = this,
				$html        = $(document.createElement('div')),
				$chatbox     = this.$el.find('#chatbox'),
				beforeHeight = $chatbox[0].scrollHeight;

			messages.each(function(message) {
				if (self.messages.findWhere({id: message.id})) {
					return;
				} else {
					self.messages.unshift(message, {silent: true});
					var View = new window.app.MessageView({model: message, parent: self});
					$html.prepend(View.render().el);
				}
			});
			this.$el.find('#chatbox').prepend($html.find('li'));
			$chatbox.scrollTop($chatbox[0].scrollHeight - beforeHeight);
			this.loader.hide();
		},
		postMessage: function postMessage() {
			var textfield  = this.$el.find('#chat-message'),
				textOutput = $('<div/>').text(textfield.val()).html();

			if (textfield.val().trim() === '') {return false;}
			$.post(apiURL + '/messages', JSON.stringify({text: textOutput, guid: this.guid(), to_user: this.toUser}));
			textfield
				.val('')
				.trigger('autosize.resize');
		},
		openChat: function openChat() {
			this.isOpen = true;
			this.count  = 0;

			$('#chat-counter').addClass('empty');

			this.$el.find('#chatbox')
				.scrollTo(99999)
				.parent().find('#chat-message').focus();

			var timestamp = Math.floor(+new Date() / 1000);

			this.user.set('last_read_chat', timestamp);
			$.post(apiURL + '/messages/read', {timestamp: timestamp});
		},
		chatLoadMore: function chatLoadMore() {
			if (!this.$el.find('#chatbox')[0].scrollTop) {
				this.loader.show();
				var self    = this,
					history = this.messages.at(0).get('created').unix(),
					moreMessages = new window.app.Messages();
				moreMessages.fetch({
					url: moreMessages.url(history),
					success: function() {
						moreMessages.models.reverse();
						self.addMoreMessages(moreMessages.models);
					}
				});
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				noPhoto  = !reqres.request('get:user').get('photo_medium');

			this.$el.html(template({noPhoto: noPhoto}));

			if (noPhoto) this.$el.children('#chatbox').addClass('nt-sidebar-chat-box__no-photo');

			return this;
		},
		guid: /* istanbul ignore next */ function guid() {
			var s4 = function() {
				return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
			};

			return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
		}
	});

	window.app.MessageView = Backbone.View.extend({
		tagName:   'li',
		className: 'nt-sidebar-chat-box__message',
		template:  '#message_item',
		tooltipTemplate: '#messagesTooltip',
		events: {
			'click .nt-sidebar-chat-box__message-name': /* istanbul ignore next */ function(event) {
				event.preventDefault();
				var userName = this.model.get('user').first_name + ' ' + this.model.get('user').last_name + ', ';

				this.options.parent.toUser = this.model.get('user')._id;

				this.$textField.val('').val(userName);

				this.setCaretAtEnd();
			},
			'click .nt-sidebar-chat-box__message-remove': /* istanbul ignore next */ function(event) {
				event.preventDefault();
				if (confirm ('Вы действительно хотите удалить сообщение из чата?')) this.model.destroy();
			},
			'click .nt-sidebar-chat-box__new-ticket': 'showContextMenu'
		},
		showContextMenu: /* istanbul ignore next */ function showContextMenu(event) {
			event.preventDefault();
			event.stopPropagation();

			this.contextMenu = new window.app.ContextMenuView({
				top:  event.pageY,
				left: event.pageX + 1,
				items: [
					{
						title: 'Общее',
						action: function() {
							this.model.createTicket('general');
						},
						context: this
					}, {
						title: 'KDM',
						action: function() {
							this.model.createTicket('kdm');
						},
						context: this
					}, {
						title: 'DD24',
						action: function() {
							this.model.createTicket('dd24');
						},
						context: this
					}, {
						title: 'Трейлеры',
						action: function() {
							this.model.createTicket('trailer');
						},
						context: this
					}
				]
			});
		},
		setCaretAtEnd: /* istanbul ignore next */ function setCaretAtEnd() {
			var elem = this.$textField[0],
				elemLen = elem.value.length,
				oSel;
			if (document.selection) {
				elem.focus();
				oSel = document.selection.createRange();
				oSel.moveStart('character', -elemLen);
				oSel.moveStart('character', elemLen);
				oSel.moveEnd('character', 0);
				oSel.select();
			} else if (elem.selectionStart || elem.selectionStart === '0') {
				elem.selectionStart = elemLen;
				elem.selectionEnd = elemLen;
				elem.focus();
			}
		},
		initialize: function initialize(options) {
			this.options = options;

			this.$textField = this.options.parent.$el.find('#chat-message');

			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
		},
		render: function render() {
			var model    = this.model.toJSON(),
				template = Backbone.TemplateCache.get(this.template),
				tooltipTemplate = Backbone.TemplateCache.get(this.tooltipTemplate),
				servicedeskURL  = (_.contains(['kinoplan24.ru', 'release.kinoplan24.ru', 'www.kinoplan24.ru'], _.getWindowLocationHost()) ?
					'https://' : 'http://dev.') + 'servicedesk.dcp24.ru/ticket/';

			this.$el.html(template({
				message:   model,
				moderator: reqres.request('get:user').isModerator(),
				servicedeskURL: servicedeskURL
			}));

			if ((_.contains(['cinema', 'mechanic'], model.user.type) && model.user.cinema && model.user.cinema.length) ||
				model.user.type === 'distributor') {
				this.$('.nt-sidebar-chat-box__message-name').tipsy({
					html: true,
					gravity: 'e',
					offset: -42,
					title: function() {
						return tooltipTemplate(model);
					}
				});
			}

			return this;
		}
	});
});

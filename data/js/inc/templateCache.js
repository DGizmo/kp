$(function() {
	'use strict';
	window.app = window.app || {};

	Backbone.TemplateCache = function() {};

	_.extend(Backbone.TemplateCache, {
		templates: {},
		get: function(selector) {
			var template = this.templates[selector],
				tmpl;

			if (!template) {
				tmpl = $(selector).html() || this.ajaxGet(selector);
				return this.add(selector, tmpl);
			}

			return template;
		},
		ajaxGet: function(template) {
			var data = '<h3> failed to load template : ' + template + '</h3>',
				file = template.substring(1);
			$.ajax({
				async: false,
				cache: true,
				url: '/packed/templates/' + file + '.html',
				success: function(response) {
					data = response;
				}
			});
			return data;
		},
		add: function(selector, tmpl) {
			var template = this.templates[selector];
			if (!template) this.templates[selector] = _.template(tmpl);
			return this.templates[selector];
		}
	});

});

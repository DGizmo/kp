$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.PopoverView = Backbone.View.extend({
		render: function render(data) {
			var offsetTop  = this.options.top,
				offsetLeft = this.options.left,
				template   = Backbone.TemplateCache.get(this.template),
				needsVerticalFlip,
				needsHorizontalFlip;

			this.$el
				.appendTo('body')
				.html(template(data));

			needsVerticalFlip   = offsetTop  + this.$el.height() > $(window).height();
			needsHorizontalFlip = offsetLeft + this.$el.width()  > $(window).width();

			if (needsVerticalFlip)   offsetTop  = offsetTop  - this.$el.height();
			if (needsHorizontalFlip) offsetLeft = offsetLeft - this.$el.width();

			this.$el.offset({
				top:  offsetTop,
				left: offsetLeft
			});

			return this;
		},
		remove: function remove() {
			this.$el.empty();
			Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ContextMenuView = window.app.PopoverView.extend({
		className: 'context-menu',
		template:  '#context_menu',
		events: {
			'click .context-menu__item': function(event) {
				event.stopPropagation();

				var index = $(event.currentTarget).index(),
					item  = this.options.items[index];

				if (item && typeof item.action === 'function') item.action.call(item.context || this);
				this.remove();
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};

			Backbone.trigger('popover:hide');

			if (options.top === undefined || options.left === undefined)
				throw 'ContextMenu must be shown with top/left coordinates.';

			if (options.items === undefined)
				throw 'ContextMenu needs items to be shown.';

			this.listenTo(Backbone, 'body:click popover:hide', this.remove);

			this.render({items: this.options.items});
		}
	});

	window.app.CommentPopoverView = window.app.PopoverView.extend({
		className: 'comment-popover',
		template:  '#comment_popover',
		events: {
			click: function(event) {
				event.stopPropagation();
			},
			'click .comment-popover__save':   'saveComment',
			'click .comment-popover__delete': 'deleteComment',
			keyup: function(event) {
				if (event.keyCode === 13) this.saveComment();
			}
		},
		saveComment: function saveComment() {
			this.onSave(this.$('.comment-popover__input').val());
			this.remove();
		},
		deleteComment: function deleteComment() {
			this.onSave('');
			this.remove();
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.onSave  = options.onSave;

			Backbone.trigger('popover:hide');

			if (options.top === undefined || options.left === undefined)
				throw 'Popover must be shown with top/left coordinates.';

			if (typeof this.onSave !== 'function')
				throw 'Popover onSave undefined.';

			this.listenTo(Backbone, 'body:click popover:hide', this.remove);

			this.render({comment: options.comment}).setCaretAtEnd();
		},
		setCaretAtEnd: /* istanbul ignore next */ function setCaretAtEnd() {
			var el = $('.comment-popover__input').get(0),
				elemLen = el.value.length;

			el.selectionStart = elemLen;
			el.selectionEnd = elemLen;
			el.focus();
		}
	});

});

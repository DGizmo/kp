$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.MechanicWebinarPopup = Backbone.View.extend({
		template: '#mechanicWebinar',
		events: {
			'click .mechanic-webinar__link': function() {
				_gaq.push(['_trackEvent', 'Реклама МК', 'Переход на страницу']);
				$.arcticmodal('close');
			}
		},
		initialize: function initialize() {
			var self = this;
			_gaq.push(['_trackEvent', 'Реклама МК', 'Открытие']);

			$('#js-popup').html(this.render().el);
			$('#js-popup').arcticmodal({
				afterClose: function() {
					_gaq.push(['_trackEvent', 'Реклама МК', 'Закрыт']);
					self.remove();
				}
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());
			return this;
		},
		remove: function remove() {
			this.$el.empty();
			Backbone.View.prototype.remove.call(this);
		}
	});
});

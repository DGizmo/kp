$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DistributorCinemaModel = Backbone.Model.extend({
		defaults: {
			title: '-',
			location: {},
			formats: ['-'],
			capacity: '-',
			halls: '-',
			link: 'неизвестен'
		},
		url: function url() {
			return '/api/distributors/cinemas/' + this.get('id');
		}
	});

	window.app.DistributorCinemasCollection = Backbone.Collection.extend({
		model: window.app.DistributorCinemaModel,
		initialize: function initialize() {
			// FIXME
			this.update();

			this.listenTo(Backbone, 'distributor_cinemas:searching', function(inputValue) {
				this.searchingCinemas(inputValue);
			});
		},
		url: '/api/distributors/cinemas',
		parse: function parse(r) {
			return _.chain(r.list)
				.each(function(cinema) {
					cinema.title = cinema.title ? String(cinema.title) : '';
					cinema.formats = _.map(cinema.formats, function(format) {
						if (format !== '35mm') {
							return format.toUpperCase();
						} else {
							return format;
						}
					});

					cinema.formats = _.sortBy(cinema.formats, function(format) {
						if (format === '35mm') return format;
					});
				})
				.sortByNat(function(cinema) {
					return cinema.title;
				})
				.value();
		},
		update: function update() {
			Backbone.trigger('loader', 'show');
			this.fetch().always(function() {
				Backbone.trigger('loader', 'hide');
			});
		},
		searchingCinemas: function(inputValue) {
			var searchingCinemas;

			if (inputValue.length) {
				inputValue = inputValue.toUpperCase();
				searchingCinemas = _.filter(this.models, function(cinema) {
					return (cinema.get('title').toUpperCase().indexOf(inputValue) > -1 ||
						cinema.get('location').region.toUpperCase().indexOf(inputValue) > -1 ||
						cinema.get('location').city.toUpperCase().indexOf(inputValue) > -1);
				});

				this.trigger('cinemas:searching', searchingCinemas, 'searching');
			} else {
				this.trigger('cinemas:searching', this.models);
			}
		}
	});

	window.app.DistributorCinemaModelView = Backbone.View.extend({
		className: 'cinema_item',
		template: '#distributor_one_cinema',
		events: {
			click: 'showCinema'
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		},
		showCinema: function showCinema(event) {
			event.preventDefault();
			Backbone.trigger('router:navigate', '/cinema/' + this.model.get('id'));
		}
	});

	window.app.DistributorCinemasCollectionView = Backbone.View.extend({
		className: 'cinemas_list',
		template: '#distributor_cinemas',
		initialize: function initialize(options) {
			this.subViews = [];
			this.options = options || {};

			if (!this.options.collections.cinemasCollection) {
				this.options.collections.cinemasCollection = new window.app.DistributorCinemasCollection();
			}

			this.collection = this.options.collections.cinemasCollection;

			if (this.collection.length) this.options.parentWrap.html(this.render().el);

			this.listenTo(this.collection, 'sync', function() {
				this.options.parentWrap.html(this.render().el);
			});

			this.listenTo(this.collection, 'cinemas:searching', function(models, mode) {
				this.renderSubViews(models, mode);
			});
		},
		renderSubViews: function renderSubViews(models, mode) {
			_.invoke(this.subViews, 'remove');

			this.subViews = [];

			_.each(models, function(cinema) {
				if (cinema.get('halls') >= 5 && cinema.get('capacity') || mode === 'searching') {
					var cinemaView = new window.app.DistributorCinemaModelView({model: cinema});

					this.$('.distributor_cinemas_body').append(cinemaView.render().el);

					this.subViews.push(cinemaView);
				}
			}, this);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.collection));
			this.renderSubViews(this.collection.models);

			return this;
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.DistributorCinemasWrapperView = Backbone.View.extend({
		className: 'distributor_cinemas_wrapper',
		template: '#distributor_cinemas_wrapper',
		unapprovedTemplate: '#distributor_blocker',
		events: {
			'input .cinemas_search input': function(e) {
				clearTimeout(this.timer);

				this.timer = setTimeout(function() {
					var inputValue = $(e.currentTarget).val();

					if (inputValue.length) {
						$(e.currentTarget).next().show();
						Backbone.trigger('distributor_cinemas:searching', inputValue);
					} else {
						$(e.currentTarget).next().hide();
						Backbone.trigger('distributor_cinemas:searching', '');
					}
				}, 500);
			},
			'click .cinemas_search .remove': function() {
				this.$('.cinemas_search input')
					.val('')
					.trigger('input');
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
		},
		render: function render() {
			var template;

			if (reqres.request('get:user').isApproved()) {
				template = Backbone.TemplateCache.get(this.template);
				this.$el.html(template);

				this.distributorCinemasCollectionView = new window.app.DistributorCinemasCollectionView({
					collections: this.options.collections,
					parentWrap:  this.$('.cinemas_list_wrapper')
				});
			} else {
				template = Backbone.TemplateCache.get(this.unapprovedTemplate);
				this.$el.html(template);
			}

			return this;
		},
		remove: function remove() {
			if (this.distributorCinemasCollectionView) this.distributorCinemasCollectionView.remove();
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

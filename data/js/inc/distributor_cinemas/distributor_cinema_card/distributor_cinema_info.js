$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DistributorCinemaInfoView = Backbone.View.extend({
		className: 'cinema_info',
		template: '#distributor_cinema_info',
		initialize: function initialize(options) {
			this.options = options || {};
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes);

			extModel.checkDistributor = false;

			this.$el.html(template(extModel));

			return this;
		}
	});

});

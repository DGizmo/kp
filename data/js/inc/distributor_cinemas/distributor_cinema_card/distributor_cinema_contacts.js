$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DistributorCinemaContactsView = Backbone.View.extend({
		className: 'cinema_contacts',
		template: '#distributor_cinema_contacts',
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template(this.model.attributes));

			return this;
		}
	});
});

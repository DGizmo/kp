$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DistributorCinemaCardWrapView = Backbone.View.extend({
		className: 'cinema_card',
		template: '#distributor_cinema_card_wrap',
		blockerTemplate: '#distributor_blocker',
		events: {
			'click .back': function(e) {
				e.preventDefault();
				window.history.back();
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
		},
		render: function render() {
			if (reqres.request('get:user').isApproved()) {
				this.$el.html(Backbone.TemplateCache.get(this.template));

				this.afterRender();
			} else {
				this.$el.html(Backbone.TemplateCache.get(this.blockerTemplate));
			}

			return this;
		},
		afterRender: function afterRender() {
			var self              = this,
				cinemasCollection = this.options.collections.cinemasCollection,
				cinema            = cinemasCollection ? cinemasCollection.find(function(cinema) { return cinema.id === self.options.modelId; }) : false,
				model,
				sortedHalls;

			if (cinema) {
				model = cinema;
			} else {
				model = new window.app.DistributorCinemaModel({id: this.options.modelId});
			}

			Backbone.trigger('loader', 'show');
			model.fetch({
				success: function() {
					sortedHalls = _.sortBy(model.get('halls'), function(hall) { return hall.number; });
					model.set('halls', sortedHalls);

					self.distributorCinemaCardView = new window.app.DistributorCinemaCardView({
						model: model,
						el:    self.$('.cinema_item')
					});
					Backbone.trigger('loader', 'hide');
				}
			});
		}
	});

	window.app.DistributorCinemaCardView = Backbone.View.extend({
		template: '#distributor_cinema_card',
		currentView: null,
		events: {
			'click .tabs .item': function(e) {
				$(e.currentTarget).addClass('active').siblings().removeClass('active');
			},
			'click .tabs .item[data-tab="cinema_info"]': function() {
				this.switchView(new window.app.DistributorCinemaInfoView({model: this.model}));
			},
			'click .tabs .item[data-tab="cinema_contacts"]': function() {
				this.switchView(new window.app.DistributorCinemaContactsView({model: this.model}));
			}
		},
		initialize: function initialize() {
			this.render();
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template(this.model.attributes));

			this.switchView(new window.app.DistributorCinemaInfoView({model: this.model}));

			if (Math.floor(this.model.get('location').latitude) !== 0 && Math.floor(this.model.get('location').longitude) !== 0) {
				this.afterRender();
			}
		},
		afterRender: function afterRender() {
			var x    = this.model.get('location').latitude,
				y    = this.model.get('location').longitude,
				map,
				placemark;

			ymaps.ready(function() {
				map = new ymaps.Map('yandex_map_location', {
					center: [x, y],
					zoom: 14
				});

				placemark = new ymaps.Placemark([x, y], {}, {
					preset: 'twirl#blueDotIcon',
					cursor: 'move'
				});

				map.geoObjects.add(placemark);
			});
		},
		switchView: function switchView(view) {
			if (this.currentView !== null && this.currentView.cid !== view.cid) {
				this.currentView.remove();
			}

			this.currentView = view;

			this.$el.find('.cinema_card_wrapper').html(view.render().el);
		}
	});
});

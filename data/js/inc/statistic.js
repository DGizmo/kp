//-------------------- Страница статистики --------------------//

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	var country_id = 3159,
		current_country_id_stat = 3159,
		current_country_id_cinema = 3159,
		current_country_id_chart = 3159;

	function statPie(type) {
		$.get('/api/info/stats.json?type=' + type + '&country_id=' + country_id, function(r) {
			var data = [],
				j = 1,
				html,
				hidden = 'hidden',
				i,
				sort;

			$('#' + type + '_table .table_item').remove();

			sort = _.sortBy(r.data, function(item) {
				return Math.max(item.percent);
			});

			sort.reverse();

			for (i in sort) {
				if (j <= 10) {
					data.push([sort[i].name, Number((sort[i].percent).toFixed(2))]);
				}

				j <= 10 ? hidden = '' : hidden = 'hidden';

				if (type === 'city') {
					html =
						'<tr class="table_item ' + hidden + '">' +
						'<td class="text_align_right"><span class="num">' + j + '.</span></td>' +
						'<td><a href="/statistic/cinemas/city_id/' + sort[i].item_id + '">' + sort[i].name + '</a></td>' +
						'<td class="text_align_right">' + sort[i].halls + '</td>' +
						'<td class="text_align_right">' + (sort[i].percent).toFixed(2) + '%</td>' +
						'<td class="text_align_right">' + sort[i].population + '</td>' +
						'</tr>';
				} else if (type === 'projector_make' || type === 'dbox' || type === 'owner' || type === 'country') {
					html =
						'<tr class="table_item ' + hidden + '">' +
						'<td class="text_align_right"><span class="num">' + j + '.</span></td>' +
						'<td>' + sort[i].name + '</td>' +
						'<td class="text_align_right">' + sort[i].halls + '</td>' +
						'<td class="text_align_right">' + (sort[i].percent).toFixed(2) + '%</td>' +
						'</tr>';
				} else {
					html =
						'<tr class="table_item ' + hidden + '">' +
						'<td class="text_align_right"><span class="num">' + j + '.</span></td>' +
						'<td><a href="/statistic/cinemas/' + type + '/' + sort[i].name + '">' + sort[i].name + '</a></td>' +
						'<td class="text_align_right">' + sort[i].halls + '</td>' +
						'<td class="text_align_right">' + (sort[i].percent).toFixed(2) + '%</td>' +
						'</tr>';
				}

				++j;
				$('#' + type + '_table tbody').append(html);
			}
			if (r.length > 10) {
				$('#' + type + '_table tbody').append('<tr class="table_item"><td></td><td colspan="4"><a href="#" class="show_all">Показать все ' + r.length + '</a></td></tr>');
			}

			$('#' + type + '_table tbody .show_all').click(function(e) {
				e.preventDefault();
				$(e.currentTarget).closest('.title').find('.hidden').removeClass('hidden');
				$(e.currentTarget).closest('.table_item').addClass('hidden');
			});

			$('#' + type).highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				title: {
					text: ''
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							format: '<b>{point.name}</b>: {point.percentage:.1f} %',
							style: {
								color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
							}
						}
					}
				},
				series: [{
					type: 'pie',
					name: '',
					data: data
				}]
			});

		}, 'json');
	}

	window.app.StatisticLoadReady = false;

	window.app.StatisticLoad = function() {
		statPie('server');
		statPie('projector_make');
		statPie('dbox');
		statPie('type3d');
		statPie('projector_model');
		statPie('country');
		statPie('integrator');
		statPie('city');
		statPie('owner');

		window.app.StatisticLoadReady = true;
	};

	//----------------- Статистика - Кинотеатры ---------------//

	window.app.StatisticCinema = Backbone.Model.extend({
		defaults: {
			projector_model: null,
			projector_make: null,
			title: null,
			country_id: null,
			type_3d: null,
			server_serial: null,
			city_id: null,
			installed_timestamp: null,
			capasity: null,
			city_title_ru: null,
			country_title_en: null,
			integrator: null,
			server: null,
			country_title_ru: null,
			installed: null,
			city_title_en: null,
			cinema_title: null
		}
	});

	window.app.StatisticsCinema = Backbone.Collection.extend({
		model: window.app.StatisticCinema,
		url: function() {
			return this.urlfor;
		},
		baseUrl: '/api/info/halls.json',
		urlfor: '/api/info/halls.json',
		currentUrl: '/api/info/halls.json',
		desc: false,
		next: null,
		pageMax: null,
		country_id: '',
		parse: function(r) {
			this.next = r.next;
			this.pageMax = r.max;
			window.app.App.allCinemas.next = this.next;
			window.app.App.allCinemas.pageMax = this.pageMax;
			return r.halls;
		},
		setParam: function(param, value, name) {
			if (window.app.controller) window.app.controller.navigate('statistic/cinemas/' + param + '/' + value, {
				trigger: false
			});
			if (name) {
				$('.cinema_tab h2 span').text(': ' + name);
			}
			$('#filt_country').val('').trigger('refresh');
			country_id = '';

			this.urlfor = this.baseUrl + '?' + param + '=' + value;
			this.currentUrl = this.urlfor;
			this.remove(this.models);
			this.fetch();
		}
	});

	window.app.StatisticsWrapperView = Backbone.View.extend({
		id: 'statistic',
		template: '#statistic_wrapper',
		events: {
			'click .tabs li span': function(e) {
				e.preventDefault();

				var country_param;

				this.$el.find('#statistic_wr').scrollTop(0);
				this.$el.find('.tabs li, .tab').removeClass('active');

				$(e.target).parent().addClass('active');
				$('.' + $(e.target).data('tab') + '_tab').addClass('active');

				if ($(e.target).data('navigate') === 'statistic/cinemas') {

					if (current_country_id_cinema !== country_id) {
						window.app.App.StatisticsCinema.country_id = country_id;
						window.app.App.StatisticsCinema.currentUrl = window.app.App.StatisticsCinema.baseUrl ? country_param = '?country_id=' : country_param = '&country_id=';
						window.app.App.StatisticsCinema.urlfor = window.app.App.StatisticsCinema.currentUrl + country_param + country_id;
					}
					current_country_id_cinema = country_id;

				} else if ($(e.target).data('navigate') === 'statistic' && !window.app.StatisticLoadReady) {
					current_country_id_stat = country_id;

				} else if ($(e.target).data('navigate') === 'statistic/charts' && !window.app.ChartsLoadReady) {
					current_country_id_chart = country_id;
				}

				if ($('#stat').hasClass('active') && current_country_id_stat !== country_id) {
					window.app.StatisticLoad();
					current_country_id_stat = country_id;
				}

				if ($('#cinema').hasClass('active') && current_country_id_cinema !== country_id) {
					$('.cinema_search').show();
					$('.cinema_tab h2 span').text('');
					window.app.App.StatisticsCinema.country_id = country_id;
					window.app.App.StatisticsCinema.currentUrl = window.app.App.StatisticsCinema.baseUrl ? country_param = '?country_id=' : country_param = '&country_id=';
					window.app.App.StatisticsCinema.urlfor = window.app.App.StatisticsCinema.currentUrl + country_param + country_id;
					window.app.App.StatisticsCinema.fetch();
					current_country_id_cinema = country_id;
				}

				if ($('#cinema').hasClass('active')) {
					$('.cinema_search').show();
				} else {
					$('.cinema_search').hide();
				}

				if ($('#chart').hasClass('active') && current_country_id_chart !== country_id) {
					window.app.ChartsLoad();
					current_country_id_chart = country_id;
				}

				window.app.controller.navigate($(e.target).data('navigate'), {
					trigger: false
				});
			},

			'click .default_filter': function(e) {
				e.preventDefault();
				$('.cinema_tab h2 span').text('');
				$('.filt_select_cinema #filt_country').val('').trigger('refresh');
				window.app.controller.navigate('statistic/cinemas', {
					trigger: false
				});
				window.app.App.StatisticsCinema.urlfor = window.app.App.StatisticsCinema.baseUrl;
				window.app.App.StatisticsCinema.currentUrl = window.app.App.StatisticsCinema.baseUrl;
				window.app.App.StatisticsCinema.fetch();
			},

			'submit #cinema_search': function(e) {
				e.preventDefault();
				$('#filt_country').val('').trigger('refresh');
				country_id = '';
				window.app.controller.navigate('statistic/cinemas/search/' + $('#cinema_search input').val(), {
					trigger: false
				});
				window.app.App.StatisticsCinema.urlfor = window.app.App.StatisticsCinema.baseUrl + '?q=' + $('#cinema_search input').val();
				window.app.App.StatisticsCinema.fetch();
			},

			'click .date_order': function(e) {
				e.preventDefault();
				if (window.app.App.StatisticsCinema.desc) {
					window.app.App.StatisticsCinema.urlfor = window.app.App.StatisticsCinema.currentUrl + '&order=desc';
				} else {
					window.app.App.StatisticsCinema.urlfor = window.app.App.StatisticsCinema.currentUrl + '&order=asc';
				}
				window.app.App.StatisticsCinema.desc = !window.app.App.StatisticsCinema.desc;
				window.app.App.StatisticsCinema.fetch();
			}
		},

		initialize: function() {
			$('#statistic').html(this.render().el).find('#statistic_wr').css({
				height: $('.sidebar').height() - 100
			});
			this.$el.find('#statistic_wr').on('scroll', _.bind(this.onStatScroll, this));

			window.app.App.StatisticsCinema = new window.app.StatisticsCinema();
			window.app.App.allCinemas = new window.app.StatisticsCinema();

			this.listenTo(window.app.App.StatisticsCinema, 'add', this.addOneStatistics);

			window.app.App.StatisticsCinema.fetch();

			window.app.StatisticCountry();
			window.app.StatisticLoad();
			window.app.ChartsLoad();

		},

		addOneStatistics: function(statistic) {
			var view = new window.app.StatisticsCinemaView({
				model: statistic
			});
			statistic.view = view;
			this.$el.find('.cinema_tab tbody').append(view.render().el);
		},

		onStatScroll: function() {
			if ($('#cinema').hasClass('active')) {
				var scrollHeight = $('#statistic_wr').get(0).scrollHeight - $('#statistic_wr').get(0).clientHeight;
				if ($('#statistic_wr').get(0).scrollTop >= scrollHeight) {
					this.loadNextCinemas();
				}
			}
		},

		loadNextCinemas: function() {
			if (window.app.App.allCinemas.next <= window.app.App.allCinemas.pageMax) {
				Backbone.trigger('loader', 'show');
				if (window.app.App.StatisticsCinema.currentUrl === window.app.App.StatisticsCinema.baseUrl) {
					window.app.App.allCinemas.urlfor = window.app.App.StatisticsCinema.currentUrl + '?page=' + window.app.App.allCinemas.next + '&country_id=' + window.app.App.StatisticsCinema.country_id;
				} else {
					window.app.App.allCinemas.urlfor = window.app.App.StatisticsCinema.currentUrl + '&page=' + window.app.App.allCinemas.next + '&country_id=' + window.app.App.StatisticsCinema.country_id;
				}

				window.app.App.allCinemas.fetch({
					success: function() {
						window.app.App.allCinemas.each(function(cinemas) {
							window.app.App.StatisticsCinema.add(cinemas);
						});
						window.app.App.allCinemas.reset();
						Backbone.trigger('loader', 'hide');
					}
				});

			}
		},

		render: function() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());
			return this;
		},

		removeView: function() {
			this.remove();
		}

	});

	window.app.StatisticsCinemaView = Backbone.View.extend({
		tagName: 'tr',
		className: 'table_item',
		template: '#statistic_cinema_item',
		events: {
			'click .s_filter': function(e) {
				e.preventDefault();
				window.app.App.StatisticsCinema.setParam($(e.currentTarget).data('filter'), $(e.currentTarget).data('param'), $(e.currentTarget).text());
			}
		},
		initialize: function() {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.remove);
		},
		render: function() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});

	//----------------- Статистика - Диграммы -----------------//

	function chart(type) {
		$.get('/api/info/graph.json?type=' + type + '&country_id=' + country_id, function(r) {
			var series = [],
				categories = r.categories,
				num,
				i,
				j;

			for (i in r.series) {
				if (r.series[i].data.length < categories.length) {
					num = categories.length - r.series[i].data.length;

					for (j = 0; j < num; j++) {
						r.series[i].data.unshift(0);
					}
				}
				series.push({
					name: r.series[i].name,
					data: r.series[i].data
				});
			}

			$('#' + type).highcharts({
				chart: {
					margin: [80, 50, 70, 70],
					defaultSeriesType: 'column'
				},
				title: {
					text: ''
				},
				subtitle: {
					text: ''
				},
				xAxis: {
					categories: r.categories,
					labels: {
						align: 'right'
					}
				},
				yAxis: {
					min: 0,
					title: {
						text: ''
					}
				},
				legend: {
					layout: 'vertical',
					backgroundColor: '#FFFFFF',
					align: 'left',
					verticalAlign: 'top',
					x: 100,
					y: 70,
					floating: false,
					shadow: false
				},
				tooltip: {
					formatter: function() {
						return '' +
							this.x + ': ' + this.y + ' ';
					}
				},
				plotOptions: {
					column: {
						pointPadding: 0.2,
						borderWidth: 0
					}
				},
				series: series
			});
		}, 'json');
	}

	function graph(type) {
		$.get('/api/info/graph.json?type=' + type + '&country_id=' + country_id, function(r) {
			var series = [],
				categories = r.categories[0],
				num,
				i,
				j;

			for (i in r.series) {
				if (r.series[i].data.length < categories.length) {
					num = categories.length - r.series[i].data.length;
					for (j = 0; j < num; j++) {
						r.series[i].data.unshift(0);
					}
				}
				series.push({
					name: r.series[i].name,
					data: r.series[i].data
				});
			}

			$('#' + type).highcharts({
				chart: {
					defaultSeriesType: 'area'
				},
				title: {
					text: ''
				},
				subtitle: {
					text: ''
				},
				xAxis: {
					categories: r.categories[0],
					labels: {
						align: 'right'
					}
				},
				labels: {
					align: 'center'
				},
				tickmarkPlacement: 'on',
				yAxis: {
					title: {
						text: 'Проценты'
					}
				},
				tooltip: {
					formatter: function() {
						return '' +
							this.x + ': ' + this.y + ' installations by ' + this.series.name;
					}
				},
				plotOptions: {
					area: {
						stacking: 'percent',
						lineColor: '#ffffff',
						lineWidth: 1,
						marker: {
							lineWidth: 1,
							lineColor: '#ffffff'
						}
					}
				},
				series: series
			});
		}, 'json');
	}

	window.app.ChartsLoadReady = false;

	window.app.ChartsLoad = function() {
		chart('cinemas_and_halls');
		chart('halls_by_period');
		graph('servers_graph');
		graph('projectors_graph');
		graph('type3d_graph');
		graph('owner_graph');

		window.app.ChartsLoadReady = true;
	};

	//--------------------      Other      --------------------//

	window.app.StatisticCountry = function() {
		if (!reqres.request('get:user').accessTo('statistic')) {
			return;
		}
		$.get('/api/start/country.json', function(r) {
			var i, html;
			for (i in r.country.list) {
				html = '<option value="' + r.country.list[i].id + '">' + r.country.list[i].title + '</option>';
				$('#filt_country').append(html).trigger('refresh');
			}
		}, 'json');
	};

	// Выбор страны
	// $('#filt_country').change(function(){
	// 	country_id = $(this).val();

	// 	if ( $('#stat').hasClass('active') ) {
	// 		window.app.StatisticLoad();
	// 		if (country_id !== '') {
	// 			$('.item_container.country').hide();
	// 		}
	// 		current_country_id_stat = country_id;
	// 	}
	// 	if ( $('#cinema').hasClass('active') ) {
	// 		$('.cinema_tab h2 span').text('');
	// 		var country_param;
	// 		window.app.App.StatisticsCinema.country_id = country_id;
	// 		window.app.App.StatisticsCinema.currentUrl == window.app.App.StatisticsCinema.baseUrl ? country_param='?country_id=' : country_param='&country_id=';
	// 		window.app.App.StatisticsCinema.urlfor = window.app.App.StatisticsCinema.currentUrl+country_param+country_id;
	// 		window.app.App.StatisticsCinema.fetch();
	// 		current_country_id_cinema = country_id;
	// 	}
	// 	if ( $('#chart').hasClass('active') ) {
	// 		window.app.ChartsLoad();
	// 		current_country_id_chart = country_id;
	// 	}
	// });

});

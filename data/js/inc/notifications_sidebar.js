$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.NotificationsSidebarView = Backbone.View.extend({
		el: '.nt-sidebar-toggle',
		events: {
			'click .nt-sidebar-toggle-icon': function(e) {
				var target = $(e.currentTarget).data('nt-sidebar');
				this.toggleSidebar(target);
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};

			var user = this.user = reqres.request('get:user'),
				userType     = user.get('type'),
				userApproved = user.isApproved(),
				userCinemas  = user.getCinemas();

			this.MyMessages = new window.app.MessagesView();
			this.MyNotifications = new window.app.NotificationsView();

			if (_.contains(['cinema', 'mechanic'], userType) && userApproved && userCinemas.length) {
				this.MyTrailers = new window.app.TrailersView();
			}

			if (userType === 'cinema' && userApproved && userCinemas.length) {
				this.MyUserUpdates = new window.app.UserUpdatesView();
			}

			this.listenTo(Backbone, 'open:notifications-sidebar', this.toggleSidebar);
		},
		toggleSidebar: /* istanbul ignore next */ function toggleSidebar(target, tab) {
			var $tab    = $('.nt-sidebar-item--' + target),
				$target = $('.nt-sidebar-toggle-icon[data-nt-sidebar="' + target + '"]');

			this.$el.find('.nt-sidebar-toggle-icon').removeClass('open');

			if ($tab.hasClass('open')) {
				$target.removeClass('open');
				$tab.removeClass('open');
			} else {
				$target.addClass('open');
				$('.nt-sidebar-item').removeClass('open');
				$tab.addClass('open');

				if ($target.data('nt-sidebar') === 'chat') {
					Backbone.trigger('chat-open');
					_gaq.push(['_trackEvent', 'Чат', 'Открытие чата']);
				} else {
					Backbone.trigger('chat-close');
				}
			}

			if (tab) this.MyNotifications.toggleFilter(tab);
		}
	});
});

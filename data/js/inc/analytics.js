$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.AnalyticsCollection = Backbone.Collection.extend({
		model: Backbone.Model,
		url: function url() {
			return this.url;
		},
		setFilter: function setFilter(filter) {
			this.currentFilter = filter;
			this.url = '/api/prediction/' + filter;
			return this;
		},
		parse: function parse(response) {
			_.each(response.list, function(release) {
				release.hireWeek = _.getWeekStartDate({week: _.getWeekNumber(moment().add(1, 'days'))}).diff(_.getWeekStartDate({isotime: release.date}), 'weeks') + 1;
				release.hireWeek = release.hireWeek < 1 ? '-' : release.hireWeek;
			});
			this.date = response.today ? moment.utc().add(1, 'days') : moment.utc();
			return response.list;
		}
	});

	window.app.AnalyticsView = Backbone.View.extend({
		className: 'analytics',
		template: '#analytics_wrapper_tmpl',
		tableTemplate: '#analytics_table_tmpl',
		events: {
			'change .b-filter': /* istanbul ignore next */ function(event) {
				this.loadAnalytics($(event.currentTarget).val());
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.currentFilter = this.options.filter || this.collection.currentFilter || 'all';
			this.listenTo(this.collection, 'sort', this.renderAnalyticsTable);
		},
		loadAnalytics: function loadAnalytics(filter) {
			if (this.collection.models.length && this.collection.currentFilter === filter) {
				this.renderAnalyticsTable();
			} else {
				this.collection.setFilter(filter).reset();
				Backbone.trigger('loader', 'show');
				this.collection.fetch({
					success: function() {
						Backbone.trigger('loader', 'hide');
					}
				});
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				approved = reqres.request('get:user').isApproved(),
				filter = [
					{
						title: 'Все',
						value: 'all'
					},
					{
						title: '1-зальные кинотеатры',
						value: 1
					},
					{
						title: '2-зальные кинотеатры',
						value: 2
					},
					{
						title: '3-зальные кинотеатры',
						value: 3
					},
					{
						title: '4-зальные кинотеатры',
						value: 4
					},
					{
						title: 'Более 5 залов',
						value: '5+'
					}
				];

			this.$el.html(template({
				filterOptions: filter,
				currentFilter: this.currentFilter,
				approved:      approved
			})).find('.b-filter').styler();

			if (approved) this.loadAnalytics(this.currentFilter);

			return this;
		},
		renderAnalyticsTable: function renderAnalyticsTable() {
			var template = Backbone.TemplateCache.get(this.tableTemplate);
			this.$('.analytics__date').text('Прогноз на ' + this.collection.date.format('DD MMMM YYYY'));
			this.$('.analytics-wrapper').empty().html(template({
				releases: this.collection.models
			}));
			return this;
		}
	});
});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.FeedbackPopup = Backbone.View.extend({
		template: '#feedback_popup_tmpl',
		ticketTemplate: _.template([
			'<p>{{- message }}</p>',
			'<p><a href="https://admin.kinoplan24.ru/user/{{- user.user_id }}/edit">{{- user.first_name }} {{- user.last_name }}</a><span>, {{- user.desc_type }}</span></p>',
			'<p><a href="mailto:{{- user.email }}">{{- user.email }}</a><span>, {{- user.phone }}</span></p>',
			'{{ if (cinemas.length) { }}',
				'<p>Кинотеарты:',
					'{{ if (cinemas.length > 1) { }}',
						'<ul> {{ _.each(cinemas, function(cinema) { }} <li>{{- cinema }}</li> {{ }) }} </ul>',
					'{{ } else { }} <span>{{- cinemas[0] }}</span> {{ } }}',
				'</p>',
			'{{ } }}'
		].join('')),
		events: {
			submit: 'beforeSubmit'
		},
		initialize: function initialize() {
			var self = this;

			$('#js-popup').html(this.render().el);

			/* istanbul ignore next */
			$('#js-popup').arcticmodal({
				overlay: {tpl: '<div class="arcticmodal-overlay" data-html2canvas-ignore></div>'},
				afterOpen: function() {
					self.$('.feedback-popup__message').focus();
				},
				afterClose: function() { self.remove(); }
			});
		},
		beforeSubmit: function beforeSubmit(event) {
			event.preventDefault();
			var self = this,
				withScreenshot = this.$('[name="screenshot"]:checked').length;

			if (!this.$('.feedback-popup__message').val() && !withScreenshot) {
				$.growl.error({message: 'Введите текст сообщения или добавьте скриншот страницы.'});
				return;
			}

			this.pending = true;
			setTimeout(function() {
				if (self.pending) {
					self.$el
						.find('.loader').show().end()
						.find('input, textarea, button').prop('disabled', true);
				}
			}, 150);

			if (withScreenshot) {
				html2canvas(document.body, {
					onrendered: function(canvas) {
						self.submit(canvas.toDataURL('image/png').substring(22));
					}
				});
			} else {
				this.submit();
			}
		},
		submit: function submit(screenshot) {
			var self = this,
				user = reqres.request('get:user').toJSON(),
				url  = (_.contains(['kinoplan24.ru', 'www.kinoplan24.ru', 'release.kinoplan24.ru', 'www.release.kinoplan24.ru'], _.getWindowLocationHost()) ?
					'https://' : 'http://dev.') +
					'servicedesk.dcp24.ru/api/ticket/servicevoice';

			$.post(url, JSON.stringify({
				email:   user.email,
				first_name: user.first_name,
				last_name: user.last_name,
				service: 'kinoplan',
				data:    this.ticketTemplate({
					user: user,
					message: this.$('.feedback-popup__message').val(),
					cinemas: _.map(user.cinema, function(cinema) {
						return (cinema.title.ru || cinema.title.en || '') + ', ' +
							(cinema.city.title.ru || cinema.city.title.en || '');
					})
				}).replace(/[\n\t]?\s{2,}/gi, ''),
				screenshot: screenshot || '',
				url: window.location.href
			}))
			.done(function(res) {
				self.pending = false;
				if (res.status === 'ok') {
					$.arcticmodal('close');
					$.growl.ok({message: 'Спасибо! Ваше сообщение отправлено.'});
				} else {
					$.growl.error({message: 'Ошибка соединения с сервером.'});
					self.$el.find('input, textarea, button').prop('disabled', false);
				}
			})
			.always(function() {
				self.$el.find('.loader').hide();
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());
			return this;
		},
		remove: function remove() {
			this.$el.empty();
			Backbone.View.prototype.remove.call(this);
		}
	});

});

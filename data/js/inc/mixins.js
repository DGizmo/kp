$(function() {
	'use strict';

	jQuery.expr[':'].containsLower = jQuery.expr.createPseudo(function(arg) {
		return function(elem) {
			var reg = new RegExp(arg.replace(/[её]/g, '(е|ё)'), 'i');
			return reg.test($(elem).text());
		};
	});

	function naturalSort(a, b) {
		var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi,
			sre = /(^[ ]*|[ ]*$)/g,
			dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
			hre = /^0x[0-9a-f]+$/i,
			ore = /^0/,
			i = function(s) { return naturalSort.insensitive && ('' + s).toLowerCase() || '' + s; },
			x = i(a).replace(sre, '') || '',
			y = i(b).replace(sre, '') || '',
			xN = x.replace(re, '\0$1\0').replace(/\0$/, '').replace(/^\0/, '').split('\0'),
			yN = y.replace(re, '\0$1\0').replace(/\0$/, '').replace(/^\0/, '').split('\0'),
			xD = parseInt(x.match(hre)) || (xN.length !== 1 && x.match(dre) && Date.parse(x)),
			yD = parseInt(y.match(hre)) || xD && y.match(dre) && Date.parse(y) || null,
			oFxNcL,
			oFyNcL,
			cLoc,
			numS;

		if (yD)
			if (xD < yD) return -1;
			else if (xD > yD) return 1;
		for (cLoc = 0, numS = Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
			oFxNcL = !(xN[cLoc] || '').match(ore) && parseFloat(xN[cLoc]) || xN[cLoc] || 0;
			oFyNcL = !(yN[cLoc] || '').match(ore) && parseFloat(yN[cLoc]) || yN[cLoc] || 0;
			if (isNaN(oFxNcL) !== isNaN(oFyNcL)) {
				return (isNaN(oFxNcL)) ? 1 : -1;
			} else if (typeof oFxNcL !== typeof oFyNcL) {
				oFxNcL += '';
				oFyNcL += '';
			}
			if (oFxNcL < oFyNcL) return -1;
			if (oFxNcL > oFyNcL) return 1;
		}
		return 0;
	}

	_.mixin({

		sortByNat: function(obj, value, context) {
			var iterator = _.isFunction(value) ? value : function(obj) { return obj[value]; };

			return _.pluck(_.map(obj, function(value, index, list) {
				return {
					value: value,
					index: index,
					criteria: iterator.call(context, value, index, list)
				};
			}).sort(function(left, right) {
				return naturalSort(left.criteria, right.criteria);
			}), 'value');
		},

		getWeekNumber: function(date) {
			return moment(date).weeks();
		},

		getWeekStartDate: function() {
			var args = arguments[0] || {},
				isotime = args.isotime || '',
				week = args.week || _.getWeekNumber(),
				year = args.year || new Date().getFullYear(),
				date;

			isotime ? date = moment(isotime).day(4) : date = moment(0, 'HH').year(year).day(4).week(week);

			if (args.type === 'date') date = date._d;
			return date;
		},

		getWeekEndDate: function() {
			var args = arguments[0] || {},
				isotime = args.isotime || '',
				week = args.week || _.getWeekNumber(),
				year = args.year || new Date().getFullYear(),
				date;

			date = _.getWeekStartDate({
				isotime: isotime,
				week: week,
				year: year,
				type: ''
			}).add(6, 'days');

			if (args.type === 'date') date = date._d;
			return date;
		},

		getWeekDates: function() {
			var args = arguments[0] || {},
				isotime = args.isotime || '',
				week = args.week || _.getWeekNumber(),
				year = args.year || new Date().getFullYear(),
				startDate,
				endDate;

			startDate = _.getWeekStartDate({
				isotime: isotime,
				week: week,
				year: year,
				type: ''
			});

			endDate = moment(startDate).add(6, 'days');

			if (args.type === 'date') {
				startDate = startDate._d;
				endDate = endDate._d;
			}

			return {
				start: startDate,
				end: endDate
			};
		},

		secondsToMinutes: function(seconds, format) {
			return moment().startOf('day').seconds(seconds).format(format);
		},

		declOfNum: function(number, titles) {
			var cases = [2, 0, 1, 1, 1, 2];
			return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
		},

		getScrollBarWidth: function() {
			var inner = document.createElement('p'),
				outer,
				w1,
				w2;

			inner.style.width = '100%';
			inner.style.height = '200px';

			outer = document.createElement('div');
			outer.style.position = 'absolute';
			outer.style.top = '0px';
			outer.style.left = '0px';
			outer.style.visibility = 'hidden';
			outer.style.width = '200px';
			outer.style.height = '150px';
			outer.style.overflow = 'hidden';
			outer.appendChild (inner);

			document.body.appendChild(outer);
			w1 = inner.offsetWidth;
			outer.style.overflow = 'scroll';
			w2 = inner.offsetWidth;
			if (w1 === w2) w2 = outer.clientWidth;
			document.body.removeChild (outer);

			return (w1 - w2);
		},

		formatBytes: function formatBytes(number) {
			if (typeof number !== 'number' || !isFinite(number)) {
				throw 'Must be number.';
			}

			var count = 0,
				unitArr = [' б', ' Кб', ' Мб', ' Гб', ' Тб', ' Пб', ' Эб', ' Зб', ' Йб'];

			while (number >= 1024) {
				number = number / 1024;
				count++;
			}

			return Math.floor(number) + unitArr[count];
		},

		isValidEmail: function isValidEmail(email) {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		},

		formatCurrency: function formatCurrency(value, currency) {
			if (!_.isFinite(value) || typeof value !== 'number') throw Error('Value must be a number');

			value = value.toFixed(0).replace(/(\d)(?=(\d{3})+?$)/g, '$1 ');

			switch (currency) {
				case 'usd': return '$' + value;
				case 'eur': return '&euro;' + value;
				case 'rub': return value + 'руб.';
				default: return value;
			}
		},

		getWindowLocationHost: function getWindowLocationHost() {
			return window.location.host;
		}

	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.Trailer = Backbone.Model.extend({
		defaults: {
			format: '',
			version: '',
			percent: 0
		},
		initialize: function initialize() {
			this.id = this.attributes.trailer_id;
		},
		url: function url() {
			return '/api/trailer/' + this.id;
		}
	});

	window.app.Trailers = Backbone.Collection.extend({
		model: window.app.Trailer,
		initialize: function initialize() {
			this.user = reqres.request('get:user');

			var currentCinemaID = this.user.getCinema().id;

			this.listenTo(Backbone, 'socket:trailer', function(data) {
				if (currentCinemaID && (data.cinema_id === currentCinemaID)) {
					var model = this.get(data.id);
					if (model) {
						if (data.delete_flag) {
							model.destroy();
						} else {
							model.fetch({data: {cinema_id: currentCinemaID}});
						}
					} else {
						if (!data.delete_flag) {
							this.add(data);
						}
					}
				}
			});
			this.listenTo(Backbone, 'trailer:download:start', function() {
				this.fetch({data: {cinema_id: currentCinemaID}});
			});
		},
		url: function url() {
			return '/api/trailer/?cinema_id=' + this.user.getCinema().id;
		},
		parse: function parse(res) {
			return res.list;
		}
	});

	window.app.TrailersView = Backbone.View.extend({
		className: 'nt-sidebar-item nt-sidebar-item--trailers',
		template: '#sb-trailers_box',
		toggleTemplate: _.template('<div class="nt-sidebar-toggle-icon icon-trailers" data-nt-sidebar="trailers" title="Информация о загрузках трейлеров на сервер DCP24."></div>'),
		initialize: function initialize() {
			$('.nt-sidebar').prepend(this.render().el);
			$('.nt-sidebar-toggle').prepend($(this.toggleTemplate()));

			this.trailers = new window.app.Trailers();

			this.listenTo(this.trailers, 'add', this.addOneTrailer);
			this.listenTo(this.trailers, 'reset', function(col, opts) {
				_.each(opts.previousModels, function(model) {
					model.trigger('remove');
				});
			});

			this.listenTo(Backbone, 'cinema-changed', this.loadTrailers);

			this.loadTrailers();
		},
		addOneTrailer: function addOneTrailer(trailer) {
			var View = new window.app.TrailerView({model: trailer});
			this.$el.find('#sb-trailers').prepend(View.render().el);
		},
		loadTrailers: function loadTrailers() {
			var self = this;

			this.trailers.reset();
			this.$el.find('.nt-sidebar-trailers-item').remove();
			this.$el.find('.nt-sidebar-trailers__empty').hide();
			this.$el.find('.loader').show();

			this.trailers.fetch({
				success: function(data) {
					self.$el.find('.loader').hide();
					if (!data.length) {
						self.$el.find('.nt-sidebar-trailers__empty').show();
					}
				}
			});

		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());
			return this;
		}
	});

	window.app.TrailerView = Backbone.View.extend({
		className: 'nt-sidebar-trailers-item-wraper',
		template: '#sb-trailer_item',
		events: {
			'click .nt-sidebar-trailers-item__name': 'showRelease',
			'click .nt-sidebar-trailers-item__control, .nt-sidebar-trailers-item__delete': 'clickControl',
			'mouseenter .nt-sidebar-trailers-item__control, .nt-sidebar-trailers-item__delete': 'mouseenterControl',
			'mouseleave .nt-sidebar-trailers-item__control, .nt-sidebar-trailers-item__delete': 'mouseleaveControl'
		},
		initialize: function initialize() {
			this.user = reqres.request('get:user');

			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.remove);
		},
		mouseenterControl: /* istanbul ignore next */  function mouseenterControl(e) {
			var action  = $(e.currentTarget).data('action'),
				$status = $(e.currentTarget).closest('.nt-sidebar-trailers-item').find('.nt-sidebar-trailers-item__status'),
				hoverStatus = {
					start:  'Возобновить загрузку',
					stop:   'Приостановить загрузку',
					delete: 'Удалить загрузку'
				};

			if (action === 'wait') {
				return false;
			}

			this.prevStatus = $status.html();
			$status.html(hoverStatus[action]);
		},
		mouseleaveControl: /* istanbul ignore next */ function mouseleaveControl(e) {
			var action  = $(e.currentTarget).data('action'),
				$status = $(e.currentTarget).closest('.nt-sidebar-trailers-item').find('.nt-sidebar-trailers-item__status');

			if (action === 'wait') return false;
			$status.html(this.prevStatus);
		},
		clickControl: /* istanbul ignore next */ function clickControl(e) {
			var self = this,
				action = $(e.currentTarget).data('action');

			if (!action) {
				return false;
			}

			_gaq.push(['_trackEvent', 'Загрузка трейлеров', 'Sidebar', action]);

			$.post('/api/trailer/do/' + action, {
				trailer_id: this.model.get('trailer_id'),
				node_id:    this.user.getNodeID()
			}).done(function() {
				if (action === 'delete') {
					self.model.trigger('destroy', self.model, self.model.collection);
					self.removeView();
				} else {
					self.model.fetch({data: {cinema_id: this.user.getCinema().id}});
				}
			});
		},
		showRelease: /* istanbul ignore next */ function showRelease(event) {
			event.preventDefault();
			Backbone.trigger('router:navigate', '/release/' + this.model.get('release_id'));
			_gaq.push(['_trackEvent', 'Загрузка трейлеров', 'Переход по ссылке']);
		},
		removeView: function removeView() {
			this.remove();
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				user = this.user;

			this.$el.html(template({
				model: this.model.attributes,
				downloadRights: user.isApproved() &&
					(user.hasFullAccess() || user.get('type') === 'mechanic' || user.accessTo('DD24'))
			}));

			return this;
		}
	});
});

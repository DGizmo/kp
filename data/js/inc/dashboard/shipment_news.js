$(function() {
	'use strict';
	window.app = window.app || {};

	window.app.DashShipmentNewsWrapView = Backbone.View.extend({
		template: '#dashboard_shipment_news_wr',
		events: {
			'click .all': 'showAllShipmentNews'
		},
		showAllShipmentNews: function showAllShipmentNews() {
			Backbone.trigger('open:notifications-sidebar', 'notifications', 'shipment');
		},

		initialize: function initialize() {
			this.subViews = [];
			this.render();

			this.listenTo(this.collection, 'sync', this.render);
		},
		render: function render() {
			var shipmentNews = _.last(this.collection.where({item: 'shipment'}), 5),
				template;

			_.invoke(this.subViews, 'remove');

			if (!this.collection.updated || !shipmentNews.length) return;

			template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template);

			this.subViews = _.map(shipmentNews, function(model) {
				var view = new window.app.DashOneShipmentNewsView({model: model});
				this.$el.find('.list').append(view.render().el);
				return view;
			}, this);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.DashOneShipmentNewsView = Backbone.View.extend({
		className: 'item',
		template: '#dashboard_shipment_news',
		initialize: function initialize() {
			this.listenTo(this.model, 'remove', this.remove);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				created = parseInt(this.model.get('created'), 10) * 1000,
				extModel = $.extend({}, this.model.attributes),
				todayTimestamp = moment().startOf('day').unix(),
				distributorsNames = _.pluck(this.model.get('distributors'), 'name');

			extModel.allDistributorsShort = _.pluck(distributorsNames, 'short').join(', ');
			extModel.allDistributorsFull  = _.pluck(distributorsNames, 'full').join(', ');
			extModel.dateText = moment(created).format(todayTimestamp < created ? 'HH:mm' : 'D MMM., HH:mm');

			this.$el.html(template(extModel));

			return this;
		}
	});

});

$(function() {
	'use strict';

	window.app = window.app || {};

	window.app.DashPemiereModel = Backbone.Model.extend({
		defaults: {
			title: '',
			key: false
		},
		parse: function parse(res) {
			var hasKey = function(flag) {
					if (flag.length) {
						return true;
					}
				},
				flags = _.values(res.kdm_flag);

			res.key = flags.length && _.every(flags, hasKey);
			return res;
		}
	});

	window.app.DashPremieresCollection = Backbone.Collection.extend({
		model: window.app.DashPemiereModel,
		initialize: function initialize(param) {
			if (param === 'next') {
				this.week = moment().add(7, 'days').week();
				this.year = moment().add(7, 'days').year();
			} else {
				this.week = moment().week();
				this.year = moment().year();
			}

			this.update();

			this.listenTo(Backbone, 'release:favourites', function(releaseStart) {
				if (this.week === releaseStart.week && this.year === releaseStart.year) this.update();
			});

			this.listenTo(Backbone, 'socket:favourite', this.update);
		},
		url: function url() {
			return '/api/releases/dashboard/?week=' + this.week + '&year=' + this.year;
		},
		parse: function parse(r) {
			return r.list;
		},
		update: function update() {
			var self = this;

			this.updated = false;

			this.fetch({
				success: function() {
					self.updated = true;
				}
			});
		}
	});

	window.app.DashPremiereView = Backbone.View.extend({
		className: 'line',
		template: '#dashboard_premiere',
		events: {
			click: function(e) {
				var addedHeight = this.$el.find('.item.info').height(),
					removedHeight = this.$el.height() - 21;

				if (!$(e.currentTarget).hasClass('open')) {
					this.$el.find('.arrow_down').addClass('arrow_up');

					this.$el.css('height', this.$el.height() + addedHeight);

					this.$el.addClass('open');
				} else {
					this.$el.find('.arrow_down').removeClass('arrow_up');

					this.$el.css('height', this.$el.height() - removedHeight);

					this.$el.removeClass('open');
				}
			},
			'mouseenter .item_copy, .item_kdm': function(e) {
				var leftPosition = $(e.currentTarget).offset().left + 33,
					topPosition = $(e.currentTarget).offset().top,
					$popup = $(e.currentTarget).find('.releases_popup');

				$popup.css({
					top: topPosition,
					left: leftPosition,
					display: 'block'
				});
			},
			'mouseleave .item_copy, .item_kdm': function(e) {
				var $popup = $(e.currentTarget).find('.releases_popup');
				$popup.css({display: 'none'});
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes),
				distributors = this.model.get('distributors'),
				allDistributorsShort = [],
				allDistributorsFull = [],
				i;

			for (i in distributors) {
				if (distributors[i].name.short) {
					allDistributorsShort.push(distributors[i].name.short);
				}
				if (distributors[i].name.full) {
					allDistributorsFull.push(distributors[i].name.full);
				}
			}

			extModel.allDistributorsShort = allDistributorsShort;
			extModel.allDistributorsFull = allDistributorsFull;
			extModel.userCinemas = reqres.request('get:user').getCinemas();

			this.$el.html(template(extModel));

			this.afterRender();

			return this;
		},
		afterRender: function afterRender() {
			var status = 'good';

			this.$el.find('.icon-key').each(function() {
				if ($(this).hasClass('bad') && !$(this).parent().hasClass('item_seance')) {
					status = 'bad';
					return false;
				}
			});

			if (status !== 'bad') {
				this.$el.find('.icon-key').each(function() {
					if ($(this).hasClass('delivery') && !$(this).parent().hasClass('item_seance')) {
						status = 'delivery';
						return false;
					}
				});
			}

			if (status === 'good') {
				this.$el.find('.item.key').html('<i class="ico active"></i>');
			} else if (status === 'delivery') {
				this.$el.find('.item.key').html('<i class="ico delivery"></i>');
			} else {
				this.$el.find('.item.key').html('<i class="ico"></i>');
			}
		}
	});

	window.app.DashPremieresCollectionView = Backbone.View.extend({
		template: '#dashboard_premieres_wr',
		initialize: function initialize() {
			this.subViews = [];
			this.render();

			this.listenTo(this.collection, 'sync', this.render);
		},
		render: function render() {
			if (this.collection.length && this.collection.updated) {
				this.$el.html(Backbone.TemplateCache.get(this.template)(this.collection));

				this.afterRender();
			}
		},
		afterRender: function afterRender() {
			var self = this;
			this.collection.each(function(premiere) {
				self.addPremiereView(premiere);
			});
		},
		addPremiereView: function addPremiereView(premiere) {
			var premiereView = new window.app.DashPremiereView({
				model: premiere
			});

			this.subViews.push(premiereView);

			this.$el.find('.body').append(premiereView.render().el);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

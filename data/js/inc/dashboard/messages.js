$(function() {
	'use strict';

	window.app = window.app || {};

	window.app.DashMessageView = Backbone.View.extend({
		className: 'item',
		template: '#message_item',
		initialize: function initialize() {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.removeView);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template({
				message: this.model.toJSON(),
				moderator: 0
			}));
			return this;
		}
	});

	window.app.DashMessagesWrapView = Backbone.View.extend({
		template: '#dashboard_messages_wr',
		events: {
			'click .all': 'showAllMessages'
		},
		initialize: function initialize() {
			this.subViews = [];
			this.render();

			this.listenTo(this.collection, 'sync', this.render);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				self     = this;

			if (this.collection.updated && this.collection.length) {
				this.$el.html(template);

				self.afterRender();
			}
		},
		afterRender: function afterRender() {
			var self = this;

			this.collection.each(function(message) {
				if (_.indexOf(self.collection.models, message) + 1 > self.collection.length - 5) {
					self.addMessageView(message);
				}
			});
		},
		addMessageView: function addMessageView(message) {
			var messageView = new window.app.DashMessageView({model: message});

			this.$el.find('.list').append(messageView.render().el);

			this.subViews.push(messageView);
		},
		showAllMessages: function showAllMessages() {
			Backbone.trigger('open:notifications-sidebar', 'chat');
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DashboardDistributorPrognosisCollection = Backbone.Collection.extend({
		model: Backbone.Model.extend(),
		url: function url() {
			return this.url;
		},
		setFilter: function setFilter(filter) {
			this.currentFilter = filter;
			this.url = '/api/prediction/' + filter;
			this.update();

			return this;
		},
		parse: function parse(response) {
			_.each(response.list, function(release) {
				release.hireWeek = _.getWeekStartDate({week: _.getWeekNumber(moment().add('days', 1))}).diff(_.getWeekStartDate({isotime: release.date}), 'weeks') + 1;
				release.hireWeek = release.hireWeek < 1 ? '-' : release.hireWeek;
			});
			this.date = response.today ? moment.utc().add('days', 1) : moment.utc();
			return response.list;
		},
		update: function update() {
			var self = this;

			this.updated = false;
			Backbone.trigger('loader', 'show');

			this.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
					self.updated = true;
				}
			});
		}
	});

	window.app.DashboardDistributorPrognosisCollectionView = Backbone.View.extend({
		className: 'analytics distributor_prognosis',
		template: '#distributor_prognosis',
		tableTemplate: '#analytics_table_tmpl',
		blockerTemplate: '#distributor_blocker',
		initialize: function initialize(options) {
			this.options = options || {};

			if (!this.options.collections.prognsisCollection && this.options.user.get('approved')) {
				this.options.collections.prognsisCollection = new window.app.DashboardDistributorPrognosisCollection();
			}

			this.collection = this.options.collections.prognsisCollection || new Backbone.Collection();

			if (this.options.user.get('approved')) {
				this.options.parentView.$('.jq-selectbox').show();
				this.collection.setFilter(this.options.parentView.$('.b-filter').val());

				this.listenTo(this.options.parentView, 'filter:change', function() {
					this.collection.setFilter(this.options.parentView.$('.b-filter').val());
				});

				this.listenTo(this.collection, 'sync', function() {
					this.options.parentWrap.html(this.render().el);
				});

				this.listenTo(this.collection, 'sort', this.renderAnalyticsTable);
			} else {
				this.options.parentWrap.html(this.render().el);
			}
		},
		render: function render() {
			var template;

			if (this.options.user.get('approved')) {
				template = Backbone.TemplateCache.get(this.template);
			} else {
				template = Backbone.TemplateCache.get(this.blockerTemplate);
			}

			this.$el.html(template);

			if (this.options.user.get('approved')) this.renderAnalyticsTable();

			return this;
		},
		renderAnalyticsTable: function renderAnalyticsTable() {
			var template = Backbone.TemplateCache.get(this.tableTemplate);

			this.$('.analytics-wrapper').empty().html('<span class="prognosis_date">' + this.collection.date.format('DD MMMM YYYY') + '</span>');
			this.$('.analytics-wrapper .prognosis_date').after(template({
				releases: this.collection.models
			}));
			return this;
		}
	});

});

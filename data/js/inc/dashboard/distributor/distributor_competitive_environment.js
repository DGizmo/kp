$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DashboardDistributorCompetitiveEnvironmentModel = Backbone.Model.extend({
		initialize: function initialize(options) {
			this.options = options || {};
			this.update();
			this.listenTo(Backbone, 'dashboard-settings:save:done', this.update);
		},
		url: function url() {
			return '/api/distributors/releases';
		},
		parse: function parse(r) {
			r.distributor_id = this.options.user.get('distributor_id');
			return r;
		},
		update: function update() {
			var self = this;

			this.updated = false;
			Backbone.trigger('loader', 'show');

			this.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
					self.updated = true;
				}
			});
		}
	});

	window.app.DashboardDistributorCompetitiveEnvironmentView = Backbone.View.extend({
		template: '#distributor_competitive_environment',
		blockerTemplate: '#distributor_blocker',
		events: {
			'click .item': function(event) {
				var releaseID = $(event.currentTarget).data('id');
				Backbone.trigger('router:navigate', '/release/' + releaseID);
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};

			if (!this.options.collections.competitiveEnvironmentModel && this.options.user.isApproved()) {
				this.options.collections.competitiveEnvironmentModel = new window.app.DashboardDistributorCompetitiveEnvironmentModel({user: this.options.user});
			}

			this.model = this.options.collections.competitiveEnvironmentModel || new Backbone.Model();

			if (this.model.updated || !this.options.user.isApproved()) this.options.parentWrap.html(this.render().el);

			this.listenTo(this.model, 'sync', function() {
				this.options.parentWrap.html(this.render().el);
			});
		},
		render: function render() {
			var self = this,
				template;

			if (this.options.user.isApproved()) {
				template = Backbone.TemplateCache.get(this.template);
			} else {
				template = Backbone.TemplateCache.get(this.blockerTemplate);
			}

			this.$el.html(template(this.model.attributes));

			setTimeout(function() {
				self.afterRender();
			}, 0);

			return this;
		},
		afterRender: function afterRender() {
			var $wrap = this.$el;

			$wrap.find('.line').each(function() {
				var $oneLine = $(this),
					height = $oneLine.height();

				$oneLine.find('.section').height(height - 32);

				$oneLine.find('.section').each(function() {
					var $section = $(this);

					$section.find('.match').each(function() {
						$section.find('.date').after($(this));
					});
				});
			});
		}
	});
});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DashboardDistributorHistoryCollection = Backbone.Collection.extend({
		model: Backbone.Model.extend(),
		initialize: function initialize() {
			this.update();

			this.listenTo(Backbone, 'dashboard-settings:save:done', this.update);
		},
		url: function url() {
			return '/api/distributors/history';
		},
		parse: function parse(r) {
			return r.list;
		},
		update: function update() {
			var self = this;

			this.updated = false;
			Backbone.trigger('loader', 'show');

			this.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
					self.updated = true;
				}
			});
		}
	});

	window.app.DashboardDistributorHistoryModelView = Backbone.View.extend({
		tagName:   'tr',
		className: 'table_item history_item',
		template:  '#distributor_history_item',
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});

	window.app.DashboardDistributorHistoryCollectionView = Backbone.View.extend({
		className: 'distributor_history',
		template: '#distributor_history',
		initialize: function initialize(options) {
			this.options = options || {};

			if (!this.options.collections.historyCollection) {
				this.options.collections.historyCollection = new window.app.DashboardDistributorHistoryCollection();
			}

			this.collection = this.options.collections.historyCollection;

			if (this.collection.updated) this.options.parentWrap.html(this.render().el);

			this.listenTo(this.collection, 'sync', function() {
				this.options.parentWrap.html(this.render().el);
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				userName = reqres.request('get:user').get('name');

			this.$el.html(template({
				length:   this.collection.length,
				userName: userName && userName.short || userName.full
			}));

			this.collection.each(function(historyItem) {
				var historyItemView = new window.app.DashboardDistributorHistoryModelView({model: historyItem});
				this.$el.find('.history_table tbody').append(historyItemView.render().el);
			}, this);

			return this;
		}
	});

});

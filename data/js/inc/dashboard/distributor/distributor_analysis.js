$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DashboardDistributorAnalysisCollection = Backbone.Collection.extend({
		model: Backbone.Model.extend(),
		initialize: function initialize(models, options) {
			this.options = options || {};

			if (this.options.releaseId) {
				this.releaseId = this.options.releaseId;
			}

			this.update();

			this.listenTo(this.options.user, 'change:partners change:competitors', this.update);
		},
		url: function url() {
			if (this.releaseId) {
				return '/api/release/' + this.releaseId + '/analysis';
			} else {
				return '/api/distributors/analysis';
			}
		},
		parse: function parse(r) {
			var self = this;

			r.list            = r.list || [];
			this.totalPercent = 0;
			this.totalNumber  = 0;
			this.total        = 0;

			if (r.list.length) {
				this.weeks = r.list[0].weeks;
			}

			_.each(r.list, function(release) {
				if (release.all_count > 0) {
					self.total = self.total + release.all_count;
				}

				release.weeks = release.weeks.sort(function(a, b) { return a.year - b.year; });

				for (var i in release.weeks) {
					release.weeks[i].start = _.getWeekStartDate({
						week: release.weeks[i].week,
						year: release.weeks[i].year
					});
					release.weeks[i].end = moment(release.weeks[i].start).add(6, 'days');
				}

				release.hireWeek = moment(_.getWeekStartDate({type: 'date'})).diff(_.getWeekStartDate({
					week: release.date.week,
					year: release.date.year,
					type: 'date'
				}), 'week');
				release.hireWeek++;
				release.hireWeek = release.hireWeek <= 0 ? 0 : release.hireWeek;

				release.distributorsList = _.map(release.distributors, function(distributor) { return distributor.name.short || distributor.name.full; }).join(', ');
			});

			if (r.list.length) {
				r.list = r.list.sort(function(a, b) { return b.all_count - a.all_count; });
			}

			return r.list;
		},
		update: function update() {
			var self = this;

			this.updated = false;
			Backbone.trigger('loader', 'show');

			this.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
					self.updated = true;
				}
			});
		}
	});

	window.app.DashboardDistributorAnalysisModelView = Backbone.View.extend({
		tagName:   'tr',
		className: 'table_item analysis_item',
		template:  '#distributor_analysis_item',
		events: {
			click: function(event) {
				var releaseID = $(event.currentTarget).attr('release_id');
				Backbone.trigger('router:navigate', '/release/' + releaseID + '/analysis');
			}
		},
		initialize: function initialize() {
			this.listenTo(this.model.collection, 'sort', this.remove);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model));
			return this;
		}
	});

	window.app.DashboardDistributorAnalysisCollectionView = Backbone.View.extend({
		className: 'distributor_analysis',
		template: '#distributor_analysis',
		blockerTemplate: '#distributor_blocker',
		events: {
			'click .sorted_item': function(e) {
				var self = this;

				self.collection.totalNumber = 0;
				self.collection.totalPercent = 0;

				if ($(e.currentTarget).hasClass('hire_week')) {
					this.collection.comparator = function(release) { return -release.get('hireWeek'); };

					this.collection.sort();
					this.render();

					setTimeout(function() {
						self.$el.find('.triangle_down').css('opacity', '0');
						self.$el.find('.sorted_item.hire_week').next().css('opacity', '1');
					});
				} else if ($(e.currentTarget).hasClass('summ')) {
					this.collection.comparator = function(release) { return -release.get('all_count'); };

					this.collection.sort();
					this.render();

					setTimeout(function() {
						self.$el.find('.triangle_down').css('opacity', '0');
						self.$el.find('.sorted_item.summ').next().css('opacity', '1');
					});
				}
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.options.user = reqres.request('get:user');

			if (this.options.user.isApproved()) {
				this.options.collections.analysisCollection = new window.app.DashboardDistributorAnalysisCollection([], {
					user: this.options.user,
					releaseId: this.options.releaseId
				});
			} else {
				this.options.parentWrap.html(Backbone.TemplateCache.get(this.blockerTemplate)());
				return;
			}

			this.collection = this.options.collections.analysisCollection || new Backbone.Collection();

			this.listenTo(this.collection, 'sync', function() {
				this.collection.my = [].concat(this.options.user.get('partners'), this.options.user.get('distributor_id'));
				this.options.parentWrap.html(this.render().el);
			});
		},
		render: function render() {
			var self = this,
				template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template(this.collection));

			this.collection.each(function(analysisItem) {
				if ((analysisItem.get('all_count') / self.collection.total) >= 0.01 || self.collection.release_id === analysisItem.id) {
					var analysisItemView = new window.app.DashboardDistributorAnalysisModelView({model: analysisItem});

					self.$el.find('.analysis-table tbody').append(analysisItemView.render().el);

					analysisItemView.$el.attr('release_id', analysisItem.id);

					if (_.some(_.map(analysisItem.get('distributors'), function(distributor) { return distributor.id; }), function(id) { return _.contains(self.collection.my, id); }) || self.collection.release_id === analysisItem.id) {
						analysisItemView.$el.addClass('analysis-table--current-release');
					}
				}
			});

			setTimeout(function() {
				if (self.options.user.isApproved()) self.afterRender();
			}, 0);

			return this;
		},
		afterRender: function afterRender() {
			this.$el.find('.analysis-table tbody').append('<tr class ="table_item"><td colspan="10">В анализе учтены релизы, доля которых в прокате больше 1%</td></tr>');
			this.$el.find('.analysis-table tbody tr:last').append('<td>' + this.collection.totalPercent.toFixed(1) + '%');
			this.$el.find('.sorted_item.summ').next().css('opacity', '1');
		}
	});

});

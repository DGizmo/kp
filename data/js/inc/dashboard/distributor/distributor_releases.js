$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DashboardDistributorReleasesCollection = Backbone.Collection.extend({
		model: window.app.Release,
		initialize: function initialize(options) {
			var self = this,
				release;

			this.options = options || {};
			this.releases_filter = 'distributor';
			this.distributorName = this.options.user.get('name').full || this.options.user.get('name').short;
			this.update();

			this.listenTo(Backbone, 'socket:releases:update', function(data) {
				_.each(data.release_id, function(relId) {
					release = self.get(relId);

					if (release) release.fetch();
				});
			});
		},
		url: function url() {
			return '/api/releases/distributor/' + this.options.user.get('distributor_id');
		},
		parse: function parse(r) {
			return r.list;
		},
		update: function update() {
			var self = this;

			this.updated = false;
			Backbone.trigger('loader', 'show');

			this.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
					self.sort();
					self.updated = true;
				}
			});
		},
		comparator: function comparator(release) {
			if (release.get('date').russia.start) {
				return parseInt(release.get('date').russia.start.replace(/-/g, ''));
			}
		}
	});

	window.app.DashboardDistributorReleaseModelView = Backbone.View.extend({
		tagName:   'tr',
		className: 'table_item history_item',
		template:  '#distributor_history_item',
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});

	window.app.DashboardDistributorReleasesCollectionView = Backbone.View.extend({
		id: 'main_wr',
		className: 'release-wrapper',
		template: '#releases_tmpl',
		initialize: function initialize(options) {
			this.options = options || {};

			if (!this.options.collections.releasesCollection) {
				this.options.collections.releasesCollection = new window.app.DashboardDistributorReleasesCollection({user: this.options.user});
			}

			this.collection = this.options.collections.releasesCollection;

			if (this.collection.updated) this.options.parentWrap.html(this.render().el);

			this.listenTo(this.collection, 'sync', function() {
				this.options.parentWrap.html(this.render().el);
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				self     = this;

			this.$el.html(template(this.collection));

			this.collection.each(function(release) {
				var releaseView = new window.app.ReleaseView({model: release});
				self.$el.find('.releases_body').append(releaseView.render().el);
			});

			return this;
		}
	});

});

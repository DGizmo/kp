$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DistributorDashboardSettings = Backbone.View.extend({
		className: 'distributor-settings',
		template:  '#distributor_settings',
		distributorTemplate: '#distributor_settings_item',
		events: {
			'click .popupWrap__button': 'saveSettings',
			'change select': function(e) {
				var selectorValue = $(e.currentTarget).val();

				$(e.currentTarget).closest('td').prev().removeClass().addClass(selectorValue);
			}
		},
		initialize: function initialize(options) {
			this.options = options;
			var self = this;

			this.listenTo(this.collection, 'sort', this.renderDistributors);

			$('body').prepend(self.render().el);
			self.$el.arcticmodal({
				afterClose: function() {
					self.remove();
				}
			});

			if (!this.collection.models.length) {
				this.$el.find('.loader').show();
				self.$el.find('.loader').show();
				this.collection.fetch({
					success: function() {
						self.$el.find('.loader').hide();
					}
				});
			} else {
				this.renderDistributors();
			}
		},
		saveSettings: function saveSettings(event) {
			event.preventDefault();
			var $partners    = this.$('.distributor-settings-table select').filter(function(index, elem) { return $(elem).val() === 'partner'; }),
				$competitors = this.$('.distributor-settings-table select').filter(function(index, elem) { return $(elem).val() === 'competitor'; }),
				partners     = _.map($partners,    function(item) { return $(item).data('id'); }),
				competitors  = _.map($competitors, function(item) { return $(item).data('id'); });

			this.options.user.set({
				partners: partners,
				competitors: competitors
			});

			$.post('/api/distributors/edit', {'partners[]': partners, 'competitors[]': competitors}).done(function() {
				$('.distributor_releases').remove();
				Backbone.trigger('loader', 'show');

				Backbone.trigger('dashboard-settings:save:done');

				$('.dashboard_sections .item').first().trigger('click');
			});

			this.$el.arcticmodal('close');
		},
		renderDistributors: function renderDistributors() {
			var template = Backbone.TemplateCache.get(this.distributorTemplate),
				self = this;
			this.$el.find('tbody').html(template({
				distributors: _.filter(this.collection.models, function(distributor) { return distributor.get('id') !== self.options.user.get('distributor_id'); }),
				competitors: this.options.user.get('competitors'),
				partners: this.options.user.get('partners')
			}));
			return this;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template);
			return this;
		}
	});

});

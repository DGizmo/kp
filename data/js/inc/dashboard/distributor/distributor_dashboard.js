$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.DashboardDistributorWrapView = Backbone.View.extend({
		id:        'dashboard_distributor',
		className: 'content',
		template:  '#distributor_dashboard',
		currentView: null,
		currentSection: 'releases',
		currentFilter: 'all',
		events: {
			'click .dashboard_sections .item': function(e) {
				this.$('.dashboard_sections .item').removeClass('active');
				this.$('.jq-selectbox').hide();
				$(e.currentTarget).addClass('active');
				this.currentSection = $(e.currentTarget).data('section');
			},
			'click .dashboard_sections .item[data-section="competitive_environment"]': function() {
				this.switchView(new window.app.DashboardDistributorCompetitiveEnvironmentView({
					parentWrap:  this.$('.distributor_dashboard_wrap'),
					user:        this.options.user,
					collections: this.options.collections
				}));
			},
			'click .dashboard_sections .item[data-section="analysis"]': function() {
				this.switchView(new window.app.DashboardDistributorAnalysisCollectionView({
					parentWrap:  this.$('.distributor_dashboard_wrap'),
					collections: this.options.collections
				}));
			},
			'click .dashboard_sections .item[data-section="history"]': function() {
				this.switchView(new window.app.DashboardDistributorHistoryCollectionView({
					parentWrap:  this.$('.distributor_dashboard_wrap'),
					collections: this.options.collections
				}));
			},
			'click .dashboard_sections .item[data-section="releases"]': function() {
				this.switchView(new window.app.DashboardDistributorReleasesCollectionView({
					parentWrap:  this.$('.distributor_dashboard_wrap'),
					user:        this.options.user,
					collections: this.options.collections
				}));
			},
			'click .dashboard_sections .item[data-section="prognosis"]': function() {
				this.switchView(new window.app.DashboardDistributorPrognosisCollectionView({
					parentWrap:  this.$('.distributor_dashboard_wrap'),
					user:        this.options.user,
					collections: this.options.collections,
					parentView:  this,
					filter:      this.$('.b-filter').val()
				}));
			},
			'change .b-filter': /* istanbul ignore next */ function(event) {
				this.trigger('filter:change', $(event.currentTarget).val());
				this.currentFilter = $(event.currentTarget).val();
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.options.user = reqres.request('get:user');
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				filter   = [
					{
						title: 'Все',
						value: 'all'
					},
					{
						title: '1-зальные кинотеатры',
						value: 1
					},
					{
						title: '2-зальные кинотеатры',
						value: 2
					},
					{
						title: '3-зальные кинотеатры',
						value: 3
					},
					{
						title: '4-зальные кинотеатры',
						value: 4
					},
					{
						title: 'Более 5 залов',
						value: '5+'
					}
				];
			this.$el.html(template({
				filterOptions: filter,
				currentFilter: this.currentFilter
			})).find('.b-filter').styler();
			this.afterRender();
			return this;
		},
		afterRender: function afterRender() {
			this.$('.dashboard_sections .item[data-section="' + this.currentSection + '"]').trigger('click');
			this.$('.jq-selectbox').hide();
		},
		switchView: function switchView(view) {
			if (this.currentView !== null && this.currentView.cid !== view.cid) {
				this.currentView.remove();
			}

			this.currentView = view;
		}
	});
});

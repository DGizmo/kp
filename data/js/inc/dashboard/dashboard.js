$(function() {
	'use strict';

	window.app = window.app || {};

	window.app.DashView = Backbone.View.extend({
		id: 'dashboard',
		template: '#dashboard_wrapper',
		events: {
			'click .send_to_releases': 'sendToReleases'
		},
		initialize: function initialize(options) {
			this.options        = options || {};
			this.subViews       = [];
			this.allowPremieres = true;

			var user = reqres.request('get:user');

			if (user.isMechanic() && !_.some(user.getCinemas(), function(cinema) {
					return !user.isInSandbox(cinema.id);
				})) {

				this.allowPremieres = false;
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({currentWeek: moment().weeks(), allowPremieres: this.allowPremieres}));
			this.afterRender();
			return this;
		},
		afterRender: function afterRender() {
			if (this.allowPremieres) {
				if (!this.options.collections.currentPremieresCollection) {
					this.options.collections.currentPremieresCollection = new window.app.DashPremieresCollection('current');
					this.options.collections.nextPremieresCollection    = new window.app.DashPremieresCollection('next');
				}

				this.currentPremieresCollection  = this.options.collections.currentPremieresCollection;
				this.nextPremieresCollection = this.options.collections.nextPremieresCollection;
			}

			this.createSubViews();

			var allCollections = _.chain(this.subViews)
				.filter(function(view) { return (view.collection && !view.collection.updated); })
				.map(function(view) { return view.collection; }).value(),
				loaderHide = _.after(allCollections.length, function() {
					Backbone.trigger('loader', 'hide');
				});

			if (allCollections.length) {
				Backbone.trigger('loader', 'show');
				_.each(allCollections, function(collection) {
					this.listenToOnce(collection, 'sync', loaderHide);
				}, this);
			}
		},
		createSubViews: function createSubViews() {
			if (this.allowPremieres) {
				this.subViews.push(new window.app.DashPremieresCollectionView({
					collection: this.currentPremieresCollection,
					el:         this.$el.find('.premieres.current')
				}));

				this.subViews.push(new window.app.DashPremieresCollectionView({
					collection: this.nextPremieresCollection,
					el:         this.$el.find('.premieres.next')
				}));
			}

			this.subViews.push(new window.app.DashReleasesNewsWrapView({
				collection: this.options.notifications.MyNotifications.notifications,
				el:         this.$el.find('.releases_news')
			}));

			this.subViews.push(new window.app.DashKeysNewsWrapView({
				collection: this.options.notifications.MyNotifications.notifications,
				el:         this.$el.find('.kdm_news')
			}));

			this.subViews.push(new window.app.DashShipmentNewsWrapView({
				collection: this.options.notifications.MyNotifications.notifications,
				el:         this.$el.find('.shipment_news')
			}));

			this.subViews.push(new window.app.DashMessagesWrapView({
				collection: this.options.notifications.MyMessages.messages,
				el:         this.$el.find('.messages')
			}));

			this.subViews.push(new window.app.DashCinemasView({
				el: this.$el.find('.cinemas')
			}));
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		},
		sendToReleases: function sendToReleases() {
			Backbone.trigger('router:navigate', '/releases');
		}
	});
});

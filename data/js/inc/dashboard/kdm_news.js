$(function() {
	'use strict';

	window.app = window.app || {};

	window.app.DashKeyNewsView = Backbone.View.extend({
		className: 'item',
		template: '#dashboard_key_news',
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes),
				now = moment().startOf('day').format('X'),
				created = this.model.get('created'),
				distributors = this.model.get('distributors'),
				dateText = (now < created) ? Date.create(created * 1000).format('{HH}:{mm}') : Date.create(created * 1000).format('{dd} {mon}., {HH}:{mm}'),
				allDistributorsShort = [],
				allDistributorsFull = [],
				i;

			for (i in distributors) {
				if (distributors[i].name.short) {
					allDistributorsShort.push(distributors[i].name.short);
				}
				if (distributors[i].name.full) {
					allDistributorsFull.push(distributors[i].name.full);
				}
			}

			extModel.dateText = dateText;
			extModel.allDistributorsShort = allDistributorsShort;
			extModel.allDistributorsFull = allDistributorsFull;

			this.$el.html(template(extModel));

			return this;
		}
	});

	window.app.DashKeysNewsWrapView = Backbone.View.extend({
		template: '#dashboard_kdm_news_wr',
		events: {
			'click .all': 'showAllKeysNews'
		},
		initialize: function initialize() {
			this.subViews = [];
			this.render();

			this.listenTo(this.collection, 'sync', this.render);
		},
		render: function render() {
			if (this.collection.updated && this.filterKeysNews().length) {
				this.$el.html(Backbone.TemplateCache.get(this.template));

				this.afterRender();
			}
		},
		afterRender: function afterRender() {
			var self     = this,
				keysNews = this.filterKeysNews();

			keysNews.each(function(key) {
				if (_.indexOf(keysNews, key) + 1 > keysNews.length - 5) {
					self.addKeyNewsView(key);
				}
			});
		},
		filterKeysNews: function filterKeysNews() {
			var keysNews = _.filter(this.collection.models, function(oneModel) {
				return oneModel.get('item') === 'kdm';
			});

			return keysNews;
		},
		addKeyNewsView: function addKeyNewsView(key) {
			var keyNewsView = new window.app.DashKeyNewsView({model: key});

			this.$el.find('.list').append(keyNewsView.render().el);

			this.subViews.push(keyNewsView);
		},
		showAllKeysNews: function showAllKeysNews() {
			Backbone.trigger('open:notifications-sidebar', 'notifications', 'kdm');
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

$(function() {
	'use strict';

	window.app = window.app || {};

	window.app.DashReleaseNewsView = Backbone.View.extend({
		className: 'item',
		template: '#dashboard_release_news',
		events: {
			click: function() {
				Backbone.trigger('router:navigate', '/release/' + this.$el.data('release_id'));
			}
		},
		initialize: function initialize() {
			this.listenTo(this.model, 'remove', this.removeView);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				extModel = $.extend({}, this.model.attributes),
				now = moment().startOf('day').format('X'),
				created = this.model.get('created'),
				distributors = this.model.get('distributors'),
				dateText = (now < created) ? Date.create(created * 1000).format('{HH}:{mm}') : Date.create(created * 1000).format('{dd} {mon}., {HH}:{mm}'),
				allDistributorsShort = [],
				allDistributorsFull = [],
				i;

			for (i in distributors) {
				if (distributors[i].name.short) {
					allDistributorsShort.push(distributors[i].name.short);
				}
				if (distributors[i].name.full) {
					allDistributorsFull.push(distributors[i].name.full);
				}
			}

			extModel.dateText = dateText;
			extModel.allDistributorsShort = allDistributorsShort;
			extModel.allDistributorsFull = allDistributorsFull;

			this.$el.html(template(extModel));

			return this;
		}
	});

	window.app.DashReleasesNewsWrapView = Backbone.View.extend({
		template: '#dashboard_releases_news_wr',
		events: {
			'click .all': 'showAllReleasesNews'
		},
		initialize: function initialize() {
			this.subViews = [];
			this.render();

			this.listenTo(this.collection, 'sync', this.render);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				self     = this;

			if (this.collection.updated && this.filterReleasesNews().length) {
				this.$el.html(template);

				self.afterRender();
			}
		},
		afterRender: function afterRender() {
			var self         = this,
				releasesNews = this.filterReleasesNews();

			releasesNews.each(function(release) {
				if (_.indexOf(releasesNews, release) + 1 > releasesNews.length - 5) {
					self.addReleaseNewsView(release);
				}
			});
		},
		filterReleasesNews: function filterReleasesNews() {
			var releasesNews = _.filter(this.collection.models, function(oneModel) {
				return oneModel.get('item') === 'release';
			});

			return releasesNews;
		},
		addReleaseNewsView: function addReleaseNewsView(release) {
			var releaseNewsView = new window.app.DashReleaseNewsView({
				model: release,
				attributes: {
					'data-release_id': release.get('item_id')
				}
			});

			this.$el.find('.list').append(releaseNewsView.render().el);

			this.subViews.push(releaseNewsView);
		},
		showAllReleasesNews: function showAllReleasesNews() {
			Backbone.trigger('open:notifications-sidebar', 'notifications', 'release');
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

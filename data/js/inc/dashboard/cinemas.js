$(function() {
	'use strict';
	window.app = window.app || {};

	window.app.DashCinemasView = Backbone.View.extend({
		template: '#dashboard_cinemas',
		events: {
			'click .more': function() {
				commands.execute('show:feedback-popup');
			}
		},
		initialize: function initialize() {
			this.render();
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template({cinemas: reqres.request('get:user').getCinemas()}));
		}
	});

});

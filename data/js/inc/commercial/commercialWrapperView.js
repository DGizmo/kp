$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.Commercial = Backbone.Model.extend({
		defaults: {
			title: '',
			date: {
				start: moment().add(1, 'w').weekday(0),
				end:   moment(moment().add(1, 'w').weekday(0)).add(7, 'd')
			},
			duration: null,
			halls: [],
			day_time: [],
			position: {
				format: [],
				age: []
			},
			cinema_id: null
		},
		initialize: function initialize() {
			this.set('cinema_id', reqres.request('get:user').getCinema().id, {silent: true});
		},
		validate: function(attr) {
			var errors = [];

			if (!attr.title.length) {
				errors.push({
					attr: 'title',
					message: 'Укажите название кампании'
				});
			}

			if (!parseInt(attr.duration)) {
				errors.push({
					attr: 'duration',
					message: 'Укажите продолжительность ролика'
				});
			}

			if (!(attr.halls.length)) {
				errors.push({
					attr: 'halls',
					message: 'Укажите, в каких залах будет проходить рекламная кампания'
				});
			}

			if (_.some(_.pluck(attr.day_time, 'period'), function(p) { return _.isEmpty(p); })) {
				errors.push({
					attr: 'day_time',
					message: 'Укажите, в какое время будет проходить рекламная кампания'
				});
			}

			if (!attr.position.age && !attr.position.format) {
				errors.push({
					attr: 'position',
					message: 'Укажите, формат релизов и возрастную аудиторию'
				});
			}

			if (errors.length) return errors;
		},
		url: function url() {
			return Backbone.Model.prototype.url.call(this) + '?cinema_id=' + this.collection.cinemaID;
		}
	});

	window.app.Commercials = Backbone.Collection.extend({
		model: window.app.Commercial,
		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {
				url: '/api/commercials/'
			});
		},
		url: function url() {
			return this.options.url;
		},
		parse: function parse(res) {
			res = res.sort(function(a, b) {
				return +moment(a.date) - +moment(b.date);
			});
			return res;
		},
		fetch: function fetch(options) {
			this.cinemaID = reqres.request('get:user').getCinema().id;

			options = _.defaults(options || {}, {
				reset: true,
				data: { cinema_id: this.cinemaID }
			});

			return Backbone.Collection.prototype.fetch.call(this, options);
		}
	});

	window.app.CommercialWrapperView = Backbone.View.extend({
		className: 'commercial',
		templates: {
			wrapper: '#commercial_wrapper',
			empty: _.template([
				'<div class="commercial-empty">',
					'<div class="commercial-empty-center">',
						'<span class="commercial-empty__warning">Данные о рекламных кампаниях отсутствуют.</span>',
						'<span class="commercial-empty__link">Добавить рекламную кампанию</span>',
					'</div>',
				'</div>'
			].join(''))
		},
		ui: {},
		events: {
			'click .commercial-tab, .commercial-panel__add, .commercial-table__add': function(e) {
				var section = $(e.currentTarget).hasClass('commercial-tab') ? 'campaigns' : 'new';
				this.openSection(section);
			},
			'click .commercial-panel__save': function() {
				Backbone.trigger('commercial:save');
			},
			'click .commercial-empty__link': function() {
				this.renderWrapper().openSection('new');
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.currentView = null;

			if (!reqres.request('get:user').isApproved()) {
				this.$el.html(Backbone.TemplateCache.get('#unapprovedTemplate')({accessTo: 'Коммерческой рекламе'}));
				return false;
			}

			this.listenTo(Backbone, 'cinema-changed', this.fetchCollection);
			this.listenTo(Backbone, 'commercials:open-section', this.openSection);
			this.listenTo(Backbone, 'commercials:empty-section', function() {
				this.$el.html(this.templates.empty());
			});

			this.fetchCollection();
		},
		fetchCollection: function fetchCollection() {
			if (!this.collection.length || this.collection.cinemaID !== reqres.request('get:user').getCinema().id) {
				Backbone.trigger('loader', 'show');

				this.collection.fetch({
					success: _.bind(function() {
						this.renderView();
					}, this),
					error: function() {
						Backbone.trigger('loader', 'hide');
					}
				});
			} else {
				this.renderView();
			}
		},
		renderView: function renderView() {
			if (this.collection.length || this.options.page === 'new') {
				this.renderWrapper().openSection();
			} else {
				this.$el.html(this.templates.empty());
			}

			Backbone.trigger('loader', 'hide');

			return this;
		},
		renderWrapper: function renderWrapper() {
			var template = Backbone.TemplateCache.get(this.templates.wrapper);
			this.$el.html(template());
			this.ui.wrapper = this.$el.children('.commercial-wrapper');
			return this;
		},
		_switchView: function _switchView(view) {
			if (this.currentView !== null && this.currentView.cid !== view.cid) this.currentView.remove();
			this.currentView = view;
			this.ui.wrapper.html(view.render().el);
		},
		openSection: function openSection(section, model) {
			var view;
			switch (section || this.options.page || 'campaigns') {
				case 'campaigns':
					view = new window.app.CommercialСampaignListView({
						collection: this.collection,
						parent: this
					});
				break;
				case 'campaign':
					view = new window.app.CommercialNewView({
						model: model,
						parent: this
					});
				break;
				case 'new':
					view = new window.app.CommercialNewView({
						model: new window.app.Commercial(),
						parent: this
					});
				break;
			}

			this._switchView(view);
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.CommercialСampaignListView = Backbone.View.extend({
		className: 'commercial-table',
		template: '#commercial_table',
		initialize: function initialize(options) {
			var self = this;

			this.options  = options || {};
			this.parent   = this.options.parent;
			this.subViews = _.map(this.collection.models, function(model) {
				return new window.app.CommercialСampaignItemView({
					model: model,
					parent: self
				});
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				self     = this;

			this.parent.$el.children('.commercial-panel').children('.commercial-panel__save').hide();
			this.parent.$el.children('.commercial-panel').children('.commercial-panel__add').show();

			this.$el.html(template());

			setTimeout(function() {
				self.checkBodyScroll();
			}, 0);

			_.each(this.subViews, function(view) {
				this.$el.find('.commercial-table-wrapper').append(view.render().el);
			}, this);

			return this;
		},
		checkBodyScroll: function checkBodyScroll() {
			var $body = this.$el.find('.commercial-table-body');

			if (this.$el.height() - 67 < this.subViews.length * 31) {
				$body.addClass('scroll').children().css({
					right: -_.getScrollBarWidth()
				});
			} else {
				$body.removeClass('scroll').children().css({
					right: 0
				});
			}
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.CommercialСampaignItemView = Backbone.View.extend({
		className: 'commercial-table-row',
		template: '#commercial_table_row',
		events: {
			'click .commercial-table__cell': function() {
				Backbone.trigger('commercials:open-section', 'campaign', this.model);
			},
			'click .commercial-table__remove': function() {
				this.remove({single: true});

				if (!this.options.parent.subViews.length) {
					Backbone.trigger('commercials:empty-section');
				}
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
		},
		render: function render() {
			var template      = Backbone.TemplateCache.get(this.template),
				extModel      = $.extend({}, this.model.attributes),
				currentCinema = reqres.request('get:user').getCinema(),
				halls         = _.sortBy(currentCinema.halls, function(hall) { return hall.title; }),
				ages,
				formats;

			if (extModel.halls.length === halls.length) {
				extModel.halls = 'Все залы';
			} else {
				extModel.halls = 'Зал ' + _.filter(halls, function(hall) {
					return _.contains(this.model.get('halls'), hall.id);
				}, this).map(function(oneHall) { return oneHall.title; }).join(', ');
			}

			if (extModel.position.format.length === 5 && extModel.position.age.length === 5) {
				extModel.position = 'Все фильмы';
			} else {
				if (!_.isEmpty(extModel.position.format)) {
					formats = extModel.position.format.join(', ');
				}

				if (!_.isEmpty(extModel.position.age)) {
					ages = extModel.position.age.join('+, ') + '+';
				}

				extModel.position = 'Фильмы ' + [formats.toUpperCase(), ages].join(', ');
			}

			extModel.targeting = extModel.halls + '. ' + extModel.position;
			extModel.date = moment(extModel.date.start).format('D MMMM') + ' - ' + moment(extModel.date.end).format('D MMMM');

			this.$el.html(template(extModel));
			return this;
		},
		remove: function remove(options) {
			if (options && options.single) {
				var parent = this.options.parent;

				parent.subViews.splice(_.indexOf(parent.subViews, this), 1);
				parent.checkBodyScroll();
				this.model.destroy();
			}

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.CommercialNewView = Backbone.View.extend({
		className: 'commercial-campaign',
		template: '#commercial_campaign',
		events: {
			'click .commercial-campaign__cancel': function() {
				this.options.parent.openSection('campaigns');
			},
			'click .commercial-campaign__save': function() {
				this.save();
			},
			'change .commercial-data-item__radio': function(e) {
				var $target = $(e.currentTarget),
					$item   = $target.closest('.commercial-data-item'),
					$hidden = $item.children('.commercial-data-hidden');

				if ($hidden) {
					$hidden.addClass('show');
				}

				$item.siblings().children('.commercial-data-hidden').removeClass('show');
			},
			'change .commercial-data-item .commercial-data-hidden input': function(e) {
				var $target = $(e.currentTarget),
					$item   = $target.closest('.commercial-data-item'),
					titles  = [];

				$item.find('.commercial-data-hidden input:checked').each(function() {
					titles.push($(this).parent().text());
				});

				$item.children('.commercial-data-item__text').children('strong').text(titles.length ? titles.join(', ') : 'Не выбрано');
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.parent  = this.options.parent;
			this.listenTo(Backbone, 'commercial:save', this.save);

			this.listenTo(this.model, 'invalid', function(model, errors) {
				$.growl.error({message: _.pluck(errors, 'message').join('<br>')});
			});
		},
		render: function render() {
			var template      = Backbone.TemplateCache.get(this.template),
				sinceDate     = moment(this.model.get('date').start),
				untilDate     = moment(this.model.get('date').end),
				extModel      = $.extend({}, this.model.attributes),
				currentCinema = reqres.request('get:user').getCinema(),
				halls         = _.sortBy(currentCinema.halls, function(hall) { return hall.title; }),
				columns       = Math.ceil(halls.length / 7),
				formats,
				ages,
				periodsData   = {
					morning: 'Утро 07:00 - 12:00',
					day: 'День 12:00 - 17:00',
					eavning: 'Вечер 17:00 - 23:00'
				};

			this.parent.$el.children('.commercial-panel').children('.commercial-panel__add').hide();

			if (!this.model.isNew()) {
				this.parent.$el.children('.commercial-panel').children('.commercial-panel__save').show();
			}

			extModel.sinceDate   = sinceDate.format('DD.MM.YYYY');
			extModel.untilDate   = untilDate.format('DD.MM.YYYY');
			extModel.hallsData   = halls;
			extModel.cinemaTitle = currentCinema.title.ru;
			extModel.duration    = _.isNull(this.model.get('duration')) ? '00:00' : moment.duration(this.model.get('duration'), 's').minutes() + ':' + (moment.duration(this.model.get('duration'), 's').seconds() < 10 ? '0' + moment.duration(this.model.get('duration'), 's').seconds() : moment.duration(this.model.get('duration'), 's').seconds());

			if ((extModel.position.format.length === 5 && extModel.position.age.length === 5) || (!extModel.position.format.length && !extModel.position.age.length)) {
				extModel.positionText = 'Все форматы и возрасты';
			} else {
				if (!_.isEmpty(extModel.position.format)) {
					formats = extModel.position.format.join(', ');
				}

				if (!_.isEmpty(extModel.position.age)) {
					ages = extModel.position.age.join('+, ') + '+';
				}

				extModel.positionText = [formats.toUpperCase(), ages].join(', ');
			}

			if (extModel.halls.length === halls.length || !extModel.halls.length) {
				extModel.hallsText = 'Зал ' + _.map([extModel.hallsData[0], extModel.hallsData[1]], function(hall) { return hall.title; }).join(', Зал');
			} else {
				extModel.hallsText = 'Зал ' + _.filter(extModel.hallsData, function(hall) {
					return _.contains(extModel.halls, hall.id);
				}, this).map(function(oneHall) { return oneHall.title; }).join(', ');
			}

			if (!extModel.day_time[0] || extModel.day_time[0].period.length === 3) {
				extModel.dayTimeText = 'Утро 07:00 - 12:00';
			} else {
				extModel.dayTimeText = _.map(_.toArray(extModel.day_time[0].period), function(period) { return periodsData[period]; }).join(', ');
			}

			this.model.isNew() ? extModel.isNew = true : extModel.isNew = false;

			this.$el.html(template(extModel));

			this.$('.commercial-data__input--time').mask('9:99');

			this.$('.commercial-data-hidden--halls').css({
				'-webkit-column-count': columns + '',
				'-moz-column-count': columns + '',
				'column-count': columns + ''
			});

			this.$el.children('.commercial-campaign-wrapper').css({
				right: -_.getScrollBarWidth()
			});

			this.setDatePicker(sinceDate, untilDate);

			return this;
		},
		setDatePicker: function setDatePicker(sinceDate, untilDate) {
			this.$el
				.find('.commercial-data__date--since, .commercial-data__date--until')
				.datepicker({
					language: 'ru',
					weekStart: 4,
					autoclose: true,
					format: 'dd.mm.yyyy',
					calendarWeeks: true
				})
				.on('show', function() {
					$('.datepicker .day.active').addClass('disabled');
				})
				.on('change', function(e) {
					var $target = $(e.currentTarget),
						val = $target.val();

					if ($target.hasClass('commercial-data__date--since')) {
						$target.siblings('.commercial-data__date').datepicker('setStartDate', val);
					} else {
						$target.siblings('.commercial-data__date').datepicker('setEndDate', val);
					}
				});

			this.$el
				.find('.commercial-data__date--since')
				.datepicker('setEndDate', untilDate.format('DD.MM.YYYY'));
			this.$el
				.find('.commercial-data__date--until')
				.datepicker('setStartDate', sinceDate.format('DD.MM.YYYY'));
		},
		serializeView: function serializeView() {
			var data = {
					title: this.$el.find('.commercial-data__input--name').val(),
					duration: Math.floor(moment.duration('00:0' + this.$el.find('.commercial-data__input--time').val()).asSeconds()),
					date: {
						start: moment(this.$el.find('.commercial-data__date--since').val(), 'DD.MM.YYYY').format('YYYY-MM-DD'),
						end:   moment(this.$el.find('.commercial-data__date--until').val(), 'DD.MM.YYYY').format('YYYY-MM-DD')
					},
					cinema_id: reqres.request('get:user').getCinema().id
				},
				getCheckFieldItemIndex = function($filed) {
					return $filed.find('.commercial-data-item__radio:checked').closest('.commercial-data-item').index();
				},
				$hallsWrapper          = this.$el.find('.commercial-section-field--halls'),
				$dayTimeWrapper        = this.$el.find('.commercial-section-field--daytime'),
				$positionWrapper       = this.$el.find('.commercial-section-field--position'),
				hallsWrapperIndex      = getCheckFieldItemIndex($hallsWrapper),
				dayTimeWrapperIndex    = getCheckFieldItemIndex($dayTimeWrapper),
				positionWrapperIndex   = getCheckFieldItemIndex($positionWrapper),
				halls                  = [],
				period                 = [],
				dayTime                = [],
				position               = {},
				d                      = 0,
				positionEmpty          = true,
				dayPosition;

			if (hallsWrapperIndex === 1) {
				$hallsWrapper.find('.commercial-data-hidden--halls input:checked').each(function() {
					halls.push($(this).data('id'));
				});
			} else {
				_.each(reqres.request('get:user').getCinema().halls, function(hall) {
					halls.push(hall.id);
				});
			}

			if (dayTimeWrapperIndex === 1) {
				$dayTimeWrapper.find('.commercial-data-hidden--time input:checked').each(function() {
					period.push($(this).data('period'));
				});
			}

			for (d; d < 7; d++) {
				dayPosition = {};
				dayPosition.day = d;

				if (!dayTimeWrapperIndex) {
					dayPosition.start  = '07:00';
					dayPosition.end    = '23:00';
					dayPosition.period = ['morning', 'day', 'eavning'];
				} else {
					dayPosition.start  = $dayTimeWrapper.find('.commercial-data-hidden--time input:checked').first().data('start');
					dayPosition.end    = $dayTimeWrapper.find('.commercial-data-hidden--time input:checked').last().data('end');
					dayPosition.period = period;
				}

				dayTime.push(dayPosition);
			}

			position.format = [];
			position.age    = [];

			$positionWrapper.find('.commercial-data-hidden--formats input' + (positionWrapperIndex ? ':checked' : '')).each(function() {
				position.format.push($(this).data('format'));
				positionEmpty = false;
			});

			$positionWrapper.find('.commercial-data-hidden--ages input' + (positionWrapperIndex ? ':checked' : '')).each(function() {
				position.age.push($(this).data('age'));
				positionEmpty = false;
			});

			if (positionEmpty) position = {};

			data.halls    = halls;
			data.day_time = dayTime;
			data.position = position;

			return data;
		},
		save: function save() {
			var parent = this.options.parent;

			this.model.set(this.serializeView());

			if (this.model.isValid()) {
				if (this.model.isNew()) parent.collection.add(this.model);
				this.model.save();
				parent.openSection('campaigns');
			}
		},
		remove: function remove() {
			this.$el.find('.commercial-data__date--since, .commercial-data__date--until').datepicker('remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

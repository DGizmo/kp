$(function() {
	'use strict';
	window.app = window.app || {};
	window.app.validate = {
		empty:          'Поле обязательное для заполнения',
		badpassword:    'Пароль от 5 до 32-x символов, только латинские',
		badoldpassword: 'Неверный пароль',
		eqpassword:     'Пароли не совпадают'
	};

	window.app.User = Backbone.Model.extend({
		urlRoot: '/api/user',
		idAttribute: 'user_id',
		defaults: {
			releases:        [],
			notes:           [],
			cinema:          [],
			cinema_settings: [],
			user_position:   '',
			approved:        0,
			last_read_chat:  0,
			photo:           '',
			photo_medium:    '',
			photo_big:       '',
			type:            '',
			access_content:  false,
			current:         {},
			tx:              null,
			service:         {}
		},
		_cinemas: {},
		_cinemasSettings: {},
		_cinemasRights: {},
		initialize: function initialize() {
			this.defaultPalette = [
				'#6f6f6f',
				'#cc6666',
				'#de935f',
				'#f0c674',
				'#b5bd68',
				'#8abeb7',
				'#81a2be',
				'#b294bb',
				'#a3685a'
			];

			this.listenTo(Backbone, 'socket:user:set:tx', function(tx) {
				this.set({tx: tx});
			});

			this.listenTo(Backbone, 'avatar-changed', function(avatar) {
				this.set('photo_medium', avatar);
			});

			this.listenTo(Backbone, 'socket:note', function(data) {
				var release = _.find(this.get('notes'), function(note) {
					return note.id === data.id;
				});

				if (release) {
					release.text = data.value;
				} else {
					this.get('notes').push({
						id:   data.id,
						text: data.value
					});
				}
			});

			this.listenTo(Backbone, 'socket:notify-custom:add', function(notify) {
				this.fetch({
					success: function() {
						Backbone.trigger('socket:notify-custom:add:user-fetched', notify);
					}
				});
			});

			this.listenTo(this, 'sync', function() {
				var timezone = new Date(),
					userTimezone = -timezone.getTimezoneOffset() / 60;

				if (!this.get('timezone') || userTimezone !== this.get('timezone')) {
					$.post('/api/user/edit', {
						timezone: userTimezone
					});
				}

				if (this.get('logout')) window.location.href = '/logout';
			});
		},
		url: function url() {
			return '/api/user/' + (this.id || 0);
		},
		parse: function parse(user) {
			var types = {
				cinema:      'Кинотеатр',
				mechanic:    'Механик',
				distributor: 'Дистрибьютор',
				guest:       'Гость',
				partner:     'Партнёр'
			};

			if (user.distributor_id) {
				user.distributor_id = parseInt(user.distributor_id, 10);
			}

			if (user.cinema.length) {
				user.cinema = _.sortByNat(user.cinema, function(c) { return c.title.ru || c.title.en || ''; });
			}

			_.each(user.cinema, function(cinema) {
				cinema.rights_id = parseInt(cinema.rights_id, 10);
				cinema.halls     = _.sortBy(cinema.halls, function(hall) { return hall.number; });
				this._cinemas[cinema.id] = cinema;
			}, this);

			_.each(user.cinema_settings, function(settings) {
				this._cinemasSettings[settings.cinema_id] = settings.cinema_id && settings;
			}, this);

			_.each(user.cinema_rights, function(rights) {
				this._cinemasRights[rights.cinema_id] = rights.cinema_id && rights;
			}, this);

			user.desc_type = types[user.type];

			return user;
		},
		isApproved: function isApproved() {
			return this.get('approved');
		},
		isAdmin: function isAdmin() {
			return this.get('admin');
		},
		isModerator: function isModerator() {
			return this.get('moderator');
		},
		isMultiplex: function isMultiplex() {
			return this.getCinemas().length > 1;
		},
		isMaster: function isMaster(id) {
			return this.getCinemaRights(id).master;
		},
		isMechanic: function isMechanic() {
			return this.get('type') === 'mechanic';
		},

		getCinema: function getCinema(id) {
			return this._cinemas[id || this.get('current').cinema_id] || {};
		},
		getCinemas: function getCinemas() {
			return this.get('cinema') || [];
		},
		getCinemaSettings: function getCinemaSettings(id) {
			return this._cinemasSettings[id || this.getCinema().id];
		},
		getCinemaRights: function getCinemaRights(id) {
			return this._cinemasRights[id || this.getCinema().id];
		},
		getNodeID: function getNodeID(id) {
			return this._cinemas[id || this.getCinema().id].dd24_pro;
		},

		/* Rights */
		isInSandbox: function isInSandbox(id) {
			return !this.getCinemaRights(id).multi_account || !this.hasRights();
		},
		hasRights: function hasRights(id) {
			return this.getCinemaRights(id).level !== 'none';
		},
		isReadOnly: function isReadOnly(id) {
			return this.getCinemaRights(id).level === 'read';
		},
		isRepertoireReadOnly: function isRepertoireReadOnly(id) {
			return (this.isReadOnly(id) || this.getCinemaRights(id).level === 'rw');
		},
		hasFullAccess: function hasFullAccess(id) {
			return this.getCinemaRights(id).level === 'write';
		},

		/* Service Access */
		hasServiceAccessLVL: function hasServiceAccessLVL() {
			return this.get('service').level;
		},

		/* Access */
		accessTo: function accessTo(page) {
			if (this.isAdmin()) return true;

			switch (page) {
				case 'statistic':  return this.get('access_stat');
				case 'kdmContent': return this.get('access_content');
				case 'DD24':       return this.get('access_dd4_serv');

				default: return false;
			}
		}
	});

	window.app.UserView = Backbone.View.extend({
		className: 'popup_content prof_cont js-profile user_profile',
		id: 'userProfileSettings',
		template: '#user_settings_tmpl',
		events: {
			'click .user_profile-top .popupWrap__button': /* istanbul ignore next */ function(event) {
				event.preventDefault();
				this.$el.find('.user_profile-top .popupWrap__button').removeClass('active');
				$(event.currentTarget).addClass('active');
				this.$el.find('form').hide();
				this.$el.children($(event.currentTarget).data('tab')).show();

				if ($(event.currentTarget).data('tab') === '#profile_form-photo') _gaq.push(['_trackEvent', 'Пользователь', 'Фото', 'Открытие']);
			},
			'click .close': /* istanbul ignore next */ function() {
				this.$el.arcticmodal('close');
			},
			'submit #profile_form-name': /* istanbul ignore next */ function(event) {
				event.preventDefault();

				var self = this;

				$.post('/api/user/edit', $(event.currentTarget).serialize(), function(r) {
					if (r.ok) {
						self.model.set('user_position', self.$el.find('#user_position').val());
						$.growl.notice({
							message: ''
						});
						self.$el.arcticmodal('close');
					} else {
						$.growl.error({
							message: ''
						});
					}
				});
			},
			'submit #profile_form-password': /* istanbul ignore next */ function(event) {
				event.preventDefault();
				var $target = $(event.currentTarget),
					self = this;

				this.$el.find('#password_send').hide();

				if (this.validatePassword($target)) {
					$.post('/api/user/edit', $target.serialize(), function(r) {
						if (r.ok) {
							$.growl.notice({
								message: ''
							});
							self.$el.find('.error').removeClass('error');
							self.$el.find('.error_text').text('');
							self.$el.find('#profile_form-password')[0].reset();
							self.$el.arcticmodal('close');

						} else {
							$.growl.error({
								message: 'Неверный пароль!'
							});
							self.$el.find('.error').removeClass('error');
							self.$el.find('.error_text').text('');

							for (var i in r) {
								self.$el.find('#profile_form-password').find('#' + i + '').addClass('error').after('<p class="error_text">' + window.app.validate[r[i]] + '</p>');
							}
						}
					});
				} else {
					$.growl.error({
						message: 'Заполните поля!'
					});
				}
			},
			'click #profile_form-password .link': /* istanbul ignore next */ function(event) {
				event.preventDefault();
				var self = this;
				$(event.currentTarget).addClass('disabled');

				$.post('/api/user/forgot', {
					email: self.model.get('email')
				}, function(r) {
					if (r.ok) {
						self.$el.find('.error').removeClass('error');
						self.$el.find('.error_text').text('');
						self.$el.find('#password_send').show();
						$(event.currentTarget).hide();
					} else {
						$.growl.error({
							message: 'Ошибка!'
						});
					}
					$(event.currentTarget).removeClass('disabled');
				});
			},
			'click #upload_avatar':  /* istanbul ignore next */  function() {
				_gaq.push(['_trackEvent', 'Пользователь', 'Фото', 'Загрузка:Клик']);
			},
			'change #profile_form-photo': /* istanbul ignore next */ function(event) {
				event.preventDefault();
				var self = this,
					image_input = self.$el.find('#upload_avatar'),
					$photo = $(event.currentTarget).find('.photo');

				function getExtension(filename) {
					var parts = filename.split('.');
					return parts[parts.length - 1];
				}

				function isImage(filename) {
					var ext = getExtension(filename);
					switch (ext.toLowerCase()) {
						case 'jpg':
						case 'jpeg':
						case 'gif':
						case 'png':

							return true;
					}
					return false;
				}

				if (!image_input.val().length) {
					$.growl.error({
						message: 'Выберите файл!'
					});
					return false;
				}

				if (!isImage(image_input.val())) {
					$.growl.error({
						message: 'Вы можете загрузить изображение в формате JPG, GIF или PNG.'
					});
					return false;
				}

				$photo.addClass('loading');

				$.ajax({
					url: '/api/user/avatar',
					type: 'POST',
					data: new FormData(event.currentTarget),
					cache: false,
					dataType: 'json',
					processData: false,
					contentType: false,
					success: function(data) {
						if (data.ok) {
							$.growl.notice({
								message: ''
							});

							self.$el.find('#profile_form-photo')[0].reset();
							$photo.removeClass('loading');
							$photo.addClass('loaded');

							self.$el.find('#profile_form-photo .loaded').css('background-image', 'url(' + data.data.photo_medium + ')');
							Backbone.trigger('avatar-changed', data.data.photo_medium);

							_gaq.push(['_trackEvent', 'Пользователь', 'Фото', 'Загрузка:Успешно']);
						}
					},
					error: function() {
						$.growl.error({
							message: 'Ошибка!'
						});

						self.$el.find('#profile_form-photo')[0].reset();
						$photo.removeClass('loading');
					}
				});
			}
		},
		initialize: function initialize(options) {
			var self = this;

			this.options = options || {};

			/* istanbul ignore next */
			this.render().$el.arcticmodal({
				afterOpen: function(data, el) {
					if (self.options && self.options.section) {
						$(el).find('.popupWrap__button[data-tab="#profile_form-' + self.options.section + '"]').trigger('click');
					}
				},
				afterClose: function() {
					self.remove();
				}
			});
		},
		validatePassword: function validatePassword(form) {
			var required = form.find('input[data-required="true"]'),
				equalto  = form.find('input[data-equalto]'),
				empty    = 0,
				equal,
				i;

			this.$el.find('.error').removeClass('error');
			this.$el.find('.error_text').text('');

			for (i = 0; i < required.length; i++) {
				$(required[i]).val() ? empty++ : $(required[i]).addClass('error').after('<p class="error_text">Поле обязательное для заполнения</p>');
			}

			equalto.val() === $(equalto.data('equalto')).val() ? equal = true : equalto.addClass('error').after('<p class="error_text">Пароли не совпадают</p>');

			if ((empty === required.length) && equal) {
				return true;
			} else {
				return false;
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});

	window.app.UserCinemasWrapper = Backbone.View.extend({
		template: '#user_settings_wrapper',
		className: 'user-cinemas',
		events: {
			'click .user-cinemas__tab': function(e) {
				var $target = $(e.currentTarget);

				$target.addClass('active').siblings().removeClass('active');

				if ($target.hasClass('user-cinemas__tab--cinemas')) {
					this.cinemasView = new window.app.UserCinemaSettingsWrapperView({
						model: this.model,
						parent: this,
						collection: this.userCinemasSettings
					});
					this.switchView(this.cinemasView);
					this.$el.find('.user-cinemas__save').show();
				} else if ($target.hasClass('user-cinemas__tab--groups')) {
					this.groupsView = new window.app.UserGroupsWrapperView({
						model: this.model,
						parent: this,
						collection: window.app.App.Collections.UserCinemaGroupsCollection
					});
					this.switchView(this.groupsView);
					this.$el.find('.user-cinemas__save').hide();
				}
			},
			'click .user-cinemas__save': function() {
				var self = this;

				if (!this.cinemasNewSettings) this.currentView.saveChanges();

				$.post('/api/user/settings', {
					tx: this.model.get('tx'),
					data: JSON.stringify(self.cinemasNewSettings)
				}, function(r) {
					if (r.ok) {
						Backbone.trigger('user:cinema:settings:save');
					} else {
						self.model.set(self.model.previousAttributes());
					}
				}, 'json');

				$.arcticmodal('close');
			}
		},
		initialize: function initialize() {
			var self = this;

			this.userCinemasSettings = new Backbone.Collection(this.getExtCinemaSettingsCollection());
			this.cinemasNewSettings = null;
			this.groupsSettings     = null;

			$('#js-popup').html(this.render().el);

			$('#js-popup > .user-cinemas').arcticmodal({
				beforeOpen: function(data, el) {
					el.closest('.arcticmodal-container_i2').addClass('center');
				},
				afterOpen: function() {
					self.currentView = new window.app.UserCinemaSettingsWrapperView({
						model: self.model,
						parent: self,
						collection: self.userCinemasSettings
					});
					self.switchView(self.currentView);
				},
				afterClose: function() {
					self.remove();
				}
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template(this.model.attributes));

			return this;
		},
		switchView: function switchView(view) {
			Backbone.trigger('loader', 'show');

			if (this.currentView !== null && this.currentView.cid !== view.cid) {
				this.currentView.remove();
			}

			this.currentView = view;

			this.$el.children('.user-cinemas-wrapper').html(view.render().el);
			Backbone.trigger('loader', 'hide');
		},
		getExtCinemaSettingsCollection: function getExtCollection() {
			var self = this;
			return _.map(this.model.get('cinema'), function(item) {
				var currentSettings = _(self.model.get('cinema_settings')).chain()
					.find(function(settindsItem) {
						return item.id === settindsItem.cinema_id;
					})
					.omit('cinema_id')
					.value(),
					extHalls;

				if ($.isEmptyObject(currentSettings)) {
					_.extend(currentSettings, self.model.get('defaultCinemaSettings'));
					currentSettings.halls = [];
				}
				extHalls = _.map(item.halls, function(hall) {
					var currentHall = _.find(currentSettings.halls, function(settingsHall) {
						return settingsHall.id === hall.id;
					}) || {};
					if ($.isEmptyObject(currentHall)) {
						_.extend(currentHall, self.model.get('defaultHallSettings'));
					}
					return _.extend(currentHall, hall);
				});
				_.extend(currentSettings, item);
				currentSettings.halls = extHalls.sortBy(function(n) {
					return n.number;
				});
				return currentSettings;
			}, this);
		},
		remove: function remove() {
			this.currentView.remove();

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

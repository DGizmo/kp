$(function() {
	'use strict';
	window.app = window.app || {};

	window.app.UserCinemaPersonalCollection = Backbone.Collection.extend({
		model: Backbone.Model,
		url: function url() {
			return (this.options.modelId ? (this.options.email ? '/api/user/list/?email=' + this.options.email + '&cinema_id=' + this.options.modelId : '/api/user/list/?cinema_id=' + this.options.modelId) : '');
		},
		parse: function(response) {
			var thisUser;
			if (response.users) response.users.options = this.options;

			if (thisUser = _.findWhere(response.users, {user_id: reqres.request('get:user').id}))
				thisUser.name = thisUser.name + ' (Вы)';
			return response.users;
		},
		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {
				parent: {}
			});
		}
	});

	window.app.UserCinemaItemView = Backbone.View.extend({
		template: _.template('<span class="user-cinemas-list-item__title">{{- title.ru }}</span><span class="user-cinemas-list-item__subtitle">{{- city.title.ru }}</span>'),
		className: 'user-cinemas-list-item',
		events: {
			click: function() {
				this.$el.addClass('active').siblings().removeClass('active');
				commands.execute('user:cinemaSettings:changeCinema', this.model);
			}
		},
		initialize: function initialize(options) {
			this.options = _.extend({}, options);
		},
		render: function render() {
			this.$el
				.append(this.template(this.model.attributes))
				.data('id', this.model.get('id'));

			if (this.options.active)
				this.$el.addClass('active');

			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.UserCinemaSettingsWrapperView = Backbone.View.extend({
		template: '#user_cinemas_settings_wrapper_tmpl',
		className: 'user-cinemas-settings',
		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {
				parent: {}
			});

			this.subViews = [];
			this.newSettings = [];
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template());

			_.each(this.collection.models, function(model, index) {
				var options = {
						model: model,
						parent: this.options.parent,
						active: !index
					};
				this.renderSubViews(options, this.$el.find('.user-cinemas-list'));
			}, this);

			this.renderSettings();
			return this;
		},
		renderSubViews: function renderSubViews(options, $wrapper) {
			var item = new window.app.UserCinemaItemView(options);

			$wrapper.append(item.render().el);
			this.subViews.push(item);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');

			this.settingsView.remove();
			this.$el.empty();
			this.subViews = [];
			return Backbone.View.prototype.remove.call(this);
		},
		renderSettings: function renderSettings() {
			this.settingsView = new window.app.UserCinemasSettingsView({
				model:  this.collection.at(0),
				parent: this.options.parent
			});
			this.$el.find('.user-cinemas-right').html(this.settingsView.render().el);
		},
		saveChanges: function saveChanges() {
			this.settingsView.saveCurrentModel();
			var newSettings = _.map(this.collection.toJSON(), function(item) {
				item.cinema_id = item.id;
				item.halls = _.map(item.halls, function(hall) {
					return _.pick(hall, 'id', 'defaultHallClose', 'defaultHallOpen', 'defaultHallPause', 'tms_id');
				});
				return _.pick(item, 'cinema_id', 'halls', 'defaultCinemaAd', 'schedule_rounding', 'cinemaOpen', 'cinemaClose', 'cinemaWeekendOpen', 'cinemaWeekendClose');
			});
			this.options.parent.model.set({
				cinema_settings: newSettings
			});
			this.options.parent.cinemasNewSettings = newSettings;

		}

	});

	window.app.UserCinemasSettingsView = Backbone.View.extend({
		template: '#user_cinemas_settings_tmpl',
		className: 'user-cinemas-settings-container',
		events: {
			'click .user-cinemas-settings-tabs-item': function(e) {
				var target = $(e.currentTarget),
					data   = this.$el.find('.user-cinemas-settings-data');

				if (!target.hasClass('active')) {
					target.addClass('active').siblings().removeClass('active');
					data.hide();
					data.eq(target.index()).show();
				}
			},
			'focusout .user-cinemas-settings__advertisement-input': function(event) {
				var $input = $(event.currentTarget);

				if (!_.isFinite($input.val()) || $input.val() < 0) {
					$input.val(0);
					return;
				}

				if ($input.val() > 300) {
					$input.val(300);
				}
			},
			'keydown .user-cinemas-settings__advertisement-input': function(event) {
				if (_.contains([46, 8, 9, 27, 13], event.keyCode) ||
					(event.keyCode === 65 && event.ctrlKey === true) ||
					(event.keyCode >= 35 && event.keyCode <= 40)) {
					return;
				}

				if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
					event.preventDefault();
				}
			}
		},
		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {
				parent: {}
			});
			this.isUsersLoaded = false;
			this.personalCollection = new window.app.UserCinemaPersonalCollection({
				modelId: this.model.get('id')
			});
			this.fetchPersonalCollection();
			this.defaultCinemaSettings = reqres.request('get:user').get('defaultCinemaSettings');
			this.defaultHallSettings = reqres.request('get:user').get('defaultHallSettings');

			commands.setHandler('user:cinemaSettings:changeCinema', this.reRender, this);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				$settingsWrapper = this.$el,
				extModel = _.extend({}, this.model.attributes);

			extModel.cinemaOpen = extModel.cinemaOpen || this.defaultCinemaSettings.cinemaOpen;
			extModel.cinemaClose = extModel.cinemaClose || this.defaultCinemaSettings.cinemaClose;
			extModel.cinemaWeekendOpen = extModel.cinemaWeekendOpen || this.defaultCinemaSettings.cinemaOpen;
			extModel.cinemaWeekendClose = extModel.cinemaWeekendClose || this.defaultCinemaSettings.cinemaClose;
			extModel.defaultCinemaAd = extModel.defaultCinemaAd || this.defaultCinemaSettings.defaultCinemaAd;
			extModel.schedule_rounding = extModel.schedule_rounding || this.defaultCinemaSettings.schedule_rounding;
			_.each(extModel.halls, function(hall) {
				hall.defaultHallOpen = hall.defaultHallOpen || this.defaultHallSettings.defaultHallOpen;
				hall.defaultHallClose = hall.defaultHallClose || this.defaultHallSettings.defaultHallClose;
				hall.defaultHallPause = hall.defaultHallPause || this.defaultHallSettings.defaultHallPause;
			}, this);
			$settingsWrapper.html(template(extModel));

			if (extModel.master === 1 || extModel.rights_id === 3)
				$settingsWrapper.addClass('master');

			$settingsWrapper.find('.user-cinema-settings__settings-input')
				.timepicker({
					timeFormat: 'H:i',
					step: 10,
					minTime: '10:00',
					maxTime: '02:00',
					forceRoundTime: true,
					useSelect: false,
					show2400: true,
					selectOnBlur: true
				});
			$settingsWrapper
				.find('.user-cinemas-halls-table__hall-open-input').each(function() {
					$(this).timepicker({
							timeFormat: 'H:i',
							step: 5,
							minTime: '08:00',
							maxTime: '23:30',
							forceRoundTime: true,
							useSelect: false,
							show2400: true
						}).on('timeFormatError timeRangeError', function() {
							$(this).timepicker('setTime', '08:00');
						})
						.css({
							width: 35,
							'text-align': 'center'
						});
				});
			$settingsWrapper.find('.user-cinemas-halls-table__hall-close-input').each(function() {
					$(this).timepicker({
						timeFormat: 'H:i',
						step: 10,
						minTime: '08:00',
						maxTime: '23:30',
						forceRoundTime: true,
						useSelect: false
					}).on('timeFormatError timeRangeError', function() {
						$(this).timepicker('setTime', '23:30');
					})
					.css({
						width: 35,
						'text-align': 'center'
					});
				});
			$settingsWrapper.find('.user-cinemas-halls-table__pause-input').each(function() {
					var $pause = $(this);

					$pause.timepicker({
						timeFormat: 'H:i',
						step: 5,
						minTime: '00:00',
						maxTime: '00:55',
						forceRoundTime: true,
						useSelect: true,
						show2400: true
					});

					$pause.each(function() {
						var defPause = $pause.data('val'),
							$select = $pause.closest('.user-cinemas-halls-table-col-pause').find('.ui-timepicker-select');

						$select.find('option').each(function() {
							var minutes = $(this).text().slice(-2);

							if (minutes === '00' || minutes === '05') {
								minutes = minutes.slice(-1);
							}

							$(this).text(minutes);
						});

						$select.val(defPause);
					});
				});

			return this;
		},
		reRender: function reRender(model) {
			this.saveCurrentModel();
			this.model = model;
			this.isUsersLoaded = false;
			this.personalCollection.options.modelId = this.model.get('id');
			if (this.$el.hasClass('master'))
				this.$el.removeClass('master');
			this.render();
			this.fetchPersonalCollection();
		},
		fetchPersonalCollection: function fetchPersonalCollection() {
			this.personalCollection.fetch({
				success: _.bind(function() {
					var options = this.personalCollection.options || {};

					if (this.personalCollection.length) {
						options.masterUser  = _.find(this.personalCollection.models, function(user) {
							return user.get('master') === 1;
						}).attributes;
						options.currentUserIsMaster = options.masterUser && options.masterUser.user_id === reqres.request('get:user').id ? true : false;

						this.personalView = new window.app.UserCinemasPersonalCollectionView({
							collection: this.personalCollection,
							parent: this.options.parent,
							model: this.model,
							el: $('.user-cinemas-settings-data.user-cinemas-settings-data--personal')
						});

						this.personalView.render();
						this.$el.find('.user-cinemas-settings-tabs__personal-count').html(this.personalCollection.length);
					}
					this.isUsersLoaded = true;
				}, this)
			});
		},
		saveCurrentModel: function saveCurrentModel() {
			if (this.model.get('rights_id') === 3 || this.model.get('master') === 1) {
				var newSettings = {},
					$currentCinema = this.$el,
					$halls         = $currentCinema.find('.user-cinemas-settings-data--halls .user-cinemas-scroll-table-row'),
					halls          = [],
					cinemaAd,
					scheduleRounding;

				cinemaAd           = parseInt($currentCinema.find('.user-cinemas-settings__advertisement-input').val());
				scheduleRounding   = $currentCinema.find('.user-cinemas-settings-schedule-rounding-select :selected').val();

				$currentCinema.find('.user-cinema-settings__settings-input').each(function() {
					var name = this.name;
					newSettings[name] = $(this).timepicker('getTime', new Date()).format('{HH}:{mm}');
				});
				$halls.each(function() {
					var $currentHall = $(this),
						hallId       = $currentHall.data('hall'),
						hallOpen     = $currentHall.find('.user-cinemas-halls-table__hall-open-input').val(),
						hallClose    = $currentHall.find('.user-cinemas-halls-table__hall-close-input').val(),
						hallPause    = parseInt(Date.create($currentHall.find('.user-cinemas-halls-table-col-pause select').val()).format('{mm}')),
						tmsID        = $currentHall.find('.user-cinemas-halls-table__tms-input').val();

					halls.push({
						id: hallId,
						defaultHallOpen: hallOpen,
						defaultHallClose: hallClose,
						defaultHallPause: hallPause,
						tms_id:           tmsID
					});
				});

				newSettings = _.extend({
					defaultCinemaAd:    cinemaAd,
					schedule_rounding:  scheduleRounding,
					halls:              _.map(this.model.get('halls'), function(hall) {
						return _.extend(hall, _.find(halls, function(newHall) {
							return newHall.id === hall.id;
						}));
					})
				}, newSettings);

				this.model.set(newSettings);
			}

		},
		remove: function remove() {
			if (this.personalView)
				this.personalView.remove();

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}

	});

	window.app.UserCinemasPersonalModelView = Backbone.View.extend({
		template: '#user_cinemas_personal_item_tmpl',
		className: 'user-cinemas-scroll-table-row',
		events: {
			'change .user-cinemas-personal-table__rights-select': function(e) {
				var item = $(e.currentTarget);

				$.post('/api/user/rights', {
					user_id:   this.model.get('user_id'),
					cinema_id: this.options.cinema_id,
					rights_id: item.val()
				}, 'json');
			}
		},
		initialize: function initialize(options) {
			this.options = options;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.UserCinemasPersonalCollectionView = Backbone.View.extend({
		template: '#user_cinemas_personal_tmpl',
		events: {
			'click .user-cinemas-personal-invite-actions__send': function(e) {
				var $send     = $(e.currentTarget),
					invite    = $send.closest('.user-cinemas-personal-invite'),
					emailVal  = invite.find('.user-cinemas-personal-invite__input').val(),
					self = this;

				if (_.isValidEmail(emailVal)) {
					$.post('/api/user/invite', {
						email:     emailVal,
						cinema_id: this.model.get('id'),
						rights_id: parseInt(invite.find('.user-cinemas-personal-invite-select').val())
					}, function(r) {
						var exists = r.exists;

						if (exists === 1) {
							self.collection.options.email = emailVal;
							self.collection.fetch({
								remove: false,
								success: function() {
									self.render();
									self.$el.closest('.user-cinemas-settings-tabs').find('.user-cinemas-settings-tabs__personal-count').html(self.collection.length);
									invite.find('.user-cinemas-personal-invite__input').val('');
								}});
						} else {
							invite.find('.user-cinemas-personal-invite__input').val('');
						}
					}, 'json');
				}
			},
			'click .user-cinemas-personal-invite-actions__cancel': function(e) {
				$(e.currentTarget).closest('.user-cinemas-personal-invite').removeClass('full');
			},
			'focus .user-cinemas-personal-invite__input': function(e) {
				$(e.currentTarget).closest('.user-cinemas-personal-invite').addClass('full');
			}
		},
		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {
				parent: {}
			});
			this.subViews = [];

		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				$wrapper;

			this.$el.html(template(this.collection.options));
			$wrapper = this.$el.find('.user-cinemas-scroll-table-bottom');
			_.each(this.collection.models, function(model) {
				if (this.collection.options.masterUser.user_id !== model.get('user_id')) {
					var options = {
						model: model,
						cinema_id: this.model.id
					};
					this.renderSubViews(options, $wrapper);
				}
			}, this);

			this.$el.find('.user-cinemas-personal-invite-select').styler();
			return this;
		},
		renderSubViews: function renderSubViews(options, $wrapper) {
			var item = new window.app.UserCinemasPersonalModelView(options);

			$wrapper.append(item.render().el);
			this.subViews.push(item);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}

	});

});

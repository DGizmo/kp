$(function() {
	'use strict';
	window.app = window.app || {};

	window.app.UserCinemasGroupModel = Backbone.Model.extend({
		urlRoot: '/api/cinema/groups',
		defaults: {
			id:      null,
			title:   'Группа кинотеатров',
			cinemas: []
		}
	});

	window.app.UserCinemaGroupsCollection = Backbone.Collection.extend({
		model: window.app.UserCinemasGroupModel,
		url: '/api/cinema/groups',
		initialize: function initialize() {
			reqres.setHandler('get:user:cinemasGroups', function() {
				return this;
			}, this);
		},
		parse: function parse(response) {
			return response.cinema_group;
		},
		getGroupedCinemas: function getGroupedCinemas() {
			return _.flatten(_.map(this.models, function(model) { return model.get('cinemas'); }));
		}
	});

	window.app.UserGroupsWrapperView = Backbone.View.extend({
		template: '#user_settings_groups_wrapper',
		className: 'user-cinemas-settings user-cinemas-settings--groups',
		events: {
			'click .cinemas-group-add': function() {
				var newGroupModel = new window.app.UserCinemasGroupModel();

				this.collection.add(newGroupModel);
				//newGroupModel.save();

				this.addOneGroupView(newGroupModel, this.$el.find('.user-cinemas-list--group'));
				if (this.collection.length === 1)
					this.renderGroupInfo(newGroupModel);
				newGroupModel.trigger('user:group:model:show');
			}
		},
		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {
				parent: {}
			});

			this.subViews = [];
			this.listenTo(this.collection, 'remove', function() {
				var index = arguments[2].index;
				if (this.collection.length)
					this.collection.at(!index ? index : index - 1).trigger('user:group:model:show');
				else
					this.userGroupsInfoView.remove();
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());

			if (this.collection.models.length) {
				this.addGroupsViews();
			}
			if (this.collection.length)
				this.renderGroupInfo(this.collection.at(0));

			Backbone.trigger('loader', 'hide');

			return this;
		},
		addGroupsViews: function addGroupsViews() {
			_.each(this.collection.models, function(model) {
				this.addOneGroupView(model, this.$el.find('.user-cinemas-list'));
			}, this);
		},
		addOneGroupView: function addOneGroupView(model, $wrapper) {
			var modelIndex = _.indexOf(this.collection.models, model),
				item = new window.app.UserGroupsItemView({
					model: model,
					parent: this.options.parent,
					first: !modelIndex
				});

			$wrapper.append(item.render().el);

			this.subViews.push(item);
		},
		removeEmptyModels: function removeEmptyModels() {
			this.collection.remove(_.filter(this.collection.models, function(model) { return !model.get('title').length || !model.get('cinemas').length; }));
		},
		renderGroupInfo: function renderGroupInfo(model) {
			this.userGroupsInfoView = new window.app.UserGroupsInfoView({
				model:  model,
				parent: this.options.parent
			});
			this.$el.find('.user-cinemas-right').html(this.userGroupsInfoView.render().el);
		},
		remove: function remove() {
			this.removeEmptyModels();
			_.invoke(this.subViews, 'remove');
			if (this.userGroupsInfoView)
				this.userGroupsInfoView.remove();

			this.$el.empty();
			this.subViews = [];
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.UserGroupsItemView = Backbone.View.extend({
		template: _.template('<span class="user-cinemas-list-item__title user-cinemas-groups-item__title" title="{{- title }}">{{- title }}</span><span class="user-cinemas-list-item__subtitle">{{- cinemasText }}</span>'),
		className: 'user-cinemas-list-item',
		events: {
			click: 'showInfo'
		},
		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {
				parent: {}
			});

			this.listenTo(this.model, 'remove', this.remove);
			this.listenTo(this.model, 'user:group:model:show', this.showInfo);
		},
		render: function render() {
			var extModel = $.extend({}, this.model.attributes);

			if (this.model.isNew()) {
				extModel.title = 'Группа кинотеатров №' + this.model.collection.length;
			}

			extModel.cinemasText = this.model.get('cinemas').length + _.declOfNum(this.model.get('cinemas').length, [' кинотеатр', ' кинотеатра', ' кинотеатров']);

			this.$el.html(this.template(extModel));

			if (this.options.first) {
				this.$el.addClass('active');
			}

			return this;
		},
		showInfo: function showInfo() {
			this.$el.addClass('active').siblings().removeClass('active');
			commands.execute('user:groupsSettings:changeGroup', this.model);
		},
		remove: function remove() {
			this.$el.empty();

			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.UserGroupsInfoView = Backbone.View.extend({
		template: '#user_settings_groups_info',
		className: 'user-cinemas-group',
		events: {
			'change .user-cinemas-group__check': function(e) {
				var $target = $(e.currentTarget),
					id      = $target.data('id'),
					cinemas = $.extend([], this.model.get('cinemas'));

				if ($target.prop('checked')) {
					cinemas.push(id);
				} else {
					cinemas.splice(_.indexOf(cinemas, id), 1);
				}

				$target.closest('.user-cinemas-settings--groups').find('.user-cinemas-left .active .user-cinemas-list-item__subtitle').text(cinemas.length +  _.declOfNum(cinemas.length, [' кинотеатр', ' кинотеатра', ' кинотеатров']));

				this.model.save({
					cinemas: cinemas
				});
			},
			'click .user-cinemas-group__remove': function() {
				if (this.model.isNew())
					this.model.collection.remove(this.model);
				else
					this.model.destroy();
			},
			'focus .user-cinemas-group__name': 'inputFocus',
			'focusout .user-cinemas-group__name': 'inputFocus'
		},
		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {
				parent: {}
			});

			commands.setHandler('user:groupsSettings:changeGroup', this.reRender, this);
		},
		render: function render() {
			var template       = Backbone.TemplateCache.get(this.template),
				extModel       = $.extend({}, this.model.attributes),
				scrollBarWidth = _.getScrollBarWidth();

			if (this.model.isNew()) {
				extModel.title = 'Группа кинотеатров №' + this.model.collection.length;
				extModel.placeholder = 'Группа кинотеатров №' + this.model.collection.length;
			} else {
				extModel.placeholder = false;
			}

			extModel.otherGroupsCinemas = _.difference(this.model.collection.getGroupedCinemas(), this.model.get('cinemas'));

			extModel.cinemas = _.map(this.options.parent.model.get('cinema'), function(cinema) {
				return {
					id: cinema.id,
					checked: _.contains(extModel.otherGroupsCinemas, cinema.id) || _.contains(this.model.get('cinemas'), cinema.id),
					readonly: _.contains(extModel.otherGroupsCinemas, cinema.id),
					title: cinema.title.ru + ', ' + cinema.city.title.ru
				};
			}, this);

			this.$el.html(template(extModel));

			if (scrollBarWidth) {
				this.$el.find('.user-cinemas-group-scroll').css({
					width: 'calc(100% + ' + scrollBarWidth + 'px)',
					right: -scrollBarWidth
				});
			}

			return this;
		},
		reRender: function reRender(model) {
			this.model = model;
			this.render();
		},
		inputFocus: function inputFocus(e) {
			var self = this,
				$input = $(e.currentTarget),
				$leftSideTitle = $input.closest('.user-cinemas-settings--groups').find('.user-cinemas-left .active .user-cinemas-list-item__title'),
				prevLeftSideTitle = $leftSideTitle.text(),
				eventType = e.type,
				text;

			if (eventType === 'focusin') {
				$input.unbind('textchange');

				$input.bind('textchange', function() {
					text = $input.val().length ? $input.val() : prevLeftSideTitle;

					$leftSideTitle.text(text).attr('title', text);

					self.model.set({
						title: text
					});
				});
			} else if (eventType === 'focusout') {
				$input.val($leftSideTitle.text());

				this.model.set({
					title: $leftSideTitle.text()
				});

				if (this.model.get('cinemas').length)
					this.model.save();
			}
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.EmployeeModel = Backbone.Model.extend({
		defaults: {
			actived: 0,
			cinemas: [],
			email: '',
			first_name: '',
			last_name: '',
			phone: ''
		}
	});

	window.app.EmployeesCollection = Backbone.Collection.extend({
		model: window.app.EmployeeModel,
		url: function url() {
			return '/api/employee';
		},
		parse: function parse(res) {
			return res.list.sort(function(a, b) {
				return b.actived - a.actived;
			});
		}
	});

	window.app.EmployeesView = Backbone.View.extend({
		className: 'employees',
		template: '#employees_tmpl',
		initialize: function initialize() {
			if (this.collection.length) {
				this.renderView();
			} else {
				this.loadEmployees();
			}
		},
		loadEmployees: function loadEmployees() {
			Backbone.trigger('loader', 'show');

			this.collection.fetch({
				success: _.bind(function() {
					this.renderView();
				}, this)
			}).always(function() {
				Backbone.trigger('loader', 'hide');
			});
		},
		renderView: function renderView() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({
				employees: this.collection.toJSON(),
				now: (new Date()).getTime()
			}));

			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

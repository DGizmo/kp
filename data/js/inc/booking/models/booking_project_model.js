$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.BookingProjectModel = Backbone.Model.extend({
		defaults: {
			project_id: null,
			request_id: null,
			from_date: null,
			to_date: null,
			first_week: 1,
			second_week: 4
		},
		url: function url() {
			return [
				'/api/booking/project.json?project_id=',
				this.get('project_id'),
				'&request_id=',
				this.get('request_id') === 'all' ? '' : this.get('request_id'),
				this.get('from_date') ? '&from=' + this.get('from_date').format('YYYY-MM-DD hh:mm:ss') : '',
				this.get('to_date')   ? '&to=' + this.get('to_date').format('YYYY-MM-DD hh:mm:ss') : ''
			].join('');
		},
		initialize: function initialize() {
			reqres.setHandlers({
				'get:booking-project': {
					callback: function() {
						return this;
					},
					context: this
				}
			});
		},
		calcCalendar: function calcCalendar() {
			var releaseDate       = this.get('from_date'),
				currentMonthStart = moment(releaseDate).startOf('month'),
				nextMonthStart    = moment(releaseDate).add(1, 'month').startOf('month'),
				daysCurrentMonth  = this.formDaysArr(_.range(currentMonthStart.daysInMonth()), currentMonthStart),
				daysNextMonth     = this.formDaysArr(_.range(nextMonthStart.daysInMonth()), nextMonthStart),
				daysLength        = 30,
				finalDays;

			if (daysCurrentMonth.splice(0, releaseDate.date() - 1).length >= daysLength) {
				daysCurrentMonth = daysCurrentMonth.splice(0, daysLength);
				daysNextMonth    = [];
			} else {
				daysNextMonth = daysNextMonth.splice(0, daysLength - daysCurrentMonth.length);
			}

			finalDays = _.union(daysCurrentMonth, daysNextMonth);
			if (finalDays.length > daysLength) finalDays = finalDays.splice(0, daysLength);

			_.each(_.range(2), function() {
				finalDays.unshift(this.formDay(moment(finalDays[0].date).add(-1, 'days')));
			}, this);

			this.set({
				days: finalDays,
				date: releaseDate.toDate(),

				currentMonth: releaseDate.month(),
				currentYear:  releaseDate.year(),
				currentWeeks: releaseDate.weeks(),

				daysCurrentMonth: daysCurrentMonth,
				daysNextMonth:    daysNextMonth
			});
		},
		formDaysArr: function formDaysArr(daysNumbers, monthStart) {
			return _.map(daysNumbers, function(dayNumber) {
				var day = moment(monthStart).add(dayNumber, 'days');
				return this.formDay(day);
			}, this);
		},
		formDay: function formDay(day) {
			return {
				date:         day.toDate(),
				day_of_month: day.date(),
				day_of_week:  day.day()
			};
		}
	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.BookingModel = Backbone.Model.extend({
		defaults: {
			id: null,
			title: null,
			requests: [],
			date: {}
		}
	});

	window.app.BookingCollection = Backbone.Collection.extend({
		model: window.app.BookingModel,
		weekday: {
			1: 'Пн',
			2: 'Вт',
			3: 'Ср',
			4: 'Чт',
			5: 'Пт',
			6: 'Сб',
			7: 'Вс'
		},
		initialize: function initialize() {
			Backbone.trigger('loader', 'show');
			this.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
				}
			});
		},
		url: function() {
			return '/api/booking.json';
		},
		parse: function(r) {
			return _.map(r, function(release) {
				var date = moment(release.released);

				release.date = {
					day: date.format('D'),
					weekday: this.weekday[date.format('E')],
					month: date.format('MMM')
				};

				return release;
			}, this);
		}
	});
});

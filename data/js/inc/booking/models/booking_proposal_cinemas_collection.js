$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.BookingProposalCinema = Backbone.Model.extend({
		defaults: {
			id: null,
			title: null,
			city: null,
			halls: [],
			checkedHalls: []
		},
		getKeysEmails: function() {
			var address = [], i;
			_.each(this.get('halls'), function(r) {
				for (i in r.keys) {
					address = _.union(address, r.keys[i].email.split('; '));
				}
			});
			return address;
		}
	});

	window.app.BookingProposalCinemas = Backbone.Collection.extend({
		model: window.app.BookingProposalCinema,
		initialize: function() {
			Backbone.trigger('loader', 'show');
			this.fetch({
				success: function() {
					Backbone.trigger('loader', 'hide');
				}
			});

			commands.setHandlers({
				'reset:booking-proposal:cinemas': {
					callback: function() {
						_.each(this.models, function(model) { model.set({checkedHalls: []}); });
					},
					context: this
				}
			});
		},
		url: function() {
			return '/api/booking/cinema.json';
		}
	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.BookingProjectWrapView = Backbone.View.extend({
		id: 'booking_table',
		className: 'booking_scroll',
		initialize: function initialize(options) {
			this.subViews = [];

			this.listenTo(this.model, 'request', function() {
				Backbone.trigger('loader', 'show');
			});

			this.listenTo(this.model, 'sync', function() {
				Backbone.trigger('loader', 'hide');
			});

			this.model.calcCalendar();
			this.model.fetch({
				success: _.bind(function(model) {
					var view = new window.app.BookingProjectView({
						model: model,
						newBooking: options.newBooking
					});
					this.$el.append(view.render().el);
					this.subViews.push(view);
				}, this)
			});
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			commands.execute('set:booking-project:header', {clean: true});
			this.model.stopListening();
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.BookingProjectView = Backbone.View.extend({
		template: '#booking_project_tmpl',
		events: {
			'click .back a': function(e) {
				e.preventDefault();
				Backbone.trigger('router:navigate', '/booking');
			},
			'click .next, .prev': function(e) {
				var weeksDiff = $(e.currentTarget).data('weeks');

				this.model.set({
					from_date:   moment(this.model.get('from_date')).add(weeksDiff, 'weeks'),
					to_date:     moment(this.model.get('to_date')).add(weeksDiff, 'weeks'),
					first_week:  this.model.get('first_week') + weeksDiff,
					second_week: this.model.get('second_week') + weeksDiff
				});

				this.model.fetch();
			},
			'change .filt_request': function(e) {
				this.model.set({request_id: $(e.currentTarget).val()});
				this.model.fetch();
			},
			'click .btn[data-action="open-popup"]': 'openBookingProposalPopup'
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.subViews = [];

			this.listenTo(this.model, 'sync', function() {
				this.model.calcCalendar();
				this.render();
			});

			if (this.options.newBooking) {
				this.openBookingProposalPopup();
			}
		},
		openBookingProposalPopup: function openBookingProposalPopup() {
			this.bookingProposalView = new window.app.BookingProposalView({
				model:      this.model,
				popupTitle: 'Новый букинг',
				cinemas:    new window.app.BookingProposalCinemas()
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));

			commands.execute('set:booking-project:header', {
				projectName: this.model.get('title'),
				projectDate: this.model.get('from_date').format('с D MMMM') || ''
			});
			this.$('.filt_request').styler();
			this.renderCinemas();
			this.renderSearchingModule();

			return this;
		},
		renderCinemas: function renderCinemas() {
			this.subViews.push(new window.app.BookingProjectCinemasView({
				el:         this.$('#booking_list_table'),
				model:      this.model,
				collection: new Backbone.Collection(this.model.get('cinemas'))
			}));
		},
		renderSearchingModule: function renderSearchingModule() {
			var searchingView = new window.app.SearchingModuleView({
				animation: false,
				onSearchingFinish: function onSearchingFinish(value) {
					commands.execute('filter:booking-project:cinemas', value);
				}
			});
			this.$('.search').html(searchingView.render().el);
			this.subViews.push(searchingView);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

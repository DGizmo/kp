$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.BookingProposalCinemaView = Backbone.View.extend({
		className: 'bp_cinema_item',
		template: '#booking_proposal_one_cinema_tmpl',
		events: {
			'click .head_leb': function(e) {
				if ($(e.target).hasClass('head_leb_input')) return;
				this.$el.toggleClass('open');
			},
			'click .head_leb_input': function(e) {
				var checkedHalls = $(e.currentTarget).is(':checked') ? _.map(this.model.get('halls'), function(hall) { return hall.id; }) : [];
				this.model.set({ checkedHalls: checkedHalls });
				Backbone.trigger('keygen:project-cinema:checked');
			},
			'click .bp_check': function() {
				var checkedHalls = [];

				_.each(this.$('.bp_check'), function(checkBox) {
					if ($(checkBox).prop('checked')) checkedHalls.push(parseInt($(checkBox).val()));
				});

				this.model.set({ checkedHalls: checkedHalls });
				Backbone.trigger('keygen:project-cinema:checked');
			}
		},
		initialize: function initialize() {
			this.listenTo(this.model, 'change', this.render);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});

	window.app.BookingProposalCinemasView = Backbone.View.extend({
		template: '#booking_proposal_cinemas_tmpl',
		initialize: function initialize() {
			this.subViews = [];
			this.render();

			this.listenTo(this.collection, 'sync', function() {
				_.invoke(this.subViews, 'remove');
				this.renderSubViews(this.collection.models);
			});
		},
		render: function remove() {
			var self     = this,
				template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template);

			this.searchingModule = new window.app.SearchingModuleView({
				animation: false,
				onSearchingFinish: _.debounce(function onSearchingFinish(value) {
					self.collection.fetch({data: {title: value}});
				}, 1000)
			});
			this.$('.search').append(this.searchingModule.render().el);
			this.renderSubViews(this.collection.models);

			return this;
		},
		renderSubViews: function renderSubViews(models) {
			_.each(models, function(model) {
				var view = new window.app.BookingProposalCinemaView({model: model});
				this.subViews.push(view);
				this.$('.bp_cinemas_list').append(view.render().el);
			}, this);
		},
		remove: function remove() {
			this.$el.empty();
			commands.execute('reset:booking-proposal:cinemas');
			_.invoke(this.subViews, 'remove');
			this.searchingModule.remove();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

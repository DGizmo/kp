$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.BookingReleaseView = Backbone.View.extend({
		className: 'date-point',
		template: '#booking_release_tmpl',
		events: {
			'click .releases_body .info_item': function(e) {
				Backbone.trigger('router:navigate', $(e.currentTarget).data('href'));
			},
			'click .new_booking': function(e) {
				Backbone.trigger('router:navigate', $(e.currentTarget).closest('.info_item').data('href') + '/newBooking');
			}
		},
		render: function() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});

	window.app.BookingReleasesListView = Backbone.View.extend({
		id: 'bookings_wrap',
		className: 'release-wrapper',
		template: '#booking_releases_list_tmpl',
		initialize: function initialize() {
			this.subViews = [];
			this.listenTo(this.collection, 'sync', function() {
				this.renderBookingReleases(this.collection.models);
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template());

			this.renderBookingReleases(this.collection.models);
			this.addSearchingModule();

			return this;
		},
		renderBookingReleases: function renderBookingReleases(models) {
			_.each(models, function(model) {
				var view = new window.app.BookingReleaseView({model: model});
				this.subViews.push(view);
				this.$('#booking_wr').append(view.render().el);
			}, this);
		},
		addSearchingModule: function addSearchingModule() {
			var searchingReleases,
				packagesTitles;

			this.releaseSearching = new window.app.SearchingModuleView({
				animation: true,
				onSearchingFinish: _.bind(function onSearchingFinish(value) {
					searchingReleases = _.filter(this.collection.models, function(model) {
						packagesTitles = _.chain(model.get('requests'))
							.map(function(packageItem) { return packageItem.title; })
							.filter(function(packageTitle) { return packageTitle.search(new RegExp(value, 'i')) > -1; })
							.value();

						return packagesTitles.length;
					});

					_.invoke(this.subViews, 'remove');
					this.renderBookingReleases(searchingReleases);
				}, this)
			});
			this.$('.filter').append(this.releaseSearching.render().el);
			this.releaseSearching.$el.addClass('searching-module--right');
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.releaseSearching.remove();
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

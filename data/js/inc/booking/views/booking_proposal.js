$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.BookingProposalView = Backbone.View.extend({
		className: 'booking_popup',
		tagName: 'form',
		template: '#booking_proposal_tmpl',
		events: {
			'click .exit': function() {
				$.arcticmodal('close');
			},
			'focusout .since_time, .till_time': function(e) {
				var $target      = $(e.currentTarget),
					defaultValue = $target.attr('value');

				_.each($target.val().split(':'), function(part) {
					if (part.length < 2) $target.val(defaultValue);
				});

				$target.val(this.validateValue($target.val()));
			},
			'click .add_save': function(e) {
				e.preventDefault();
				var hall_and_request = this.$el.serialize(),
					generate_key     = this.$('.generate_key').prop('checked') ? 1 : 0,
					dcp_project_id   = this.model.get('project_id'),
					valid_since      = this.$('.valid_since').val() + ' ' + this.$('.since_time').val(),
					valid_till       = this.$('.valid_till').val() + ' ' + this.$('.till_time').val(),
					param            = hall_and_request + '&dcp_project_id=' + dcp_project_id + '&valid_since=' + valid_since + '&valid_till=' + valid_till + '&generate_key=' + generate_key;

				if (hall_and_request.search('hall_id') >= 0 && hall_and_request.search('request_id') >= 0 && this.$('.valid_since').val() && $('.valid_till').val()) {
					$.post('/api/booking/queue', param, function(r) {
						if (r.result === 'Success') {
							$.arcticmodal('close');
							$.growl.notice({ title: 'Готово', message: 'Данные поставлены в очередь на обработку' });
						}
					});
				} else {
					$.growl.error({ title: 'Ошибка', message: 'Не все данные отмечены' });
				}
			},
			'click .week': function(e) {
				var days = $(e.currentTarget).data('days');
				this.$('.week').removeClass('active');
				$(e.currentTarget).addClass('active');

				if (this.selectedDate) {
					this.$('.daterange').daterangepicker('setRange', {
						start: moment(this.selectedDate),
						end: moment(this.selectedDate).add(days, 'days')
					});
					this.$('.comiseo-daterangepicker-calendar.ui-widget-content.hasDatepicker').datepickerui('setDate', moment(this.selectedDate).toDate());
				}
			},
			'click .more': function(e) {
				this.$('.bp_package_block .hide').toggleClass('show');
				if (this.$('.bp_package_block .hide').hasClass('show')) {
					$(e.currentTarget).text('Скрыть');
				} else {
					$(e.currentTarget).text('Ещё');
				}
			}
		},
		initialize: function initialize(options) {
			this.options  = options || {};
			this.subViews = [];

			$('body').append(this.render().el);
			this.$el.arcticmodal({
				afterOpen: _.bind(function() {
					this.setDateRangePicker();
				}, this),
				afterClose: _.bind(function() {
					this.remove();
				}, this)
			});
		},
		render: function render() {
			var self     = this,
				template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({
				popupTitle:   this.options.popupTitle,
				title:        this.model.get('title'),
				requests:     this.model.get('requests'),
				request_id:   this.model.get('request_id'),
				release_date: this.model.get('release_date')
			}));

			this.subViews.push(new window.app.BookingProposalCinemasView({
				el:         this.$('.bp_cinemas'),
				collection: this.options.cinemas
			}));

			this.$('.since_time, .till_time').mask('99:99:99', {
				placeholder: '',
				autoclear: 0,
				completed: function() {
					this.val(self.validateValue(this.val()));
				}
			});

			setTimeout(function() {
				self.setDateRangePicker();
			}, 0);

			return this;
		},
		setDateRangePicker: function setDateRangePicker() {
			this.$('.daterange').daterangepicker({
				datepickerOptions: {
					numberOfMonths: 2,
					showButtonPanel: false,
					regional: 'ru',
					minDate: null,
					maxDate: null
				}
			});
			this.$('.daterange').daterangepicker('open');
			this.$('.comiseo-daterangepicker-calendar.ui-widget-content.hasDatepicker').datepickerui('setDate', moment(this.model.get('release_date')).toDate());
			this.selectedDate = this.model.get('release_date');
		},
		validateValue: function validateValue(value) {
			return _.map(value.split(':'), function(part, index) {
				if (index) {
					part = parseInt(part) > 59 ? '59' : part;
				} else {
					part = parseInt(part) > 23 ? '23' : part;
				}

				return part;
			}).join(':');
		},
		remove: function remove() {
			if (this.options.cinemas) this.options.cinemas.stopListening();
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

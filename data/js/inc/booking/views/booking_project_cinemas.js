$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.BookingProjectCinemasView = Backbone.View.extend({
		template: '#booking_project_cinemas_tmpl',
		initialize: function initialize() {
			this.subViews = [];
			this.days = reqres.request('get:booking-project').get('days');
			this.render();

			commands.setHandlers({
				'filter:booking-project:cinemas': {
					callback: function(value) {
						_.invoke(this.subViews, 'remove');
						this.$('tbody').empty();

						if (value.length) {
							var searchingCinemas = _.filter(this.collection.models, function(model) {
								return model.get('title').search(new RegExp(value, 'i')) > -1;
							});
							this.renderCinemas(searchingCinemas);
							this.addEmptyStrings(this.days, searchingCinemas);
						} else {
							this.renderCinemas(this.collection.models);
							this.addEmptyStrings(this.days, this.collection.models);
						}
					},
					context: this
				}
			});
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({days: this.days}));
			this.renderCinemas(this.collection.models);
			this.addEmptyStrings(this.days, this.collection.models);
			return this;
		},
		renderCinemas: function renderCinemas(models) {
			_.each(models, function(model) {
				var view = new window.app.BookingProjectCinemaView({model: model});
				this.$('tbody').append(view.render().el);
				this.subViews.push(view);
			}, this);
		},
		addEmptyStrings: function addEmptyStrings(days, models) {
			var emptyStrings = _.range(Math.ceil(($('.wrapper').height() - 100 - (models.length * 44)) / 44)  + 1),
				emptyString;

			_.each(emptyStrings, function() {
				emptyString = [];

				emptyString.push('<tr><td class="left"></td>');
				_.each(days, function(day) {
					emptyString.push('<td class="' + (day.day_of_week === 3 ? 'start_w' : '') + (day.day_of_month === 1 ? ' start_month' : '') + '"></td>');
				});
				emptyString.push('</tr>');

				this.$('tbody').append(emptyString.join(''));
			}, this);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.BookingProjectCinemaView = Backbone.View.extend({
		tagName: 'tr',
		template: '#booking_project_cinema_tmpl',
		events: {
			'click .project_cinema': function(e) {
				if (reqres.request('get:booking-project').get('request_id') === 'all') return;
				var $target = $(e.currentTarget),
					view;

				$target.toggleClass('active');
				if (!$target.hasClass('active')) {
					_.invoke(this.subViews, 'remove');
				} else {
					view = new window.app.BookingProjectCinemaHallsView({model: this.model});
					this.$el.after(view.render().el);
					this.subViews.push(view);
				}
			}
		},
		initialize: function initialize() {
			this.subViews = [];
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.model.set({
				days:       reqres.request('get:booking-project').get('days'),
				request_id: reqres.request('get:booking-project').get('request_id')
			});

			this.$el.html(template(this.model.attributes));
			return this;
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.BookingProjectCinemaHallsView = Backbone.View.extend({
		tagName: 'tr',
		template: '#booking_project_cinema_halls_tmpl',
		render: function() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});

});

//-------------------- Изменение расписания --------------------//
$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ScheduleView = Backbone.View.extend({
		tagName: 'div',
		className: 'change_my_popup',
		template: '#schedule_change_tmpl',
		events: {
			'click .exit': /* istanbul ignore next */ function() {
				$.arcticmodal('close');
			},
			'click .btn_export.disabled': /* istanbul ignore next */ function(event) {
				event.preventDefault();
			},
			'click .add': /* istanbul ignore next */ function() {
				this.model.addWeek();

				if (this.options.saveOnChange) this.model.save(this.options.tx);

				if (this.options.page === 'releaseCard') {
					_gaq.push(['_trackEvent', 'Релизы', 'Карточка релиза', 'Роспись::Продление недели']);
				} else if (this.options.page === 'planning') {
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Попап::Продление недели']);
				}
			},
			'click .remove_week': /* istanbul ignore next */ function(e) {
				var date = $(e.currentTarget).closest('.item').data();
				this.model.deleteWeek(date.week, date.year);

				if (this.options.saveOnChange) this.model.save(this.options.tx);

				if (this.options.page === 'releaseCard') {
					_gaq.push(['_trackEvent', 'Релизы', 'Карточка релиза', 'Роспись::Удаление недели']);
				} else if (this.options.page === 'planning') {
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Попап::Удаление недели']);
				}
			},
			'click .save': /* istanbul ignore next */ function() {
				if (this.options.parentModel) this.options.parentModel.set({
					weeks: this.model.get('weeks'),
					color: this.model.get('color')
				});
				$.arcticmodal('close');
				this.removeView();
				this.model.save(reqres.request('get:user').get('tx'));
			},
			'click .active_status': /* istanbul ignore next */ function(e) {
				if (reqres.request('get:user').isRepertoireReadOnly(this.model.get('cinema').id)) return;
				$(e.currentTarget).closest('.choose_wrap').addClass('visible');
			},
			'click .status .choose .item': /* istanbul ignore next */ function(e) {
				var status = parseInt($(e.currentTarget).data('status'));
				$(e.currentTarget)
					.addClass('active').siblings().removeClass('active').end()
					.closest('.choose_wrap').removeClass('visible');
				this.model.set('color', status);

				if (this.options.saveOnChange) this.model.save(this.options.tx);

				if (this.options.page === 'releaseCard') {
					_gaq.push(['_trackEvent', 'Релизы', 'Карточка релиза', 'Роспись::Изменение статуса']);
				} else if (this.options.page === 'planning') {
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Попап::Изменение статуса']);
				}
			},

			// TODO refact
			'click .center .format .text': /* istanbul ignore next */ function(event) { // клик по текстовому виду кол-ва сенсов
				event.stopPropagation();

				var format = $(event.target).closest('.format').data(),
					date = $(event.target).closest('.item').data(),
					currentWeek = _.find(this.model.get('weeks'), function(week) {
						return week.number === date.week && week.year === date.year;
					});

				if (this.scheduleEditSeances) {
					this.scheduleEditSeances.remove();
				}

				this.scheduleEditSeances = new window.app.ScheduleEditSeancesView({
					week: currentWeek,
					technology: currentWeek.technology,

					format: format.format,
					upgrades: format.upgrades.toLowerCase().split(', '),

					tx: reqres.request('get:user').get('tx'),
					date: date,

					halls: this.model.get('cinema').halls,

					cinema_id: this.model.get('cinema_id'),
					release_id: this.model.get('release_id'),

					parent: this,
					saveOnChange: this.options.saveOnChange,
					page: this.options.page
				});

				$(event.target).parent().prepend(this.scheduleEditSeances.render().el).find('.halls-popup-top input').focus().select();

				if (this.options.page === 'releaseCard') {
					_gaq.push(['_trackEvent', 'Релизы', 'Карточка релиза', 'Роспись::Редактирование сеанса']);
				} else if (this.options.page === 'planning') {
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Попап::Редактирование сеанса']);
				}
			},

			'click .new_format__top': /* istanbul ignore next */ function(e) { // Открытие окна добавления кастомного формата
				var $parent = $(e.currentTarget).closest('.new_format'),
					$block = $parent.find('.add_format'),
					$firstFormatItem = $block.find('.list .item').first(),
					$inputs = $block.find('.technology input').not('[data-format="sub"], [data-format="hfr"]');

				$block
					.css({
						marginLeft: -($block.outerWidth() / 2)
					})
					.toggleClass('visible');

				$firstFormatItem.addClass('active');

				$parent
					.toggleClass('active')
					.siblings().find('.custom_format').each(function() {
						$(this).removeClass('visible');
					}).end()
					.siblings().find('.format_custom_block').each(function() {
						$(this).removeClass('active');
					});

				if (_.contains(['public video', 'imax', 'luxe'], $firstFormatItem.data('format').toLowerCase())) {
					$inputs.attr('disabled', 'disabled').removeAttr('checked');
				}
			},
			'click .format_custom_block': /* istanbul ignore next */ function(e) { // Открытие окна редактирования кастомного формата
				var $parent = $(e.currentTarget).closest('.format'),
					$block = $parent.find('.custom_format'),
					$firstFormatItem = $block.find('.list .item').first(),
					$inputs = $block.find('.technology input').not('[data-format="sub"], [data-format="hfr"]');

				if (reqres.request('get:user').isRepertoireReadOnly(this.model.get('cinema').id)) return;

				$block
					.css({
						marginLeft: -($block.outerWidth() / 2)
					})
					.toggleClass('visible');

				$(e.currentTarget).toggleClass('active');

				$parent
					.siblings('.new_format').removeClass('active').end()
					.siblings().find('.custom_format, .add_format').each(function() {
						$(this).removeClass('visible');
					}).end()
					.siblings().find('.format_custom_block').each(function() {
						$(this).removeClass('active');
					});

				if (_.contains(['public video', 'imax', 'luxe'], $firstFormatItem.data('format').toLowerCase())) {
					$inputs.attr('disabled', 'disabled').removeAttr('checked');
				}
			},

			'click .add_format .list .item': /* istanbul ignore next */ function(e) { // смена активного статуса у основного формата в окне кастомного формата
				var $target = $(e.currentTarget),
					$inputs = $target.closest('.add_format').find('.technology input').not('[data-format="sub"], [data-format="hfr"]');

				$target.addClass('active').siblings().removeClass('active');

				if (_.contains(['public video', 'imax', 'luxe'], $target.data('format').toLowerCase())) {
					$inputs.attr('disabled', 'disabled').removeAttr('checked');
				} else {
					$inputs.removeAttr('disabled');
				}
			},
			'click .custom_format .list .item': /* istanbul ignore next */ function(e) { // смена активного статуса у основного формата в окне кастомного формата
				var $target = $(e.currentTarget),
					$inputs = $target.closest('.custom_format').find('.technology input').not('[data-format="sub"], [data-format="hfr"]');

				$target.addClass('active').siblings().removeClass('active');

				if (_.contains(['public video', 'imax', 'luxe'], $target.data('format').toLowerCase())) {
					$inputs.attr('disabled', 'disabled').removeAttr('checked');
				} else {
					$inputs.removeAttr('disabled');
				}
			},

			'click .add_format .technology input': /* istanbul ignore next */ function(e) { // блокировка выбора несовместимых улучшателей в окне кастомного формата
				$(e.currentTarget)
					.closest('.couple__item')
					.siblings().find('input').removeAttr('checked');
			},
			'click .custom_format .technology input': /* istanbul ignore next */ function(e) { // блокировка выбора несовместимых улучшателей в окне кастомного формата
				var $block = $(e.currentTarget).closest('.format');

				$(e.currentTarget)
					.closest('.couple__item')
					.siblings().find('input').removeAttr('checked');

				if ($block.find('.couple__item input:checked').length) {
					$block.find('.save_format_pack').show();
				} else {
					$block.find('.save_format_pack').hide();
				}
			},

			'click .add_format_pack, .save_format_pack, .remove_format': /* istanbul ignore next */ function(e) { // добавление кастомного формата
				var $target = $(e.currentTarget),
					$block = $target.closest('.format, .add_format'),
					format = $block.find('.name').text().toLowerCase() || $block.find('.list .active').data('format').toLowerCase() || '',
					upgrades = $block.find('.upgrades').text().toLowerCase().split(', '),
					newUpgrades = _.map($block.find('input:checkbox:checked'), function(item) {
						return $(item).data('format').toLowerCase();
					}) || [];

				this.model.changeFormat(format === 'pv' ? 'Public Video' : format, upgrades, newUpgrades, $target.data('action'));

				if (this.options.saveOnChange) this.model.save(this.options.tx);

				if (this.options.page === 'releaseCard') {
					_gaq.push(['_trackEvent', 'Релизы', 'Карточка релиза', 'Роспись::Новый формат']);
				} else if (this.options.page === 'planning') {
					_gaq.push(['_trackEvent', 'Репертуарное планирование', 'По кинотеатру', 'Попап::Новый формат']);
				}
			},
			click: /* istanbul ignore next */ function(event) {
				if (!$(event.target).closest('.choose_wrap.visible').length) {
					this.$('.choose_wrap.visible').removeClass('visible');
				}
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.options.page = 'planning';
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.removeView);
		},
		render: function render() {
			var template  = Backbone.TemplateCache.get(this.template),
				extModel  = $.extend({}, this.model.attributes),
				cinema    = this.model.get('cinema'),
				scheduleCinemaFormats;

			extModel.cinemaRights = !reqres.request('get:user').isRepertoireReadOnly(cinema.id);
			extModel.colorName = {
				1: 'Утверждён',
				2: 'Рассматривается',
				3: 'Запланировано',
				4: 'Переговоры'
			};

			_.each(extModel.weeks, function(oneWeek) {
				var thirtyFiveMM = _.find(oneWeek.technology, function(t) {
					return t.format.toLowerCase() === '35mm';
				}),
				thirtyFiveIndex;
				if (!_.isUndefined(thirtyFiveMM)) {
					thirtyFiveIndex = _.indexOf(oneWeek.technology, thirtyFiveMM);
					oneWeek.technology.splice(thirtyFiveIndex, 1);
					oneWeek.technology.unshift(thirtyFiveMM);
				}
			});

			extModel.hireRange = this.getHireRange();
			extModel.firstWeekNumber = this.getFirstWeekNumber(extModel.hireRange.sinceDate, extModel.hireRange.untilDate);

			if (this.options.page === 'releaseCard') {
				scheduleCinemaFormats = reqres.request('get:user').getCinema(this.model.get('cinema_id')).formats;
			} else if (this.options.page === 'planning') {
				scheduleCinemaFormats = reqres.request('get:user').getCinema().formats;
			}

			extModel.allowedFormats = _.intersection(_.without(scheduleCinemaFormats, '35mm'), _.map(this.model.get('release').formats, function(format) { return format.toLowerCase(); }));

			if (_.contains(scheduleCinemaFormats, 'luxe')) {
				extModel.allowedFormats.push('luxe');
			}

			if (_.contains(scheduleCinemaFormats, 'Public Video')) {
				extModel.allowedFormats.push('Public Video');
			}

			this.$el.html(template(extModel));

			if (extModel.cinemaRights) this.setDatePicker(extModel.hireRange.sinceDate, extModel.hireRange.untilDate);

			return this;
		},
		getFirstWeekNumber: function getFirstWeekNumber(sinceDate) {
			var firstWeekNumber = moment(moment(sinceDate).weekday(0)).diff(moment(moment(this.model.get('release').date.russia.start).weekday(0)), 'weeks') + 1;

			return firstWeekNumber;
		},
		getHireRange: function getHireRange() {
			var firstWeek = _.first(this.model.get('weeks')),
				lastWeek  = _.last(this.model.get('weeks')),
				sinceDate = _.getWeekStartDate({year: firstWeek.year, week: firstWeek.number}),
				untilDate = _.getWeekStartDate({year: lastWeek.year, week: lastWeek.number}),
				hireRange;

			if (firstWeek.days) {
				sinceDate.add(_.first(firstWeek.days) - 1, 'days');
			} else {
				sinceDate.add(moment(this.model.get('release').date.russia.start).weekday(), 'days');
			}

			if (lastWeek.days) {
				untilDate.add(_.last(lastWeek.days) - 1, 'days');
			} else {
				untilDate.add(6, 'days');
			}

			hireRange = {
				sinceDate: sinceDate,
				untilDate: untilDate
			};

			return hireRange;
		},
		setDatePicker: function setDatePicker(sinceDate, untilDate) {
			var self = this;

			this.$el
				.find('.since_date, .until_date')
				.datepicker({
					language: 'ru',
					weekStart: 4,
					autoclose: true,
					format: 'dd.mm.yyyy',
					calendarWeeks: true
				})
				.on('hide', function() {
					self.changeHireRange();
				})
				.on('show', function() {
					$('.datepicker .day.active').addClass('disabled');
				});

			this.$el
				.find('.since_date')
				.datepicker('setEndDate', untilDate.format('DD.MM.YYYY'));
			this.$el
				.find('.until_date')
				.datepicker('setStartDate', sinceDate.format('DD.MM.YYYY'));
		},
		changeHireRange: function changeHireRange() {
			var self        = this,
				sinceDate   = moment(this.$el.find('.since_date').val(), 'DD.MM.YYYY'),
				untilDate   = moment(this.$el.find('.until_date').val(), 'DD.MM.YYYY'),
				lastDayNum  = moment(untilDate).subtract(4, 'days').day() + 1,
				firstDayNum = moment(sinceDate).subtract(4, 'days').day() + 1,
				weeksLength = moment(moment(untilDate).weekday(0)).diff(moment(moment(sinceDate).weekday(0)), 'weeks') + 1,
				newWeeks    = [],
				i           = 0,
				searchingWeek;

			while (i < weeksLength) {
				newWeeks.push({
					number: moment(moment(sinceDate).weekday(0)).add(7 * i, 'days').weeks(),
					year:   moment(moment(sinceDate).weekday(0)).add(7 * i, 'days').year()
				});

				i++;
			}

			_.each(newWeeks, function(newWeek) {
				searchingWeek = _.find(self.model.get('weeks'), function(week) { return week.number === newWeek.number; });

				newWeek.count      = searchingWeek ? searchingWeek.count : 0;
				newWeek.technology = searchingWeek ? searchingWeek.technology : self.model.createNewTechnology();
			});

			if (newWeeks.length > 1) {
				_.first(newWeeks).days = _.range(firstDayNum, 8);
				_.last(newWeeks).days = _.range(1, lastDayNum + 1);
			} else if (newWeeks.length === 1) {
				newWeeks[0].days = _.range(firstDayNum, lastDayNum + 1);
			}

			if (newWeeks.length > 2) {
				_.each(_.rest(_.initial(newWeeks)), function(week) {
					week.days = _.range(1, 8);
				});
			}

			this.checkDeletedWeeks(newWeeks);
		},
		checkDeletedWeeks: function checkDeletedWeeks(newWeeks) {
			var deletedWeeks = [],
				agree        = true,
				searchingWeek;

			_.each(this.model.get('weeks'), function(week) {
				searchingWeek = _.find(newWeeks, function(newWeek) { return week.number === newWeek.number; });
				if (!searchingWeek && week.count) deletedWeeks.push(week);
			});

			if (deletedWeeks.length) agree = confirm(this.warnAboutDeletedWeeks(deletedWeeks));

			if (agree) {
				this.model.set({
					weeks: newWeeks
				});
				if (this.options.saveOnChange) this.model.save(this.options.tx);
			} else {
				this.render();
			}
		},
		warnAboutDeletedWeeks: function warnAboutDeletedWeeks(deletedWeeks) {
			var message = 'Вы изменили сроки проката фильма.\n\n',
				startDay,
				endDay,
				startMonth,
				endMonth,
				techWithCount;

			_.each(deletedWeeks, function(week) {
				techWithCount = [];
				startDay      = _.getWeekStartDate({year: week.year, week: week.number}).date();
				endDay        = _.getWeekEndDate({year: week.year, week: week.number}).date();
				startMonth    = _.getWeekStartDate({year: week.year, week: week.number}).format('MMM');
				endMonth      = _.getWeekEndDate({year: week.year, week: week.number}).format('MMM');

				_.each(week.technology, function(oneTech) {
					if (oneTech.count) techWithCount.push(oneTech);
				});

				if (startMonth === endMonth) {
					message += 'Неделя ' + startDay + ' - ' + endDay + ' ' + startMonth + '. будет удалена. На ней запланировано\n';
				} else {
					message += 'Неделя ' + startDay + ' ' + startMonth + '. - ' + endDay + ' ' + endMonth + '. будет удалена. На ней запланировано\n';
				}

				if (techWithCount.length === 1) {
					message += week.technology[0].count + ' сеанса(ов) в ' + week.technology[0].format.toUpperCase();
					message += week.technology[0].upgrades.length ? ' ' + week.technology[0].upgrades.join(', ').toUpperCase() : '.\n';
				} else {
					_.each(techWithCount, function(oneTech, index) {
						message += oneTech.count + ' сеанса(ов) в ' + oneTech.format.toUpperCase();
						message += oneTech.upgrades.length ? ' ' + oneTech.upgrades.join(', ').toUpperCase() : '';
						message += index === techWithCount.length - 1 ? '.\n' : ',\n';
					});
				}

				message += '\n';
			});

			message += 'Вы уверены?';

			return message;
		},
		removeView: function removeView() {
			this.$el.find('.since_date, .until_date').datepicker('remove');
			this.unbind();
			this.model.destroy();
			this.remove();
		}
	});

	window.app.ReleasePaintingCinemaView = window.app.ScheduleView.extend({
		className: 'release_painting-item',
		template: '#release_painting_item_tmpl',
		initialize: function initialize(options) {
			this.options = options || {};
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.removeView);
			this.listenTo(Backbone, 'schedule:save:done', function() {
				Backbone.trigger('releaseCard:model:change', {id: this.model.get('release_id')});
			});
		}
	});

	window.app.ScheduleEditSeancesView = Backbone.View.extend({
		className: 'halls-popup',
		template: '#schedule_edit_seances_tmpl',
		events: {
			mousewheel: 'mouseWheel',
			DOMMouseScroll: 'mouseWheel',
			click: function(event) {
				event.stopPropagation();
			},
			'click .halls-popup-top__input-plus': /* istanbul ignore next */ function() {
				this.changeInputCount(1);
			},
			'click .halls-popup-top__input-minus': /* istanbul ignore next */ function() {
				this.changeInputCount(-1);
			},
			'focusout .halls-popup-top__input': /* istanbul ignore next */ function() {
				this.changeInputCount();
			},
			'keydown .halls-popup-top__input': /* istanbul ignore next */ function(event) {
				// Allow: backspace, delete, tab, escape, enter
				if ($.inArray(event.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
					// Allow: Ctrl+A
					(event.keyCode === 65 && event.ctrlKey === true) ||
					// Allow: home, end, left, right
					(event.keyCode >= 35 && event.keyCode <= 40)) {
					// let it happen, don't do anything
					return;
				}
				// Ensure that it is a number and stop the keypress
				if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
					event.preventDefault();
				}
			},
			'click .halls-popup-header__all input': /* istanbul ignore next */ function(event) {
				this.$('.halls-popup-list input').prop('checked', $(event.target).prop('checked'));
			},
			'change .halls-popup-list-hall input': /* istanbul ignore next */ function() {
				var $inputs = this.$('.halls-popup-list input'),
					$checked = $inputs.filter(':checked');
				this.$('.halls-popup-header__all input').prop('checked', $checked.length === $inputs.length);
			},
			'click .halls-popup__save': 'changeSeances'
		},
		mouseWheel: /* istanbul ignore next */ function mouseWheel(event) {
			var $input = this.$('.halls-popup-top__input:focus'),
				wheelDelta = event.originalEvent.detail ? event.originalEvent.detail * (-1) : event.originalEvent.wheelDelta;
			if ($input.length) {
				wheelDelta > 0 ? this.changeInputCount(1) : this.changeInputCount(-1);
			}
			event.preventDefault();
		},
		changeInputCount: /* istanbul ignore next */ function changeInputCount(value) {
			var $input = this.$('.halls-popup-top__input'),
				currentVal = +$input.val(),
				newVal = currentVal + (value || 0);

			if ((currentVal === 0 && value < 0) || (currentVal === 99 && value > 0)) {
				return this;
			} else if (newVal.length === 0 || newVal < 0 || newVal > 99 || !_.isFinite(newVal)) {
				$input.val(0);
			} else {
				$input.val(newVal);
			}

			return this;
		},
		initialize: function initialize(options) {
			this.options = options || {};
			var self = this;
			$(document).on('keydown', /* istanbul ignore next */ function(event) {
				if (event.keyCode === 13) {
					self.changeInputCount().changeSeances();
				}
			});
			this.listenTo(Backbone, 'body:click', this.remove);
		},
		changeSeances: function changeSeances() {
			var technologyCount = parseInt(this.$('.halls-popup-top__input').val()),
				checkedHalls = _.map(this.$('.halls-popup-list').find('input:checkbox:checked'), function(item) {
					return parseInt($(item).data('hall-id'));
				});
			this.updateCount(checkedHalls, technologyCount).save();
		},
		updateCount: function updateCount(checkedHalls, technologyCount) {
			var currentTechnology = this._findTechnology(this.options.technology, this.options.format, this.options.upgrades),
				newHalls = {};

			_.each(checkedHalls, function(hallID) {
				if (currentTechnology.hall_id && currentTechnology.hall_id[hallID]) {
					newHalls[hallID] = currentTechnology.hall_id[hallID];
				} else {
					newHalls[hallID] = 0;
				}
			});

			currentTechnology.count = technologyCount;
			currentTechnology.hall_id = newHalls;
			currentTechnology.count_hall = _.reduce(newHalls, function(sum, item) {
				return parseInt(sum) + parseInt(item);
			}, 0);

			return this;
		},
		_concatFormat: function _concatFormat(format, upgrades) {
			if (format === 'Public Video') {
				return format + upgrades.join(',').toLowerCase();
			} else {
				return (format + upgrades.join(',')).toLowerCase();
			}
		},
		_findTechnology: function _findTechnology(technology, format, upgrades) {
			var self = this;
			technology = _.find(technology, function(technology) {
				return (self._concatFormat(technology.format, technology.upgrades) === self._concatFormat(format, upgrades));
			});
			return technology;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				technology = this._findTechnology(this.options.technology, this.options.format, this.options.upgrades),
				columns = Math.ceil(this.options.halls.length / 7);

			this.$el.html(template({
				halls: this.options.halls.sort(function(a, b) {
					return a.number - b.number;
				}),
				technology: technology,
				allChecked: _.keys(technology.hall_id).length === this.options.halls.length ? true : false
			}));

			this.$('.halls-popup-list').css({
				'-webkit-column-count': columns + '',
				'-moz-column-count': columns + '',
				'column-count': columns + ''
			});

			return this;
		},
		save: function save() {
			var data,
				postData;

			if (this.options.week) {
				this.options.week.count = _.reduce(this.options.technology, function(sum, item) {
					return parseInt(sum) + parseInt(item.count);
				}, 0);
			}

			if (this.options.parent && this.options.parent.model) this.options.parent.model.trigger('change');

			data = {
				tx: this.options.tx,
				week: this.options.date.week,
				year: this.options.date.year,
				cinema_id: this.options.cinema_id,
				release_id: this.options.release_id,
				technology: this.options.technology
			};

			if (this.options.saveOnChange) {
				postData = _.clone(data);
				postData.technology = $.toJSON(postData.technology);

				$.post('/api/schedule/change_count', postData)
					.done(function() {
						Backbone.trigger('technology:save:done', _.omit(data, 'tx'));
					});
			}

			this.remove();
		},
		remove: function remove() {
			$(document).off('keydown');
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.SidebarView = Backbone.View.extend({
		tagName: 'nav',
		className: 'sidebar',
		template: '#sidebar_tmpl',
		events: {
			'click .list': /* istanbul ignore next */ function(e) {
				e.preventDefault();
				if ($(e.target).hasClass('timeline_submenu')) return;

				var page = $(e.currentTarget).attr('page');
				Backbone.trigger('router:navigate', '/' + page);
			},
			'click .timeline_submenu': function() {
				_gaq.push(['_trackEvent', 'Расписание', 'Таймлайн::Открытие']);
				Backbone.trigger('router:navigate', '/schedule/timeline');
			},
			'click .feedback': /* istanbul ignore next */ function() {
				commands.execute('show:feedback-popup');
			}
		},
		initialize: function initialize() {
			this.model = reqres.request('get:user');

			this.listenTo(this.model, 'change:photo_medium', this.render);
			$('body').prepend(this.render().el);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});
});

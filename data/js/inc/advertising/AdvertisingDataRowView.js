$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.AdvertisingDataRowView = Backbone.View.extend({
		className: 'advertising-data-row',
		initialize: function initialize(options) {
			this.options = _.defaults(options, {
				ElementConstructor: options.readOnly ?
					window.app.AdvertisingDataRowReadonlyElementView :
					window.app.AdvertisingDataRowElementView,
				ageRestrictions: false
			});

			this.subViews = [];

			this.listenTo(this.model,            'change', this.updateTrailers);
			this.listenTo(this.model,            'remove', this.remove);
			this.listenTo(this.model.collection, 'reset',  this.remove);

			this.listenTo(Backbone, 'set:advertising-age-restrictions', function(ageRestrictions) {
				this.options.ageRestrictions = ageRestrictions;
			});

			if (this.options.readOnly) this.$el.addClass('readonly');
		},

		updateTrailers: function updateTrailers() {
			var trailers = _.union(this.model.get('trailers'), this.model._previousAttributes.trailers);
			_.each(this.subViews, function(view) {
				if (_.contains(trailers, view.model.id)) view.render();
			});
			this.model.trigger('trailer:added');
		},
		addTrailersGroup: function addTrailersGroup(trailersGroup, trailersGroupLength) {
			var view;
			_.each(trailersGroup, function(model, i) {
				view = new this.options.ElementConstructor({
					parent: this,
					model:  model,
					positionInGroup: (function(trailersGroupLength, i) {
						if (trailersGroupLength === 1) return 'single';
						if (i === 0) return 'first';
						if (trailersGroupLength - 1 === i) return 'last';
					})(trailersGroupLength, i)
				});
				this.$el.append(view.render().el);
				this.subViews.push(view);
			}, this);
		},
		renderElements: function renderElements() {
			_.each(this.options.groups, function(trailersGroup) {
				this.addTrailersGroup(trailersGroup, trailersGroup.length);
			}, this);

			return this;
		},
		render: function render() {
			this.renderElements();
			return this;
		},
		remove: function remove() {
			var releasesRowsGroups;
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			if (!this.$el.siblings().length) {
				this.$el.parent().remove();
				releasesRowsGroups = this.options.parent.subViews.releasesRowsGroups;
				releasesRowsGroups.splice(_.indexOf(releasesRowsGroups, this), 1);
			}
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.AdvertisingDataRowElementView = Backbone.View.extend({
		className: 'advertising-data__cell',
		tooltipTemplate: '#advertisingReleaseTrailersTooltip',
		events: {
			mouseenter: 'hover',
			mouseleave: 'hover',
			click:      'click'
		},
		click: function click() {
			var release = this.parent.model,
				trailer = this.model,
				state   = release.containsTrailer(trailer),
				ageRestrictions = this.parent.options.ageRestrictions,
				trailers,
				action;

			if (state === 'inner') return false;

			if (state === 'added') {
				trailers = _.reject(release.get('trailers'), function(oldTrailer) {
					return oldTrailer.id === trailer.id;
				});
				action = 'removed';
			} else {
				if (ageRestrictions && trailer.get('age') > release.get('age')) {
					_.invoke([release, trailer], 'trigger', 'wrongRating');
					$.growl.error({message: 'Вы пытаетесь добавить трейлер со слишком большим возрастным рейтингом для этого фильма.'});
					return false;
				} else if (trailer.id === release.id) {
					$.growl.error({message: 'Добавить трейлер фильма, к его же фильму не получится.'});
					return false;
				}

				trailers = release.get('trailers').concat(trailer.toJSON());
				action = 'added';
			}

			release.save({trailers: trailers}, {
				patch: true,
				wait:  true,
				success: _.bind(function success() {
					this.render().model.trigger('added');
					release.trigger('trailer:' + action);
				}, this)
			});
		},
		hover: function hover(event) {
			var eventType = event.type,
				release   = this.parent.model,
				trailer   = this.model,
				ageRestrictions = this.parent.options.ageRestrictions;

			trailer.trigger('hover', eventType);

			if (trailer.id === release.id || (ageRestrictions && trailer.get('age') > release.get('age'))) {
				if (eventType === 'mouseenter' && !release.containsTrailer(trailer))
					this.$el.addClass('blocked');
				if (eventType === 'mouseleave')
					this.$el.removeClass('blocked');
			}

			Backbone.trigger('advertising:element:hover', {
				release: release.id,
				inner:   release.get('integrated_trailers'),
				added:   release.get('trailers')
			});
		},

		/*====================*/

		initialize: function initialize(options) {
			this.options = options;
			this.parent  = this.options.parent;

			this.listenTo(this.model, 'remove', this.remove);
			this.listenTo(this.model, 'hover',  function(type) {
				if (type === 'mouseenter') this.$el.addClass('hover');
				if (type === 'mouseleave') this.$el.removeClass('hover');
			});
		},
		render: function render() {
			var tooltipTemplate = Backbone.TemplateCache.get(this.tooltipTemplate),
				position = this.options.positionInGroup,
				release  = this.parent.model,
				trailer  = this.model,
				state    = release.containsTrailer(trailer);

			this.$el.attr('class', 'advertising-data__cell');

			if (position) this.$el.addClass('advertising-data__cell--' + position);

			if (state) {
				this.$el.addClass('advertising-data__cell--' + state);
				if (state === 'inner') this.$el.tipsy({
					html: true,
					title: _.bind(function() {
						return tooltipTemplate(release.toJSON());
					}, this)
				});
			}

			if (trailer.get('age') > release.get('age')) {
				this.$el.attr('title', 'Трейлер со слишком большим возрастным рейтингом для этого фильма.');
			} else if (trailer.id === release.id) {
				this.$el.attr('title', 'Добавить трейлер фильма, к его же фильму не получится.');
			}

			if (moment(trailer.get('date_end')).diff(_.getWeekStartDate(), 'weeks') === 0)
				this.$el.addClass('advertising-data__cell--last-week');

			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.AdvertisingDataRowReadonlyElementView = window.app.AdvertisingDataRowElementView.extend({
		events: {
			mouseenter: 'hover',
			mouseleave: 'hover'
		}
	});

});

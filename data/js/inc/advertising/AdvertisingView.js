$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.AdvertisingView = Backbone.View.extend({
		className: 'advertising',
		templates: {
			wrapper: '#advertisingWrapper',
			empty: _.template([
				'<div class="advertising-empty">',
					'<div class="advertising-empty-center">',
						'<span class="advertising-empty__warning">Данные о росписи фильмов отсутствуют.</span>',
						'<a class="advertising-empty__link" href="releases">Добавить в репертуар</a>',
					'</div>',
				'</div>'
			].join(''))
		},
		ui: {},
		events: {
			'mouseenter .advertising-data':               'hover',
			'mouseleave .advertising-data':               'hover',
			'click  .advertising-empty__link':            'showReleases',
			'click  .advertising__mode':                  'changeMode',
			'click  .advertising-sections__item': function(event) {
				$(event.currentTarget)
					.addClass('active')
					.siblings()
					.removeClass('active');
			},
			'change .advertising-age-restrictions input': 'toggleAgeRestrictions'
		},
		hover: function hover() {
			this.ui.trailersContainer.toggleClass('advertising-trailers--highlighted');
		},
		showReleases: function showReleases(event) {
			event.preventDefault();
			Backbone.trigger('router:navigate', 'releases/');
		},
		changeMode: function changeMode(event) {
			event.preventDefault();
			var $target = $(event.currentTarget);

			$target
				.addClass('active')
				.siblings()
				.removeClass('active');

			this.options.mode = $target.data('mode');
			this.fetchCollections();
		},
		toggleAgeRestrictions: function toggleAgeRestrictions(event) {
			var ageRestrictions = $(event.currentTarget).prop('checked');
			this.options.ageRestrictions = ageRestrictions;

			Backbone.trigger('set:advertising-age-restrictions', ageRestrictions);
			$.cookie('advertising-age-restrictions', ageRestrictions ? ageRestrictions : '', {expires: 365});

			_gaq.push(['_trackEvent', 'Медиапланирование трейлеров', 'Возрастные ограничения ' +
				(ageRestrictions ? 'включены' : 'выключены')]);
		},
		scroll: function scroll() {
			this.ui.releasesContainer.scrollTop(this.ui.dataFieldWrapper[0].scrollTop);
			this.ui.trailersContainer.scrollLeft(this.ui.dataFieldWrapper[0].scrollLeft);
		},
		isReadOnly: function isReadOnly() {
			var user = this.user;
			return user.isMechanic() || (user.isReadOnly() && !user.isMaster());
		},

		/*====================*/

		initialize: function initialize(options) {
			this.options = _.defaults(options || {}, {
				scrollWidth: _.getScrollBarWidth(),
				ageRestrictions: Boolean($.cookie('advertising-age-restrictions')) || false,
				readOnly: this.isReadOnly(),
				mode: 'current-week'
			});

			this.subViews = {
				releasesGroups: [],
				releasesRowsGroups: []
			};

			this.user = reqres.request('get:user');

			if (this.user.isApproved()) {
				this.collections = this.options.collections;

				this.listenTo(Backbone, 'cinema-changed', this.fetchCollections);
				this.listenTo(this.collections.releases, 'add:release', this.addRelease);

				this.fetchCollections();
				Backbone.TemplateCache.get('#advertisingReleaseTrailersTooltip');
			} else {
				this.$el.html(Backbone.TemplateCache.get('#unapprovedTemplate')({
					accessTo: 'Медиапланированию трейлеров'
				}));
			}
		},

		fetchCollections: function fetchCollections() {
			var releases      = this.collections.releases,
				trailers      = this.collections.trailers,
				currentMode   = this.options.mode,
				currentCinema = this.user.getCinema(),
				fetchOptions  = {
					reset: true,
					data: {
						mode:      currentMode,
						cinema_id: currentCinema.id
					}
				},
				collections = [];

			this.options.readOnly = this.isReadOnly();

			if (this.ui.dataFieldWrapper) this.ui.dataFieldWrapper.unbind('scroll');

			if (releases.length === 0 || releases.options.cinemaID !== currentCinema.id)
				collections.push(releases);

			if (trailers.length === 0 || trailers.options.mode !== currentMode)
				collections.push(trailers);

			Backbone.trigger('loader', 'show');
			$.when.apply($, _.invoke(collections, 'fetch', fetchOptions))
				.done(_.bind(function() {
					trailers.trigger('reset', {});
					if (releases.length) {
						this.renderWrapper().renderGroups().renderDataField();
						this.ui.dataField.css('width', trailers.length * 39 - _.size(trailers.groupByDate()));
					} else {
						this.$el.html(this.templates.empty());
					}
				}, this))
				.always(function() {
					Backbone.trigger('loader', 'hide');
				});
		},
		renderWrapper: function renderWrapper() {
			var template = Backbone.TemplateCache.get(this.templates.wrapper),
				options = this.options;

			this.$el.html(template({
				ageRestrictions: options.ageRestrictions,
				mode: options.mode
			}));

			this.ui = {
				dataFieldWrapper:  this.$('.advertising-data-wrapper'),
				dataField:         this.$('.advertising-data'),
				releasesContainer: this.$('.advertising-releases'),
				trailersContainer: this.$('.advertising-trailers')
			};

			this.ui.dataFieldWrapper
				.bind('scroll', _.bind(this.scroll, this))
				.css('right', -options.scrollWidth);

			this.ui.releasesContainer
				.css('bottom', options.scrollWidth);

			return this;
		},
		renderGroups: function renderGroups() {
			var view;
			_.each(this.collections.releases.groupByDate(), function(models, group) {
				view = new window.app.AdvertisingReleasesGroupView({
					parent: this,
					models: models,
					group: group
				});
				this.ui.releasesContainer.append(view.render().el);
				this.subViews.releasesGroups.push(view);
			}, this);

			_.each(this.collections.trailers.groupByDate(), function(models, group) {
				view = new window.app.AdvertisingTrailersGroupView({
					parent: this,
					models: models,
					group: group,
					mode: this.options.mode
				});
				this.ui.trailersContainer.append(view.render().el);
			}, this);

			return this;
		},
		addRelease: function addRelease(model) {
			var releasesGroup = _.findWhere(this.subViews.releasesGroups, {group: model.get('date')}),
				position = _.indexOf(this.subViews.releasesGroups, releasesGroup),
				view;

			if (releasesGroup) {
				releasesGroup.addView(model);
				view = new window.app.AdvertisingDataRowView({
					parent: this,
					model: model,
					groups: this.collections.trailers.groupByDate(),
					ageRestrictions: this.options.ageRestrictions,
					readOnly: this.options.readOnly
				});

				this.ui.dataField.children().eq(position).append(view.render().el);
				this.subViews.releasesRowsGroups.push(view);
			} else {
				releasesGroup = new window.app.AdvertisingReleasesGroupView({
					parent: this,
					models: [model],
					group: model.get('date')
				});
				this.ui.releasesContainer.append(releasesGroup.render().el);
				view = new window.app.AdvertisingDataRowView({
					parent: this,
					model: model,
					groups: this.collections.trailers.groupByDate(),
					readOnly: this.options.readOnly,
					ageRestrictions: this.options.aeRestrictions
				});

				this.ui.dataField.append('<div class="advertising-data-group"></div>');
				this.ui.dataField.children().last().append(view.render().el);
				this.subViews.releasesGroups.push(releasesGroup);
			}

			return this;
		},
		renderDataField: function renderDataField() {
			var trailersGroups = this.collections.trailers.groupByDate(),
				releasesLength,
				diff,
				view,
				releasesGroupWrap;

			_.each(this.collections.releases.groupByDate(), function(releasesGroup) {
				releasesLength = releasesGroup.length;

				diff = (function() {
					var date = moment(releasesGroup[0].get('date'));
					return _.getWeekStartDate({isotime: date}).diff(_.getWeekStartDate(), 'weeks');
				})();

				this.ui.dataField.append('<div class="advertising-data-group"></div>');
				releasesGroupWrap = this.ui.dataField.children().last();

				_.each(releasesGroup, function(release, i) {
					view = new window.app.AdvertisingDataRowView({
						parent: this,
						model: release,
						groups: trailersGroups,
						ageRestrictions: this.options.ageRestrictions,
						readOnly: this.options.readOnly
					});

					releasesGroupWrap.append(view.render().el);
					this.subViews.releasesRowsGroups.push(view);

					if (releasesLength - 1 === i && diff === 0) releasesGroupWrap.addClass('advertising-data-group--current');
				}, this);

			}, this);
		},
		remove: function remove() {
			Backbone.trigger('remove:tipsy');
			if (this.ui.dataFieldWrapper) this.ui.dataFieldWrapper.unbind('scroll');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

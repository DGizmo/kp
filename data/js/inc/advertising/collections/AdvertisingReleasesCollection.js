$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.AdvertisingRelease = window.app.AdvertisingModel.extend({
		urlRoot: '/api/ads/releases',
		defaults: {
			id: null,
			title: '',
			age: null,
			duration: {},
			date: '',
			trailers: [],
			integrated_trailers: []
		},
		containsTrailer: function containsTrailer(trailer) {
			if (_.chain(this.get('trailers')).pluck('id').contains(trailer.id).value()) {
				return 'added';
			} else if (_.chain(this.get('integrated_trailers')).pluck('id').contains(trailer.id).value()) {
				return 'inner';
			} else {
				return false;
			}
		}
	});

	window.app.AdvertisingReleases = window.app.AdvertisingCollection.extend({
		model: window.app.AdvertisingRelease,
		initialize: function initialize(models, options) {
			this.options = _.defaults(options || {}, {
				url: '/api/ads/releases'
			});

			this.listenTo(Backbone, 'socket:advertising:release', this._socketUpdate);
			this.listenTo(Backbone, 'advertising:element:hover',  this._triggerHoveredModel);
		},
		_socketUpdate: function _socketUpdate(data) {
			var currentCinemaID = reqres.request('get:user').getCinema().id,
				model;

			if (currentCinemaID === data.cinema_id) {
				switch (data.event) {
					case 'update':
						(this.get(data.id) || this.add({id: data.id})).fetch();
					break;
					case 'add':
						model = new window.app.AdvertisingRelease({id: data.id});
						this.add(model).fetch({success: _.bind(function() {
							this.trigger('add:release', model);
						}, this)});
					break;
					case 'remove':
						this.remove(this.get(data.id));
					break;
				}
			}
		},
		_triggerHoveredModel: function _triggerHoveredModel(data) {
			var model = this.get(data.release);
			model && model.trigger('hover');
		}
	});

});

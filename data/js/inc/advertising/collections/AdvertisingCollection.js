$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.AdvertisingModel = Backbone.Model.extend({
		url: function url() {
			return Backbone.Model.prototype.url.call(this) +
				'?cinema_id=' + reqres.request('get:user').getCinema().id +
				'&tx='        + reqres.request('get:user').get('tx');
		}
	});

	window.app.AdvertisingCollection = Backbone.Collection.extend({
		groupByDate: function groupByDate() {
			var groups =
			_.each(this.groupBy(function(model) {
				return model.get('date');
			}), function(group) {
				group.sort(function(a, b) {
					var date = +moment(a.get('date_end') || '3000-01-01') - +moment(b.get('date_end') || '3000-01-01');
					if (date) return date;

					if (a.get('title') < b.get('title')) return -1;
					if (b.get('title') < a.get('title')) return 1;
					return 0;
				});
			});
			return groups;
		},
		parse: function parse(res) {
			res.list = res.list.sort(function(a, b) {
				return +moment(a.date) - +moment(b.date);
			});
			return res.list;
		},
		url: function url() {
			return this.options.url;
		}
	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.AdvertisingTrailer = window.app.AdvertisingModel.extend({
		urlRoot: '/api/ads/trailers',
		defaults: {
			id: null,
			title: '',
			age: null,
			duration: null,
			date: ''
		}
	});

	window.app.AdvertisingTrailers = window.app.AdvertisingCollection.extend({
		model: window.app.AdvertisingTrailer,
		initialize: function initialize(models, options) {
			this.options = _.defaults(options || {}, {
				url: '/api/ads/trailers'
			});

			this.listenTo(Backbone, 'advertising:element:hover', this._triggerHoveredModels);
		},
		_triggerHoveredModels: function _triggerHoveredModels(data) {
			var model;
			_.each(data.inner, function(id) {
				model = this.get(id); if (model) model.trigger('inner');
			}, this);

			_.each(data.added, function(id) {
				model = this.get(id); if (model) model.trigger('added');
			}, this);
		}
	});

});

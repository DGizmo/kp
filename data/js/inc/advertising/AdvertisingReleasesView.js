$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.AdvertisingReleasesGroupView = window.app.AdvertisingGroupView.extend({
		className: 'advertising-releases-group',
		template: _.template('<span class="advertising-releases-group__header" title="{{- header }}"></span>'),
		initialize: function initialize(options) {
			this.options = options;
			this.options.groupModelView = window.app.AdvertisingReleaseView;

			this.header = this.getGroupTitle();
			this.group  = this.options.group;
			this.subViews = [];
		},
		getGroupTitle: function getGroupTitle() {
			var date = moment(this.options.group),
				diff = _.getWeekStartDate({isotime: date}).diff(_.getWeekStartDate(), 'weeks');

			if (diff === 0) {
				return 'В прокате';
			} else {
				return moment(date).format('DD.MM');
			}
		},
		calcStyle: function calcStyle() {
			var modelsLength = this.subViews.length,
				header = modelsLength > 1 ? this.header : moment(this.options.group).format('DD');

			if (this.header === 'В прокате') this.$el.addClass('advertising-releases-group--cureent-week');

			this.$el.children().first().css({
				width:      modelsLength * 30,
				marginLeft: -(modelsLength * 30 - 30)
			});

			this.$el.children().first().text(header);
		}
	});

	window.app.AdvertisingReleaseView = window.app.AdvertisingModelView.extend({
		tagName: 'a',
		className: 'advertising-release',
		template:  '#advertisingRelease',
		tooltipTemplate: '#advertisingReleaseTrailersTooltip',
		events: {
			click: 'showReleaseCard'
		},
		showReleaseCard: function showReleaseCard(event) {
			var id = this.model.get('id');

			event.preventDefault();

			if (event.ctrlKey || event.metaKey || event.which === 2) {
				window.open(window.location.origin + '/release/' + id, '_blank');
			} else {
				Backbone.trigger('router:navigate', '/release/' + id);
			}
		},
		initialize: function initialize(options) {
			this.options = options;

			this.listenTo(this.model.collection, 'reset', this.remove);

			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'remove', this.remove);

			this.listenTo(this.model, 'hover', function() {
				this.$el.toggleClass('hover');
			});

			this.listenTo(this.model, 'trailer:added trailer:removed', function() {
				this.render().animateChange('.advertising-release-duration__total', 'animate', 300);
			});

			this.listenTo(this.model, 'wrongRating', function() {
				this.animateChange('.advertising-release__age', 'animate', 300);
			});
		},
		render: function render() {
			var tooltipTemplate = Backbone.TemplateCache.get(this.tooltipTemplate),
				template = Backbone.TemplateCache.get(this.template),
				release  = this.model,
				durationTitle;

			this.$el
				.html(template({
					release: release.toJSON(),
					trailersDuration: _.reduce(release.get('trailers'), function(memo, trailer) {
						return memo + trailer.duration;
					}, 0)
				}))
				.attr('href', '/release/' + release.get('id'));

			if (release.get('trailers').length) {
				durationTitle = 'Длительность фильма с учетом дополнительных трейлеров';
			} else if (release.get('integrated_trailers').length) {
				durationTitle = 'Длительность фильма со вшитыми трейлерами';
			} else {
				durationTitle = 'Длительность фильма без вшитых трейлеров';
			}

			this.$('.advertising-release-duration, .advertising-release__title').tipsy({
				html: true,
				title: _.bind(function() {
					return tooltipTemplate(release.toJSON());
				}, this)
			});

			this.$('.advertising-release-duration__total').attr('title', durationTitle);
			return this;
		}
	});

});

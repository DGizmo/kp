$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.AdvertisingGroupView = Backbone.View.extend({
		calcStyle: function calcStyle() {

		},
		addView: function addView(model, options) {
			options = options || {};

			var modelView = new this.options.groupModelView({
				model:  model,
				parent: this
			});

			this.$el.append(modelView.render().el);

			this.subViews.push(modelView);

			if (!options.silent) this.calcStyle();
			return this;
		},
		getGroupTitle: function getGroupTitle() {
			return moment(this.options.group).format('DD.MM');
		},
		renderModels: function renderModels() {
			_.each(this.options.models, function(model) {
				this.addView(model, {silent: true});
			}, this);
			return this;
		},
		render: function render() {
			this.$el.html(this.template({header: this.header}));
			this.renderModels().calcStyle();
			return this;
		},
		removeView: function removeView(view) {
			this.subViews.splice(_.indexOf(this.subViews, view), 1);

			if (this.subViews.length) {
				this.calcStyle();
			} else {
				this.remove();
			}
		},
		remove: function remove() {
			var releasesGroups;
			if (this.subViews.length) {
				_.invoke(this.subViews, 'remove', {parentRemove: true});
				this.subViews = [];
			} else {
				releasesGroups = this.options.parent.subViews.releasesGroups;
				releasesGroups.splice(_.indexOf(releasesGroups, this), 1);
			}
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.AdvertisingModelView = Backbone.View.extend({
		animateChange: function animateChange(selector, classSelector, timeout) {
			var element = this.$el.find(selector);
			element.addClass(classSelector);
			setTimeout(function() {
				element.removeClass(classSelector);
			}, timeout);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		},
		remove: function remove(options) {
			if (options && !(options.parentRemove)) this.options.parent.removeView(this);
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.AdvertisingTrailersGroupView = window.app.AdvertisingGroupView.extend({
		className: 'advertising-trailers-group',
		template: _.template('<span class="advertising-trailers-group__header">{{- header }}</span>'),
		initialize: function initialize(options) {
			this.options = options;
			this.subViews = [];
			this.options.groupModelView = window.app.AdvertisingTrailerView;
			this.header = (function(view) {
				var date = moment(view.options.group),
					diff = _.getWeekStartDate({isotime: date}).diff(_.getWeekStartDate(), 'weeks');

				if (view.options.mode === 'current-week' && diff === 0) {
					return 'В прокате';
				} else {
					return moment(date).format('DD.MM');
				}
			})(this);
		}
	});

	window.app.AdvertisingTrailerView = window.app.AdvertisingModelView.extend({
		className: 'advertising-trailer',
		template:  '#advertisingTrailer',
		tooltipTemplate: '#advertisingTrailerTooltip',
		initialize: function initialize(options) {
			this.options = options;

			this.listenTo(this.model, 'remove',           this.remove);
			this.listenTo(this.model.collection, 'reset', this.remove);

			this.listenTo(this.model, 'hover', function() {
				this.$el.toggleClass('hover');
			});
			this.listenTo(this.model, 'inner', function() {
				this.$el.toggleClass('advertising-trailer--inner');
			});
			this.listenTo(this.model, 'added', function() {
				this.$el.toggleClass('advertising-trailer--added');
			});

			this.listenTo(this.model, 'wrongRating', function() {
				this.animateChange('.advertising-trailer__age', 'animate', 300);
			});
		},
		render: function render() {
			var tooltipTemplate = Backbone.TemplateCache.get(this.tooltipTemplate),
				template = Backbone.TemplateCache.get(this.template),
				trailers = _.sortBy(this.model.get('trailers'), function(trailer) {
					return -moment(trailer.date).unix();
				});

			this.$el.html(template(this.model.attributes));

			this.$el.tipsy({
				html: true,
				title: function() {
					return tooltipTemplate({trailers: trailers});
				}
			});

			return this;
		}
	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.copyingScheduleModel = Backbone.Model.extend({
		defaults: {
			cinemaId: null,
			cinemaTitle: '',
			cinemaCity: '',
			id: null,
			title: '',
			number: null,
			formats: [],
			capasity: null,
			checked: false
		}
	});

	window.app.copyingScheduleCollection = Backbone.Collection.extend({
		model: window.app.copyingScheduleModel,
		initialize: function initialize(options) {
			this.allHalls = options;

			this.listenTo(Backbone, 'copying:model:change:checked', function(model) {
				var checkedModel = _.find(this.allHalls, function(oneModel) {
					return oneModel.id === model.get('id');
				});

				if (checkedModel) {
					checkedModel.checked = model.get('checked');
				}
			});
		},
		filterHalls: function filterHalls(value) {
			var searching;

			if (value.length) {
				searching = _.filter(this.allHalls, function(hall) {
					return (hall.cinemaTitle.toLowerCase().indexOf(value) > -1 ||
						(Boolean(hall.title) ? hall.title.toLowerCase().indexOf(value) > -1 : false) ||
						hall.formats.join(', ').toLowerCase().indexOf(value) > -1);
				});
			} else {
				searching = this.allHalls;
			}

			this.reset(searching);
			this.trigger('halls:searching');
		}
	});

	window.app.copyingScheduleCinemasWrapperView = Backbone.View.extend({
		className: 'copying',
		template: '#copying',
		events: {
			'input .copying-body-search__input': function(e) {
				this.collection.filterHalls($(e.currentTarget).val().toLowerCase());
			},
			'click .copying-footer__copy': 'save'
		},
		initialize: function initialize(options) {
			this.options         = options || {};
			this.allCinemas      = reqres.request('get:user').getCinemas();
			this.currentCinema   = reqres.request('get:user').getCinema();
			this.week            = this.options.week;
			this.hallId          = this.options.hallId;
			this.singleDay       = this.options.singleDate;
			this.year            = this.options.year;
			this.subViews        = [];
			this.checkedHalls    = [];
			this.cinemasSortedId = [];
			this.fromPlanning    = this.options.source === 'planning:hall:week' ? true : false;

			var halls = _.map(_.sortByNat(_.without(this.allCinemas, this.currentCinema), function(cinema) { return cinema.title; }), function(cinema) {
				this.cinemasSortedId.push(cinema.id);
				return _.map(_.sortBy(cinema.halls, function(hall) { return hall.number; }), function(hall) {
					return {
						cinemaId: cinema.id,
						cinemaTitle: cinema.title.ru,
						cinemaCity: cinema.city.title.ru,
						id: hall.id,
						title: hall.title,
						number: hall.number,
						formats: hall.formats,
						capasity: hall.capasity,
						checked: false
					};
				});
			}, this);

			this.collection = new window.app.copyingScheduleCollection(_.flatten(halls));

			this.listenTo(this.collection, 'halls:searching', function() {
				_.invoke(this.subViews, 'remove');
				this.renderCinemas(_.groupBy(this.collection.models, function(model) { return model.get('cinemaId'); }));
			});

			this.listenTo(this.collection, 'check:hall', this.renderCheckedHalls);
		},
		render: function render() {
			var template    = Backbone.TemplateCache.get(this.template),
				currentHall = _.find(this.currentCinema.halls, function(hall) { return hall.id === this.hallId; }, this),
				scrollWidth = _.getScrollBarWidth(),
				date,
				dateWeek;

			if (this.singleDay) {
				date = moment(this.singleDay).format('D MMMM');
			} else {
				dateWeek = moment().week(this.week);

				if (dateWeek.startOf('week').date() < dateWeek.endOf('week').date()) {
					date = 'c ' + dateWeek.startOf('week').format('D') + ' по ' + dateWeek.endOf('week').format('D MMMM');
				} else {
					date = 'c ' + dateWeek.startOf('week').format('D MMMM') + ' по ' + dateWeek.endOf('week').format('D MMMM');
				}
			}

			this.$el.html(template({
				currentHallTile: currentHall.title,
				currentCinemaTitle: this.currentCinema.title.ru,
				date: date,
				fromPlanning: this.fromPlanning
			}));

			this.renderCinemas(_.groupBy(this.collection.models, function(model) { return model.get('cinemaId'); }));

			if (scrollWidth) {
				this.$('.copying-body-list').css({
					width: 'calc(100% + ' + scrollWidth + 'px + 7px)'
				});

				this.$('.copying-body-targets-list').css({
					width: 'calc(100% + ' + scrollWidth + 'px + 5px)'
				});
			}

			return this;
		},
		renderCinemas: function renderCinemas(cinemasGroups) {
			_.each(this.cinemasSortedId, function(key) {
				var view = new window.app.copyingScheduleCinemaView({
						title:  _.first(_.property(key)(cinemasGroups)).get('cinemaTitle'),
						city:   _.first(_.property(key)(cinemasGroups)).get('cinemaCity'),
						halls:  _.property(key)(cinemasGroups),
						parent: this
					});

				this.$('.copying-body-list').append(view.render().el);
				this.subViews.push(view);
			}, this);
		},
		renderCheckedHalls: function renderCheckedHalls() {
			_.invoke(this.checkedHalls, 'remove');
			this.checkedHalls = [];

			_.each(this.collection.models, function(model) {
				if (model.get('checked')) {
					var view = new window.app.copyingScheduleCinemasTargetsView({
							model: model,
							parent: this
						});

					this.$('.copying-body-targets-list').append(view.render().el);

					this.checkedHalls.push(view);
				}
			}, this);

			if (this.checkedHalls.length) {
				this.$('.copying-body-targets__title').show();
			} else {
				this.$('.copying-body-targets__title').hide();
			}
		},
		save: function save() {
			var checkedModels = _.chain(this.collection.models)
									.filter(function(model) {
										return model.get('checked');
									})
									.groupBy(function(model) {
										return model.get('cinemaId');
									}).value(),
				data;

			if (_.isEmpty(checkedModels)) {
				$.growl.error({
					title: 'Ошибка',
					message: 'Пожалуйста, выберите залы в которые будет ' + (this.fromPlanning ? 'скопирован репертуар' : 'скопировано расписание')
				});
				return;
			}

			data = {
				hall_id_from: this.hallId,
				cinema_id_from: this.currentCinema.id,
				week: this.week,
				year: this.year,
				targets: _.map(_.keys(checkedModels), function(cinemaId) {
					return {
						cinema_id: parseInt(cinemaId),
						halls: _.map(_.property(cinemaId)(checkedModels), function(hall) { return hall.get('id'); })
					};
				})
			};

			if (this.fromPlanning) {
				$.post('/api/schedule/copy', JSON.stringify(data), function(r) {
					if (r.status === 'ok') {
						$.growl.notice({
							title: 'Успешно!',
							message: 'Репертуар скопирован'
						});
					}

					$.arcticmodal('close');
				});
			} else {
				if (this.singleDay) {
					data.date = this.singleDay;
				}

				$.post('/api/schedule/week/v2/copy', JSON.stringify(data), function(r) {
					if (r.status === 'ok') {
						$.growl.notice({
							title: 'Успешно!',
							message: 'Расписание скопировано'
						});
					}

					$.arcticmodal('close');
				});
			}
		},
		remove: function remove() {
			_.invoke(this.checkedHalls, 'remove');
			_.invoke(this.subViews, 'remove');

			this.collection.stopListening();
			this.collection = undefined;

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.copyingScheduleCinemaView = Backbone.View.extend({
		className: 'copying-body-list-item',
		template: _.template([
			'<span class="copying-body-list-item__cinema">{{- title }}',
				'<span class="copying-body-list-item__city">{{- city }}</span>',
			'</span>',
			'<div class="copying-body-list-halls"></div>'].join('')),
		events: {
			'click .copying-body-list-item__cinema': function(e) {
				var $item  = $(e.currentTarget).closest('.copying-body-list-item'),
					$halls = $item.children('.copying-body-list-halls');

				$item.toggleClass('active');

				if ($item.hasClass('active')) {
					$halls.height($halls.children().length * $halls.children().first().outerHeight());
				} else {
					$halls.height(0);
				}
			}
		},
		initialize: function initialize(options) {
			this.options  = options || {};
			this.parent   = this.options.parent;
			this.subViews = [];
		},
		render: function render() {
			this.$el.html(this.template({
				title: this.options.title,
				city: this.options.city
			}));

			this.renderHalls(this.options.halls);

			return this;
		},
		renderHalls: function renderHalls(halls) {
			_.each(halls, function(hall) {
				var view = new window.app.copyingScheduleHallView({
					model: hall,
					parent: this
				});

				this.$('.copying-body-list-halls').append(view.render().el);
				this.subViews.push(view);
			}, this);
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');

			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.copyingScheduleHallView = Backbone.View.extend({
		tagName: 'label',
		className: 'copying-body-list-halls-item',
		template: _.template([
			'<input type="checkbox" class="copying-body-list-halls-item__checkbox" {{- checked ? "checked" : "" }}>',
			'<span class="copying-body-list-halls-item__title">{{- title }}</span>',
			'<span class="copying-body-list-halls-item__info"> | {{- info }}</span>'].join('')),
		events: {
			'change .copying-body-list-halls-item__checkbox': function(e) {
				this.model.set({
					checked: $(e.currentTarget).prop('checked')
				}, { silent: true });

				Backbone.trigger('copying:model:change:checked', this.model);

				this.parent.parent.collection.trigger('check:hall');
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.parent  = this.options.parent;

			this.listenTo(this.model, 'change:checked', function() {
				Backbone.trigger('copying:model:change:checked', this.model);
				this.render();
			});
		},
		render: function render() {
			var capacity;

			if (this.model.get('capasity')) {
				capacity = this.model.get('capasity') + _.declOfNum(this.model.get('capasity'), [' место', ' места', ' мест']) + ', ';
			}

			this.$el.html(this.template({
				title: this.model.get('title'),
				info: capacity + this.model.get('formats').join(', ').toUpperCase(),
				checked: this.model.get('checked')
			}));

			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.copyingScheduleCinemasTargetsView = Backbone.View.extend({
		tagName: 'li',
		className: 'copying-body-targets-list-item',
		template: _.template('<span class="copying-body-targets-list-item__title">{{- title }}</span><i class="fa fa-trash copying-body-targets-list-item__remove"></i>'),
		events: {
			'click .copying-body-targets-list-item__remove': function() {
				this.model.set({
					checked: false
				});

				this.parent.collection.trigger('check:hall');
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.parent  = this.options.parent;
		},
		render: function render() {
			this.$el.html(this.template({
				title: this.model.get('cinemaTitle') + ': ' + this.model.get('title') + ' зал'
			}));

			return this;
		},
		remove: function remove() {
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

});

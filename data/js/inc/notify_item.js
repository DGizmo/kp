$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.Notify = Backbone.Model.extend({
		defaults: {
			show:         0,
			format:       '',
			distributors: []
		}
	});

	window.app.Notifications = Backbone.Collection.extend({
		model: window.app.Notify,
		initialize: function initialize() {
			this.listenTo(Backbone, 'socket:notifications:add', function(notify) {
				this.add(notify);
			});
		},
		url: function url() {
			return '/api/notifications';
		},
		parse: function parse(r) {
			var arr = [],
				k,
				j;
			for (k in r.notification) {
				for (j in r.notification[k]) {
					arr.push(r.notification[k][j]);
				}
			}
			return arr;
		}
	});

	window.app.NotificationsView = Backbone.View.extend({
		className: 'nt-sidebar-item nt-sidebar-item--notifications',
		template: '#push_box',
		toggleTemplate: _.template('<div class="nt-sidebar-toggle-icon icon-notifications" data-nt-sidebar="notifications" title="Последние изменения в системе."><span id="notifications-counter" class="nt-sidebar-toggle-icon__counter empty">0</span></div>'),
		count: 0,
		releaseCount: 0,
		kdmCount: 0,
		shipmentCount: 0,
		currentTab: 'release',
		events: {
			'click .nt-sidebar-item-filter__btn': /* istanbul ignore next */ function(event) {
				event.preventDefault();
				this.toggleFilter($(event.target).data('filter'));
			},
			'click .nt-sidebar-item-header__read-all': /* istanbul ignore next */ function(event) {
				event.preventDefault();

				var self = this;

				$.post('/api/notifications/mark', {item: 'all'}).done(function() {
					self.$el.find('.nt-sidebar-notifications-item').removeClass('active');

					$('#notifications-counter, .nt-sidebar-item-header__count, .nt-sidebar-item-header__read-all').addClass('empty');
					self.$el.find('.nt-sidebar-item-filter .count').addClass('empty');

					self.notifications.each(function(notif) {
						notif.set('is_new', 0);
					});
					self.count = 0;
					self.releaseCount = 0;
					self.kdmCount = 0;
					self.shipmentCount = 0;
				});
			}
		},
		initialize: function initialize() {
			var self = this;

			$('.nt-sidebar').prepend(this.render().el);
			$('.nt-sidebar-toggle').prepend($(this.toggleTemplate()));
			this.notifications = new window.app.Notifications();
			this.listenTo(this.notifications, 'add', this.addOneNotify);

			this.notifications.updated = false;

			self.notifications.fetch({
				success: function() {
					var arr = self.notifications.where({item: 'release'}),
						i;
					for (i = 0; i < arr.length; i++) {
						arr[i].set('show', 1);
					}
					self.notifications.updated = true;
				}
			});
		},
		addOneNotify: function addOneNotify(notify) {
			if (notify.get('item') === 'custom') return;
			if (notify.get('item') === this.currentTab) notify.set('show', 1);

			var View = new window.app.NotifyView({ model: notify }),
				self = this,
				notifyType,
				$curCount;

			View.parent = this;

			this.$el.find('#notifications').prepend(View.render().el);

			if (notify.get('is_new')) {
				notifyType = notify.get('item');
				$curCount  = this.$el.find('.nt-sidebar-item-filter [data-filter="' + notifyType + '"] .count');

				this.$el.find('.nt-sidebar-item-header__count, .nt-sidebar-item-header__read-all').removeClass('empty');
				this.$el.find('.notify_count').text(++this.count);
				$('.nt-sidebar-toggle').find('#notifications-counter').removeClass('empty').text(this.count);

				switch (notifyType) {
					case 'release':
						$curCount.removeClass('empty').text(++self.releaseCount);
					break;
					case 'kdm':
						$curCount.removeClass('empty').text(++self.kdmCount);
					break;
					case 'shipment':
						$curCount.removeClass('empty').text(++self.shipmentCount);
					break;
				}
			}
		},
		filterItem: function filterItem() {
			var self = this,
				arr,
				i;
			_.map(self.notifications.models, function(i) { i.set('show', 0); });
			arr = this.notifications.where({item: self.currentTab});
			for (i = 0; i < arr.length; i++) {
				if (arr[i].get('item') === self.currentTab) {
					arr[i].set('show', 1);
				}
			}
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template({
				user: reqres.request('get:user').toJSON()
			}));
			return this;
		},
		toggleFilter: function toggleFilter(target) {
			var $target = this.$el.find('.nt-sidebar-item-filter__btn[data-filter="' + target + '"]');

			this.$el.find('.nt-sidebar-item-filter__btn').removeClass('active');
			$target.addClass('active');
			this.currentTab = target;
			this.filterItem();
		}
	});

	window.app.NotifyView = Backbone.View.extend({
		className: 'nt-sidebar-notifications-item-wraper',
		template: '#notify_item',
		events: {
			'mouseenter .nt-sidebar-notifications-item.active':  /* istanbul ignore next */ function() {
				var self       = this,
					notifyType = self.model.get('item'),
					$curCount  = this.parent.$el.find('.nt-sidebar-item-filter [data-filter="' + notifyType + '"] .count');

				$.post('/api/notifications/mark', {id: self.model.get('_id')}).done(function() {
					self.model.set('is_new', 0);

					self.parent.$('.notify_count').text(--self.parent.count);
					$('#notifications-counter').text(self.parent.count);

					if (self.parent.count === 0) {
						$('#notifications-counter, .nt-sidebar-item-header__count, .nt-sidebar-item-header__read-all').addClass('empty');
					}
					self.$el.find('.nt-sidebar-notifications-item').removeClass('active');

					switch (notifyType) {
						case 'release':
							$curCount.text(--self.parent.releaseCount);
						break;
						case 'kdm':
							$curCount.text(--self.parent.kdmCount);
						break;
						case 'shipment':
							$curCount.text(--self.parent.shipmentCount);
						break;
					}

					if ($curCount.text() === '0') {
						$curCount.addClass('empty');
					}
				});
			},
			'click a.nt-sidebar-notifications-item__name': 'showRelease'
		},
		initialize: function initialize() {
			this.listenTo(this.model, 'change', this.render);
		},
		showRelease: /* istanbul ignore next */ function showRelease(event) {
			event.preventDefault();
			var release = this.model.get('item') === 'shipment' ? this.model.get('rel_id') : this.model.get('item_id');
			Backbone.trigger('router:navigate', '/release/' + release);
			_gaq.push(['_trackEvent', 'Оповещения', 'Переход по ссылке']);
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});
});

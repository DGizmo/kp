$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ServiceModel = Backbone.Model.extend({
		idAttribute: 'serialNumber',
		urlRoot: function urlRoot() {
			return 'api/service/' + (this.get('type') === 'lamp' ? 'lamp' : 'equipment');
		},
		initialize: function initialize() {
			this.listenTo(this, 'request', function() {
				Backbone.trigger('loader', 'show');
			});

			this.listenTo(this, 'sync', function() {
				Backbone.trigger('loader', 'hide');
			});
		}
	});

	window.app.ServiceCollection = Backbone.Collection.extend({
		model: window.app.ServiceModel,
		url: function url() {
			return 'api/service/list';
		},
		initialize: function initialize() {
			this.listenTo(this, 'request', function() {
				Backbone.trigger('loader', 'show');
				this.halls   = undefined;
				this.archivedLamps = undefined;
			});

			this.listenTo(this, 'sync', function() {
				Backbone.trigger('loader', 'hide');
			});
		},
		parse: function parse(response) {
			return response.list;
		},
		getHalls: function getHalls(cinema) {
			if (!this.halls) {
				var hallsGroups = _.chain(this.toJSON())
					.filter(function(item) {
						return !item.archive;
					})
					.groupBy(function(item) {
						return item.hallID;
					}).value();

				this.halls = _.map(cinema.halls, function(hall) {
					return {
						title:       hall.title,
						formats:     hall.formats.join(', '),
						projectors:  _.where(hallsGroups[hall.id], {type: 'projector'}),
						playservers: _.where(hallsGroups[hall.id], {type: 'playserver'}),
						lamps:       _.where(hallsGroups[hall.id], {type: 'lamp'})
					};
				}, this);
			}

			return this.halls;
		},
		getArchivedLamps: function getArchivedLamps() {
			if (!this.archivedLamps) {
				this.archivedLamps = _.chain(this.toJSON())
					.filter(function(item) {
						return item.archive;
					}).value();
			}

			return this.archivedLamps;
		}
	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ServiceCinemasListView = Backbone.View.extend({
		template: _.template([
			'<div class="service-cinemas-search"></div>',
			'<div class="service-cinemas-list"></div>'
		].join('')),
		cinemasListTmpl: '#service_cinemas_list',
		events: {
			'click .service-cinemas-list-item': function(e) {
				$(e.currentTarget).addClass('active')
					.siblings().removeClass('active');

				var cinema = this.user.getCinema($(e.currentTarget).data('cinema-id'));
				Backbone.trigger('set:service:cinema', cinema);
			}
		},
		initialize: function initialize(options) {
			this.options = options || {};
			this.user    = this.options.user;
		},
		render: function render() {
			var cinemasListTmpl = Backbone.TemplateCache.get(this.cinemasListTmpl),
				cinemas         = _.map(this.user.getCinemas(), function(cinema) {
					return {
						id:    cinema.id,
						title: cinema.title.ru || cinema.title.en,
						city:  cinema.city.title.ru || cinema.city.title.en,
						halls: cinema.halls.length + _.declOfNum(cinema.halls.length, [' зал', ' зала', ' залов'])
					};
				});

			this.$el.html(this.template);

			this.$('.service-cinemas-list').html(cinemasListTmpl({
				cinemas:         cinemas,
				currentCinemaID: this.user.getCinema().id
			}));

			this.searchingModule = new window.app.SearchingModuleView({
				animation: false,
				onSearchingFinish: _.bind(function onSearchingFinish(value) {
					this.$('.service-cinemas-list-item')
					.hide().end()
					.find('.service-cinemas-list-item__title:containsLower(' + value + '), ' +
						'.service-cinemas-list-item__description:containsLower(' + value + ')')
					.closest('.service-cinemas-list-item').show();
				}, this)
			});

			this.$('.service-cinemas-search').html(this.searchingModule.render().el);

			return this;
		},
		remove: function remove() {
			this.searchingModule.remove();
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

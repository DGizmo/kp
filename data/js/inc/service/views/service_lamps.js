$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ServiceLampsView = Backbone.View.extend({
		className: 'service-lamps',
		template: '#service_lamps',
		currentView: null,
		events: {
			'click .btn-tab': function(event) {
				$(event.currentTarget).addClass('btn-tab--active')
					.siblings().removeClass('btn-tab--active');
				this.currentTab = $(event.currentTarget).data('tab');
				this.switchView(new this.lampsViews[this.currentTab]({
					cinema:     this.cinema,
					collection: this.collection
				}));
			}
		},
		initialize: function initialize(options) {
			this.cinema     = options.cinema;
			this.lampsViews = {
				actual:   window.app.ServiceLampsActualView,
				archived: window.app.ServiceLampsArchivedView
			};
			this.tabs       = {
				actual: 'В работе'
			};
			if (reqres.request('get:user').hasServiceAccessLVL() > 1) {
				this.tabs.archived = 'Архивные';
			}
			this.currentTab = 'actual';
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({
				currentTab: this.currentTab,
				cinema:     this.cinema,
				tabs:       this.tabs
			}));

			this.switchView(new this.lampsViews[this.currentTab]({
				cinema:     this.cinema,
				collection: this.collection
			}));

			this.searchingModule = new window.app.SearchingModuleView({
				className: 'searching-module searching-module--right',
				onSearchingFinish: _.bind(function onSearchingFinish(value) {
					this.$('.service-table-body__row')
					.hide().end()
					.find('.service__device-link:containsLower(' + value + '), ' +
						'.service__lamp-link:containsLower(' + value + '), ' +
						'.service__description:containsLower(' + value + ')')
					.closest('.service-table-body__row').show();
				}, this)
			});
			this.$('.service-lamps-header').append(this.searchingModule.render().el);

			return this;
		},
		switchView: function switchView(view) {
			if (this.currentView !== null && this.currentView.cid !== view.cid) {
				this.currentView.remove().$el.empty();
			}
			this.$el.append(view.render().el);
			this.currentView = view;
		}
	});

	window.app.ServiceLampsActualView = Backbone.View.extend({
		tagName:    'table',
		className:  'service-table table-default table-default--condensed table-default--striped',
		template:   '#service_lamps_actual',
		initialize: function initialize(options) {
			this.cinema = options.cinema;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				halls    = this.collection.getHalls(this.cinema);

			this.$el.html(template({
				halls: halls,
				hasAccessToLampCard: reqres.request('get:user').hasServiceAccessLVL() > 1
			}));

			return this;
		}

	});

	window.app.ServiceLampsArchivedView = Backbone.View.extend({
		tagName:    'table',
		className:  'service-table table-default table-default--condensed table-default--striped',
		template:   '#service_lamps_archived',
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({
				archivedLamps: this.collection.getArchivedLamps()
			}));

			return this;
		}

	});

});

$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ServiceDahboardView = Backbone.View.extend({
		className: 'service-dashboard',
		template: '#service_dashboard',
		initialize: function initialize(options) {
			this.cinema = options.cinema;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template),
				halls    = this.collection.getHalls(this.cinema);

			this.$el.html(template({
				halls:  halls,
				cinema: this.cinema
			}));

			return this;
		}
	});

});

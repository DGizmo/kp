$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ServiceDevicesListView = Backbone.View.extend({
		className: 'service-devices-list-wrap',
		template: '#service_devices_list',
		events: {
			'click .service-devices-list__item': function(event) {
				$(event.currentTarget).addClass('active')
					.siblings().removeClass('active');

				this.deviceCard.model = this.collection.get($(event.currentTarget).data('serial-number'));
				this.deviceCard.render();
			}
		},
		initialize: function initialize(options) {
			this.serialNumber = options.serialNumber;
			this.cinema       = options.cinema;
		},
		render: function render() {
			var template    = Backbone.TemplateCache.get(this.template),
				halls       = this.collection.getHalls(this.cinema);

			this.$el.html(template({
				halls:        halls,
				cinema:       this.cinema,
				serialNumber: this.serialNumber
			}));

			this.deviceCard = new window.app.ServiceDeviceCardView({
				el:       this.$('.service-device-card'),
				model:    this.collection.get(this.serialNumber),
				cinema:   this.cinema
			});
			this.deviceCard.render();

			return this;
		},
		remove: function remove() {
			this.deviceCard.remove();
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ServiceDeviceCardView = Backbone.View.extend({
		template: '#service_device_card',
		initialize: function initialize(options) {
			this.options = options || {};
			this.cinema  = this.options.cinema;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.model.fetch({
				data: {
					cinema: this.cinema.id
				},
				success: _.bind(function(model) {
					this.$el.html(template({
						device: model.toJSON(),
						hall:   _.findWhere(this.cinema.halls, {id: model.get('hallID')})
					}));
					this.deviceParamsGroups = new window.app.ServiceDeviceParamsGroupsView({
						el:           this.$('.service-device-params'),
						paramsGroups: this.model.get('params')
					});
					this.deviceParamsGroups.render();
				}, this)
			});

			return this;
		},
		remove: function remove() {
			if (this.deviceParamsGroups) this.deviceParamsGroups.remove();
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ServiceDeviceParamsGroupsView = Backbone.View.extend({
		template: '#service_device_params_groups',
		paramsGroupsTitles: {
			COMMON:   'Общее',
			FAN:      'Вентиляторы',
			TEMP:     'Температура',
			VOLT:     'Вольтаж',
			STORAGE:  'Жесткие диски',
			SECURITY: 'Защитный контур'
		},
		events: {
			'click .service-device-params-groups__item': function(event) {
				$(event.currentTarget).addClass('active')
					.siblings().removeClass('active');

				this.currentParamsGroupKey = $(event.currentTarget).data('params-group');
				this.renderParamsGroup();
			}
		},
		initialize: function initialize(options) {
			this.paramsGroups          = options.paramsGroups;
			this.currentParamsGroupKey = _.keys(this.paramsGroups)[0];
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.$el.html(template({
				paramsGroups:          this.paramsGroups,
				paramsGroupsTitles:    this.paramsGroupsTitles,
				currentParamsGroupKey: this.currentParamsGroupKey
			}));

			this.currentParamsGroup = new window.app.ServiceDeviceParamsGroupView({
				el: this.$('.service-device-params-group')
			});
			this.renderParamsGroup();

			return this;
		},
		renderParamsGroup: function renderParamsGroup() {
			this.currentParamsGroup.render({
				groupTitle:         this.paramsGroupsTitles[this.currentParamsGroupKey],
				paramsGroup:        this.paramsGroups[this.currentParamsGroupKey],
				hasServiceFirstLVL: reqres.request('get:user').hasServiceAccessLVL() === 1
			});
		},
		remove: function remove() {
			this.currentParamsGroup.remove();
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});

	window.app.ServiceDeviceParamsGroupView = Backbone.View.extend({
		template: '#service_device_params_group',
		render: function render(options) {
			var template = Backbone.TemplateCache.get(this.template);
			this.$el.html(template(options));
			return this;
		}
	});

});

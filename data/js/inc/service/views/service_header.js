$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	var templateWithTabs = _.template([
			'<div class="sections-tabs">',
			'{{ _.each(tabs, function(value, key) { }}',
				'<div class="sections-tabs-item {{ if (key === currentTab) { }}active{{ } }}" data-tab="{{- key }}">',
					'<span class="sections-tabs-item__text">{{- value }}</span>',
				'</div>',
			'{{ }); }}',
			'</div>'
		].join('')),
		templateWithBack = _.template([
			'<button class="btn btn-default" data-action="back">',
			'<i class="btn-default__back-ico"></i>Назад</button>'
		].join(''));

	window.app.ServiceHeaderView = Backbone.View.extend({
		templateWithTabs: templateWithTabs,
		templateWithBack: templateWithBack,
		events: {
			'click .sections-tabs-item': function(event) {
				$(event.currentTarget).addClass('active')
					.siblings().removeClass('active');

				this.currentTab = $(event.currentTarget).data('tab');
			}
		},
		initialize: function initialize(options) {
			this.tabs       = {
				serviceDashboard: 'Мониторинг',
				serviceLamps:     'Лампы'
			};
			this.currentTab = options.currentTab;
		},
		render: function render(template) {
			this.$el.html(this[template](this));
			return this;
		}
	});

});

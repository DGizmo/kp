$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ServiceLampCardView = Backbone.View.extend({
		className: 'service-lamp-card',
		template: '#service_lamp_card',
		initialize: function initialize(options) {
			this.options = options || {};
			this.cinema = this.options.cinema;
		},
		render: function render() {
			var template = Backbone.TemplateCache.get(this.template);

			this.model.fetch({
				data: {
					cinema: this.cinema.id
				},
				success: _.bind(function(model) {
					this.$el.html(template({
						lamp: model.toJSON(),
						hall: _.findWhere(this.cinema.halls, {id: model.get('hallID')})
					}));
				}, this)
			});

			return this;
		}
	});

});

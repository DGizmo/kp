$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ServiceCinemaInfoView = Backbone.View.extend({
		template: _.template([
			'<div class="service-cinema-info-header"></div>',
			'<div class="service-cinema-info-content"></div>'
		].join('')),
		templateEmpty: _.template('<span class="service__empty">Информация об оборудовании в этом кинотеатре отсутствует.</span>'),
		events: {
			'click .btn[data-action="back"]': function() {
				this.headerView.render('templateWithTabs');
				this.switchContentView(new this.tabs[this.currentTab]({
					cinema:     this.cinema,
					collection: this.collection
				}));
			},
			'click .sections-tabs-item': function(event) {
				this.currentTab = $(event.currentTarget).data('tab');
				this.switchContentView(new this.tabs[this.currentTab]({
					cinema:     this.cinema,
					collection: this.collection
				}));
			},
			'click .service__lamp-link': function(event) {
				var serialNumber = $(event.currentTarget).data('serial-number');
				if (!serialNumber) {
					$.growl.error({
						title: 'Ошибка',
						message: 'Отсутствует серийный номер лампы'
					});
				} else {
					this.headerView.render('templateWithBack');
					this.switchContentView(new window.app.ServiceLampCardView({
						cinema: this.cinema,
						model:  this.collection.get(serialNumber)
					}));
				}
			},
			'click .service__device-link': function(event) {
				if (!this.collection.get($(event.currentTarget).data('serial-number'))) {
					$.growl.error({
						title: 'Ошибка',
						message: 'В выбранном конотеатре отсутствует данное оборудование'
					});
				} else {
					this.headerView.render('templateWithBack');
					this.switchContentView(new window.app.ServiceDevicesListView({
						cinema:       this.cinema,
						collection:   this.collection,
						serialNumber: $(event.currentTarget).data('serial-number')
					}));
				}
			}
		},
		initialize: function initialize(options) {
			this.options    = options || {};
			this.collection = new window.app.ServiceCollection(),
			this.setCinema(this.options.cinema);

			this.contentView = null;
			this.tabs        = {
				serviceDashboard: window.app.ServiceDahboardView,
				serviceLamps:     window.app.ServiceLampsView
			};
			this.currentTab  = 'serviceDashboard';

			this.listenTo(Backbone, 'set:service:cinema', this.setCinema);
		},
		render: function render() {
			this.$el.html(this.template);

			this.headerView = new window.app.ServiceHeaderView({
				el:         this.$('.service-cinema-info-header'),
				currentTab: this.currentTab
			});

			return this;
		},
		switchContentView: function switchContentView(view) {
			if (this.contentView !== null && this.contentView.cid !== view.cid) {
				this.contentView.remove().$el.empty();
			}
			this.$('.service-cinema-info-content').html(view.render().el);
			this.contentView = view;
		},
		setCinema: function setCinema(cinema) {
			this.cinema = cinema;
			this.collection.fetch({
				data: {
					cinema: cinema.id
				},
				success: _.bind(function(collection) {
					if (collection.length) {
						this.switchContentView(new this.tabs[this.currentTab]({
							cinema:     this.cinema,
							collection: this.collection
						}));
						this.headerView.render('templateWithTabs');
					} else {
						this.$('.service-cinema-info-content').html(this.templateEmpty);
						this.headerView.$el.empty();
					}
				}, this),
				error: _.bind(function() {
					this.$('.service-cinema-info-content').html(this.templateEmpty);
					this.headerView.$el.empty();
				}, this)
			});
		},
		remove: function remove() {
			if (this.headerView) this.headerView.remove();
			if (this.contentView) this.contentView.remove();
			this.collection.stopListening();
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}

	});

});

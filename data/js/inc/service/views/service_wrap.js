$(function() {
	'use strict';
	window.app = window.app || /* istanbul ignore next */ {};

	window.app.ServiceWrapView = Backbone.View.extend({
		template: _.template([
			'{{ if (needCinemasList) { }}<div class="service-cinemas"></div>{{ } }}',
			'<div class="service-cinema-info {{ if (!needCinemasList) { }}service-cinema-info--wide{{ } }}"></div>'
		].join('')),
		inviteTemplate: '#service_invite_tmpl',
		className: 'service',
		events: {
			'click .btn[data-action="invite"]': function() {
				$.post('/api/service/invite', _.bind(function() { this.render(); }, this));
			}
		},
		initialize: function initialize(options) {
			this.options       = options || {};
			this.user          = this.options.user;
			this.subViews      = [];
		},
		render: function render() {
			var inviteTemplate  = Backbone.TemplateCache.get(this.inviteTemplate),
				needCinemasList = this.user.isMultiplex();

			if (!this.user.hasServiceAccessLVL()) {
				$.get('/api/service/invite/check', _.bind(function(data) {
					this.$el.html(inviteTemplate({ serviceInvite: data.exists }));
				}, this));
			} else {
				this.$el.html(this.template({needCinemasList: needCinemasList}));
				if (needCinemasList) {
					this.subViews.push(new window.app.ServiceCinemasListView({
						el:   this.$('.service-cinemas'),
						user: this.user
					}));
				}
				this.subViews.push(new window.app.ServiceCinemaInfoView({
					el:     this.$('.service-cinema-info'),
					cinema: this.user.getCinema()
				}));
				_.invoke(this.subViews, 'render');
			}

			return this;
		},
		remove: function remove() {
			_.invoke(this.subViews, 'remove');
			this.$el.empty();
			return Backbone.View.prototype.remove.call(this);
		}
	});
});

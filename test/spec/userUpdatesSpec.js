describe('User Updates', function() {
	'use strict';
	var request;

	beforeEach(function() {
		this.user = new window.app.User(JSON.parse(TestResponses.user.cinema.responseText));
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});
	});

	afterEach(function() {
		this.user.stopListening();
		reqres.removeHandler('get:user');
	});

	describe('Collection', function() {
		beforeEach(function() {
			this.collection = new window.app.UserUpdates();
		});

		afterEach(function() {
			this.collection.stopListening();
		});

		describe('URL', function() {
			it('should have url', function() {
				expect(_.isFunction(this.collection.url)).toBe(true);
			});
			it('should get history by user current cinema id', function() {
				var historyUrl = '/api/history?cinema_id=602';
				expect(this.collection.url()).toBe(historyUrl);
			});
		});

	});

	describe('View', function() {

		beforeEach(function() {
			appendSetFixtures(sandbox());
			$('#sandbox').append('<div class="nt-sidebar"></div>');

			getDatFckTemplate('user_updates_box');
			getDatFckTemplate('user_updates_item');

			jasmine.Ajax.install();

			this.MyUserUpdates = new window.app.UserUpdatesView();

			spyOn(this.MyUserUpdates.updates, 'fetch').and.callThrough();

			request = jasmine.Ajax.requests.mostRecent();
			request.response(TestResponses.history.success);
		});

		afterEach(function() {
			this.MyUserUpdates.remove();
			jasmine.Ajax.uninstall();
		});

		it('collection with models', function() {
			expect(this.MyUserUpdates.updates).toBeDefined();
			expect(this.MyUserUpdates.updates.models.length).toBe(2);
		});

		it('reset collection', function() {
			this.MyUserUpdates.updates.reset();
			expect(this.MyUserUpdates.updates.models.length).toBe(0);
			expect($('.nt-sidebar-user-updates-item')).not.toExist();
		});

		it('user_updates_box should be in dom', function() {
			expect($('.nt-sidebar-item-header')).toExist();
		});

		it('user_updates_item should be in dom', function() {
			expect($('.nt-sidebar-user-updates-item')).toExist();
		});

		it('on avatar changed fetch models', function() {
			Backbone.trigger('avatar-changed', 'test');
			expect(this.MyUserUpdates.updates.fetch).toHaveBeenCalled();
		});

	});

});

describe('Chat', function() {
	'use strict';
	var request;

	beforeEach(function() {
		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText));
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});
	});

	afterEach(function() {
		this.user.stopListening();
		reqres.removeHandler('get:user');
	});

	describe('Message Model', function() {
		beforeEach(function() {
			jasmine.Ajax.install();
			this.model = new window.app.Message({_id: 999});
		});

		afterEach(function() {
			this.model.stopListening();
			jasmine.Ajax.uninstall();
		});

		it('should convert text2link', function() {
			expect(this.model._text2link('www.test.ru')).toBe('<a href="www.test.ru" target="_blank">www.test.ru</a>');
		});

		describe('Creating servicedesk tickets', function() {

			beforeEach(function() {
				spyOn(Backbone, 'trigger').and.callThrough();
				spyOn($.growl, 'ok');
				spyOn($.growl, 'error');

				this.model.createTicket('general');
			});

			it('should trigger "chat:loader" :show before request and :hide after', function() {
				expect(Backbone.trigger).toHaveBeenCalledWith('chat:loader', 'show');

				request = jasmine.Ajax.requests.mostRecent();
				request.response({status: 200, responseText: JSON.stringify({ticket_id: 111})});

				expect(Backbone.trigger).toHaveBeenCalledWith('chat:loader', 'hide');
			});

			it('should send correct ajax reuqest§', function() {
				request = jasmine.Ajax.requests.mostRecent();

				expect(request.url).toBe('/api/v2/messages/999/ticket');
				expect(request.method).toBe('PUT');
			});

			it('should set ticket_id and growl on success', function() {
				request = jasmine.Ajax.requests.mostRecent();
				request.response({status: 200, responseText: JSON.stringify({ticket_id: 111})});

				expect(this.model.get('ticket_id')).toBe(111);
				expect($.growl.ok).toHaveBeenCalled();
			});

			it('should growl on error', function() {
				request = jasmine.Ajax.requests.mostRecent();
				request.response({
					status: 500,
					responseText: '{status: "error"}'
				});

				expect($.growl.error).toHaveBeenCalled();
			});

		});

	});

	describe('Collection', function() {
		beforeEach(function() {
			this.collection = new window.app.Messages();
		});

		afterEach(function() {
			this.collection = undefined;
		});

		describe('URL', function() {

			it('url as function', function() {
				expect(this.collection.url).toBeFunction();
			});

			it('without param', function() {
				expect(this.collection.url()).toBe('/api/v2/messages');
			});

			it('with param', function() {
				var timestamp = '000000000000';
				expect(this.collection.url(timestamp)).toBe('/api/v2/messages?history=' + timestamp);
			});

		});

	});

	describe('View', function() {
		beforeEach(function() {
			appendSetFixtures(sandbox());
			$('#sandbox').append('<div class="nt-sidebar"></div>');

			getDatFckTemplate('messages_box');
			getDatFckTemplate('message_item');

			jasmine.Ajax.install();

			this.user = new window.app.User();
			this.MyMessages = new window.app.MessagesView();

			spyOn(this.MyMessages.messages, 'where').and.callThrough();

			request = jasmine.Ajax.requests.mostRecent();
			request.response(TestResponses.messages.success);

		});

		afterEach(function() {
			this.user.stopListening();
			this.MyMessages.remove();
			jasmine.Ajax.uninstall();
		});

		it('collection with models', function() {
			expect(this.MyMessages.messages).toBeDefined();
			expect(this.MyMessages.messages.models.length).toBe(3);
		});

		it('reset collection', function() {
			this.MyMessages.messages.reset();
			expect(this.MyMessages.messages.models.length).toBe(0);
			expect($('.nt-sidebar-user-updates-item')).not.toExist();
		});

		it('messages_box should be in dom', function() {
			expect($('.nt-sidebar-item--chat')).toExist();
		});

		it('message_item should be in dom', function() {
			expect($('.nt-sidebar-chat-box__message')).toExist();
		});

		it('on trigger chat-close', function() {
			Backbone.trigger('chat-close');
			expect(this.MyMessages.isOpen).toBe(false);
		});

		it('on trigger chat-open', function() {
			Backbone.trigger('chat-open');
			expect(this.MyMessages.isOpen).toBe(true);
		});

		// it('on avatar changed set new photo for models', function() {
		// 	Backbone.trigger('avatar-changed', 'test');
		// 	expect(this.MyMessages.messages.where).toHaveBeenCalled();
		// });

	});

});

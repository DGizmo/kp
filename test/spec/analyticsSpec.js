describe('Analytics', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('analytics_wrapper_tmpl');
		getDatFckTemplate('analytics_table_tmpl');

		appendSetFixtures(sandbox({id:'analytics'}));

		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText));

		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		jasmine.Ajax.install();

		this.analyticsCollection = new window.app.AnalyticsCollection();
		this.analyticsView = new window.app.AnalyticsView({collection: this.analyticsCollection});

		this.analyticsView.render();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.analytics);
	});

	afterEach(function() {
		this.user.stopListening();
		this.analyticsCollection.stopListening();
		this.analyticsView.remove();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	it('Collection should return right url', function() {
		this.analyticsCollection.setFilter('5+');
		expect(this.analyticsCollection.url).toEqual('/api/prediction/5+');
	});

	describe('View', function() {

		it('should show date', function() {
			expect(this.analyticsView.$('.analytics__date').length).toBe(1);
		});

		it('should show % if delta >= 0.05', function() {
			expect(this.analyticsView.$('.analytics-plus').length).toBe(2);
		});

		it('should show new releases', function() {
			expect(this.analyticsView.$('.analytics-new').length).toBe(2);
		});

	});

});

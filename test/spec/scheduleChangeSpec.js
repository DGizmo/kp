describe('Schedule Change View', function() {
	'use strict';

	beforeEach(function() {
		getDatFckTemplate('schedule_change_tmpl');
		appendSetFixtures(sandbox({id:'schedule'}));

		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText));
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		this.model = new window.app.Repertoire(JSON.parse(TestResponses.repertoire.defaultModel.responseText));
		this.scheduleView = new window.app.ScheduleView({model: this.model});

		$('#schedule').append(this.scheduleView.render().el);
	});

	afterEach(function() {
		this.user.stopListening();
		this.model.stopListening();
		this.scheduleView.removeView();
		reqres.removeHandler('get:user');
	});

	it('should correctly calculate hire range', function() {
		expect($('.hire_range .since_date').val() + $('.hire_range .until_date').val()).toBe('01.01.201528.01.2015');
	});

	it('should correctly calcute first week number', function() {
		var sinceDate = this.scheduleView.getHireRange().sinceDate;
		expect(this.scheduleView.getFirstWeekNumber(sinceDate)).toBe(4);
	});

	/**
	*
	* https://coderwall.com/p/elevha/stubbing-out-confirm-dialogs-in-jasmine
	*
	**/

	// it('method changeHireRange should create array of new weeks', function() {
	// 	spyOn(this.scheduleView, 'checkDeletedWeeks').and.callThrough();
	// 	this.scheduleView.$el.find('.until_date').val('02.01.2015');
	// 	this.scheduleView.changeHireRange();
	// 	expect(this.scheduleView.checkDeletedWeeks).toHaveBeenCalledWith(JSON.parse(TestResponses.repertoire.newWeeks.responseText));
	// });

	// describe('method checkDeletedWeeks', function() {
	// 	it('should save weeks, if no deleted weeks', function() {
	// 		spyOn(this.scheduleView.model, 'set').and.callThrough();
	// 		var newWeeks = this.scheduleView.model.get('weeks');
	// 		this.scheduleView.checkDeletedWeeks(newWeeks);
	// 		expect(this.scheduleView.model.set).toHaveBeenCalledWith({weeks: newWeeks});
	// 	});

	// 	it('should warnAboutDeletedWeeks, if some weeks with count deleted', function() {
	// 		var newWeeks     = JSON.parse(TestResponses.repertoire.newWeeks.responseText),
	// 			deletedWeeks = JSON.parse(TestResponses.repertoire.deletedWeeks.responseText);

	// 		spyOn(this.scheduleView, 'warnAboutDeletedWeeks').and.callThrough();

	// 		this.scheduleView.checkDeletedWeeks(newWeeks);
	// 		expect(this.scheduleView.warnAboutDeletedWeeks).toHaveBeenCalledWith(deletedWeeks);
	// 	});
	// });

	describe('method warnAboutDeletedWeeks', function() {
		var deletedWeeks = JSON.parse(TestResponses.repertoire.deletedWeeks.responseText);

		it('should correctly create message', function() {
			expect(this.scheduleView.warnAboutDeletedWeeks(deletedWeeks)).toBe('Вы изменили сроки проката фильма.\n\nНеделя 22 - 28 янв. будет удалена. На ней запланировано\n0 сеанса(ов) в 2D.\n\nВы уверены?');
		});
	});

	describe('datepicker for inputs since & until date', function() {

		afterEach(function() {
			$('body .datepicker').remove();
		});

		// it('should appear on focus input', function() {
		// 	this.scheduleView.$el.find('.since_date').trigger('focus');
		// 	this.scheduleView.$el.find('.until_date').trigger('focus');

		// 	expect($('body .datepicker').length).toBe(2);
		// });

		it('should disable right dates', function() {
			this.scheduleView.$el.find('.since_date').datepicker('show');
			this.scheduleView.$el.find('.until_date').datepicker('show');
			expect($('body .datepicker:first .datepicker-days tbody tr:last .day').hasClass('disabled')).toBe(true);
			expect($('body .datepicker:last .datepicker-days tbody tr:first .day').hasClass('disabled')).toBe(true);
		});

		it('should disable active date on show', function() {
			this.scheduleView.$el.find('.since_date').datepicker('show');
			expect($('body .datepicker .day.active').hasClass('disabled')).toBe(true);
		});

		it('should call changing hire range on hide', function() {
			spyOn(this.scheduleView, 'changeHireRange').and.callThrough();
			this.scheduleView.$el.find('.since_date').datepicker('show');
			this.scheduleView.$el.find('.since_date').datepicker('hide');
			expect(this.scheduleView.changeHireRange).toHaveBeenCalled();
		});
	});

});

describe('Schedule Seances View', function() {
	'use strict';

	beforeEach(function() {
		getDatFckTemplate('schedule_edit_seances_tmpl');
		appendSetFixtures(sandbox({id:'schedule-seances'}));

		jasmine.Ajax.install();

		this.model = new window.app.Repertoire(JSON.parse(TestResponses.repertoire.defaultModel.responseText));

		this.scheduleEditSeances = new window.app.ScheduleEditSeancesView({
			technology: this.model.get('weeks')[0].technology,

			format: '2d',
			upgrades: ['dbox'],

			tx: 999,
			date: {
				week: 50,
				year: 2014
			},
			halls: this.model.get('cinema').halls,
			cinema_id: this.model.get('cinema_id'),
			release_id: this.model.get('release_id'),
			parent: this.model,
			saveOnChange: true
		});

		$('#schedule-seances').prepend(this.scheduleEditSeances.render().el);

	});

	afterEach(function() {
		this.model.stopListening();
		this.scheduleEditSeances.remove();
		jasmine.Ajax.uninstall();
	});

	it('should change seances', function() {
		$('.halls-popup-top__input').val(10);
		$('.halls-popup__save').click();
		expect(this.scheduleEditSeances.options.technology[1].count).toBe(10);
	});

});

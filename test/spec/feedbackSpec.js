describe('Feedback Popup', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('feedback_popup_tmpl');

		this.user = new window.app.User();

		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		jasmine.Ajax.install();

		this.feedback = new window.app.FeedbackPopup();
	});

	afterEach(function() {
		this.user.stopListening();
		this.feedback.remove();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	describe('url', function() {
		it('should be http://dev.servicedesk on dev', function() {
			this.feedback.$el
					.find('.feedback-popup__message').val('test').end()
					.find('form').submit();

			request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe('http://dev.servicedesk.dcp24.ru/api/ticket/servicevoice');
		});

		it('should be https://servicedesk on production', function() {
			spyOn(_, 'getWindowLocationHost').and.returnValue('kinoplan24.ru');

			this.feedback.$el
				.find('.feedback-popup__message').val('test').end()
				.find('form').submit();

			request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe('https://servicedesk.dcp24.ru/api/ticket/servicevoice');
		});
	});

	describe('on submit success', function() {
		beforeEach(function() {
			spyOn($.growl, 'ok');
			spyOn($.growl, 'error');

			this.feedback.$el
				.find('.feedback-popup__message').val('test').end()
				.find('form').submit();

			request = jasmine.Ajax.requests.mostRecent();
		});

		it('should show ok message', function() {
			request.response({
				status: 200,
				responseText: '{"status":"ok","ticket_id":"999999"}'
			});
			expect($.growl.ok).toHaveBeenCalled();
		});

		it('should show error message', function() {
			request.response({
				status: 200,
				responseText: '{"status":"error","msg":"yolo"}'
			});
			expect($.growl.error).toHaveBeenCalled();
		});

	});

	it('should\'nt submit empty feedback', function() {
		spyOn($.growl, 'error');
		this.feedback.$el.find('form').submit();
		expect($.growl.error).toHaveBeenCalled();
	});

});

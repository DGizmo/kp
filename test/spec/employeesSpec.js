describe('Employees', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('employees_tmpl');

		jasmine.Ajax.install();

		this.collection = new window.app.EmployeesCollection();
		this.employeesView = new window.app.EmployeesView({collection: this.collection});

		this.employeesView.render();
		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.employee.success);
	});

	afterEach(function() {
		jasmine.Ajax.uninstall();
		this.collection.stopListening();
		this.employeesView.remove();
	});

	it('should be defined', function() {
		expect(this.employeesView).toBeDefined();
	});

});

describe('Keys', function() {
	'use strict';
	var request;

	describe('Collection', function() {

		beforeEach(function() {
			this.colletion = new window.app.Keys();
		});

		afterEach(function() {
			this.collection.stopListening();
		});

		it('should be defined', function() {
			expect(this.collection).toBeDefined();
		});

	});

	beforeEach(function() {
		getDatFckTemplate('keys_wrapper');
		getDatFckTemplate('keys_filter_tmpl');
		getDatFckTemplate('keys_tmpl');

		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText), {parse: true});
		this.user.set({
			tx: 111,
			current: {
				cinema_id: 602
			}
		});

		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		jasmine.Ajax.install();

		this.collection = new window.app.Keys();
		this.keysWrapView = new window.app.KeysWrapView({collection: this.collection});

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.keys.success);
	});

	afterEach(function() {
		this.user.stopListening();
		this.collection.stopListening();
		this.keysWrapView.remove();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	it('should be defined', function() {
		expect(this.keysWrapView).toBeDefined();
	});

});

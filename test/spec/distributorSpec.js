describe('Distributors', function() {
	'use strict';

	var request;

	beforeEach(function() {
		getDatFckTemplate('distributor_wrapper');
		getDatFckTemplate('distributor_item');

		this.user = new window.app.User();

		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		jasmine.Ajax.install();

		this.collection = new window.app.Distributors();
		this.distributors = new window.app.DistributorsWrapView({
			collection: this.collection
		});

		this.distributors.render();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.distributors.success);
	});

	afterEach(function() {
		this.user.stopListening();
		this.collection.stopListening();
		this.distributors.remove();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	it('should be defined', function() {
		expect(this.distributors).toBeDefined();
	});

});

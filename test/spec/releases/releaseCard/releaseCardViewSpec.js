describe('Release card view', function() {
	'use strict';

	beforeEach(function() {
		getDatFckTemplate('release_card_tmpl');

		jasmine.Ajax.install();

		this.user = new window.app.User();
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		this.release = new window.app.Release(JSON.parse(TestResponses.releases.defaultModel.responseText));

		this.releaseCardView = new window.app.ReleaseCardView({
			model: this.release,
			tab: 'release_info'
		});
	});

	afterEach(function() {
		this.user.stopListening();
		this.release.stopListening();
		this.releaseCardView.remove();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	describe('Reaction on sockets', function() {
		it('socket:note - should change note of found model', function() {
			Backbone.trigger('socket:note', {id: 9999, value: 'check this note!!!'});
			expect(this.releaseCardView.model.get('note')).toBe('check this note!!!');
		});
	});

	describe('Check opportunity of edit by distributor', function() {
		it('should be editable for own distributor', function() {
			this.release.set({distributors: [{id: 111}]}, {silent: true});
			this.user.set({approved: 1, type: 'distributor', distributor_id: 111});

			expect(this.releaseCardView.checkEditable()).toBeTruthy();
		});

		it('shouldn"t be editable for foreign distributor', function() {
			this.user.set({distributor_id: 222});
			expect(this.releaseCardView.checkEditable()).toBeFalsy();
		});
	});

});

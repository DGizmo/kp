describe('Release card reports view', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('release-report');
		getDatFckTemplate('release-report-week');
		appendSetFixtures(sandbox({class:'release_card_wrapper'}));

		jasmine.Ajax.install();

		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText));

		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		this.release = new window.app.Release(JSON.parse(TestResponses.releases.defaultModel.responseText));
		this.release.set({my: [602]});

		this.releaseReportView = new window.app.ReleaseReportsWrapperView({model: this.release});

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.releases.reportsCollection);
	});

	afterEach(function() {
		this.user.stopListening();
		this.release.stopListening();
		this.releaseReportView.remove();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	it('should be defined', function() {
		expect(this.releaseReportView).toBeDefined();
	});

	describe('cinema subview', function() {
		beforeEach(function() {
			this.cinemaView = this.releaseReportView.subViews[0];
		});

		afterEach(function() {
			this.cinemaView.remove();
		});

		it('should call addDay function by click add button', function() {
			spyOn(this.cinemaView, 'addDay').and.callThrough();

			this.cinemaView.$('.release-reports-cinema__add').trigger('click');

			expect(this.cinemaView.addDay).toHaveBeenCalled();
		});

		it('should add new week by click add button if last cinema report day is wednesday or cinema haven"t weeks', function() {

			this.cinemaView.subViews.splice(-1, 1);

			var oldSubviewLength  = this.cinemaView.subViews.length,
				oldSubviewLastDay = moment(_.last(this.cinemaView.models).get('date')),
				newSubviewLastDay;

			spyOn(this.cinemaView.subViews, 'push').and.callThrough();

			this.cinemaView.$('.release-reports-cinema__add').trigger('click');

			newSubviewLastDay = moment(_.last(this.cinemaView.models).get('date'));

			expect(this.cinemaView.subViews.push).toHaveBeenCalled();
			expect(oldSubviewLength + 1).toBe(this.cinemaView.subViews.length);
			expect(newSubviewLastDay.diff(oldSubviewLastDay, 'days')).toBe(1);
		});

		it('should create day model if week dont have models', function() {

			this.cinemaView.models = [];

			spyOn(this.cinemaView, 'createDayModel').and.callThrough();

			var model = this.cinemaView.createDayModel(),
				modelData = {
					cinema_id: model.get('cinema_id'),
					date:      model.get('date')
				},
				neededData = {
					cinema_id: this.cinemaView.options.cinemaId,
					date:      this.cinemaView.options.releaseDate
				};

			expect(this.cinemaView.createDayModel).toHaveBeenCalled();
			expect(_.isEqual(modelData, neededData)).toBe(true);
		});

		describe('full week view', function() {
			beforeEach(function() {
				this.fullWeek = this.cinemaView.subViews[0];
			});

			afterEach(function() {
				this.fullWeek.remove();
			});

			it('should be defined', function() {
				expect(this.fullWeek).toBeDefined();
			});

			it('days length should be 7', function() {
				expect(this.fullWeek.subViews.length).toBe(7);
			});
		});

		describe('short week view', function() {
			beforeEach(function() {
				this.shortWeek = this.cinemaView.subViews[1];
			});

			afterEach(function() {
				this.shortWeek.remove();
			});

			it('should be defined', function() {
				expect(this.shortWeek).toBeDefined();
			});

			it('days length should be less then 7', function() {
				expect(this.shortWeek.subViews.length).toBe(1);
			});

			describe('one day view', function() {
				beforeEach(function() {
					this.dayView = _.last(this.shortWeek.subViews);
				});

				afterEach(function() {
					this.dayView.remove();
					jasmine.Ajax.uninstall();
				});

				it('should be defined', function() {
					expect(this.dayView).toBeDefined();
				});

				it('should call model save by click save button', function() {

					spyOn(this.dayView.model, 'save').and.callThrough();

					this.dayView.$('.release-reports-day__change').trigger('click');
					this.dayView.$('.release-reports-day__save').trigger('click');

					expect(this.dayView.model.save).toHaveBeenCalled();
				});

				it('should set view saved param to false by click change button', function() {
					this.dayView.$('.release-reports-day__change').trigger('click');

					expect(this.dayView.saved).toBe(false);
				});

				it('should set input value to model by change', function() {
					this.dayView.$('.release-reports-day__change').trigger('click');

					this.dayView.render();

					var $input = this.dayView.$('.release-reports-day__box-office');

					jasmine.Ajax.install();

					spyOn(this.dayView.model, 'set').and.callThrough();

					$input.val('1111').trigger('change');

					expect(this.dayView.model.set).toHaveBeenCalledWith('box_office', '1111', { validate: true });
				});

				// it('should add error class and show growl if changed input value not a number', function() {
				// 	spyOn($, 'growl');

				// 	this.dayView.$('.release-reports-day__change').trigger('click');

				// 	this.dayView.render();

				// 	var $input = this.dayView.$('.release-reports-day__box-office');

				// 	$input.val('fff').trigger('change');

				// 	expect($input.hasClass('error')).toBe(true);
				// 	expect($.growls).toHaveBeenCalled();
				// });
			});
		});
	});

});

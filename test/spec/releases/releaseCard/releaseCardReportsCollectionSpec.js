describe('Release card reports Collection', function() {
	'use strict';
	var request;

	beforeEach(function() {
		jasmine.Ajax.install();

		this.collection = new window.app.ReleaseReportCollection([], { releaseId: 111 });
		this.collection.fetch();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.releases.reportsCollection);
	});

	afterEach(function() {
		this.collection.stopListening();
		jasmine.Ajax.uninstall();
	});

	it('should be not empty', function() {
		expect(this.collection.length).toBe(10);
	});

	it('should return right url', function() {
		expect(request.url).toBe('/api/v2/release/111/reports');
	});

});

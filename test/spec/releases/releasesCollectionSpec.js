describe('Releases Collection', function() {
	'use strict';
	var request;

	beforeEach(function() {
		jasmine.Ajax.install();

		this.collection = new window.app.Releases();
		this.collection.distList = JSON.parse(TestResponses.releases.distributorsList.responseText).list;

		this.collection.fetch();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.releases.defaultCollection);
	});

	afterEach(function() {
		this.collection.stopListening();
		jasmine.Ajax.uninstall();
	});

	it('should be right parsed', function() {
		expect(this.collection.length).toBe(1);
		expect(this.collection.lastChange).toBe(1422604837);
	});

	describe('URL', function() {

		it('should return url for mode "date"', function() {
			this.collection.year  = '2014';
			this.collection.month = '12';
			expect(this.collection.url()).toBe('/api/releases/2014/12?web=1&first=1');
		});

		it('should return url for mode "distributor"', function() {
			this.collection.distributor = '96';
			expect(this.collection.url()).toBe('/api/releases/distributor/96?web=1&first=1');
		});

		it('should return url for mode "format"', function() {
			this.collection.format = '2D';
			expect(this.collection.url()).toBe('/api/releases/format/2D?web=1&first=1');
		});

		it('should return url for mode "myRep"', function() {
			this.collection.my = true;
			expect(this.collection.url()).toBe('/api/releases/my?web=1&first=1');
		});

	});

	describe('react on events', function() {
		it('release:delete - should delete release from collection', function() {
			Backbone.trigger('release:delete', 4913);

			expect(this.collection.length).toBe(0);
		});

		describe('socket:releases:update:all', function() {
			it('should update collection', function() {
				spyOn(this.collection, 'update');
				this.collection.releases_filter = 'distributor';
				Backbone.trigger('socket:releases:update:all');
				expect(this.collection.update).toHaveBeenCalledWith('distributor');
			});

			it('should setYearAndMonth to actual date for filter date', function() {
				var actualYear  = _.getWeekStartDate({type: 'date'}).getFullYear(),
					actualMonth = _.getWeekStartDate({type: 'date'}).getMonth() + 1;

				spyOn(this.collection, 'setYearAndMonth');

				this.collection.releases_filter = 'date';
				Backbone.trigger('socket:releases:update:all');

				expect(this.collection.setYearAndMonth).toHaveBeenCalledWith(actualYear, actualMonth);
			});
		});

		describe('socket:favourite', function() {
			beforeEach(function() {
				this.release = this.collection.get(4913);
				jasmine.Ajax.install();
			});

			afterEach(function() {
				jasmine.Ajax.uninstall();
				this.release = undefined;
			});

			it('if model found, it should be updated', function() {
				spyOn(this.release, 'fetch');

				Backbone.trigger('socket:favourite', {
					favourite_id: 4913
				});

				expect(this.release.fetch).toHaveBeenCalled();
			});

			it('if model not found, it should be created for myrep', function() {
				spyOn(Backbone, 'trigger').and.callThrough();

				this.collection.releases_filter = 'myrep';
				Backbone.trigger('socket:favourite', {
					favourite_id: 9999
				});

				request = jasmine.Ajax.requests.mostRecent();
				request.response(TestResponses.releases.defaultModel);

				expect(Backbone.trigger.calls.argsFor(1)[0]).toBe('releases:add-release');
			});
		});

		describe('socket:releases:update', function() {
			beforeEach(function() {
				this.release = this.collection.get(4913);
				jasmine.Ajax.install();
			});

			afterEach(function() {
				jasmine.Ajax.uninstall();
				this.release = undefined;
			});

			it('if model found, it should be updated', function() {
				spyOn(this.release, 'fetch');
				Backbone.trigger('socket:releases:update', {release_id: [4913]});

				expect(this.release.fetch).toHaveBeenCalled();
			});

			it('if model not found, it should be created for date', function() {
				spyOn(this.collection, 'moveReleaseDate').and.callThrough();

				this.collection.releases_filter = 'date';
				Backbone.trigger('socket:releases:update', {release_id: [9999]});

				request = jasmine.Ajax.requests.mostRecent();
				request.response(TestResponses.releases.defaultModel);

				expect(this.collection.moveReleaseDate).toHaveBeenCalled();
			});
		});
	});

	describe('Filter', function() {

		it('should filter by date', function() {
			this.collection.setYearAndMonth('2014', '12');
			expect(this.collection.distributor || this.collection.my || this.collection.format).toBe(false);
			expect(this.collection.year + this.collection.nextYear + this.collection.prevYear + this.collection.month + this.collection.prevMonth + this.collection.nextMonth + this.collection.releases_filter).toBe('201420142014121212date');
		});

		it('should filter by distributor with valid id', function() {
			this.collection.setDistributor(50);
			expect(this.collection.my || this.collection.format).toBe(false);
			expect(this.collection.distributor + this.collection.distributorName + this.collection.releases_filter).toBe('50Cinema Prestigedistributor');
		});

		it('shouldn\'t filter by distributor with invalid distributor id', function() {
			this.collection.setDistributor(75);
			expect(this.collection.distributor || this.collection.my || this.collection.format).toBe(false);
		});

		it('should filter by myRep', function() {
			this.collection.showMy();
			expect(this.collection.my).toBe(true);
			expect(this.collection.releases_filter).toBe('myrep');
		});

		it('should filter by format', function() {
			this.collection.setFormat('3D');
			expect(this.collection.format + this.collection.releases_filter).toBe('3Dformats');
		});

	});

});

describe('Release model', function() {
	'use strict';
	var request;

	beforeEach(function() {
		this.user = new window.app.User();
		this.user.set({tx: 111});

		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		this.model = new window.app.Release(JSON.parse(TestResponses.releases.defaultModel.responseText));
	});

	afterEach(function() {
		this.user.stopListening();
		this.model.stopListening();
		reqres.removeHandler('get:user');
	});

	it('should return url', function() {
		this.model.set('id', '4321');
		expect(this.model.url()).toBe('/api/v2/release/4321');
	});

	describe('Validation', function() {

		it('should check custom duration data format', function() {
			this.model.set({duration: {custom: '123'}});
			expect(this.model.isValid()).toBe(false);
		});

		it('should check custom duration not more 500', function() {
			this.model.set({duration: {custom: 501}});
			expect(this.model.isValid()).toBe(false);
		});

		it('should check custom duration not less 0', function() {
			this.model.set({duration: {custom: -1}});
			expect(this.model.isValid()).toBe(false);
		});

		it('should check chosen duration data format', function() {
			this.model.set({chosen_duration: 12});
			expect(this.model.isValid()).toBe(false);
		});

	});

	describe('Saving duration', function() {

		beforeEach(function() {
			jasmine.Ajax.install();

			spyOn(Backbone, 'trigger').and.callThrough();
			spyOn($, 'growl');

			this.model.set({duration: {custom: 120}}).saveDuration();

			request = jasmine.Ajax.requests.mostRecent();
			request.response({status: 200, responseText: '{"ok":1}'});
		});

		afterEach(function() {
			jasmine.Ajax.uninstall();
		});

		it('should save duration if model is valid', function() {
			expect(Backbone.trigger).toHaveBeenCalledWith('release:duration:changed', {
				id: 9999,
				duration: {custom: 120},
				chosen_duration: 'full'
			});
		});

		it('should return previous attrs for invalid model', function() {
			// Set valid attr
			this.model.set({duration: {clean: 111, full: 222, custom: 333}});

			// Set invalid attr
			this.model.set({duration: {clean: null, custom: -1}}).saveDuration();

			expect(this.model.get('duration').custom).toBe(333);
		});

	});

	it('should calc files number', function() {
		expect(this.model.getFilesNumber()).toBe(5);
	});

	it('should calc active keys number', function() {
		expect(this.model.getActiveKey()).toBe(1);
	});

	it('should group trailers by version', function() {
		var trailersGroups = this.model.parseTrailers();
		expect(Object.keys(trailersGroups).length).toBe(3);
	});

	describe('Add/remove from favourites', function() {

		beforeEach(function() {
			jasmine.Ajax.install();
		});

		afterEach(function() {
			jasmine.Ajax.uninstall();
		});

		it('should add model to favourites', function() {
			this.model.addToFavourites('2, 3');

			request = jasmine.Ajax.requests.mostRecent();
			request.response({
				status: 200,
				responseText: '{"ok":1}'
			});

			expect(this.model.get('my')).toEqual([2, 3]);
		});

		it('should remove model from favourites', function() {
			this.model.addToFavourites('');

			request = jasmine.Ajax.requests.mostRecent();
			request.response({
				status: 200,
				responseText: '{"ok":1}'
			});

			expect(this.model.get('my')).toEqual([]);
		});

		it('should trigger event', function() {
			spyOn(Backbone, 'trigger').and.callThrough();

			this.model.addToFavourites('2, 3');

			request = jasmine.Ajax.requests.mostRecent();
			request.response({
				status: 200,
				responseText: '{"ok":1}'
			});

			expect(Backbone.trigger).toHaveBeenCalledWith('release:favourites', { week: 32, year: 2014, id: 9999 });
		});

	});

	describe('Remove duration', function() {
		beforeEach(function() {
			jasmine.Ajax.install();
		});

		afterEach(function() {
			jasmine.Ajax.uninstall();
		});

		it('should save chosen duration on remove custome duration (chosenDuration:full)', function() {
			this.model.set({chosen_duration: 'full'});
			this.model.removeDuration();
			expect(this.model.get('chosen_duration')).toBe('full');
		});

		it('should save chosen duration on remove custome duration (chosenDuration:clean)', function() {
			this.model.set({chosen_duration: 'clean'});
			this.model.removeDuration();
			expect(this.model.get('chosen_duration')).toBe('clean');
		});

		it('should change chosen duration on remove custome duration (chosenDuration:custom)', function() {
			this.model.set({chosen_duration: 'custom'});
			this.model.removeDuration();
			expect(this.model.get('chosen_duration')).toBe('full');
		});

	});

});

/**
*
* so flaky
*
**/

describe('Searching module view', function() {
	'use strict';

	beforeEach(function() {
		appendSetFixtures(sandbox({class:'searching_block'}));

		jasmine.Ajax.install();

		this.searching = new window.app.SearchingModuleView({
			animation: true,
			onSearchingFinish: function onSearchingFinish() {}
		});
		$('.searching_block').html(this.searching.render().el);
	});

	afterEach(function() {
		this.searching.remove();
		jasmine.Ajax.uninstall();
	});

	// it('should add class on input focus', function() {
	// 	this.searching.$('.input').trigger('focus');

	// 	expect(this.searching.$el.hasClass('focus')).toBe(true);
	// });

	it('should not add class on input focus if animating is false', function() {
		this.searching.options.animation = false;
		this.searching.$('.input').trigger('focus');

		expect(this.searching.$el.hasClass('focus')).toBe(false);
	});

	// it('should remove class on input blur', function() {
	// 	this.searching.$('.input').trigger('focus');
	// 	expect(this.searching.$el.hasClass('focus')).toBe(true);

	// 	this.searching.$('.input').trigger('blur');
	// 	expect(this.searching.$el.hasClass('focus')).toBe(false);
	// });

	// it('should not remove class on input blur if animating is false', function() {
	// 	this.searching.$('.input').trigger('focus');
	// 	expect(this.searching.$el.hasClass('focus')).toBe(true);

	// 	this.searching.options.animation = false;

	// 	this.searching.$('.input').trigger('blur');
	// 	expect(this.searching.$el.hasClass('focus')).toBe(true);
	// });

	it('should remove input val on click remove cross', function() {
		this.searching.$('.input').val(123123121);
		this.searching.$('.remove').trigger('click');

		expect(this.searching.$('.input').val().length).toBe(0);
	});

	it('should add class if input has val', function() {
		this.searching.$('.input').val(123123121).trigger('input');

		expect(this.searching.$('.input').hasClass('full')).toBe(true);
	});

	it('should remove class if input val is empty', function() {
		this.searching.$('.input').val('').trigger('input');

		expect(this.searching.$('.input').hasClass('full')).toBe(false);
	});

	it('should select first result', function() {
		var searchingReleaseView = new window.app.SearchingReleaseView(),
			jEvent = $.Event('keydown', { keyCode: 40 });

		this.searching.subViews.push(searchingReleaseView, searchingReleaseView, searchingReleaseView);

		_.each(this.searching.subViews, function(view) {
			this.searching.$('.results').append(view.render().el);
		}, this);

		this.searching.onKeyDown(jEvent);

		expect(this.searching.$('.results .item:first').hasClass('selected')).toBe(true);

		searchingReleaseView.remove();
	});

	/**
	*
	* WTF
	*
	**/

	// it('should select next result until current selected result', function() {
	// 	var jEventDown = $.Event('keydown', { keyCode: 40 }),
	// 		searchingReleaseView = new window.app.SearchingReleaseView();

	// 	this.searching.subViews.push(searchingReleaseView, searchingReleaseView, searchingReleaseView);

	// 	_.each(this.searching.subViews, function(view) {
	// 		this.searching.$('.results').append(view.render().el);
	// 	}, this);

	// 	this.searching.onKeyDown(jEventDown);

	// 	expect(this.searching.$('.results .item:first').hasClass('selected')).toBe(true);

	// 	this.searching.onKeyDown(jEventDown);

	// 	expect(this.searching.$('.results .item:first').hasClass('selected')).toBe(false);
	// 	expect(this.searching.$('.results .item:first').next().hasClass('selected')).toBe(true);

	// 	searchingReleaseView.remove();
	// });

	// it('should select prev result until current selected result', function() {
	// 	var jEventDown = $.Event('keydown', { keyCode: 40 }),
	// 		jEventUp   = $.Event('keydown', { keyCode: 38 });

		// this.searching.subViews.push(new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView());

	// 	_.each(this.searching.subViews, function(view) {
	// 		this.searching.$('.results').append(view.render().el);
	// 	}, this);

	// 	this.searching.onKeyDown(jEventDown);
	// 	this.searching.onKeyDown(jEventDown);

	// 	expect(this.searching.$('.results .item:first').next().hasClass('selected')).toBe(true);

	// 	this.searching.onKeyDown(jEventUp);

	// 	expect(this.searching.$('.results .item:first').hasClass('selected')).toBe(true);
	// 	expect(this.searching.$('.results .item:first').next().hasClass('selected')).toBe(false);
	// });

	// it('should trigger router navigate by enter press', function() {
	// 	var jEventDown  = $.Event('keydown', { keyCode: 40 }),
	// 		jEventEnter = $.Event('keydown', { keyCode: 13 });

		// this.searching.subViews.push(new window.app.SearchingReleaseView({release: { id: 1 }}),
		// 	new window.app.SearchingReleaseView({release: { id: 2 }}),
		// 	new window.app.SearchingReleaseView({release: { id: 3 }}));

	// 	_.each(this.searching.subViews, function(view) {
	// 		this.searching.$('.results').append(view.render().el);
	// 	}, this);

	// 	this.searching.onKeyDown(jEventDown);

	// 	spyOn(Backbone, 'trigger');

	// 	this.searching.onKeyDown(jEventEnter);

	// 	expect(Backbone.trigger).toHaveBeenCalledWith('router:navigate', 'release/1');
	// });

	// it('should scroll bottom if selected release is after restults block viewport', function() {
	// 	var jEventDown = $.Event('keydown', { keyCode: 40 }),
	// 		beforeScrollPotision,
	// 		afterScrollPosition;

		// this.searching.subViews.push(new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView());

	// 	_.each(this.searching.subViews, function(view) {
	// 		this.searching.$('.results').append(view.render().el);
	// 	}, this);

	// 	this.searching.$('.results').css({
	// 		height: 100,
	// 		'overflow-y': 'scroll'
	// 	});

	// 	beforeScrollPotision = this.searching.$('.results').scrollTop();

	// 	_.times(9, function() {
	// 		this.searching.onKeyDown(jEventDown);
	// 	}, this);

	// 	afterScrollPosition = this.searching.$('.results').scrollTop();

	// 	expect(afterScrollPosition).not.toBeGreaterThan(afterScrollPosition);
	// });

	// it('should scroll top if selected release is before restults block viewport', function() {
	// 	var jEventDown = $.Event('keydown', { keyCode: 40 }),
	// 		jEventUp   = $.Event('keydown', { keyCode: 38 }),
	// 		beforeScrollPosition,
	// 		afterScrollPosition;

		// this.searching.subViews.push(new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView(), new window.app.SearchingReleaseView(),
		// 	new window.app.SearchingReleaseView());

	// 	_.each(this.searching.subViews, function(view) {
	// 		this.searching.$('.results').append(view.render().el);
	// 	}, this);

	// 	this.searching.$('.results').css({
	// 		height: 100,
	// 		'overflow-y': 'scroll'
	// 	});

	// 	_.times(9, function() {
	// 		this.searching.onKeyDown(jEventDown);
	// 	}, this);

	// 	beforeScrollPosition = this.searching.$('.results').scrollTop();

	// 	_.times(8, function() {
	// 		this.searching.onKeyDown(jEventUp);
	// 	}, this);

	// 	afterScrollPosition = this.searching.$('.results').scrollTop();

	// 	expect(beforeScrollPosition).toBeGreaterThan(afterScrollPosition);
	// });

});

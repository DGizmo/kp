describe('UserPopups - ', function() {
	'use strict';

	describe('user settings popup', function() {

		beforeEach(function() {
			getDatFckTemplate('user_settings_tmpl');

			this.user = new window.app.User(JSON.parse(TestResponses.user.cinema.responseText), {parse: true});
			this.userView = new window.app.UserView({model: this.user});
		});

		afterEach(function() {
			this.user.stopListening();
			this.userView.remove();
		});

		it('should be defined', function() {
			expect(this.userView).toBeDefined();
		});

	});

	// describe('cinemas settings popup', function() {

	// 	beforeEach(function() {
	// 		getDatFckTemplate('user_settings_cinemas_tmpl');
	// 		appendSetFixtures(sandbox({id: 'js-popup'}));

	// 		this.user = new window.app.User(JSON.parse(TestResponses.user.cinema.responseText), {parse: true});
	// 		this.userViewCinemas = new window.app.UserViewCinemas({model: this.user});
	// 	});

	// 	afterEach(function() {
	// 		this.user = undefined;
	// 		this.userViewCinemas.remove();
	// 		this.userViewCinemas = undefined;
	// 	});

	// 	it('should be defined', function() {
	// 		expect(this.userViewCinemas).toBeDefined();
	// 	});

	// });

});

// describe('Advertising Group View', function() {
// 	'use strict';

// 	var fakeModelJson = {
// 		id: 666,
// 		title: 'Планета Обезьян: Революция',
// 		age: 16,
// 		duration: {
// 			clean: 136,
// 			full: 142
// 		}
// 	};

// 	beforeEach(function() {
// 		getDatFckTemplate('advertisingRelease');

// 		this.advertisingGroupView = new window.app.AdvertisingGroupView({
// 			header: '31.07',
// 			models: [new Backbone.Model(fakeModelJson)],
// 			groupModelView: window.app.AdvertisingModelView
// 		});

// 		spyOn(this.advertisingGroupView, 'remove').and.callThrough();
// 	});

// 	afterEach(function() {
// 		this.advertisingGroupView.remove();
// 		this.advertisingGroupView = undefined;
// 	});

// 	it('should be defined', function() {
// 		expect(this.advertisingGroupView).toBeDefined();
// 	});

// 	it('should add model', function() {
// 		this.advertisingGroupView.render();
// 		this.advertisingGroupView.addView(new Backbone.Model(fakeModelJson));

// 		expect(this.advertisingGroupView.subViews.length).toBe(2);
// 		expect(this.advertisingGroupView.$el.find('.advertising-release').length).toBe(2);
// 	});

// 	it('should remove view on model trigger remove', function() {
// 		this.advertisingGroupView.render().addView(new Backbone.Model(fakeModelJson));

// 		this.advertisingGroupView.subViews[0].model.trigger('remove');

// 		expect(this.advertisingGroupView.subViews.length).toBe(1);
// 		expect(this.advertisingGroupView.$el.find('.advertising-release').length).toBe(1);
// 	});

// 	it('should remove them self when 0 models', function() {
// 		this.advertisingGroupView.render();

// 		this.advertisingGroupView.subViews[0].model.trigger('remove');

// 		expect(this.advertisingGroupView.remove.calls.all().length).toBe(1);
// 	});

// });

describe('Advertising', function() {
	'use strict';
	var request;

	beforeEach(function() {
		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText), {parse: true});
		this.user.set({
			tx: 111,
			current: {
				cinema_id: 602
			}
		});

		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});
		jasmine.Ajax.install();
	});

	afterEach(function() {
		this.user.stopListening();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	describe('Releases', function() {

		describe('Model', function() {
			beforeEach(function() {
				this.model = new window.app.AdvertisingRelease({id: 1});
				this.model.fetch();

				request = jasmine.Ajax.requests.mostRecent();
				request.response(TestResponses.advertising.releases.defaultModel);
			});

			afterEach(function() {
				this.model.stopListening();
			});

			it('should fetch correct url', function() {
				expect(request.url).toBe('/api/ads/releases/1?cinema_id=' + this.user.getCinema().id + '&tx=111');
			});
		});

		describe('Collection', function() {
			beforeEach(function() {
				var fetchOptions = {
					reset: true,
					data: {
						mode: 'current-week',
						cinema_id: '111'
					}
				};

				this.collection = new window.app.AdvertisingReleases();
				this.collection.fetch(fetchOptions);

				request = jasmine.Ajax.requests.mostRecent();
				request.response(TestResponses.advertising.releases.collection);
			});

			afterEach(function() {
				this.collection.stopListening();
			});

			it('should be defined and fetch correct url', function() {
				expect(request.url).toBe('/api/ads/releases?mode=current-week&cinema_id=111');
			});

			it('should parse and sort models', function() {
				var isSortedByDate = _.every(this.collection.models, function(model, index, array) {
					return index === 0 || +moment(array[index - 1].date) <= +moment(model.date);
				});
				expect(isSortedByDate).toBe(true);
			});

			it('should return sorted & grouped models', function() {
				var isGroupedByDate = _.every(_.keys(this.collection.groupByDate()), function(model, index, array) {
					return index === 0 || +moment(array[index - 1].date) <= +moment(model.date);
				});
				expect(isGroupedByDate).toBe(true);
			});

			it('should listen "advertising:element:hover" & trigger model', function() {
				var model = this.collection.get(1);

				spyOn(model, 'trigger').and.callThrough();

				Backbone.trigger('advertising:element:hover', {release: 0});
				Backbone.trigger('advertising:element:hover', {release: 1});

				expect(model.trigger).toHaveBeenCalledWith('hover');
			});

			describe('Sockets', function() {
				it('should update models when receiving socket "UPDATE"', function() {
					var model = this.collection.get(1);

					spyOn(model, 'fetch').and.callThrough();

					// Update existing model
					Backbone.trigger('socket:advertising:release', {
						event: 'update',
						cinema_id: this.user.getCinema().id,
						id: 1
					});

					// Update unexisting model
					Backbone.trigger('socket:advertising:release', {
						event: 'update',
						cinema_id: this.user.getCinema().id,
						id: 0
					});

					// Shouldn't Update model of other cinemas
					Backbone.trigger('socket:advertising:release', {
						event: 'update',
						cinema_id: 0,
						id: 0
					});

					expect(model.fetch).toHaveBeenCalled();
					expect(this.collection.length).toBe(4);
				});

				it('should add models when receiving socket "ADD"', function() {
					Backbone.trigger('socket:advertising:release', {
						event: 'add',
						cinema_id: this.user.getCinema().id,
						id: 999
					});

					Backbone.trigger('socket:advertising:release', {
						event: 'add',
						cinema_id: 0,
						id: 999
					});

					request = jasmine.Ajax.requests.mostRecent();

					expect(this.collection.length).toBe(4);
					expect(request.url).toBe('/api/ads/releases/999?cinema_id=' + this.user.getCinema().id + '&tx=111');
				});

				it('should add models when receiving socket "REMOVE"', function() {
					Backbone.trigger('socket:advertising:release', {
						event: 'remove',
						cinema_id: this.user.getCinema().id,
						id: 3
					});

					expect(this.collection.length).toBe(2);
				});
			});
		});
	});

	describe('Trailers', function() {

		describe('Model', function() {
			beforeEach(function() {
				this.model = new window.app.AdvertisingTrailer({id: 1});
				this.model.fetch();

				request = jasmine.Ajax.requests.mostRecent();
				request.response(TestResponses.advertising.trailers.defaultModel);
			});

			afterEach(function() {
				this.model.stopListening();
			});

			it('should fetch correct url', function() {
				expect(request.url).toBe('/api/ads/trailers/1?cinema_id=' + this.user.getCinema().id + '&tx=111');
			});
		});

		describe('Collection', function() {
			beforeEach(function() {
				var fetchOptions = {
					reset: true,
					data: {
						mode: 'current-week',
						cinema_id: '111'
					}
				};

				this.collection = new window.app.AdvertisingTrailers();
				this.collection.fetch(fetchOptions);

				request = jasmine.Ajax.requests.mostRecent();
				request.response(TestResponses.advertising.trailers.collection);
			});

			afterEach(function() {
				this.collection.stopListening();
			});

			it('should be defined and fetch correct url', function() {
				expect(request.url).toBe('/api/ads/trailers?mode=current-week&cinema_id=111');
			});

			it('should parse and sort models', function() {
				var isSortedByDate = _.every(this.collection.models, function(model, index, array) {
					return index === 0 || +moment(array[index - 1].date) <= +moment(model.date);
				});
				expect(isSortedByDate).toBe(true);
			});

			it('should return sorted & grouped models', function() {
				var isGroupedByDate = _.every(_.keys(this.collection.groupByDate()), function(model, index, array) {
					return index === 0 || +moment(array[index - 1].date) <= +moment(model.date);
				});
				expect(isGroupedByDate).toBe(true);
			});

			it('should listen "advertising:element:hover" & trigger models states', function() {
				var innerModel = this.collection.get(1),
					addedModel = this.collection.get(2);

				spyOn(innerModel, 'trigger').and.callThrough();
				spyOn(addedModel, 'trigger').and.callThrough();

				Backbone.trigger('advertising:element:hover', {
					inner: [0, 1],
					added: [0, 2]
				});

				expect(innerModel.trigger).toHaveBeenCalledWith('inner');
				expect(addedModel.trigger).toHaveBeenCalledWith('added');
			});
		});
	});
});

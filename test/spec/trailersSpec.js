describe('Trailers', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('sb-trailers_box');
		getDatFckTemplate('sb-trailer_item');
		appendSetFixtures(sandbox({class:'nt-sidebar'}));

		this.user = new window.app.User(JSON.parse(TestResponses.user.cinema.responseText), {parse: true});
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		jasmine.Ajax.install();

		this.MyTrailers = new window.app.TrailersView();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.trailers.success);
	});

	afterEach(function() {
		this.user.stopListening();
		this.MyTrailers.remove();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	it('should be defined', function() {
		expect(this.MyTrailers).toBeDefined();
		expect(this.MyTrailers.$el).toContainElement('.nt-sidebar-trailers-item');
	});

});

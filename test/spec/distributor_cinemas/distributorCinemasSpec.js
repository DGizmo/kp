describe('Distributor cinema', function() {
	'use strict';
	var request;

	beforeEach(function() {
		jasmine.Ajax.install();

		this.collection = new window.app.DistributorCinemasCollection();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.distributorCinemas.defaultCollection);
	});

	afterEach(function() {
		this.collection.stopListening();
		jasmine.Ajax.uninstall();
	});

	describe('Model', function() {
		it('should return right url', function() {
			expect(this.collection.at(0).url()).toEqual('/api/distributors/cinemas/1');
		});
	});

	describe('Collection', function() {

		it('should parse formats to uppercase', function() {
			expect(this.collection.at(0).get('formats')).toEqual(['35mm', '2D', '3D', 'IMAX']);
		});

		it('should be sorted', function() {
			var sortedModelsID = _.map(this.collection.models, function(model) {
				return model.id;
			});

			expect(sortedModelsID).toEqual([1, 2, 3]);
		});

		it('should searching by distributor_cinemas:searching', function() {
			spyOn(this.collection, 'searchingCinemas');
			Backbone.trigger('distributor_cinemas:searching', 'Нау');
			expect(this.collection.searchingCinemas).toHaveBeenCalled();
		});

		it('should searching by distributor_cinemas:searching with empty input value', function() {
			spyOn(this.collection, 'searchingCinemas');
			Backbone.trigger('distributor_cinemas:searching', '');
			expect(this.collection.searchingCinemas).toHaveBeenCalled();
		});

		it('should searching with not empty input value', function() {
			spyOn(this.collection, 'trigger');
			this.collection.searchingCinemas('Наутилус');
			expect(this.collection.trigger).toHaveBeenCalledWith('cinemas:searching', jasmine.any(Object), 'searching');
		});

		it('should searching with empty input value', function() {
			spyOn(this.collection, 'trigger');
			this.collection.searchingCinemas('');
			expect(this.collection.trigger).toHaveBeenCalledWith('cinemas:searching', jasmine.any(Object));
		});

	});

	describe('Collection view', function() {
		beforeEach(function() {
			getDatFckTemplate('distributor_cinemas');
			appendSetFixtures(sandbox({class:'cinemas_list'}));

			this.collectionView = new window.app.DistributorCinemasCollectionView({
				collections: {cinemasCollection: this.collection},
				parentWrap: $('.cinemas_list')
			});
		});

		afterEach(function() {
			this.collectionView.remove();
		});

		it('should contains 1 subView', function() {
			expect(this.collectionView.subViews.length).toEqual(1);
		});

		it('should filter cinemas with halls > 5 & correctly render subViews with empty searching value', function() {
			this.collection.searchingCinemas('');
			expect(this.collectionView.subViews.length).toEqual(1);
		});

		it('should correctly render subViews', function() {
			this.collection.searchingCinemas('Наутилус');
			expect(this.collectionView.subViews.length).toEqual(2);
		});

	});

});

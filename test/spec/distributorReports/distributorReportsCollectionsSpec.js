describe('Distributor reports collections', function() {
	'use strict';
	var request;

	beforeEach(function() {
		jasmine.Ajax.install();

		this.releases = new window.app.DistributorReportsReleasesCollection();
		this.cinemas  = new window.app.DistributorReportsCinemasCollection();
		this.reports  = new window.app.DistributorReportsCollection();
	});

	afterEach(function() {
		this.releases.stopListening();
		this.cinemas.stopListening();
		this.reports.stopListening();
		reqres.removeAllHandlers();
		jasmine.Ajax.uninstall();
	});

	describe('releases', function() {
		it('should be not empty', function() {
			this.releases.fetch();

			request = jasmine.Ajax.requests.mostRecent();
			request.response(TestResponses.distributorReports.releases.collection);

			expect(this.releases.length).toBe(5);
		});
	});

	describe('cinemas', function() {
		it('should be not empty', function() {
			this.cinemas.release_id = 1140;
			this.cinemas.fetch();

			request = jasmine.Ajax.requests.mostRecent();
			request.response(TestResponses.distributorReports.cinemas.collection);

			expect(this.cinemas.length).toBe(6);
		});
	});

	describe('reports', function() {
		it('should be not empty', function() {
			this.reports.release_id = 1140;
			this.reports.cinema_id = 749;
			this.reports.fetch();

			request = jasmine.Ajax.requests.mostRecent();
			request.response(TestResponses.distributorReports.reports.collection);

			expect(this.reports.length).toBe(7);
		});
	});

});

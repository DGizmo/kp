describe('Distributor reports section views', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('distributorReportsWrapper');
		getDatFckTemplate('distributorReports');
		getDatFckTemplate('distributor_cinema_contacts');
		appendSetFixtures(sandbox({class:'reports_block'}));

		this.releasesCollection = new window.app.DistributorReportsReleasesCollection();
		this.cinemasCollection  = new window.app.DistributorReportsCinemasCollection();
		this.reportsCollection  = new window.app.DistributorReportsCollection();

		this.reportsWrapper = new window.app.DistributorReportsWrapperView({
			releasesCollection: this.releasesCollection,
			cinemasCollection: this.cinemasCollection,
			reportsCollection: this.reportsCollection
		});

		$('.reports_block').html(this.reportsWrapper.render().el);
	});

	afterEach(function() {
		this.reportsWrapper.remove();

		this.releases.stopListening();
		this.cinemas.stopListening();
		this.reports.stopListening();

		reqres.removeAllHandlers();
		jasmine.Ajax.uninstall();
	});

	it('should fetch releases collection', function() {
		jasmine.Ajax.install();

		spyOn(this.reportsWrapper, 'renderReports').and.callThrough();

		this.reportsWrapper.getReportsCollectionData(this.reportsWrapper.releasesCollection, {withFetch: true});

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.distributorReports.releases.collection);

		expect(this.reportsWrapper.releasesCollection.length).toBe(5);
		expect(this.reportsWrapper.renderReports).toHaveBeenCalledWith(this.reportsWrapper.releasesCollection.models, this.reportsWrapper.reportsState);
	});

	it('should fetch cinemas collection by trigger', function() {
		jasmine.Ajax.install();

		spyOn(this.reportsWrapper, 'renderReports').and.callThrough();

		Backbone.trigger('distributor:reports:switch:report:view', 'cinemas', 1140, true);

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.distributorReports.cinemas.collection);

		expect(this.reportsWrapper.cinemasCollection.length).toBe(6);
		expect(this.reportsWrapper.renderReports).toHaveBeenCalledWith(this.reportsWrapper.cinemasCollection.models, this.reportsWrapper.reportsState);
	});

	it('should fetch reports collection by trigger', function() {
		jasmine.Ajax.install();

		this.cinemasCollection.release_id = 1140;

		spyOn(this.reportsWrapper, 'renderReports').and.callThrough();

		Backbone.trigger('distributor:reports:switch:report:view', 'reports', 749, true);

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.distributorReports.reports.collection);

		expect(this.reportsWrapper.reportsCollection.length).toBe(7);
		expect(this.reportsWrapper.renderReports).toHaveBeenCalledWith(this.reportsWrapper.reportsCollection.groupByWeeks(), this.reportsWrapper.reportsState);
	});

	it('should back to releases view from cinemas view by click back button', function() {
		jasmine.Ajax.install();
		Backbone.trigger('distributor:reports:switch:report:view', 'cinemas', 1140, false);

		spyOn(Backbone, 'trigger');

		this.reportsWrapper.$('.back').trigger('click');

		expect(Backbone.trigger).toHaveBeenCalledWith('distributor:reports:switch:report:view', 'releases', null, false);
	});

	it('should back to cinemas view from reports view by click back button', function() {
		jasmine.Ajax.install();
		Backbone.trigger('distributor:reports:switch:report:view', 'reports', 749, false);

		spyOn(Backbone, 'trigger');

		this.reportsWrapper.$('.back').trigger('click');

		expect(Backbone.trigger).toHaveBeenCalledWith('distributor:reports:switch:report:view', 'cinemas', this.reportsWrapper.cinemasCollection.release_id, false);
	});

	it('should hide back button and search if reportState is releases', function() {
		jasmine.Ajax.install();
		Backbone.trigger('distributor:reports:switch:report:view', 'releases', 111, false);

		expect(this.reportsWrapper.$('.back').hasClass('hide')).toBe(true);
		expect(this.reportsWrapper.$('.searching-module').is(':visible')).toBe(false);
	});

	it('should back to reports view from contacts view by click back button without fetch', function() {
		this.reportsWrapper.currentCinema = 1140;
		jasmine.Ajax.install();
		this.reportsWrapper.showContacts(this.reportsWrapper.currentCinema);

		spyOn(Backbone, 'trigger');

		this.reportsWrapper.$('.back').trigger('click');

		expect(Backbone.trigger).toHaveBeenCalledWith('distributor:reports:switch:report:view', 'reports', this.reportsWrapper.currentCinema, false);
	});

	it('should render contacts view without render', function() {
		this.reportsWrapper.currentCinema = 1140;
		jasmine.Ajax.install();
		this.reportsWrapper.showContacts(this.reportsWrapper.currentCinema);

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.distributorReports.contacts.success);

		this.reportsWrapper.$('.back').trigger('click');

		this.reportsWrapper.showContacts(this.reportsWrapper.currentCinema);

		expect(this.reportsWrapper.contactsView.options.cinemaId).toBe(1140);
	});

	it('should search cinemas by input value set', function() {
		jasmine.Ajax.install();

		Backbone.trigger('distributor:reports:switch:report:view', 'cinemas', 1140, true);

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.distributorReports.cinemas.collection);

		this.reportsWrapper.$('.searching-module input').val('cupidatat').trigger('keyup');

		var $findedCinema = this.reportsWrapper.$('.reports-table-scroll .reports-table-item').children('.reports-table__cell:first-child:containsLower(cupidatat)').closest('.reports-table-item');

		expect($findedCinema.is(':visible')).toBe(true);
		expect($findedCinema.siblings().length).toBe(this.reportsWrapper.$('.reports-table-scroll .reports-table-item').length - 1);
	});

	it('should hide search remove cross if input value is clear', function() {
		jasmine.Ajax.install();

		Backbone.trigger('distributor:reports:switch:report:view', 'cinemas', 1140, true);

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.distributorReports.cinemas.collection);

		this.reportsWrapper.$('.searching-module input').val('').trigger('keyup');

		expect(this.reportsWrapper.$('.searching-module .remove').is(':visible')).toBe(false);
	});

	it('should show all cinemas and clean input value by click remove cross', function() {
		jasmine.Ajax.install();

		Backbone.trigger('distributor:reports:switch:report:view', 'cinemas', 1140, true);

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.distributorReports.cinemas.collection);

		this.reportsWrapper.$('.searching-module input').val('cupidatat');

		this.reportsWrapper.$('.searching-module .remove').trigger('click');

		expect(this.reportsWrapper.$('.searching-module input').val().length).toBe(0);
		expect(this.reportsWrapper.$('.reports-table-scroll .reports-table-item:visible').length).toBe(this.reportsWrapper.$('.reports-table-scroll .reports-table-item').length);
	});

	describe('check reports releases view', function() {
		beforeEach(function() {
			jasmine.Ajax.install();

			spyOn(this.reportsWrapper, 'renderReports').and.callThrough();

			this.reportsWrapper.getReportsCollectionData(this.reportsWrapper.releasesCollection, {withFetch: true});

			request = jasmine.Ajax.requests.mostRecent();
			request.response(TestResponses.distributorReports.releases.collection);
		});

		it('should be defined', function() {
			expect(this.reportsWrapper.currentView).toBeDefined();
		});

		it('reportsState should be releases', function() {
			expect(this.reportsWrapper.currentView.reportsState).toBe('releases');
		});

		it('data should be not empty', function() {
			expect(this.reportsWrapper.currentView.data.length).toBe(5);
		});

		it('click to release should call backbone trigger to render cinemas view', function() {
			var $firstRelease = this.reportsWrapper.currentView.$('.reports-table-body .reports-table-item').first(),
				firstReleaseId = $firstRelease.data('id');

			spyOn(Backbone, 'trigger');

			$firstRelease.trigger('click');

			expect(Backbone.trigger).toHaveBeenCalledWith('distributor:reports:switch:report:view', 'cinemas', firstReleaseId, true);
		});
	});

	describe('check reports cinemas view', function() {
		beforeEach(function() {
			jasmine.Ajax.install();

			spyOn(this.reportsWrapper, 'renderReports').and.callThrough();

			Backbone.trigger('distributor:reports:switch:report:view', 'cinemas', 1140, true);

			request = jasmine.Ajax.requests.mostRecent();
			request.response(TestResponses.distributorReports.cinemas.collection);
		});

		it('should be defined', function() {
			expect(this.reportsWrapper.currentView).toBeDefined();
		});

		it('reportsState should be releases', function() {
			expect(this.reportsWrapper.currentView.reportsState).toBe('cinemas');
		});

		it('data should be not empty', function() {
			expect(this.reportsWrapper.currentView.data.length).toBe(6);
		});

		it('click to cinema should call backbone trigger to render reports view', function() {
			var $firstCinema = this.reportsWrapper.currentView.$('.reports-table-body .reports-table-item').first(),
				firstCinemaId = $firstCinema.data('id');

			spyOn(Backbone, 'trigger');

			$firstCinema.trigger('click');

			expect(Backbone.trigger).toHaveBeenCalledWith('distributor:reports:switch:report:view', 'reports', firstCinemaId, true);
		});
	});

	describe('check reports reports view', function() {
		beforeEach(function() {
			this.cinemasCollection.release_id = 1140;
			jasmine.Ajax.install();

			spyOn(this.reportsWrapper, 'renderReports').and.callThrough();

			Backbone.trigger('distributor:reports:switch:report:view', 'reports', 749, true);

			request = jasmine.Ajax.requests.mostRecent();
			request.response(TestResponses.distributorReports.reports.collection);
		});

		it('should be defined', function() {
			expect(this.reportsWrapper.currentView).toBeDefined();
		});

		it('reportsState should be releases', function() {
			expect(this.reportsWrapper.currentView.reportsState).toBe('reports');
		});

		it('data should be not empty', function() {
			expect(_.keys(this.reportsWrapper.currentView.data).length).toBe(1);
		});

		it('should render cinema contacts by click to contacts tab', function() {
			this.reportsWrapper.currentCinema = 1140;

			jasmine.Ajax.install();

			spyOn(this.reportsWrapper, 'showContacts').and.callThrough();

			this.reportsWrapper.$('.sections-tabs-inner__item[data-tab="contacts"]').trigger('click');

			expect(this.reportsWrapper.showContacts).toHaveBeenCalledWith(1140);
		});

		it('should render reports reports view by click to reports tab without fetch', function() {
			this.reportsWrapper.currentCinema = 1140;

			jasmine.Ajax.install();

			spyOn(Backbone, 'trigger');

			this.reportsWrapper.$('.sections-tabs-inner__item[data-tab="reports"]').trigger('click');

			expect(Backbone.trigger).toHaveBeenCalledWith('distributor:reports:switch:report:view', 'reports', this.reportsWrapper.currentCinema, false);
		});
	});

	describe('check reports contacts view', function() {
		beforeEach(function() {
			this.reportsWrapper.currentCinema = 1140;
			jasmine.Ajax.install();
			this.reportsWrapper.showContacts(this.reportsWrapper.currentCinema);

			request = jasmine.Ajax.requests.mostRecent();
			request.response(TestResponses.distributorReports.contacts.success);
		});

		it('should be defined', function() {
			expect(this.reportsWrapper.contactsView).toBeDefined();
		});
	});
});

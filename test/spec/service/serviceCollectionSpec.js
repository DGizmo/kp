describe('Service collection', function() {
	'use strict';

	beforeEach(function() {
		this.collection = new window.app.ServiceCollection(JSON.parse(TestResponses.service.defaultCollection.responseText));
	});

	afterEach(function() {
		this.collection.stopListening();
	});

	it('should return halls array', function() {
		var cinema = { halls: [{id: 3636, formats: [], title: ''}] },
			halls  = this.collection.getHalls(cinema);

		expect(halls[0].projectors.length + halls[0].playservers.length + halls[0].lamps.length).toBe(3);
	});

	it('should return archived lamps', function() {
		var archivedLamps = this.collection.getArchivedLamps();
		expect(archivedLamps.length).toBe(1);
	});

});

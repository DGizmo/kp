describe('Main.js', function() {
	'use strict';

	describe('Date Utils', function() {

		it('should return correct dates 2015/2016', function() {
			expect(_.getWeekStartDate({week:52, year: 2015}).format('YYYY-MM-DD')).toEqual('2015-12-24');
			expect(_.getWeekStartDate({week:53, year: 2015}).format('YYYY-MM-DD')).toEqual('2015-12-31');
			expect(_.getWeekStartDate({week:53, year: 2015, type: 'date'}).format('{yyyy}-{MM}-{dd}')).toEqual('2015-12-31');

			expect(_.getWeekEndDate  ({week:53, year: 2015}).format('YYYY-MM-DD')).toEqual('2016-01-06');
			expect(_.getWeekEndDate  ({week:53, year: 2015, type: 'date'}).format('{yyyy}-{MM}-{dd}')).toEqual('2016-01-06');

			expect(_.getWeekNumber('2016-01-06')).toEqual(53);

			expect(_.map(_.toArray(_.getWeekDates({week: 53, year: 2015})), function(date) { return date.format('YYYY-MM-DD'); })).toEqual(['2015-12-31', '2016-01-06']);
		});

		it('should return correct dates 2016/2017', function() {
			expect(_.getWeekStartDate({week:52, year: 2016}).format('YYYY-MM-DD')).toEqual('2016-12-29');
			expect(_.getWeekStartDate({week:52, year: 2016, type: 'date'}).format('{yyyy}-{MM}-{dd}')).toEqual('2016-12-29');

			expect(_.getWeekEndDate  ({week:52, year: 2016}).format('YYYY-MM-DD')).toEqual('2017-01-04');
			expect(_.getWeekEndDate  ({week:52, year: 2016, type: 'date'}).format('{yyyy}-{MM}-{dd}')).toEqual('2017-01-04');

			expect(_.getWeekNumber('2017-01-04')).toEqual(52);

			expect(_.map(_.toArray(_.getWeekDates({week: 52, year: 2016})), function(date) { return date.format('YYYY-MM-DD'); })).toEqual(['2016-12-29', '2017-01-04']);
		});

		it('should correctly return window location host', function() {
			expect(_.getWindowLocationHost()).toBe(window.location.host);
		});

		it('should correctly format bytes', function() {
			var unitArr = [' б', ' Кб', ' Мб', ' Гб', ' Тб', ' Пб', ' Эб', ' Зб', ' Йб'],
				dangerousValues = [null, 'xyu', NaN, undefined];

			_.each(dangerousValues, function(value) {
				expect(function() {_.formatBytes(value);}).toThrow('Must be number.');
			});

			_.each(unitArr, function(unit, index) {
				expect(_.formatBytes(Math.pow(1024, index))).toBe(1 + unit);
			});
		});

	});

});

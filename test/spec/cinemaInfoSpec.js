describe('Cinemas Info', function() {
	'use strict';
	var request;

	beforeEach(function() {
		appendSetFixtures(sandbox({id:'cinemas'}));
		getDatFckTemplate('cinema_info_tmpl');

		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText), {parse: true});
		this.user.set({
			tx: 111,
			current: {
				cinema_id: 602
			}
		});

		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		jasmine.Ajax.install();

		this.cinemaInfoView = new window.app.CinemaInfoView();

		spyOn(this.cinemaInfoView.model, 'fetch').and.callThrough();
		request = jasmine.Ajax.requests.mostRecent();
	});

	afterEach(function() {
		this.user.stopListening();
		this.cinemaInfoView.remove();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	it('should be defined & parse full response correctly', function() {
		request.response(TestResponses.cinemainfo.success);

		expect(this.cinemaInfoView.$el).toContainElement('.cinema-info-item');

		expect(this.cinemaInfoView.model.get('cinema')).toBeNonEmptyObject();
		expect(this.cinemaInfoView.model.get('content')).toBeArrayOfObjects();
		expect(this.cinemaInfoView.model.get('kdm')).toBeArrayOfObjects();
	});

	it('should parse "wrong cinema" response correctly', function() {
		request.response(TestResponses.cinemainfo.wrongCinema);
		expect(this.cinemaInfoView).toBeDefined();
	});

	it('should parse response just with cinema correctly', function() {
		request.response(TestResponses.cinemainfo.onlyCinema);
		expect(this.cinemaInfoView.model.get('cinema')).toBeNonEmptyObject();
	});

	it('should parse empty response correctly', function() {
		request.response(TestResponses.cinemainfo.empty);

		expect(this.cinemaInfoView.model.get('cinema')).toBeEmptyObject();
		expect(this.cinemaInfoView.model.get('content')).toBeEmptyArray();
		expect(this.cinemaInfoView.model.get('kdm')).toBeEmptyArray();
	});

	it('should parse content without format & aspect', function() {
		request.response(TestResponses.cinemainfo.contentWithoutFormatAspect);
		expect(this.cinemaInfoView.model.get('content')).toBeArrayOfObjects();
	});

	it('should fetch model when cinema changed', function() {
		Backbone.trigger('cinema-changed');
		expect(this.cinemaInfoView.model.fetch).toHaveBeenCalled();
	});

});

describe('Week Schedule Export Popup', function() {
	'use strict';

	beforeEach(function() {
		getDatFckTemplate('export_week_schedule');
		appendSetFixtures(sandbox({id:'js-popup'}));

		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText));
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		this.exportWeekScheduleView = new window.app.ExportWeekScheduleView();

		$('#js-popup').html(this.exportWeekScheduleView.render().el);
	});

	afterEach(function() {
		this.user.stopListening();
		this.exportWeekScheduleView.remove();
		reqres.removeHandler('get:user');
	});

	it('should start download', function() {
		spyOn(this.exportWeekScheduleView, '_download');
		this.exportWeekScheduleView.$('.done').click();
		expect(this.exportWeekScheduleView._download).toHaveBeenCalled();
	});

});

describe('Week schedule model', function() {
	'use strict';
	var request;

	beforeEach(function() {
		jasmine.Ajax.install();

		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText));
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		this.model = new window.app.WeekSchedule();
		this.model.fetch();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.weekSchedule.success);
	});

	afterEach(function() {
		this.user.stopListening();
		this.model.stopListening();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	describe('Backbone events', function() {

		beforeEach(function() {
			spyOn(this.model, 'update').and.callThrough();
		});

		it('should update by weekschedule change date', function() {
			Backbone.trigger('weekschedule:changeDate');
			expect(this.model.update).toHaveBeenCalled();
		});

		it('should update by change current cinema', function() {
			Backbone.trigger('cinema-changed');
			expect(this.model.update).toHaveBeenCalled();
		});

		it('should update by socket schedule_week', function() {
			Backbone.trigger('socket:schedule_week', {cinema_id: 602});
			expect(this.model.update).toHaveBeenCalled();
		});

		it('should update by user change cinema settings', function() {
			spyOn(this.model, 'setCinemaParameters').and.callThrough();
			Backbone.trigger('user:cinema:settings:save');
			expect(this.model.setCinemaParameters).toHaveBeenCalled();
		});
	});

	it('should right set date params', function() {
		var model = this.model;

		model.setDateParameters(moment('30.03.2015', 'DD.MM.YYYY'));

		expect(model.get('currentWeek') + model.get('currentMonth') + model.get('currentYear')).toBe(2030);
		expect(model.get('date')._i).toBe('30.03.2015');
		expect(JSON.stringify(model.get('days_week')[6])).toBe(JSON.stringify({
			date:         '2015-04-01',
			month:        'апр',
			day_of_month: '1',
			day_of_week:  6
		}));
	});

	it('should update after set date params', function() {
		spyOn(this.model, 'update');
		this.model.setDateParameters(moment('30.03.2015', 'DD.MM.YYYY'));
		expect(this.model.update).toHaveBeenCalled();
	});

	it('should right set cinemas params', function() {
		this.model.setCinemaParameters();
		expect(this.model.get('schedule_rounding') + this.model.get('defaultCinemaAd')).toBe('ceil00');
	});

	it('should convert minutes to time', function() {
		expect(this.model.minutesToTime(1860) + this.model.minutesToTime(0)).toBe('07:0000:00');
	});

	it('should convert time to minutes', function() {
		expect(this.model.timeToMinutes('07:00') + this.model.timeToMinutes('00:00')).toBe(3300);
	});

	it('should update', function() {
		spyOn(this.model, 'fetch').and.callThrough();
		this.model.update();
		expect(this.model.fetch).toHaveBeenCalled();
	});

});

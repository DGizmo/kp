describe('Week schedule releases list collection', function() {
	'use strict';
	var request;

	beforeEach(function() {
		jasmine.Ajax.install();

		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText));
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		// reqres.request('get:weekschedule').set({
		// 	currentWeek: 9,
		// 	currentYear: 2015
		// });

		this.collection = new window.app.WeekScheduleReleasesListCollection();
		this.collection.fetch();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.weekScheduleReleasesList.success);
	});

	afterEach(function() {
		this.user.stopListening();
		this.collection.stopListening();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	describe('Backbone events', function() {

		beforeEach(function() {
			spyOn(this.collection, 'fetch').and.callThrough();
		});

		it('should update by weekschedule change date', function() {
			Backbone.trigger('weekschedule:changeDate');
			expect(this.collection.fetch).toHaveBeenCalled();
		});

		it('should update by change current cinema', function() {
			Backbone.trigger('cinema-changed');
			expect(this.collection.fetch).toHaveBeenCalled();
		});

		it('should update by saving technology', function() {
			Backbone.trigger('technology:save:done', {release_id: 9999});
			expect(this.collection.fetch).toHaveBeenCalled();
		});

		it('should update by saving schedule', function() {
			Backbone.trigger('schedule:save:done', {release_id: 9999});
			expect(this.collection.fetch).toHaveBeenCalled();
		});

		it('should update by saving cinemas settings', function() {
			Backbone.trigger('user:cinema:settings:save');
			expect(this.collection.fetch).toHaveBeenCalled();
		});

		it('should update by socket schedule:update', function() {
			Backbone.trigger('socket:schedule:update', {cinema_id: 602});
			expect(this.collection.fetch).toHaveBeenCalled();
		});

		it('should update if existing release change favourites', function() {
			Backbone.trigger('release:favourites', {id: 9999});
			expect(this.collection.fetch).toHaveBeenCalled();
		});

		it('should update if existing release change duration', function() {
			Backbone.trigger('release:duration:changed', {
				id:              9999,
				duration:        {full: 90, clean: 80, custom: 85},
				chosen_duration: 'custom'
			});
			expect(_.first(this.collection.models).get('duration')).toBe(85);
		});

		it('should update if existing release updates', function() {
			Backbone.trigger('socket:releases:update', {release_id:[9999]});
			expect(this.collection.fetch).toHaveBeenCalled();
		});
	});

	describe('Parse', function() {
		it('should right parse releases technology', function() {
			expect(this.collection.length).toBe(4);
		});

		it('should parse releases params', function() {
			var releasesParams = reqres.request('get:weekschedule:releasesParams');

			expect(releasesParams.length).toBe(2);
			expect(releasesParams[0].hasCopy.length + releasesParams[1].hasCopy.length).toBe(5);
			expect(releasesParams[0].hasKdm.length + releasesParams[1].hasKdm.length).toBe(9);
			expect(releasesParams[0].adParams[0].halls.length +
				releasesParams[0].adParams[0].title +
				releasesParams[0].adParams[0].duration +
				releasesParams[0].adParams[0].type
			).toBe('11Дивергент, глава 2: Инсургент151advertize');
		});

		it('should parse release identifiers', function() {
			expect(JSON.stringify(reqres.request('get:weekschedule').get('identifiers'))).toBe(JSON.stringify(['99992D', '99993D', '99993DATMOS']));
			expect(reqres.request('get:weekschedule').get('actualReleases').length).toBe(1);
		});

		it('should parse actual releases', function() {
			expect(JSON.stringify(reqres.request('get:weekschedule').get('actualReleases'))).toBe(JSON.stringify(['9999']));
		});
	});
});

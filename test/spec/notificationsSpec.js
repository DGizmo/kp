describe('Notifications', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('push_box');
		getDatFckTemplate('notify_item');

		jasmine.Ajax.install();

		this.user = new window.app.User();
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		this.notifications = new window.app.NotificationsView({user: this.user});

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.notifications.success);
	});

	afterEach(function() {
		this.user.stopListening();
		this.notifications.remove();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	it('should be defined', function() {
		expect(this.notifications).toBeDefined();
	});

});

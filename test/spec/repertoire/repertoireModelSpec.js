describe('Repertoire Model', function() {
	'use strict';
	var request;

	beforeEach(function() {
		this.model = new window.app.Repertoire(JSON.parse(TestResponses.repertoire.defaultModel.responseText));
	});

	afterEach(function() {
		this.model.stopListening();
	});

	it('should be defined', function() {
		expect(this.model).toBeDefined();
		expect(this.model.get('weeks').length).toBe(2);
	});

	it('should return ulr', function() {
		expect(this.model.url()).toEqual('/api/schedule/get/?cinema_id=999&release_id=999');
	});

	describe('Weeks', function() {

		it('should correctly adding weeks 2014/2015', function() {
			_.last(this.model.get('weeks')).year = 2014;
			_.last(this.model.get('weeks')).number = 52;

			this.model.addWeek();

			expect(_.last(this.model.get('weeks')).year).toEqual(2015);
			expect(_.last(this.model.get('weeks')).number).toEqual(1);

			this.model.addWeek();

			expect(_.last(this.model.get('weeks')).year).toEqual(2015);
			expect(_.last(this.model.get('weeks')).number).toEqual(2);
		});

		it('should correctly adding weeks 2015/2016', function() {
			_.last(this.model.get('weeks')).year = 2015;
			_.last(this.model.get('weeks')).number = 52;

			this.model.addWeek();

			expect(_.last(this.model.get('weeks')).year).toEqual(2015);
			expect(_.last(this.model.get('weeks')).number).toEqual(53);

			this.model.addWeek();

			expect(_.last(this.model.get('weeks')).year).toEqual(2016);
			expect(_.last(this.model.get('weeks')).number).toEqual(1);
		});

		it('should correctly adding weeks 2016/2017', function() {
			_.last(this.model.get('weeks')).year = 2016;
			_.last(this.model.get('weeks')).number = 52;

			this.model.addWeek();

			expect(_.last(this.model.get('weeks')).year).toEqual(2017);
			expect(_.last(this.model.get('weeks')).number).toEqual(1);

			this.model.addWeek();

			expect(_.last(this.model.get('weeks')).year).toEqual(2017);
			expect(_.last(this.model.get('weeks')).number).toEqual(2);
		});

	});

	it('should add days to last week', function() {
		var lastWeek = _.last(this.model.get('weeks'));
		lastWeek.days = [4, 5];
		this.model.addWeek();
		expect(lastWeek.days).toEqual([4, 5, 6, 7]);
	});

	it('should delete weeks', function() {
		this.model.deleteWeek(52, 2014);
		this.model.deleteWeek(51, 2014);
		expect(this.model.get('weeks').length).toBe(1);
	});

	it('shouldn"t delete last week', function() {
		this.model.deleteWeek(50, 2014);
		expect(this.model.get('weeks').length).toBe(1);
	});

	describe('add method', function() {

		it('should add formats', function() {
			this.model.changeFormat('2d', [], ['test'], 'add');
			expect(_.all(this.model.get('weeks'), function(week) { return week.technology.length === 3; })).toBe(true);
		});

		it('shouldn"t add without upgrades', function() {
			this.model.changeFormat('2d', [], [], 'add');
			expect(_.all(this.model.get('weeks'), function(week) { return week.technology.length === 2; })).toBe(true);
		});

		it('shouldn"t add same format', function() {
			this.model.changeFormat('2d', [], ['test'], 'add');
			this.model.changeFormat('2d', [], ['test'], 'add');
			expect(_.all(this.model.get('weeks'), function(week) { return week.technology.length === 3; })).toBe(true);
		});

	});

	describe('save method', function() {

		it('should save formats', function() {
			this.model.changeFormat('2d', ['dbox'], ['dbox', 'test'], 'save');
			expect(_.all(this.model.get('weeks'), function(week) { return week.technology.length === 2; })).toBe(true);

			var newUpgrade = _.uniq(_.map(_.pluck(this.model.get('weeks'), 'technology'), function(technology) {
				return technology[1].upgrades[1]; // should save format to each week.technology
			}));
			expect(newUpgrade).toEqual(['test']);
		});

		it('shouldn"t save same format', function() {
			this.model.changeFormat('2d', ['dbox'], ['dbox', 'test'], 'save');
			this.model.changeFormat('2d', [], ['dbox', 'test'], 'save');
			expect(_.all(this.model.get('weeks'), function(week) { return week.technology.length === 2; })).toBe(true);
		});

		it('shouldn"t save without upgrades', function() {
			this.model.changeFormat('2d', ['dbox'], [], 'save');
			expect(_.all(this.model.get('weeks'), function(week) { return week.technology.length === 2; })).toBe(true);
		});

	});

	describe('remove method', function() {

		it('should remove formats', function() {
			this.model.changeFormat('2d', ['dbox'], [], 'remove');
			expect(_.all(this.model.get('weeks'), function(week) { return week.technology.length === 1; })).toBe(true);
		});

		it('shouldn"t remove formats without upgrades', function() {
			this.model.changeFormat('2d', [], [], 'remove');
			expect(_.all(this.model.get('weeks'), function(week) { return week.technology.length === 2; })).toBe(true);
		});

		it('should update week.count after removing formats', function() {
			this.model.changeFormat('2d', ['dbox'], [], 'remove');
			expect(_.all(this.model.get('weeks'), function(week) { return week.count === 0; })).toBe(true);
		});

	});

	it('should save schedule and trigger event', function() {
		jasmine.Ajax.install();
		spyOn(Backbone, 'trigger').and.callThrough();

		this.model.save(999);

		request = jasmine.Ajax.requests.mostRecent();
		request.response({
			status: 200,
			responseText: '{"ok":1}'
		});
		jasmine.Ajax.uninstall();
		expect(Backbone.trigger).toHaveBeenCalledWith('schedule:save:done', { release_id: 999, color: 3, cinema_id: 999 });
	});

});

describe('CreateCalendar', function() {
	'use strict';

	beforeEach(function() {
		this.user = new window.app.User(JSON.parse(TestResponses.user.cinemas.responseText), {parse: true});
		this.user.set({
			tx: 111,
			current: {
				cinema_id: 602
			}
		});

		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});
	});

	afterEach(function() {
		this.user.stopListening();
		reqres.removeHandler('get:user');
	});

	describe('default calendar', function() {
		beforeEach(function() {
			this.calendar = new window.app.CreateCalendar();
		});

		afterEach(function() {
			this.calendar = undefined;
		});

		it('should be define current month by change date with month param', function() {
			this.calendar.changeDate(2015, 3);
			expect(this.calendar.current.month).toBeDefined();
		});

		it('weeks array should not empty and contain week number', function() {
			var firstMonthWeekNumber = moment().startOf('month').weeks();

			expect(this.calendar.weeks.length > 0).toBe(true);
			expect(this.calendar.weeks[0].value).toBe(firstMonthWeekNumber);
		});

		it('should correctly return days length of current month', function() {
			var monthDaysLength = moment().daysInMonth();

			expect(this.calendar.days.length).toBe(monthDaysLength);
		});

		it('days array first element should contain correct data', function() {
			var firstMomentDay = moment().startOf('month'),
				firstDay = {
					date: firstMomentDay.toDate(),
					month: firstMomentDay.format('MMMM').toLowerCase(),
					day_of_month: 1,
					day_of_week: firstMomentDay.day(),
					number_of_week: firstMomentDay.week(),
					start: firstMomentDay.day() === 4 ? true : false,
					start_time: [{
						hall: 1,
						date: '2015-06-01T07:30:00.000Z'
					}, {
						hall: 2,
						date: '2015-06-01T07:30:00.000Z'
					}, {
						hall: 3,
						date: '2015-06-01T07:30:00.000Z'
					}]
				};

			expect(JSON.stringify(this.calendar.days[0])).toBe(JSON.stringify(firstDay));
		});

		it('current object should contain correct data', function() {
			var currentMoment = moment(),
				current = {
					month: currentMoment.month(),
					year: currentMoment.year(),
					week: currentMoment.week()
				};

			expect(JSON.stringify(this.calendar.current)).toBe(JSON.stringify(current));
		});

		it('check object should contain correct data', function() {
			var currentMoment = moment(),
				check = {};

			if (this.calendar.days[0].number_of_week !== this.calendar.days[2].number_of_week) check.start = currentMoment.startOf('month').week();
			if (this.calendar.days[this.calendar.days.length - 1].number_of_week !== this.calendar.days[this.calendar.days.length - 3].number_of_week) check.end = currentMoment.endOf('month').week();

			expect(JSON.stringify(this.calendar.check)).toBe(JSON.stringify(check));
		});
	});

	describe('invoked with month weeks starts', function() {
		beforeEach(function() {
			this.calendar = new window.app.CreateCalendar({monthWeeksStarts: true});
		});

		afterEach(function() {
			this.calendar = undefined;
		});

		it('should be defined', function() {
			expect(this.calendar).toBeDefined();
		});
	});

	describe('invoked with date_param from booking', function() {
		beforeEach(function() {
			this.calendar = new window.app.CreateCalendar({date_param: Date.create('2015-02-02')});
		});

		afterEach(function() {
			this.calendar = undefined;
		});

		it('should be defined', function() {
			expect(this.calendar).toBeDefined();
		});
	});
});

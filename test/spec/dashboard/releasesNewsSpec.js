describe('Dashboard releases news view', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('dashboard_releases_news_wr');
		appendSetFixtures(sandbox({class:'dashboard_block'}));

		jasmine.Ajax.install();

		this.notifications = new window.app.Notifications();
		this.notifications.updated = true;
		this.notifications.fetch();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.notifications.success);

		this.releasesNewsView = new window.app.DashReleasesNewsWrapView({
			collection: this.notifications,
			el:         $('.dashboard_block')
		});
	});

	afterEach(function() {
		this.notifications = undefined;
		this.releasesNewsView.remove();
		this.releasesNewsView = undefined;
		jasmine.Ajax.uninstall();
	});

	it('should filter releases news from notifications', function() {
		expect(this.releasesNewsView.filterReleasesNews().length).toBe(4);
	});

	it('should be right rendered', function() {
		expect(this.releasesNewsView.$('.list .item').length).toBe(4);
	});

	it('should show all releases news drom sidebar', function() {
		spyOn(Backbone, 'trigger');
		this.releasesNewsView.$('.all').trigger('click');
		expect(Backbone.trigger).toHaveBeenCalledWith('open:notifications-sidebar', 'notifications', 'release');
	});

	it('should send to release card by click', function() {
		spyOn(Backbone, 'trigger');
		this.releasesNewsView.$('.item[data-release_id="3224"]').trigger('click');
		expect(Backbone.trigger).toHaveBeenCalledWith('router:navigate', '/release/3224');
	});
});

describe('Dashboard messages view', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('dashboard_messages_wr');
		appendSetFixtures(sandbox({class:'dashboard_block'}));

		jasmine.Ajax.install();

		this.messages = new window.app.Messages();
		this.messages.updated = true;
		this.messages.fetch();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.messages.success);

		this.messagesView = new window.app.DashMessagesWrapView({
			collection: this.messages,
			el:         $('.dashboard_block')
		});
	});

	afterEach(function() {
		this.messages = undefined;
		this.messagesView.remove();
		this.messagesView = undefined;
		jasmine.Ajax.uninstall();
	});

	it('should be right rendered', function() {
		expect(this.messagesView.$('.list .item').length).toBe(3);
	});

	it('should be able to open chat', function() {
		spyOn(Backbone, 'trigger');
		this.messagesView.$('.all').trigger('click');
		expect(Backbone.trigger).toHaveBeenCalledWith('open:notifications-sidebar', 'chat');
	});
});

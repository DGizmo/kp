describe('Dashboard premieres', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('dashboard_premieres_wr');

		jasmine.Ajax.install();

		this.user = new window.app.User();
		reqres.setHandlers({
				'get:user': {
				callback: function() {
					return this.user;
				},
				context: this
			}
		});

		this.premieres = new window.app.DashPremieresCollection();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.dashboard.premieresCollection);
	});

	afterEach(function() {
		this.user.stopListening();
		this.premieres.stopListening();
		reqres.removeHandler('get:user');
		jasmine.Ajax.uninstall();
	});

	describe('premieres collection', function() {
		it('should be right parsed', function() {
			expect(this.premieres.get(4516).get('key') && this.premieres.updated).toBe(true);
		});

		it('should updates by release:favourites', function() {
			spyOn(this.premieres, 'update').and.callThrough();

			this.premieres.week = 1;
			this.premieres.year = 1;

			Backbone.trigger('release:favourites', {week: 1, year: 1});

			expect(this.premieres.update).toHaveBeenCalled();
		});
	});

	describe('premieres view', function() {
		beforeEach(function() {
			appendSetFixtures(sandbox({class:'dashboard_block'}));

			this.premieres.updated = true;
			this.premieresView = new window.app.DashPremieresCollectionView({
				collection: this.premieres,
				el:         $('.dashboard_block')
			});
		});

		afterEach(function() {
			this.premieresView.remove();
		});

		it('should right rendered', function() {
			expect(this.premieresView.$('.line').length).toBe(1);
		});

		it('should open/close by click', function() {
			this.premieresView.$('.line:first').trigger('click');
			expect(this.premieresView.$('.line:first').hasClass('open')).toBe(true);
			this.premieresView.$('.line:first').trigger('click');
			expect(this.premieresView.$('.line:first').hasClass('open')).toBe(false);
		});
	});

});

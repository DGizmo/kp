describe('Dashboard kyes view', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('dashboard_kdm_news_wr');
		appendSetFixtures(sandbox({class:'dashboard_block'}));

		jasmine.Ajax.install();

		this.notifications = new window.app.Notifications();

		this.notifications.updated = true;
		this.notifications.fetch();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.notifications.success);

		this.keysView = new window.app.DashKeysNewsWrapView({
			collection: this.notifications,
			el:         $('.dashboard_block')
		});
	});

	afterEach(function() {
		this.notifications = undefined;
		this.keysView.remove();
		this.keysView = undefined;
		jasmine.Ajax.uninstall();
	});

	it('should filter kyes from notifications', function() {
		expect(this.keysView.filterKeysNews().length).toBe(4);
	});

	it('should be right rendered', function() {
		expect(this.keysView.$('.list .item').length).toBe(4);
	});

	it('should show all kdm news drom sidebar', function() {
		spyOn(Backbone, 'trigger');
		this.keysView.$('.all').trigger('click');
		expect(Backbone.trigger).toHaveBeenCalledWith('open:notifications-sidebar', 'notifications', 'kdm');
	});
});

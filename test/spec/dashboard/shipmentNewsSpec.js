describe('Dashboard shipment news view', function() {
	'use strict';
	var request;

	beforeEach(function() {
		getDatFckTemplate('dashboard_shipment_news_wr');
		appendSetFixtures(sandbox({class:'dashboard_block'}));

		jasmine.Ajax.install();

		this.collection = new window.app.Notifications();
		this.collection.updated = true;
		this.collection.fetch();

		request = jasmine.Ajax.requests.mostRecent();
		request.response(TestResponses.notifications.success);

		this.shipmentNewsView = new window.app.DashShipmentNewsWrapView({
			collection: this.collection,
			el:         $('.dashboard_block')
		});
	});

	afterEach(function() {
		this.collection.stopListening();
		this.shipmentNewsView.remove();
		jasmine.Ajax.uninstall();
	});

	it('should filter shipment news from notifications', function() {
		expect(this.shipmentNewsView.subViews.length).toBe(4);
	});

	it('should be right rendered', function() {
		expect(this.shipmentNewsView.$('.list .item').length).toBe(4);
	});

	it('should show all shipment news drom sidebar', function() {
		spyOn(Backbone, 'trigger');
		this.shipmentNewsView.showAllShipmentNews();
		expect(Backbone.trigger).toHaveBeenCalledWith('open:notifications-sidebar', 'notifications', 'shipment');
	});
});

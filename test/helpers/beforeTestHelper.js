(function() {
	'use strict';

	window._gaq = [];

	window.ReconnectingWebSocket = function() {
		return {};
	};

	jasmine.getFixtures().fixturesPath     = '/data/packed/templates';
	jasmine.getJSONFixtures().fixturesPath = '/test/json';

	window.getDatFckTemplate = function(template) {
		appendSetFixtures('<script type="text/template" id="' +
			template + '">' + readFixtures(template + '.html') +
			'</script>');
	};

	window.getDatFckJSON = function(json) {
		return JSON.stringify(window.getJSONFixture(json));
	};

	window.TestResponses = {
		user: {
			guest: {
				status: 200,
				responseText: getDatFckJSON('user/guest.json')
			},
			cinema: {
				status: 200,
				responseText: getDatFckJSON('user/cinema.json')
			},
			cinemas: {
				status: 200,
				responseText: getDatFckJSON('user/cinemas.json')
			},
			mechanic: {
				status: 200,
				responseText: getDatFckJSON('user/mechanic.json')
			},
			distributor: {
				status: 200,
				responseText: getDatFckJSON('user/distributor.json')
			},
			partner: {
				status: 200,
				responseText: getDatFckJSON('user/partner.json')
			}
		},
		history: {
			success: {
				status: 200,
				statusText: 'OK',
				responseText: getDatFckJSON('history.json')
			}
		},
		messages: {
			success: {
				status: 200,
				responseText: getDatFckJSON('messages.json')
			}
		},
		notifications: {
			success: {
				status: 200,
				responseText: getDatFckJSON('notifications.json')
			}
		},
		trailers: {
			success: {
				status: 200,
				responseText: getDatFckJSON('trailers.json')
			}
		},
		cinemainfo: {
			success: {
				status: 200,
				responseText: getDatFckJSON('cinemaInfo/cinemainfo.json')
			},
			wrongCinema: {
				status: 200,
				responseText: '{"status":"error","error":"You have no access for this cinema"}'
			},
			onlyCinema: {
				status: 200,
				responseText: getDatFckJSON('cinemaInfo/onlyCinema.json')
			},
			empty: {
				status: 200,
				responseText: '{"content":[],"kdm":[],"cinema":{}}'
			},
			contentWithoutFormatAspect: {
				status: 200,
				responseText: getDatFckJSON('cinemaInfo/contentWithoutFormatAspect.json')
			}
		},
		distributors: {
			success: {
				status: 200,
				responseText: getDatFckJSON('distributors.json')
			}
		},
		keys: {
			success: {
				status: 200,
				responseText: getDatFckJSON('keys.json')
			}
		},
		employee: {
			success: {
				status: 200,
				responseText: getDatFckJSON('employee.json')
			}
		},
		repertoire: {
			byCinema: {
				status: 200,
				responseText: getDatFckJSON('repertoire/byCinema.json')
			},
			defaultModel: {
				status: 200,
				responseText: getDatFckJSON('repertoire/defaultModel.json')
			},
			newWeeks: {
				responseText: getDatFckJSON('repertoire/newWeeks.json')
			},
			deletedWeeks: {
				responseText: getDatFckJSON('repertoire/deletedWeeks.json')
			}
		},
		weekSchedule: {
			success: {
				status: 200,
				responseText: getDatFckJSON('schedule/weekSchedule.json')
			}
		},
		weekScheduleReleasesList: {
			success: {
				status: 200,
				responseText: getDatFckJSON('schedule/weekScheduleReleasesList.json')
			}
		},
		analytics: {
			status: 200,
			responseText: getDatFckJSON('analytics.json')
		},
		releases: {
			defaultCollection: {
				status: 200,
				responseText: getDatFckJSON('releases/defaultCollection.json')
			},
			defaultModel: {
				status: 200,
				responseText: getDatFckJSON('releases/defaultModel.json')
			},
			distributorsList: {
				status: 200,
				responseText: getDatFckJSON('releases/distributorsList.json')
			},
			reportsCollection: {
				status: 200,
				responseText: getDatFckJSON('releases/releaseCardReportsCollection.json')
			}
		},
		dashboard: {
			premieresCollection: {
				status: 200,
				responseText: getDatFckJSON('dashboard/premieresCollection.json')
			}
		},
		advertising: {
			releases: {
				collection: {
					status: 200,
					responseText: getDatFckJSON('advertising/advertisingReleases.json')
				},
				defaultModel: {
					status: 200,
					responseText: getDatFckJSON('advertising/defaultReleaseModel.json')
				}
			},
			trailers: {
				collection: {
					status: 200,
					responseText: getDatFckJSON('advertising/advertisingTrailers.json')
				},
				defaultModel: {
					status: 200,
					responseText: getDatFckJSON('advertising/defaultTrailerModel.json')
				}
			}
		},
		distributorReports: {
			releases: {
				collection: {
					status: 200,
					responseText: getDatFckJSON('distributorReports/distributorReportsReleases.json')
				}
			},
			cinemas: {
				collection: {
					status: 200,
					responseText: getDatFckJSON('distributorReports/distributorReportsCinemas.json')
				}
			},
			reports: {
				collection: {
					status: 200,
					responseText: getDatFckJSON('distributorReports/distributorReportsReports.json')
				}
			},
			contacts: {
				success: {
					status: 200,
					responseText: getDatFckJSON('distributorReports/distributorReportsContacts.json')
				}
			}
		},
		distributorCinemas: {
			defaultCollection: {
				status: 200,
				responseText: getDatFckJSON('distributor_cinemas/distributorCinemas.json')
			}
		},
		service: {
			defaultCollection: {
				status: 200,
				responseText: getDatFckJSON('service/defaultCollection.json')
			},
			defaultModel: {
				status: 200,
				responseText: getDatFckJSON('service/defaultModel.json')
			}
		}
	};

})();

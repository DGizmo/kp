#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use App;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app = App->new(mode => $mode);

my $log = Mojo::Log->new(path => $app->home->rel_dir('script/booking/kdm.log'), level => 'error');

my $collection = $app->mongo->get_collection('request_record_queue');

for (;;) {
	my $queue = [ $collection->find({status => 'wait'}, {request_id => 1, distributor_id => 1, user_id => 1, full_name => 1, booking_email => 1, hall_id => 1, valid_till => 1, valid_since => 1, _id => 1})->all ];

	my $delay = Mojo::IOLoop->delay;

	for my $task (@$queue) {
		my $end = $delay->begin;
		my $request_record;

		my $task_id = $task->{_id};
		delete $task->{_id};

		$app->ua->post($app->config('copicus')->{kdm_url}."/add" => form => $task => sub {
			my ($ua, $tx) = @_;

			if ($tx->res->code == 200) {
				$request_record = $tx->res->json;

				if ($request_record->{status} eq 'ok') {
					$collection->update({_id => $task_id}, {'$set' => {status => 'ready', kdm_id => $request_record->{id}}});
				} elsif ($request_record->{error}->{code} == 112) {
					$collection->remove({_id => $task_id});
				} else {
					$collection->update({_id => $task_id}, {'$set' => {status => 'error', error => $request_record->{error}->{message}}});
				}

			} else {
				$log->error("Request_id: $task->{request_id}, hall_id: $task->{hall_id}, distributor_id: $task->{distributor_id}, task_id: $task_id. ERROR ".$tx->res->code);
				last;
			}

			$end->();
		});
	}

	$delay->wait;

	sleep 5 unless @$queue;
}
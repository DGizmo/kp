#!/usr/bin/env perl
use common::sense;
use FindBin;
use lib "$FindBin::Bin/../../lib";

use Data::Dumper;
use App;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app = App->new(mode => $mode);

my $loop = Mojo::IOLoop->singleton;

$loop->recurring(1 => sub {
	$app->redis->llen($app->config->{redis}->{queue} =>
		sub {
		  my ($redis, $res) = @_;
		  if (defined $res) {
		    if($res+0 > $app->config->{redis}->{alarm_size}) {
		    	$self->notice_to(
					type       => 'kp_releases_alarm',
					queue_str  => $app->config->{redis}->{queue}.' length: '.($res+0),
					template   => 'etc/queue_alarm',
				)
		    }
		  }
		  $loop->stop;
		}
	);
});

$loop->start;

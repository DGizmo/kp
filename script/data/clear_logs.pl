#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use App;
use Data::Dumper;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $where;
my $app = App->new(mode => $mode);

my $logs = [$app->mongo->get_collection('logs')->find()->all];
my $i;
for (@$logs) {
	if ($_->{start} < 1405684800 && ($_->{end} - $_->{start}) < 150) {
		my $where->{_id} = $_->{_id};
		$app->mongo->get_collection('logs')->remove($where);
	}
}


1;
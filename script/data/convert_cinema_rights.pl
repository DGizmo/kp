#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use App;
use Data::Dumper;
use Date::Calc qw/:all/;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production|release)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $where;
my $self = App->new(mode => $mode);

my $masters   = [ $self->mongo->get_collection('user')->find({'cinema_rights.master' => 1})->fields({ cinema_id => 1, cinema_rights => 1, cinema_settings => 1 })->all ];
my $all_users = [ $self->mongo->get_collection('user')->find({})->fields({ cinema_id => 1, cinema_rights => 1, cinema_settings => 1 })->all ];

my $cinemas;

for my $m (@$masters) {

	my $cids = [map {int $_->{cinema_id}} grep {$_->{master}} @{$m->{cinema_rights}}];
	push @$cinemas, @$cids;
}

for my $c (@$cinemas) {
	my $hash;
	my $m_user = [ grep { grep {$_->{cinema_id} == $c && $_->{master}} @{$_->{cinema_rights}} } @$masters ]->[0];

	my $cinema_users = [ grep { (grep {$_->{cinema_id}== $c && !$_->{master}} @{$_->{cinema_rights}}) } @$all_users ];
	
	for my $u (map {$_->{_id}->{value}} grep { scalar @{$_->{cinema_rights}} } @$cinema_users) {
		next unless my $level = [grep {$c == $_->{cinema_id}} @{[ grep {$_->{_id}->{value} eq $u} @$all_users]->[0]->{cinema_rights}}]->[0]->{level};
		$hash->{cinema_rights}->{$u}->{level} = $level;
	}

	$hash->{user_id}    = $m_user->{_id}->{value};
	$hash->{cinema_id}  = $c;
	my $cinema_settings = [ grep {$_->{cinema_id} == $c} @{$m_user->{cinema_settings}} ]->[0];

	if ($cinema_settings) {
		$hash->{cinema_settings} = $cinema_settings;
	} else {
		$hash->{cinema_settings}->{cinema_id} = int $c;
		$hash->{cinema_settings}->{$_} = $self->config->{default_cinema_sett}->{$_} for qw/defaultCinemaAd cinemaWeekendClose schedule_rounding cinemaWeekendOpen cinemaOpen cinemaClose/;
		
		my $halls = $self->mongo->get_collection('cinema')->find_one({ id => $c });

		for my $h (@{$halls->{halls}}) {
			my $tmp;
			$tmp->{id} = int $h->{id};
			$tmp->{$_} = $self->config->{default_cinema_sett}->{$_} for qw/defaultHallOpen defaultHallClose defaultHallPause/; 
			push @{$hash->{cinema_settings}->{halls}}, $tmp;
		}
	}

	$self->mongo->get_collection('cinema_rights')->insert($hash);
}

say 'Update is finished ';

#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use App;
use Data::Dumper;
use Date::Calc qw/:all/;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $where;
my $self = App->new(mode => $mode);

my $time = time;
my ($year_r,$month_r,$day_r) = Util::time2isodate($time) =~ /(\d{4})-(\d{2})-(\d{2})/;

my $week_r = $self->week_number(date => Util::time2isodate($time));
my $weeks; my $weeks_next_y; my $weeks_prev_y;
my $weeks_year;

for (my $i = $week_r-8; $i<= $week_r+12; $i++) {
	if ( $i > Weeks_in_Year($year_r) ){ 
		push @$weeks_next_y, $i - Weeks_in_Year($year_r); 
		$weeks_year->{$i - Weeks_in_Year($year_r)} = $year_r+1; 
	} elsif ($i < 1 ) {
		push @$weeks_prev_y, $i + Weeks_in_Year($year_r);
		$weeks_year->{$i + Weeks_in_Year($year_r-1)} = $year_r-1; 
	} else {
		push @$weeks, $i;
		$weeks_year->{$i} = $year_r;
	}
}

my $releases = [ $self->mongo->get_collection('release')->find({
	'hidden' => 0, 
	'date.year' => int $year_r, 
	'date.month' => {'$gte' => $month_r - 6}, 
	'date.month' => {'$lte' => $month_r + 4} })->sort({'date.russia.start' => -1})->all ];

my $releases_next_y = [ $self->mongo->get_collection('release')->find({
	'hidden' => 0, 
	'date.year' => int $year_r+1, 
	'date.week' => {'$in' => $weeks_next_y } })->sort({'date.russia.start' => -1})->all ] if $weeks_next_y;

my $releases_prev_y = [ $self->mongo->get_collection('release')->find({
	'hidden' => 0, 
	'date.year' => int $year_r-1, 
	'date.week' => {'$in' => $weeks_prev_y } 
})->sort({'date.russia.start' => -1})->all ] if $weeks_prev_y;

push @$releases, @$releases_next_y, @$releases_prev_y;

my $ww;
push @$ww, @$weeks, @$weeks_next_y, @$weeks_prev_y;

my $list;
for my $r (@$releases) {
	my $schedule = [ $self->mongo->get_collection('schedule')->find({ release_id => $r->{id} })->all ];
	next unless scalar @$schedule > 0;
	
	for my $w_num (@$ww) {
		my $tmp;
		for my $s (@$schedule) {
			my $w = [ grep { $_->{number} == $w_num && $_->{year} ~~[$year_r, $year_r+1, $year_r-1]} @{$s->{weeks}} ]->[0];
			next unless $w->{count};
			$tmp->{count} += int $w->{count};
		}

		$tmp->{week} = int $w_num;
		$tmp->{year} = int $weeks_year->{$w_num};
		$tmp->{count} = 0 unless $tmp->{count};
		push @{$r->{weeks}}, $tmp;
	}
	$r = +{map { $_ => $r->{$_} } qw/date title distributors id weeks/};
	push @{$list->{list}}, $r;
}

my $count_week;
for my $i (@{$list->{list}}) {
	my $count;
	for (@{$i->{weeks}}){
		$count += $_->{count};
	}

	$i->{all_count} = $count;
}

$list->{list} = [grep {$_->{all_count}} @{$list->{list}}];

for my $i (@{$list->{list}}) {
	for my $w (sort {$a <=> $b} @$ww) {
		$count_week->{$w} += [grep {$_->{week} == $w && $_->{count}} @{$i->{weeks}}]->[0]->{count};
	}
}

$self->mongo->get_collection('analyze')->remove({ updated => {'$lt' => time - 604800*2} } ) if $list;
$self->mongo->get_collection('analyze')->insert({ %$list , updated => time } ) if $list;


say 'Update is finished ';

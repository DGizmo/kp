#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use App;
use Data::Dumper;
use Date::Calc qw/:all/;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $self = App->new(mode => $mode);

my $users = [$self->mongo->get_collection('user')->find()->fields({ first_name => 1, last_name => 1, email => 1 })->all];
my $logs = [$self->mongo->get_collection('logs')->find({os => {'$in' => [ 'ios', 'iOS' , 'Android'] }, end => {'$exists' => 1} })->sort({end => -1})->all];

my $logs_last_week = [ grep { time - $_->{end} < 604800 } @$logs ];
my $id_users_last_week = [ map {$_->{user_id}} @$logs_last_week ];

my $last_users = [ grep {$_->{_id}->{value} ~~ @$id_users_last_week} @$users ];

for my $us (@$last_users){
	my $platform = [map {$_->{os}} grep {$_->{user_id} eq $us->{_id}->{value}} @$logs_last_week]->[0];
	my $count_enter = scalar grep {$_->{user_id} eq $us->{_id}->{value}} @$logs_last_week;

	say join( ",", ($us->{first_name}." ".$us->{last_name}, $us->{email}, $platform, $count_enter, $us->{type}));
}

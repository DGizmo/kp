#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use App;
use Data::Dumper;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $where;
my $app = App->new(mode => $mode);

my $users   = [$app->mongo->get_collection('user')->find({type => {'$in' => ['cinema', 'mechanic']}})->all];
my $cinemas = [$app->mongo->get_collection('cinema')->find()->all];
my $default_settings = {
	"defaultCinemaAd"    => "00",
	"cinemaWeekendClose" => "02:00",
	"schedule_rounding"  => "ceil",
	"cinemaWeekendOpen"  => "10:00",
	"cinemaOpen"   	     => "10:00",
	"cinemaClose"  	     => "02:00",
	"defaultHallOpen"  	 => "10:00",
	"defaultHallClose" 	 => "01:00",
	"defaultHallPause" 	 => 10
};

warn 'Start script';
for my $u (@$users) {

	for my $c (@{$u->{cinema_id}}) {
		my $sett = [grep {$c == $_->{cinema_id}} @{$u->{cinema_settings}}]->[0];

		if ($sett) {

				for (qw/defaultCinemaAd cinemaWeekendClose schedule_rounding cinemaWeekendOpen cinemaOpen cinemaClose/) {
					$sett->{$_} = $default_settings->{$_} unless $sett->{$_};
				}

				for my $h (@{$sett->{halls}}) {
					for (qw/defaultHallOpen defaultHallClose defaultHallPause/) {
						$h->{$_} = $default_settings->{$_} unless $h->{$_};
					}
				}

		} else {
			my $tmp_set;
			$tmp_set->{cinema_id} = int $c;
			$tmp_set->{$_} = $default_settings->{$_} for qw/defaultCinemaAd cinemaWeekendClose schedule_rounding cinemaWeekendOpen cinemaOpen cinemaClose/;
			
			for my $hid (map {$_->{id}} @{[grep { $_->{id} == $c } @$cinemas]->[0]->{halls}}) {
				my $tmp_h;
				$tmp_h->{id} = int $hid;
				$tmp_h->{$_} = $default_settings->{$_} for qw/defaultHallOpen defaultHallClose defaultHallPause/;
				push @{$tmp_set->{halls}}, $tmp_h;
			}
			push @{$u->{cinema_settings}}, $tmp_set;
		}
	}

	$app->mongo->get_collection('user')->update(
		{ '_id' => $u->{_id} },
		{'$set' => { 'cinema_settings' => $u->{cinema_settings}}}
	);
}

warn 'Script is down!';

1;
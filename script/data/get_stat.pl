#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use App;
use Data::Dumper;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app = App->new(mode => $mode);

my $cinema = $app->mongo->get_collection('cinema');
my $stat   = $app->mongo->get_collection('stat');

my ($stat_data,$items);

my $time = '00:00:00';

# bring all halls in array ang get countries list

for ($cinema->find->all) {

	for my $hall (@{$_->{halls}}) {
		my $hall_info;

		$hall->{support}             =~ s{\.}{ }sg;
		$_->{cinema_network}->{name} =~ s{\.}{ }sg;

		$hall_info->{cinema_id          } = $_->{id};
		$hall_info->{cinema_title       } = $_->{title}->{ru};
		$hall_info->{city_title_ru      } = $_->{city}->{title}->{ru};
		$hall_info->{city_title_en      } = $_->{city}->{title}->{en};
		$hall_info->{city               } = $_->{city}->{id};
		$hall_info->{country_title_ru   } = $_->{country}->{title}->{ru};
		$hall_info->{country_title_en   } = $_->{country}->{title}->{en};
		$hall_info->{country_id         } = $_->{country}->{id};
		$hall_info->{server             } = $hall->{server}->{type};
		$hall_info->{server_serial      } = $hall->{server}->{serial};
		$hall_info->{installed          } = $hall->{installed};
		$hall_info->{title              } = $hall->{title};
		$hall_info->{capasity           } = $hall->{capasity};
		$hall_info->{projector_model    } = $hall->{projector}->{model};
		$hall_info->{projector_make     } = $hall->{projector}->{brand};
		$hall_info->{integrator         } = $hall->{support},
		$hall_info->{type3d             } = $hall->{type3d},
		$hall_info->{owner              } = $_->{cinema_network}->{name},
		$hall_info->{dbox               } = $hall->{type_dbox},
		$hall_info->{population         } = $_->{city}->{population},
		$hall_info->{cinema_network     } = $hall->{cinema_network}->{name}, 
		$hall_info->{cinemas_count      } = $cinema->count({"city.id" => int $_->{city}->{id}}),
		$hall_info->{installed_timestamp} = Util::iso2time(join " ", ($hall->{installed},$time));
		push @$items, $hall_info;
	}

	$stat_data->{country}{$_->{country}->{id}}->{halls} += ($#{$_->{halls}}+1);
	$stat_data->{country}{$_->{country}->{id}}->{name}   = $_->{country}->{title}->{ru};

}

$stat->update(
	{ type => 'country', item_id => 'all'}, 
	{ '$set'   => { data => $stat_data->{country}, total => int $#$items+1 }},
	{ 'upsert' => 1 }
);

my @country_ids = keys $stat_data->{country};
push @country_ids, 'all';

# halls stat info

for my $t (qw{server projector_make dbox projector_model type3d integrator city owner}) {
	for my $c (@country_ids) {
		my $halls_list = [ grep {$c eq 'all' ? $_->{country_id} > 0 : $_->{country_id} eq $c} @$items ];

		for (@$halls_list) {
			next if $t eq 'city' && $_->{city} eq 446; # XXX remove 'Москва-дистрибьюторы' from statistic

			$stat_data->{$t}->{$c}->{$_->{$t} || 'Без названия'}->{halls} += 1;

			$stat_data->{$t}->{$c}->{$_->{$t} || 'Без названия'}->{name}   = $_->{owner} if $t eq 'owner';

			if ($t eq 'city') {
				$stat_data->{$t}->{$c}->{$_->{$t} || 'Без названия'}->{cinemas}    = $_->{cinemas_count};
				$stat_data->{$t}->{$c}->{$_->{$t} || 'Без названия'}->{name}       = $_->{city_title_ru};
				$stat_data->{$t}->{$c}->{$_->{$t} || 'Без названия'}->{population} = $_->{population};
			}
		}
		
		$stat->update(
			{ type => $t, item_id => $c }, 
			{ '$set'   => { data => $stat_data->{$t}->{$c}, total => int $#$halls_list+1 }},
			{ 'upsert' => 1 }
		);
	}
}

# halls selection by params

for my $type (
	[country          => 'countries'   => 'country_id'],
	[city             => 'cities'      => 'city'],
	[server           => 'servers'     => 'server'],
	[projector_model  => 'projectors'  => 'projector_model'],
	[integrator       => 'integrators' => 'integrator'],
	[type3d           => 'type3ds'     => 'type3d'],
) {
	my @ids = $type->[0] eq 'country' ? keys $stat_data->{$type->[0]} : keys $stat_data->{$type->[0]}->{all};
	push @ids, 'all' if $type->[0] eq 'country';

	for my $item (@ids) {
		my $j = 0;

		for ($item eq 'all' ? @$items : grep {$_->{$type->[2]} eq $item} @$items) {
			$stat_data->{$type->[1]}->{$item}->{halls}[$j] = $_;
			$j++;
		}

		$stat->update(
			{ type => $type->[1], item_id => $item }, 
			{ '$set'   => { data => $stat_data->{$type->[1]}->{$item} }},
			{ 'upsert' => 1 }
		);
	}
}

# period graphs 

my $type3d = [ splice [ sort {$b->{halls} <=> $a->{halls}} map { {halls => $stat_data->{type3d}->{all}->{$_}->{halls}, id => $_} } grep {$_ ne 'Без названия'} keys $stat_data->{type3d}->{all} ], 0, 6 ];
my $owner  = [ splice [ sort {$b->{halls} <=> $a->{halls}} map { {halls => $stat_data->{owner}->{all}->{$_}->{halls}, id => $stat_data->{owner}->{all}->{$_}->{name}} } grep {$stat_data->{owner}->{all}->{$_}->{name} ne 'Без названия'} keys $stat_data->{owner}->{all} ], 0, 6 ];

my $last_installed = [ sort { $b->{installed_timestamp} <=> $a->{installed_timestamp} } @$items ]->[0];

my @period;

for my $year (2008..[ $last_installed->{installed} =~ m{(\d{4})}s ]->[0]) {
	push @period, ["$year-${_}-01" => Util::iso2time("$year-${_}-01 $time")] for qw(01 04 07 10);
}

my @cinemas_count;

for my $id (@country_ids) {

	my ($n,$dates);

	# type3d and owner graphs

	for my $r (
		[type3d => 'type3d_graph' => $type3d],
		[owner  => 'owner_graph'  => $owner],
	) {

		my $data_count;

		for my $s (@{$r->[2]}) {
			my $m;

			for my $p (@period) {
				$data_count->{$p->[0]}->{$s->{id}}->{$id} += 1 for grep {$_->{$r->[0]} eq $s->{id} && $_->{installed_timestamp} <= $p->[1] && $_->{installed} && ($id eq 'all' ? $_->{country_id} > 0 : $_->{country_id} eq $id)} @$items;

				if (ref $data_count->{$p->[0]} eq 'HASH') {

					$stat_data->{graph}->{$r->[0]}->{$s->{id}}->{$id}[$m] = {
						period => $p->[0],
						items  => $data_count->{$p->[0]}->{$s->{id}}->{$id},
					};
					push @$dates,$p->[0];

					$m++;
				}
			}

			$stat->update(
				{ type => $r->[1], country_id => $id, item_id => $s->{id}, count => $m }, 
				{ '$set'   => { data => $stat_data->{graph}->{$r->[0]}->{$s->{id}}->{$id} }},
				{ 'upsert' => 1 }
			);
		}

		$stat->update(
			{ type => $r->[1], country_id => $id, item_id => 'period' }, 
			{ '$set'   => { data => [ map {my $v = $_; $v =~ s{(\d{4})-\d{2}-\d{2}}{$1}sg; $v; } sort {$a <=> $b} keys %{{ map { $_ => 1 } @$dates }} ] }},
			{ 'upsert' => 1 }
		);
	}

	# servers and projectors graph

	for my $t (
		[server         => 'servers_graph'],
		[projector_make => 'projectors_graph'],
	) {

		my $data_count;

		for my $s (keys $stat_data->{$t->[0]}->{all}) {
			my $m;

			for my $p (@period) {
				$data_count->{$p->[0]}->{$s}->{$id} += 1 for grep {$_->{$t->[0]} eq $s && $_->{installed_timestamp} <= $p->[1] && $_->{installed} && ($id eq 'all' ? $_->{country_id} > 0 : $_->{country_id} eq $id)} @$items;

				if (ref $data_count->{$p->[0]} eq 'HASH') {

					$stat_data->{graph}->{$t->[0]}->{$s}->{$id}[$m] = {
						period => $p->[0],
						items  => $data_count->{$p->[0]}->{$s}->{$id},
					};
					push @$dates,$p->[0];

					$m++;
				}
			}

			$stat->update(
				{ type => $t->[1], country_id => $id, item_id => $s, count => $m }, 
				{ '$set'   => { data => $stat_data->{graph}->{$t->[0]}->{$s}->{$id} }},
				{ 'upsert' => 1 }
			);
		}

		$stat->update(
			{ type => $t->[1], country_id => $id, item_id => 'period' }, 
			{ '$set'   => { data => [ map {my $v = $_; $v =~ s{(\d{4})-\d{2}-\d{2}}{$1}sg; $v; } sort {$a <=> $b} keys %{{ map { $_ => 1 } @$dates }} ] }},
			{ 'upsert' => 1 }
		);

	}

	for my $period (@period) {
		
		my ($halls_count, $cinemas, $count_by_period, $start_period);
		
		# halls and cinemas by years graph
		
		for (grep {$_->{installed_timestamp} <= $period->[1] && $_->{installed} && ($id eq 'all' ? $_->{country_id} > 0 : $_->{country_id} eq $id)} @$items) {
			$halls_count++;
			$cinemas->{$period}->{$_->{cinema_title}} += 1; 
		}
		
		# halls by periods graph
		
		if ($period->[0] =~ m{-01-\d{2}}s) {
			$start_period = Util::iso2time(([ $period->[0] =~ m{(\d{4})}s ]->[0]) - 1 . "-10-01");
		} elsif($period->[0] =~ m{-04-\d{2}}s) {
			$start_period = Util::iso2time(([ $period->[0] =~ m{(\d{4})}s ]->[0]) . "-01-01");
		} elsif($period->[0] =~ m{-07-\d{2}}s) {
			$start_period = Util::iso2time(([ $period->[0] =~ m{(\d{4})}s ]->[0]) . "-04-01");
		} elsif($period->[0] =~ m{-10-\d{2}}s) {
			$start_period = Util::iso2time(([ $period->[0] =~ m{(\d{4})}s ]->[0]) . "-07-01");
		}
		
		$count_by_period++ for grep {$_->{installed_timestamp} <= $period->[1] && $_->{installed_timestamp} > $start_period && $_->{installed} && ($id eq 'all' ? $_->{country_id} > 0 : $_->{country_id} eq $id)} @$items;
		
		if (ref $cinemas->{$period} eq 'HASH') {
			@cinemas_count = keys $cinemas->{$period};
			
			$stat_data->{graph}->{cinemas_and_halls}->{$id}[$n] = {
				period  => $period->[0],
				halls   => $halls_count,
				cinemas => $#cinemas_count+1,
			};
			
			$stat_data->{graph}->{halls_by_period}->{$id}[$n] = {
				period => $period->[0],
				halls  => $count_by_period,
			};
			
			$n++;
		}
	}
}

for my $graph (qw(cinemas_and_halls halls_by_period)) {
	$stat->update(
		{ type => $graph, item_id => $_ }, 
		{ '$set'   => { data => $stat_data->{graph}->{$graph}->{$_} }},
		{ 'upsert' => 1 }
	) for @country_ids;
}

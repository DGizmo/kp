#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use Data::Dumper;
use App;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app = App->new(mode => $mode);

my $schedule = [$app->mongo->get_collection('schedule')->find({'date.start' => {'$type' => 10} })->all];

my $rids = [ map { int $_->{release_id} } @$schedule];

my $releases = [$app->mongo->get_collection('release')->find({ id => {'$in' => $rids} })->all];

for my $item (@$schedule) {
	next if ($item->{date}->{start} eq [grep { $_->{id} == $item->{release_id} } @$releases]->[0]->{date}->{russia}->{start} );
	
	$app->mongo->get_collection('schedule')->update({ 'release_id' => int $item->{release_id} }, 
		{'$set' => {'date.start' => [grep { $_->{id} == $item->{release_id} } @$releases]->[0]->{date}->{russia}->{start}}},
		{ multiple => 1}
	);
}
#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Data::Dumper;
use App;

chdir "$FindBin::Bin/../.."; 

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app = App->new(mode => $mode);

my $list = $app->dw->user([ $app->mongo->get_collection('user')->find({'cinema_id' => 136})->all ]);
warn Dumper @$list;

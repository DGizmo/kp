#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use App;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app = App->new(mode => $mode);


my $user = $app->mongo->get_collection('user');

my $one_hall_cinemas  = [ map { int $_->{id} } $app->mongo->get_collection('cinema')->find({ halls => {'$size' => 1} })->fields({ id => 1 })->all ];
my $noadmin = { '$or' => [ { admin => {'$exists' => 0} }, { admin => 0 } ] };

my $user_with_cinema = { type => { '$in' => ['cinema', 'mechanic'] }, cinema_id => { '$exists' => 1 }, %$noadmin };

my $cnt_user_cinema;

for ($user->find($user_with_cinema)->fields({cinema_id => 1})->all) {
	$cnt_user_cinema->{ scalar @{$_->{cinema_id}} }++;
}

my $data = {
	user => {
		all => int $user->count($noadmin),
		approved => (int $user->count({%$noadmin, approved => 1}) || 0),
		(map { $_ => {
			all      => int $user->count({ type => $_, %$noadmin }) || 0,
			approved => int $user->count({ type => $_, %$noadmin, approved => 1}) || 0
		} } qw{cinema distributor partner guest mechanic}),
	},
	user_cinema => {
		only_one       => int $user->count({ %$user_with_cinema, cinema_id => { '$size' => 1 }, %$noadmin }),
		more_than_one  => int $user->count({ %$user_with_cinema, '$where'  => 'this.cinema_id.length>1', %$noadmin }),
		have_one_hall  => int $user->count({ %$user_with_cinema, cinema_id => { '$in' => $one_hall_cinemas } }),
		have_many_hall => int $user->count({ %$user_with_cinema, cinema_id => { '$not' => {'$in' => $one_hall_cinemas} } })
	},
	cnt_user_cinema => $cnt_user_cinema
};

$app->mongo->get_collection('statistics')->update(
	{ type   => 'user' },
	{ '$set' => { data   => $data  }},
	{ upsert => 1      }
);



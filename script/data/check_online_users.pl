#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use App;
use Mojo::Log;
use Data::Dumper;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $self = App->new(mode => $mode); $self->log(Mojo::Log->new('path' => 'log/check_online.log'));
my $time = time;

my $users = [ $self->mongo->get_collection('user')->find({actived => {'$gt' => 0}})->all ];

for my $user (@$users) {

	if ($time - $user->{actived} > $self->config->{user}->{active}) {

		my $users_logs = [ $self->mongo->get_collection('logs')->find({
			'$or' => [
				{ 'end' => { '$exists' => 0 } },
				{ 'end' => 0                  },
			],
			'user_id' => $user->{_id}->to_string,
        	})->all ];

		for my $log (@$users_logs) {
			$self->log->info("End session for user $log->{user_id}");
			$self->mongo->get_collection('logs')->update(
				{ _id    => $log->{_id} },
				{ '$set' => { end => $user->{actived} }},
			);
		}
	} 
}

1;

#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use App;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app = App->new(mode => $mode);

my $max_time = time - 60 * 60 * 24 * 7;

for my $kdm ($app->mongo->get_collection('kdm')->find({valid_till_time => {'$exists' => 0}})->all) {
	my $vt = Util::iso2time($kdm->{valid_till});
	
	$app->mongo->get_collection('kdm')->update({_id => $kdm->{_id}}, {'$set' => {valid_till_time => $vt}});
}

$app->mongo->get_collection('kdm')->remove({valid_till_time => { '$lt' => $max_time }}, {multi => 1});
#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use App;
use Data::Dumper;
use Date::Calc qw/:all/;
use DateTime qw/:all/;
use List::MoreUtils qw/uniq/;

my $db = Util->db(do 'conf/mysql.conf');

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $self = App->new(mode => $mode);

my $today = DateTime->now()->add(days => 1);
my $week  = $self->week_number(date => $today->ymd);
my $year  = $today->year;

my $yandex = $db->select(
	'select cinema.title, cinema.id as cinema_id, session.session_count as count,
	event.title, event.id as release_id, session.session_date as date from cinema
	inner join session on cinema.id = session.cinema_id inner join event
	on session.event_id = event.id where
	session.session_date = DATE_ADD(CURDATE(), INTERVAL 1 DAY)'
);

my $cinemas_id = [ uniq sort map { int $_->{cinema_id} } @$yandex ];
my $cinemas_y  = [
	$self->mongo->get_collection('cinema')
	->find({ ext_id => {'$in' => $cinemas_id} })
	->all
];

my $chy;
for my $c (@$cinemas_y ) {
	$chy->{$c->{ext_id}} = scalar @{$c->{halls}} if scalar @{$c->{halls}};
}

my $schedule = [
	$self->mongo->get_collection('schedule')
	->find({ 'weeks.number' => int $week, 'weeks.year' => int $year })
	->all
];

my $rids     = [ uniq grep { $_ < 10000000 } map { int $_->{release_id} } @$schedule ];
my $releases = [ $self->mongo->get_collection('release')->find({ id => {'$in' => $rids} })->all ];

my $rids_y     = [ uniq map { int $_->{release_id} } @$yandex ];
my $releases_y = [ $self->mongo->get_collection('release')->find({ 'ext_id' => {'$in' => $rids_y} })->all ];

my $release_yandex;
for my $r (@$releases_y) {
	for (@{ $r->{ext_id} || [] }) {
		$release_yandex->{$_} = $r;
	}
}

my $yandex_list;

for my $y (@$yandex) {
	my $rel = $release_yandex->{$y->{release_id}};
	next unless $rel->{id};

	$yandex_list->{$rel->{id}}->{title}      = $y->{title};
	$yandex_list->{$rel->{id}}->{release_id} = int $rel->{id};
	$yandex_list->{$rel->{id}}->{duration}   = $rel->{duration}->{full} || $rel->{duration}->{clean} || 100;
	$yandex_list->{$rel->{id}}->{date}       = $rel->{date}->{russia}->{start} || $y->{date};
	$yandex_list->{$rel->{id}}->{count}->{($chy->{$y->{cinema_id}} >= 5 ? '5+' : $chy->{$y->{cinema_id}}) || 0} += $y->{count} if $y->{count};

}

$yandex_list = [ values %$yandex_list ];

my $analyze_kp;

my $schedule_cinema_id = [ uniq sort map { int $_->{cinema_id} } @$schedule ];
my $schedule_cinema    = [
	$self->mongo->get_collection('cinema')
	->find({ id => {'$in' => $schedule_cinema_id} })
	->all
];

my $ch;
for my $c (@$schedule_cinema) {
	$ch->{$c->{id}} = scalar @{$c->{halls}};
}

for my $r (@$rids) {
	my $schedule_rel = [ grep { $_->{release_id} == $r } @$schedule ];

	my $tmp;

	for my $s (@$schedule_rel) {
		for my $w (@{$s->{weeks}}) {
			next unless $w->{number} == $week && $w->{year} == $year;

			$w->{count}
				? $tmp->{count}->{ $ch->{$s->{cinema_id}} >= 5 ? '5+' : $ch->{$s->{cinema_id}} } += $w->{count}
				: {}
			;
		}
	}

	if ($tmp->{count} ) {
		my $rel = [grep { $_->{id} == $r } @$releases]->[0];

		$tmp->{release_id} = $r;
		$tmp->{duration}   = $rel->{duration}->{full} || $rel->{duration}->{clean} || 100;

		$tmp->{date} = @$schedule_rel->[-1]->{date}->{start} || $rel->{date}->{russia}->{start};

		$tmp->{title} = $rel->{title}->{ru};
		$tmp->{ext_id} = [ map { my $i = $_; map { $i->{$_} } qw/ext_id/ } grep { $_->{id} == $r } @$releases ]->[0];

		$analyze_kp->{$r} = $tmp;
	}
}

for my $item (@$yandex_list) {
	my $search = $analyze_kp->{$item->{release_id}};

	if ($search->{release_id}) {
		$item->{$_} = $search->{$_} for keys %$item;
	}

}

$self->mongo->get_collection('analyze_y')->remove({ created => {'$lt' => time - 604800 } });

$self->mongo->get_collection('analyze_y')->update(
	{
		date => $today->ymd,
	},
	{
		'$set' => {
			list    => $yandex_list,
			created => time,
		},
	},
	{
		upsert => 1,
	},
);

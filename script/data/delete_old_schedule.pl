#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use Util;
use Data::Dumper;
use App;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app = App->new(mode => $mode);
my $count = 0;
for my $item ($app->mongo->get_collection('schedule')->find()->all) {

	my $fav = $app->mongo->get_collection('favourite')->find_one({user_id => $item->{user_id}, cinema_id => $item->{cinema_id}, release_id => $item->{release_id}, value => 1});

	unless ($fav) {$app->mongo->get_collection('schedule')->remove({user_id => $item->{user_id}, cinema_id => $item->{cinema_id}, release_id => $item->{release_id}});}
	# warn Dumper $fav;
}

# $app->mongo->get_collection('kdm')->remove({valid_till_time => { '$lt' => $max_time }}, {multi => 1});
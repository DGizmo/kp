#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use Util;
use common::sense;
use App;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app = App->new(mode => $mode);

my $iso_date = Util::time2iso();

my $ids = [map { $_->{id} } $app->mongo->get_collection('release')->find({change_count => { '$gte' => 5 }})->fields({id => 1})->all];

$app->mongo->get_collection('release')->update(
	{ id       => {'$in' => $ids}       },
	{ '$set'   => { change_count => 0 } },
	{ multiple => 1                     }
);

my $stat;

for my $s ($app->mongo->get_collection('schedule')->find({release_id => {'$in' => $ids}})->fields({weeks => 1, release_id => 1})->all) {

	$stat->{$s->{release_id}}->{"$_->{year}_$_->{number}"} =+ $_->{count} for @{$s->{weeks}};

}

for my $release_id (keys %$stat) {
	my $sort_id = 0;

	for my $yw (sort keys %{$stat->{$release_id}}) {
		$sort_id++;
		my ($year, $week) = split '_', $yw;

		$app->db->prepare("insert into week_stat set created=?, release_id=?, week=?, year=?, sort_id=?, count=?")
				 ->execute($iso_date, $release_id, $week, $year, $sort_id, $stat->{$release_id}->{$yw});
	}
}

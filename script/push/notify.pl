#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";

use common::sense;
use Encode ();
use Net::APNS::Persistent;
use Net::APNS::Feedback;
use WWW::Google::Cloud::Messaging;
use Data::Dumper;
use App;
use Fcntl ':flock';
use Mojo::Util qw/trim/;

INIT {
  open  *{0} or print "What!? $0:$!\n" and exit 0;
  flock *{0}, LOCK_EX|LOCK_NB
    or print "$0 is already running somewhere!\n" and exit 0;
}

chdir "$FindBin::Bin/../..";

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $app  = App->new(mode => $mode); $app->log(Mojo::Log->new('path' => 'log/push_notify.log'));
my $conf = $app->config->{apns};

my $tmpl = {
	age                => { text => 'возрастное ограничение фильма', pref => '+'   },
	duration_clean     => { text => 'длительность',                  pref => ' мин'},
	duration_full      => { text => 'длительность c трейлером',      pref => ' мин'},
	date_russia_string => { text => 'дата выхода фильма',            pref => ''    },
	formats            => { text => 'форматы',                       pref => ''    },
};

my $apns  = Net::APNS::Persistent->new( $conf );
my $gcm   = WWW::Google::Cloud::Messaging->new(api_key => 'AIzaSyBUHHHbwZt_ixe63FKIhk62EcT-69SWyuM');

my $n = $app->mongo->get_collection('notification');

my $list = [ $n->find({push => 0})->all ];

$app->log->info("Notification count: " . scalar @$list) if @$list;

PUSH: for my $push (@$list) {

	$app->log->info("Push for $push->{user}");

	$app->mongo->get_collection('notification')->update({ _id => $push->{_id} }, { '$set' => { push => time } });

	next if $push->{item} eq 'kdm';

	my $last_push = $n->aggregate(
		[
			{ "\$match" => { item => 'kdm', item_id => $push->{item_id}, user => $push->{user} } },
			{ "\$group" => {"_id" => 0, "push" => {"\$max" => "\$push"}}}
		]
	)->[0]->{push} if $push->{item} eq 'kdm';

	next if !$push->{user} || ($push->{item} eq 'kdm' && $last_push > time - 1800);

	my $tokens = [ 
		map { my $t = $_; +{ map { $_ => $t->{$_} } qw/token android/ }} 
		$app->mongo->get_collection('user_token')
		->find({ apns_mode => $mode, user_id => $push->{user}})
		->fields({token => 1, android => 1})
		->all
	];

	unless (@$tokens) {
		$app->log->error("User $push->{user} without token, next");
		next;
	}
	
	my $data = { id => $push->{item_id} };

	given ($push->{item}) {
		when ('release') {
			$data->{section} = 'releases';

			unless ($push->{event} eq 'add') {
				$data->{text   } = "В релизе «$push->{title}->{ru}» есть изменения: ";
				
				for (@{$push->{updated_field}}) {
					my $val = ref $_->{updated}->{new} eq 'ARRAY' ? join(', ', @{$_->{updated}->{new}}) : trim $_->{updated}->{new};

					next PUSH if $_->{field} eq 'hidden';

					$data->{text} .= "$tmpl->{$_->{field}}->{text} - $val$tmpl->{$_->{field}}->{pref}; ";
				}
			} else {
				$data->{text} = "В релизы добавлен фильм «$push->{title}->{ru}».";
			}
		}
		when ('shipment') {
			$data->{section} = 'shipment';
			if($push->{shipper} eq 'DD'){
				$data->{text   } = $push->{event} eq 'add' 
					? "Вам отправлен фильм «$push->{title}->{ru}» по цифровой доставке DCP24." 
					: "Фильм «$push->{title}->{ru}» доставлен по цифровой доставке DCP24.";
			} else {
				$data->{text   } = $push->{event} eq 'add' 
					? "Вам отправлен диск с фильмом «$push->{title}->{ru}»." 
					: "Диск доставлен. Фильм «$push->{title}->{ru}».";
			}
		}
		when ('kdm') {
			$data->{section} = 'keys';
			$data->{text   } = "Для фильма «$push->{title}->{ru}» появились новые KDM.";
		}
		when ('custom') {
			$data->{section} = 'custom';
			$data->{text   } = "$push->{message}";
		}
		when ('news') {
			$data->{section} = 'news';
			$data->{text   } = "$push->{message}";
		}
	}

	Encode::_utf8_off $data->{text};

	for my $token ( grep {!$_->{android}} @$tokens ) {

		$app->log->info("Send token [$token->{token}] user [$push->{user}] section [$data->{section}]");

		$apns->queue_notification(
			$token->{token},
			{
				aps  => {
					alert => $data->{text},
					badge => 1,
					sound => $push->{sound} eq 'on' ? 'default' : '',
				},
				section => $data->{section},
				id => int $push->{item_id}
			}
		);
	}

	eval { $apns->send_queue; }; 

	if ($@) {
		$app->log->error("APNS ERROR: " . $@);
		last;
	}
	
	my $android_tokens = [grep {$_->{android}} @$tokens];

	if (@$android_tokens) {
		my $res = $gcm->send({
			registration_ids => [ map {$_->{token}} @$android_tokens ],
			data             => {
				message => $data->{text},
				section => $data->{section},
				id => int $push->{item_id}
			},
		});
		warn $res->error unless $res->is_success;
	}
}

$apns->disconnect;

my $apns = Net::APNS::Feedback->new($conf);

my $feedback = $apns->retrieve_feedback;

exit 0;

# ???

#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use App;
use Data::Dumper;
use SMS::API;
my $sms = SMS::API->new;
use strict;
use JSON;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $self   = App->new(mode => $mode);

my $mongo = $self->mongo->get_collection('user');

my $res = [$mongo->find({'email' => {'$exists' => 1 }, 'type' => 'partner'})->fields({'email' => 1})->all];

# warn Dumper $res;
my $j    = JSON::Any->new(convert_blessed => 1);

open(my $fh, '>', 'script/mail/contacts_partner.txt') or die 'Не могу открыть файл!';
for (@$res){
	print $fh $_->{email}."\n";
}

close $fh;

1;
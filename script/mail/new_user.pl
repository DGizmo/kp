#!/usr/bin/env perl
use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use DateTime::Tiny;
use App;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $self = App->new(mode => $mode);

for my $user ($self->mongo->get_collection('user')->find({send => {'$exists' => undef }, email => {'$exists' => 1}})->all) {

	$user->{cinemas} = [$self->mongo->get_collection('cinema')->find({id => {'$in' => $user->{cinema_id}}})->all] if $user->{cinema_id};

	$self->notice_to(
		type     => 'kp_new_user',
		user     => $user,
		template => 'mail/new',
	);

	$self->mongo->get_collection('user')->update({_id => $user->{_id}}, {'$set' => { send => 1 } });
}

#!/usr/bin/env perl

use FindBin;
use lib "$FindBin::Bin/../../lib";
use common::sense;
use App;
use Data::Dumper;
use Mojo::Log;
use SMS::API;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;
print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $self = App->new(mode => $mode); $self->log(Mojo::Log->new('path' => 'log/confirm_sms.log'));
my $sms  = SMS::API->new;

my $mongo = $self->mongo->get_collection('user');
my $res   = [ $mongo->find({ 'tmp_phone' => {'$exists' => 1 }})->all ];

for my $i (@$res) {
	my $status = $sms->send(
		phone => [ $i->{'tmp_phone'} ], 
		text  => 'Ваш код подтверждения в системе "Киноплан": ' . $i->{'code'},
	);

	$self->log->info("Code [$i->{code}] to phone [$i->{tmp_phone}]. Status $status->{status}");
	if ($status->{status} eq 'ok') {
		$mongo->update(
			{ _id      => $i->{_id} }, 
			{ '$unset' => { 'tmp_phone' => "none" } },
		);
	} else {
		$self->log->error(Dumper $status);
	}
}


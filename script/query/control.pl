#!/usr/bin/env perl
use common::sense;
use FindBin;
use lib "$FindBin::Bin/../../lib";

use Query::Daemon;

my $mode = $ARGV[0] && $ARGV[0] =~ /^(development|production)$/ ? $ARGV[0] : undef;

print qq{\033[1;31mWARN: App mode requires ('development', 'production'). Example: carton exec $0 development; Now used default: development!\033[0m\n} unless $mode;

$mode ||= 'development';

my $c = Query::Daemon->new(mode => $mode);
$c->start;
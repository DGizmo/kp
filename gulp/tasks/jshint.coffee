gulp         = require 'gulp'
plumber      = require 'gulp-plumber'
jshint       = require 'gulp-jshint'
stylish      = require 'jshint-stylish'
errorHandler = require '../utils/errorHandler'

srcJS = [
	'gruntfile.js'
	'data/js/inc/**/*.js'
	'data/js/external/**/*.js'
	'test/**/*.js',
	'!test/coverage/**/*.js'
]

gulp.task 'jshint', ->
	gulp.src srcJS
		.pipe plumber errorHandler: errorHandler
		.pipe jshint()
		.pipe jshint.reporter 'jshint-stylish'

gulp        = require 'gulp'
runSequence = require 'run-sequence'
paths       = require '../paths'
# reload      = require('browser-sync').reload

gulp.task 'watch', ->
	global.watch = true

	gulp.watch [paths.templatesSrc + '**/*.html', '!main.html'], ['copy:templates']

	gulp.watch paths.templatesSrc + '/main.html', ['processhtml']

	gulp.watch paths.stylesSrc + '/external/**/*.scss', ['external:styles']
	gulp.watch paths.stylesSrc + '**/*.scss',           ['main:styles']

	gulp.watch paths.jsSrc + '**/*.js', ['jscs', 'jshint', 'scripts:main']

	# gulp.watch 'app/templates/**/*.jade', -> runSequence 'jade', reload

	# gulp.watch 'app/resources/**/*', ['copy:resources', reload]

	# gulp.watch 'app/scripts/**/*.js', [
	# 		'scripts'
	# 		'jscs'
	# 		'jshint'
	# 		reload
	# 	]

	# gulp.watch 'app/images/svg/**/*.svg', ['svg', reload]

notify = require 'gulp-notify'

gulp.task 'notify', ->
    gulp.src ''
    .pipe notify 'MinJS Done!'



gulp.task 'wt', ->
	gulp.watch ['gulpfile.js','gulp/tasks/*'], ['jscs', 'jshint']
gulp         = require 'gulp'
plumber      = require 'gulp-plumber'
jscs         = require 'gulp-jscs'
errorHandler = require '../utils/errorHandler'

srcJS = [
	'gruntfile.js'
	'data/js/inc/**/*.js'
	'data/js/external/**/*.js'
	'test/**/*.js',
	'!test/coverage/**/*.js'
]

# "maxparams":     7,   // 5
# "maxdepth":      6,   // 5
# "maxstatements": 154, // 25
# "maxcomplexity": 68,  // 10

gulp.task 'jscs', ->
	gulp.src srcJS
		.pipe plumber errorHandler: errorHandler
		.pipe do jscs

gulp         = require 'gulp'
plumber      = require 'gulp-plumber'
flatten      = require 'gulp-flatten'
errorHandler = require '../utils/errorHandler'
paths        = require '../paths'

gulp.task 'copy:templates', ->
	gulp.src ['**/*.html', '!main.html'], cwd: paths.templatesSrc
		.pipe plumber errorHandler: errorHandler
			.pipe do flatten
		.pipe gulp.dest paths.dist + '/templates'

gulp.task 'copy', [
		'copy:templates'
	]

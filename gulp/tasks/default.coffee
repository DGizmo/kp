runSequence = require 'run-sequence'
gulp        = require 'gulp'
gutil       = require 'gulp-util'

gulp.task 'default', ->
	runSequence(
		'build'
		'watch'
	)

gulp.task 'build', ['del'], ->
	runSequence(
		['copy','processhtml', 'styles', 'styles:components']
		'jscs'
		'jshint'
		'scripts'
	)

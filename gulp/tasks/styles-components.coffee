gulp         = require 'gulp'
gutil        = require 'gulp-util'
gulpif       = require 'gulp-if'
plumber      = require 'gulp-plumber'
concat       = require 'gulp-concat'
rename       = require 'gulp-rename'
autoprefixer = require 'gulp-autoprefixer'
cmq          = require 'gulp-combine-media-queries'
minifyCss    = require 'gulp-minify-css'
errorHandler = require '../utils/errorHandler'
browsersConf = require '../browsersConf'
paths        = require '../paths'

mainCssComponents = [
	'bower_components/jReject/css/jquery.reject.css'
	'bower_components/growl/stylesheets/jquery.growl.css'
	'bower_components/failsafe.js/failsafe.css'
	'data/css/jquery.formstyler.css'
	'data/css/jquery.arcticmodal.css'
	'data/css/font-awesome.css'
	'data/css/daterangepicker-ui.css'
	'data/js/datepicker/datepicker3.standalone.min.css'
	'data/css/bootstrap-tour-standalone.min.css'
	'data/css/tipsy.css'
]

cssComponents = [
	'data/css/jquery.timepicker.css'
	'bower_components/growl/stylesheets/jquery.growl.css'
]

gulp.task 'styles:components:main', ->
	gulp.src mainCssComponents
		.pipe plumber errorHandler: errorHandler
			.pipe concat 'components.min.css'
			.pipe gulpif !gutil.env.dev, autoprefixer browsersConf.browsers.internal
			.pipe gulpif !gutil.env.dev, cmq()
			.pipe gulpif !gutil.env.dev, minifyCss()
		.pipe gulp.dest paths.dist

gulp.task 'styles:components:external', ->
	gulp.src cssComponents
		.pipe plumber errorHandler: errorHandler
			.pipe gulpif !gutil.env.dev, autoprefixer browsersConf.browsers.external
			.pipe gulpif !gutil.env.dev, cmq()
			.pipe gulpif !gutil.env.dev, minifyCss()
			.pipe rename extname: '.min.css'
		.pipe gulp.dest paths.dist + '/css'

gulp.task 'styles:components', [
	'styles:components:main'
	'styles:components:external'
]

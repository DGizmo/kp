gulp         = require 'gulp'
plumber      = require 'gulp-plumber'
tar          = require 'gulp-tar'
gzip         = require 'gulp-gzip'
errorHandler = require '../utils/errorHandler'

files = [
	'**/*',
	'!**/*.log',
	'!.DS_Store',
	'!.git/**',
	'!.gitignore',
	'!bower_components/**',
	'!build/**',
	'!data/img/avatar/**',
	'!data/reports/**',
	'!local/**',
	'!log/**',
	'!node_modules/**',
	'!test/coverage/**',
	'!tmp/**'
]

gulp.task 'compress', ->
	gulp.src files
		.pipe plumber errorHandler: errorHandler
			.pipe tar 'project.tar'
			.pipe gzip
				append: false
				gzipOptions: level: 9

		.pipe gulp.dest 'build/'

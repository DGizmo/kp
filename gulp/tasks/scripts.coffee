gulp         = require 'gulp'
plumber      = require 'gulp-plumber'
gutil        = require 'gulp-util'
gulpif       = require 'gulp-if'
header       = require 'gulp-header'
concat       = require 'gulp-concat'
uglify       = require 'gulp-uglify'
rename       = require 'gulp-rename'
sourcemaps   = require 'gulp-sourcemaps'
requireJSON  = require '../utils/requireJSON'
errorHandler = require '../utils/errorHandler'
paths        = require '../paths'
pkg          = requireJSON('./package.json');

banner = [
	'/*!'
	' * Киноплан (<%= pkg.homepage %>)'
	' * Copyright (c) <%= new Date().getFullYear() %> <%= pkg.author %>'
	' */'
	''
].join '\n'

uglifyOptions =
	preserveComments: 'some',
	compress:
		drop_console: true,
		dead_code: true
	mangle:
		except: ['jQuery', 'Backbone']

componentsSrc = [
	'bower_components/jquery/dist/jquery.min.js'
	'bower_components/underscore/underscore-min.js'
	'bower_components/jReject/js/jquery.reject.js'
	'bower_components/raven-js/dist/raven.min.js'

	'data/js/jquery-ui-1.10.4.custom.js'

	'bower_components/backbone/backbone.js'
	'bower_components/backbone.wreqr/lib/backbone.wreqr.js'
	'bower_components/jquery.cookie/jquery.cookie.js'
	'bower_components/growl/javascripts/jquery.growl.js'
	'bower_components/html2canvas/build/html2canvas.js'
	'bower_components/jquery.scrollTo/jquery.scrollTo.min.js'
	'bower_components/jquery-textchange/jquery.textchange.js'
	'bower_components/jquery-autosize/jquery.autosize.js' # ??? в 2х местах
	'bower_components/bootstrap-tour/build/js/bootstrap-tour-standalone.min.js'
	'bower_components/jquery-json/dist/jquery.json.min.js' # ?????
	'bower_components/failsafe.js/failsafe.js'

	'data/js/web_socket.js'
	'bower_components/reconnectingWebsocket/reconnecting-websocket.min.js'

	'data/js/sugar-1.3.9-custom.min.js'
	'data/js/jquery.formstyler.min.js' # ??? ---
	'data/js/jquery.timepicker.min.js'
	'data/js/jquery.arcticmodal.custom.min.js'
	'data/js/jquery.comiseo.daterangepicker.js'
	'data/js/jquery.maskedinput.min.js'

	'data/js/datepicker/bootstrap-datepicker.min.js'
	'data/js/datepicker/bootstrap-datepicker.locales.min.js'
	'data/js/datepicker-ru.js'
	'data/js/moment.min.js'
	'data/js/moment.ru.js'
	'data/js/jquery.tipsy.js'
]

mainSrc = [
	'main.js',
	'mixins.js',
	'header.js',
	'sidebar.js',
	'feedback.js',
	'context_menu.js',
	'distributor_dashboard.js',
	'create_calendar.js',
	'repertoire/**/*.js',
	'releases/**/*.js',
	'dashboard/**/*.js',
	'advertising/**/*.js',
	'commercial/**/*.js',
	'user/**/*.js',
	'distributor.js',
	'distributor_reports.js',
	'keys.js',
	'notify_item.js',
	'messages.js',
	'trailers.js',
	'user.js',
	'user_updates.js',
	'schedule_change.js',
	'edit_value.js',
	'employees.js',
	'cinema_Info.js',
	'tutorial.js',
	'searching_module.js',
	'notifications_sidebar.js',
	'schedule/**/*.js',
	'analytics.js',
	'distributor_cinemas/**/*.js',
	'controller.js',
	'statistic.js',
	'service/**/*.js',
	'booking/**/*.js',
	'keygen/**/*.js',
	'copying_schedule.js',
	'entities_storage.js',
	'templateCache.js',
	'app.js',
	'init.js'
]

externalSrc = [
	'data/js/external/**/*.js'
	'bower_components/growl/javascripts/jquery.growl.js'
]

gulp.task 'scripts:components', ->
	gulp.src componentsSrc
		.pipe plumber errorHandler: errorHandler
		.pipe concat 'components.min.js'
		.pipe gulpif !gutil.env.dev, uglify uglifyOptions
		.pipe gulp.dest paths.dist

gulp.task 'scripts:main', ->
	gulp.src mainSrc, cwd: paths.jsSrc
		.pipe plumber errorHandler: errorHandler
		.pipe(sourcemaps.init())
			.pipe concat 'kinoplan.min.js'
			.pipe header banner, pkg: pkg
			.pipe gulpif !gutil.env.dev, uglify uglifyOptions
		.pipe sourcemaps.write './',
			addComment: true
			includeContent: false
			sourceRoot: '../js/inc'
		.pipe gulp.dest paths.dist

gulp.task 'scripts:external', ->
	gulp.src externalSrc
		.pipe plumber errorHandler: errorHandler
		.pipe gulpif !gutil.env.dev, uglify uglifyOptions
		.pipe rename extname: '.min.js'
		.pipe gulp.dest paths.dist + '/js'

gulp.task 'scripts', [
	'scripts:components'
	'scripts:main'
	'scripts:external'
]

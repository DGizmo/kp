gulp         = require 'gulp'
plumber      = require 'gulp-plumber'
plato        = require 'gulp-plato'
requireJSON  = require '../utils/requireJSON'
errorHandler = require '../utils/errorHandler'
jshintrc     = requireJSON './.jshintrc'
paths        = require '../paths'

gulp.task 'plato', ->
	gulp.src ['**/*.js'], cwd: paths.jsSrc
		.pipe plumber errorHandler: errorHandler
		.pipe plato 'build/report/plato', jshint:
			options: jshintrc

gulp         = require 'gulp'
gutil        = require 'gulp-util'
plumber      = require 'gulp-plumber'
processhtml  = require 'gulp-processhtml'
rename       = require 'gulp-rename'
errorHandler = require '../utils/errorHandler'
paths        = require '../paths'

gulp.task 'processhtml', ->
	gulp.src paths.templatesSrc + '/main.html'
		.pipe plumber errorHandler: errorHandler
		.pipe processhtml process: true
		.pipe rename extname: '.html.ep'
		.pipe gulp.dest 'templates'

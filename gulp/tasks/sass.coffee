gulp         = require 'gulp'
plumber      = require 'gulp-plumber'
gutil        = require 'gulp-util'
gulpif       = require 'gulp-if'
rename       = require 'gulp-rename'
sass         = require 'gulp-sass'
sourcemaps   = require 'gulp-sourcemaps'
autoprefixer = require 'gulp-autoprefixer'
cmq          = require 'gulp-combine-media-queries'
minifyCss    = require 'gulp-minify-css'
errorHandler = require '../utils/errorHandler'
browsersConf = require '../browsersConf'
paths        = require '../paths'

gulp.task 'styles:main', ->
	gulp.src ['main.scss'], cwd: paths.stylesSrc
		.pipe plumber errorHandler: errorHandler
		.pipe(sourcemaps.init())
			.pipe sass
				errLogToConsole: true
				sync: true
			.pipe gulpif !gutil.env.dev, autoprefixer browsersConf.browsers.internal
			.pipe gulpif !gutil.env.dev, cmq()
			.pipe gulpif !gutil.env.dev, minifyCss()
			.pipe rename
				basename: 'kinoplan'
				extname:  '.min.css'

		.pipe gulpif !gutil.env.dev, sourcemaps.write('./')
		.pipe gulp.dest paths.dist

gulp.task 'styles:external', ->
	gulp.src ['**/*.scss'], cwd: paths.stylesSrc + '/external'
		.pipe plumber errorHandler: errorHandler
		.pipe do sass
		.pipe gulpif !gutil.env.dev, autoprefixer browsersConf.browsers.external
		.pipe gulpif !gutil.env.dev, cmq()
		.pipe gulpif !gutil.env.dev, minifyCss()
		.pipe rename extname: '.min.css'
		.pipe gulp.dest paths.dist + '/css'

gulp.task 'styles', [
	'styles:main'
	'styles:external'
]

module.exports =

	browsers:
		internal:
			'Android  >= 4'  +
			'Chrome   >= 34' +
			'Firefox  >= 29' +
			'Explorer >= 10' +
			'iOS      >= 6'  +
			'Opera    >= 21' +
			'Safari   >= 6'

		external:
			'Android  >= 4'  +
			'Chrome   >= 25' +
			'Firefox  >= 25' +
			'Explorer >= 8'  +
			'iOS      >= 5'  +
			'Opera    >= 12' +
			'Safari   >= 5'
